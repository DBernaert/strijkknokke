unit ParamsReportOverViewInvoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniSpinEdit, uniButton, UniThemeButton, uniPanel;

type
  TParamsReportOverViewInvoiceFrm = class(TUniForm)
    EditMonth: TUniSpinEdit;
    EditYear: TUniSpinEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
    IdUser: longint;
  public
    { Public declarations }
    procedure Start_module(UserId: longint);
  end;

function ParamsReportOverViewInvoiceFrm: TParamsReportOverViewInvoiceFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, DateUtils, Main, ReportsMain;

function ParamsReportOverViewInvoiceFrm: TParamsReportOverViewInvoiceFrm;
begin
  Result := TParamsReportOverViewInvoiceFrm(UniMainModule.GetFormInstance(TParamsReportOverViewInvoiceFrm));
end;

{ TParamsReportOverViewInvoiceFrm }

procedure TParamsReportOverViewInvoiceFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TParamsReportOverViewInvoiceFrm.BtnSaveClick(Sender: TObject);
begin
  if Trim(EditMonth.Text) = '' then
  begin
    UniMainModule.show_warning('De maand is een verplichte ingave.');
    EditMonth.SetFocus;
    Exit;
  end;

  if Trim(EditYear.Text) = '' then
  begin
    UniMainModule.show_warning('Het jaar is een verplichte ingave.');
    EditYear.SetFocus;
    Exit;
  end;

  With ReportsMainFrm do
  begin
    Generate_overview_invoicing(EditMonth.Value, EditYear.Value, IdUser);
    ShowModal;
  end;
end;

procedure TParamsReportOverViewInvoiceFrm.Start_module(UserId: longint);
begin
  Caption := 'Afdrukken overzicht facturatie';
  IdUser := UserId;
  EditMonth.Value := MonthOfTheYear(Now);
  EditYear.Value := YearOf(Now);
end;

procedure TParamsReportOverViewInvoiceFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
