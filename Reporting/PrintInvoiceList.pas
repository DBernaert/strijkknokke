unit PrintInvoiceList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniDateTimePicker, uniRadioButton, uniButton, UniThemeButton, uniPanel,
  uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC;

type
  TPrintInvoiceListFrm = class(TUniForm)
    EditBeginDate: TUniDateTimePicker;
    EditEndDate: TUniDateTimePicker;
    CheckboxAllCustomers: TUniRadioButton;
    CheckboxSpecificCustomer: TUniRadioButton;
    GridRelations: TUniDBGrid;
    Relations: TIBCQuery;
    DsRelations: TDataSource;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module;
  end;

function PrintInvoiceListFrm: TPrintInvoiceListFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ReportsMain, DateUtils;

function PrintInvoiceListFrm: TPrintInvoiceListFrm;
begin
  Result := TPrintInvoiceListFrm(UniMainModule.GetFormInstance(TPrintInvoiceListFrm));
end;

{ TPrintInvoiceListFrm }

procedure TPrintInvoiceListFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TPrintInvoiceListFrm.BtnSaveClick(Sender: TObject);
var SQL: string;
begin
  BtnSave.SetFocus;

  if EditBeginDate.IsBlank then
  begin
    UniMainModule.show_warning('De begindatum is een verplichte ingave.');
    EditBeginDate.SetFocus;
    Exit;
  end;

  if EditEndDate.IsBlank then
  begin
    UniMainModule.show_warning('De einddatum is een verplichte ingave.');
    EditEndDate.SetFocus;
    Exit;
  end;

  if (CheckboxSpecificCustomer.Checked) and (Relations.FieldByName('Id').IsNull) then
  begin
    UniMainModule.show_warning('Gelieve een klant te selecteren.');
    GridRelations.SetFocus;
    Exit;
  end;


  SQL := 'SELECT INVOICES.ID, INVOICES.INVOICENUMBER, INVOICES.INVOICEDATE, RELATIONS.COMPANYNAME, RELATIONS.NAME, ' +
           'RELATIONS.FIRSTNAME, PROJECTS.NAME AS PROJECTNAME, INVOICES.AMOUNT, INVOICES.VATAMOUNT, INVOICES.TOTALAMOUNT ' +
           'FROM INVOICES ' +
           'LEFT OUTER JOIN RELATIONS ON (INVOICES.RELATION = RELATIONS.ID) ' +
           'LEFT OUTER JOIN PROJECTS ON (INVOICES.PROJECT = PROJECTS.ID) ';

  SQL := SQL + ' WHERE INVOICES.INVOICEDATE > ' + QuotedStr(FormatDateTime('yyyy-mm-dd', IncDay(EditBeginDate.DateTime, -1)));
  SQL := SQL + ' AND INVOICES.INVOICEDATE < '   + QuotedStr(FormatDateTime('yyyy-mm-dd', IncDay(EditEndDate.DateTime, 1)));

  if CheckboxSpecificCustomer.Checked
  then SQL := SQL + ' AND INVOICES.RELATION=' + Relations.FieldByName('Id').AsString;


  SQL := SQL + ' ORDER BY INVOICES.INVOICENUMBER';

  With ReportsMainFrm do
  begin
    InvoiceList.Close;
    InvoiceList.SQL.Clear;
    InvoiceList.SQL.Add(SQL);
    Prepare_InvoiceList;
    ShowModal();
  end;
end;

procedure TPrintInvoiceListFrm.Start_module;
begin
  Relations.Close;
  Relations.SQL.Clear;
  Relations.SQL.Add('Select ID, NAME || '' '' || FIRSTNAME AS FULLNAME, COMPANYNAME from relations ' + ' where companyId=' +
                    IntToStr(UniMainModule.Company_id) + ' and Removed=''0''' + ' order by Name, Firstname');
  Relations.Open;
end;

procedure TPrintInvoiceListFrm.UniFormCreate(Sender: TObject);
begin
 with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
