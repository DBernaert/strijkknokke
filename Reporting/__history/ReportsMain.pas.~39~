unit ReportsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, frxClass, frxExportBaseDialog, frxExportPDF, frxDBSet, Data.DB, MemDS,
  VirtualTable, uniGUIBaseClasses, uniURLFrame, uniButton, UniThemeButton, uniPanel;

type
  TReportsMainFrm = class(TUniForm)
    Invoice: TfrxReport;
    InvoiceLinesPrint: TVirtualTable;
    DsInvoiceLines: TDataSource;
    FrxInvoiceLines: TfrxDBDataset;
    PdfExport: TfrxPDFExport;
    InvoiceTimeRegistrations: TIBCQuery;
    DsInvoiceTimeRegistrations: TDataSource;
    frxInvoiceTimeRegistrations: TfrxDBDataset;
    ReportsInvoicingOverview: TfrxReport;
    TimeRegistrations: TIBCQuery;
    DsTimeRegistrations: TDataSource;
    FrxTimeRegistrations: TfrxDBDataset;
    PanelFooter: TUniContainerPanel;
    Invoices: TIBCQuery;
    DsInvoices: TDataSource;
    frsInvoices: TfrxDBDataset;
    ContainerButtons: TUniContainerPanel;
    BtnSave: TUniThemeButton;
    ReportInvoiceList: TfrxReport;
    InvoiceList: TIBCQuery;
    DsInvoiceList: TDataSource;
    frxInvoiceList: TfrxDBDataset;
    InvoiceListID: TLargeintField;
    InvoiceListINVOICENUMBER: TIntegerField;
    InvoiceListINVOICEDATE: TDateField;
    InvoiceListCOMPANYNAME: TWideStringField;
    InvoiceListNAME: TWideStringField;
    InvoiceListFIRSTNAME: TWideStringField;
    InvoiceListPROJECTNAME: TWideStringField;
    InvoiceListAMOUNT: TFloatField;
    InvoiceListVATAMOUNT: TFloatField;
    InvoiceListTOTALAMOUNT: TFloatField;
    InvoiceListFullName: TStringField;
    URLFrame: TUniURLFrame;
    Offer: TfrxReport;
    ObjectLines: TIBCQuery;
    ObjectLinesID: TLargeintField;
    ObjectLinesCOMPANYID: TLargeintField;
    ObjectLinesOBJECT: TLargeintField;
    ObjectLinesQUANTITY: TIntegerField;
    ObjectLinesAMOUNT: TFloatField;
    ObjectLinesVATAMOUNT: TFloatField;
    ObjectLinesTOTALAMOUNT: TFloatField;
    ObjectLinesDESCRIPTION: TWideMemoField;
    DsObjectLines: TDataSource;
    frxObjectLines: TfrxDBDataset;
    Purchases: TIBCQuery;
    DsPurchases: TDataSource;
    frxPurhcases: TfrxDBDataset;
    ObjectLinesCUSTOMERAMOUNT: TFloatField;
    ObjectLinesCUSTOMERVATAMOUNT: TFloatField;
    ObjectLinesCUSTOMERTOTALAMOUNT: TFloatField;
    ObjectLinesTOTALLINECUSTOMER: TCurrencyField;
    ObjectLinesTOTALLINEVATCUSTOMER: TCurrencyField;
    ObjectLinesGENERALTOTALLINECUSTOMER: TCurrencyField;
    ObjectLinesTOTALLINE: TCurrencyField;
    ObjectLinesTOTALLINEVAT: TCurrencyField;
    ObjectLinesGENERALTOTALLINE: TCurrencyField;
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure InvoiceListCalcFields(DataSet: TDataSet);
    procedure ObjectLinesCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure PrepareAndShow(Report: TfrxReport; Exp: TfrxPDFExport);
  public
    { Public declarations }
    procedure Prepare_InvoiceList;
    procedure Generate_overview_invoicing(Month: integer; Year: integer; UserId: longint);
    procedure Prepare_invoice;
    procedure PrepareAndEmail(Report: TfrxReport; Exp: TfrxPDFExport;
                              SenderName : string;
                              SenderEmail: string;
                              RecipientEmail: string;
                              BccEmail: string;
                              EmailSubject: string;
                              EmailBody: string;
                              PdfName: string);
    procedure Prepare_offer(ObjectsId: longint);
  end;

function ReportsMainFrm: TReportsMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, EASendMailObjLib_TLB, ServerModule, Main;

function ReportsMainFrm: TReportsMainFrm;
begin
  Result := TReportsMainFrm(UniMainModule.GetFormInstance(TReportsMainFrm));
end;

{ TReportsMainFrm }

procedure TReportsMainFrm.BtnSaveClick(Sender: TObject);
begin
  Close;
end;

procedure TReportsMainFrm.Generate_overview_invoicing(Month, Year: integer; UserId: longint);
var Totalhours:         currency;
    HourlyRate:         currency;
    Totalkms:           currency;
    InvoiceAmountHours: currency;
    InvoiceAmountKms:   currency;
    SQL: string;
    Memo:               TfrxMemoView;
    Total:              currency;
    Vat:                currency;
    GeneralTotal:       currency;
begin
  Caption := 'Overzicht facturatie';

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select sum(kilometers) as total from timeregistrations ' +
         'where userid=' + IntToStr(UserId) + ' ' +
         'and removed=''0'' ';
  SQL := SQL + ' and RegistrationDate >=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EncodeDate(Year, Month, 1)));
  SQL := SQL + ' and RegistrationDate <'  + QuotedStr(FormatDateTime('yyyy-mm-dd', IncMonth(EnCodeDate(Year, Month, 1), 1)));
  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  Totalkms := UniMainModule.QWork.FieldByName('Total').AsCurrency;
  UniMainModule.QWork.Close;

  Memo := ReportsInvoicingOverview.FindObject('LblKmQuantity') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Text := FormatCurr('#,##0.00', Totalkms);

  Memo := ReportsInvoicingOverview.FindObject('LblKmRate') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Text := FormatCurr('#,##0.00 euro', UniMainModule.KilometerAllowance);



  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  SQL := 'Select sum(saldovalue) as total from timeregistrations ' +
         'where userid=' + IntToStr(UserId) + ' ' +
         'and removed=''0''';
  SQL := SQL + ' and RegistrationDate >=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EncodeDate(Year, Month, 1)));
  SQL := SQL + ' and RegistrationDate <'  + QuotedStr(FormatDateTime('yyyy-mm-dd', IncMonth(EnCodeDate(Year, Month, 1), 1)));
  UniMainModule.QWork.SQL.Add(SQL);
  UniMainModule.QWork.Open;
  TotalHours := UniMainModule.QWork.FieldByName('Total').AsCurrency;
  UniMainModule.QWork.Close;

  Memo := ReportsInvoicingOverview.FindObject('LblHourQuantity') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Text := FormatCurr('#,##0.00', TotalHours);

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from users where Id=' + IntToStr(UserId));
  UniMainModule.QWork.Open;
  HourlyRate := UniMainModule.QWork.FieldByName('HourlyRate').AsCurrency;
  UniMainModule.QWork.Close;

  Memo := ReportsInvoicingOverview.FindObject('LblHourlyRate') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Text := FormatCurr('#,##0.00 euro', HourlyRate);

  if (TotalHours <> 0) and (HourlyRate <> 0)
  then InvoiceAmountHours := TotalHours * HourlyRate
  else InvoiceAmountHours := 0;

  if (Totalkms <> 0) and (UniMainModule.KilometerAllowance <> 0)
  then InvoiceAmountKms := Totalkms * UniMainModule.KilometerAllowance
  else InvoiceAmountKms := 0;

  //Get the details of the employee
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select name, firstname from users where id=' + IntToStr(UserId));
  UniMainModule.QWork.Open;

  Memo := ReportsInvoicingOverview.FindObject('LblMedewerker') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').asString);
  UniMainModule.QWork.Close;

  Memo := ReportsInvoicingOverview.FindObject('LblPeriode') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(IntToStr(Year) + ' - ' + IntToStr(Month));

  Memo := ReportsInvoicingOverview.FindObject('LblKms') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatCurr('#,##0.00 euro', InvoiceAmountKms));

  Memo := ReportsInvoicingOverview.FindObject('LblHours') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatCurr('#,##0.00 euro', InvoiceAmountHours));

  Memo := ReportsInvoicingOverview.FindObject('LblTotal') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatCurr('#,##0.00 euro', InvoiceAmountHours + InvoiceAmountKms));

  Total := InvoiceAmountHours + InvoiceAmountKms;
  Memo := ReportsInvoicingOverview.FindObject('LblTotal') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatCurr('#,##0.00 euro', Total));

  Vat  := Total / 100 * 21;
  Memo := ReportsInvoicingOverview.FindObject('LblVat') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatCurr('#,##0.00 euro', Vat));

  GeneralTotal  := Total + Vat;
  Memo := ReportsInvoicingOverview.FindObject('LblGeneralTotal') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatCurr('#,##0.00 euro', GeneralTotal));

  TimeRegistrations.Close;
  TimeRegistrations.SQL.Clear;
  SQL := 'SELECT PROJECTS.NAME, TIMEREGISTRATIONS.ID, TIMEREGISTRATIONS.REGISTRATIONDATE, TIMEREGISTRATIONS.SALDOVALUE, ' +
         'TIMEREGISTRATIONS.KILOMETERS, TIMEREGISTRATIONS.DESCRIPTION, BASICTABLES.DUTCH FROM TIMEREGISTRATIONS ' +
         'RIGHT OUTER JOIN PROJECTS ON (TIMEREGISTRATIONS.PROJECT = PROJECTS.ID) ' +
         'LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = BASICTABLES.ID) ' +
         'WHERE TIMEREGISTRATIONS.REMOVED = ''0'' AND USERID=' + IntToStr(UserId) +
         ' AND TIMEREGISTRATIONS.REGISTRATIONDATE >=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EncodeDate(Year, Month, 1))) +
         ' AND TIMEREGISTRATIONS.REGISTRATIONDATE <'  + QuotedStr(FormatDateTime('yyyy-mm-dd', IncMonth(EnCodeDate(Year, Month, 1), 1))) +
         ' ORDER BY TIMEREGISTRATIONS.REGISTRATIONDATE';
  TimeRegistrations.SQL.Add(SQL);
  TimeRegistrations.Open;
  PrepareAndShow(ReportsInvoicingOverview, PdfExport);
end;

procedure TReportsMainFrm.InvoiceListCalcFields(DataSet: TDataSet);
begin
  InvoiceList.FieldByName('FullName').AsString :=
  Trim(InvoiceList.FieldByName('Name').AsString + ' ' + InvoiceList.fieldbyname('FirstName').AsString);
  if Trim(InvoiceList.FieldByName('CompanyName').AsString) <> ''
  then begin
         if Trim(InvoiceList.FieldByName('FullName').AsString) = ''
         then InvoiceList.FieldByName('FullName').AsString := InvoiceList.FieldByName('CompanyName').AsString
         else InvoiceList.FieldByName('FullName').AsString := InvoiceList.FieldByName('FullName').AsString +
                                                              ' (' + InvoiceList.FieldByName('CompanyName').AsString + ')';
       end;
end;

procedure TReportsMainFrm.ObjectLinesCalcFields(DataSet: TDataSet);
begin
  ObjectLines.fieldbyname('TotalLine').AsCurrency := ObjectLines.FieldByName('Amount').AsCurrency *
                                                     ObjectLines.FieldByName('Quantity').AsCurrency;
  ObjectLines.FieldByName('TotalLineVat').AsCurrency := ObjectLines.FieldByName('TotalLine').asCurrency / 100 * 21;
  ObjectLines.FieldByName('GeneralTotalLine').AsCurrency := ObjectLines.FieldByName('TotalLine').AsCurrency +
                                                            ObjectLines.FieldByName('TotalLineVat').AsCurrency;

  ObjectLines.fieldbyname('TotalLineCustomer').AsCurrency := ObjectLines.FieldByName('CustomerAmount').AsCurrency *
                                                             ObjectLines.FieldByName('Quantity').AsCurrency;
  ObjectLines.FieldByName('TotalLineVatCustomer').AsCurrency := ObjectLines.FieldByName('TotalLine').asCurrency / 100 * 21;
  ObjectLines.FieldByName('GeneralTotalLineCustomer').AsCurrency := ObjectLines.FieldByName('TotalLineCustomer').AsCurrency +
                                                                    ObjectLines.FieldByName('TotalLineVatCustomer').AsCurrency;
end;

procedure TReportsMainFrm.PrepareAndEmail(Report: TfrxReport; Exp: TfrxPDFExport; SenderName, SenderEmail,
  RecipientEmail, BccEmail, EmailSubject, EmailBody, PdfName: string);
var AUrl : string;
    oSmtp : TMail;
begin
  Report.PrintOptions.ShowDialog := False;
  Report.ShowProgress := false;

  Report.EngineOptions.SilentMode := True;
  Report.EngineOptions.EnableThreadSafe := True;
  Report.EngineOptions.DestroyForms := False;
  Report.EngineOptions.UseGlobalDataSetList := False;

  Exp.Background := True;
  Exp.ShowProgress := False;
  Exp.ShowDialog := False;
  Exp.FileName := UniServerModule.NewCacheFileUrl(False, 'pdf', PdfName, '', AUrl, True);
  Exp.DefaultPath := '';

  Report.PreviewOptions.AllowEdit := False;
  Report.PrepareReport;
  Report.Export(Exp);

  oSmtp := TMail.Create(UniApplication);
  oSmtp.ServerAddr    := 'vps.transip.email';
  oSmtp.ServerPort    := 587;
  oSmtp.SSL_init();
  oSmtp.LicenseCode   := 'ES-D1508812687-00323-4AC1DB277882BAF4-1878D6BC21AAA48U';
  oSmtp.FromAddr      := SenderEmail;
  oSmtp.From          := SenderName;
  oSmtp.AddRecipientEx(RecipientEmail, 0);
  oSmtp.AddRecipientEx(BccEmail, 2 );

  oSmtp.Subject       := EmailSubject;
  oSmtp.BodyFormat    := 0;
  oSmtp.BodyText      := EmailBody;

  if oSmtp.AddAttachment(Exp.FileName) <> 0
  then UniMainModule.show_error('Fout bij het toevoegen van het document aan de mail.');

  oSmtp.UserName      := 'dbernaert@vps.transip.email';
  oSmtp.Password      := 'uPqpTLmuBwJRyEoE';
  if oSmtp.SendMailToQueue()  = 0
  then UniMainModule.Show_confirmation('Het document werd correct verzonden.')
  else UniMainModule.Show_warning('Er is een fout opgetreden bij het verzenden!');
  oSmtp.Free;
end;

procedure TReportsMainFrm.PrepareAndShow(Report: TfrxReport; Exp: TfrxPDFExport);
var AUrl : string;
begin
  Report.PrintOptions.ShowDialog := False;
  Report.ShowProgress := false;

  Report.EngineOptions.SilentMode := True;
  Report.EngineOptions.EnableThreadSafe := True;
  Report.EngineOptions.DestroyForms := False;
  Report.EngineOptions.UseGlobalDataSetList := False;

  Exp.Background := True;
  Exp.ShowProgress := False;
  Exp.ShowDialog := False;
  Exp.FileName := UniServerModule.NewCacheFileUrl(False, 'pdf', '', '', AUrl, True);
  Exp.DefaultPath := '';

  Report.PreviewOptions.AllowEdit := False;
  Report.PrepareReport;
  Report.Export(Exp);

  URLFrame.URL := AUrl;
end;

procedure TReportsMainFrm.Prepare_invoice;
begin
  Caption := 'Afdruk factuur';
  PrepareAndShow(Invoice, PdfExport);
end;

procedure TReportsMainFrm.Prepare_InvoiceList;
begin
  Caption := 'Afdruk factuurlijst';
  PrepareAndShow(ReportInvoiceList, PdfExport);
end;

procedure TReportsMainFrm.Prepare_offer(ObjectsId: longint);
var Memo: TfrxMemoView;
    RelationId: longint;
begin
  Caption := 'Afdruk offerte';
  UniMainModule.UCompany.Close;
  UniMainModule.UCompany.SQL.Clear;
  UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
  UniMainModule.UCompany.Open;

  ObjectLines.Close;
  ObjectLines.SQL.Clear;
  ObjectLines.SQL.Add('Select * from ObjectLines where Object=' + IntToStr(ObjectsId));
  ObjectLines.Open;

  // Header
  Memo := Offer.FindObject('LblAddress1') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Address').AsString);

  Memo := Offer.FindObject('LblAddress2') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Zipcode').AsString + ' ' + UniMainModule.UCompany.FieldByName('City').AsString);

  Memo := Offer.FindObject('LblTelephone') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Phone').AsString);

  Memo := Offer.FindObject('LblEmail') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Email').AsString);

  Memo := Offer.FindObject('LblWeb') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Website').AsString);

  Memo := Offer.FindObject('OfferNumber') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(IntToStr(ObjectsId));

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from Objects where id=' + IntToStr(ObjectsId));
  UniMainModule.QWork.Open;

  Memo := Offer.FindObject('LblDocDate') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', UniMainModule.QWork.FieldByName('OfferDate').AsDateTime));

  Memo := Offer.FindObject('LblEndDate') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', IncMonth(UniMainModule.QWork.FieldByName('OfferDate').AsDateTime, 1)));

  UniMainModule.QWork.Close;

  // Central

  // Footer
  Memo := Offer.FindObject('LblBank11') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount').AsString);

  Memo := Offer.FindObject('LblBank12') as TfrxMemoView;
  Memo.Memo.Clear;
  if Trim(UniMainModule.UCompany.FieldByName('Iban').AsString) <> ''
  then Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban').AsString);

  Memo := Offer.FindObject('LblBank13') as TfrxMemoView;
  Memo.Memo.Clear;
  if Trim(UniMainModule.UCompany.FieldByName('Bic').AsString) <> ''
  then Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic').AsString);

  Memo := Offer.FindObject('LblVat') as TfrxMemoView;
  Memo.Memo.Clear;
  if Trim(UniMainModule.UCompany.FieldByName('VatNumber').AsString) <> ''
  then Memo.Memo.Add('BTW: ' + UniMainModule.UCompany.FieldByName('VatNumber').AsString);

  Memo := Offer.FindObject('LblBank21') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount2').AsString);

  Memo := Offer.FindObject('LblBank22') as TfrxMemoView;
  Memo.Memo.Clear;
  if Trim(UniMainModule.UCompany.FieldByName('Iban2').AsString) <> ''
  then Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban2').AsString);

  Memo := Offer.FindObject('LblBank23') as TfrxMemoView;
  Memo.Memo.Clear;
  if Trim(UniMainModule.UCompany.FieldByName('Bic2').AsString) <> ''
  then Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic2').AsString);

  // Get Customer details
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select relation from Objects where Id=' + IntToStr(ObjectsId));
  UniMainModule.QWork.Open;
  RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;
  UniMainModule.QWork.Close;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from relations where Id=' + IntToStr(RelationId));
  UniMainModule.QWork.Open;

  Memo := Offer.FindObject('LblCustomerName') as TfrxMemoView;
  Memo.Memo.Clear;
  Case UniMainModule.QWork.FieldByName('RelationType').AsInteger of
  0: begin
       Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyName').AsString);
     end;
  1: begin
       if not UniMainModule.QWork.FieldByName('Appelation').isNull
       then begin
              UniMainModule.QWork2.Close;
              UniMainModule.QWork2.SQL.Clear;
              UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('Appelation').AsString);
              UniMainModule.QWork2.Open;
              Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('Name').AsString + ' ' +
              UniMainModule.QWork.FieldByName('FirstName').AsString));
              UniMainModule.QWork2.Close;
            end
       else Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
     end;
  End;

  Memo := Offer.FindObject('LblAppelation') as TfrxMemoView;
  Memo.Memo.Clear;
  if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
  then begin
         if (Trim(UniMainModule.QWork.FieldByName('Name').AsString) <> '') or
            (Trim(UniMainModule.QWork.FieldByName('FirstName').asString) <> '')
         then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
       end;

  Memo := Offer.FindObject('LblCustomerAddress1') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Address').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumber').AsString));

  Memo := Offer.FindObject('LblCustomerAddress2') as TfrxMemoView;
  Memo.Memo.Clear;
  Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' + UniMainModule.QWork.FieldByName('City').AsString));

  Memo := Offer.FindObject('LblCustomerVatNumber') as TfrxMemoView;
  Memo.Memo.Clear;
  if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
  then begin
         if Trim(UniMainModule.QWork.FieldByName('VatNumber').AsString) <> ''
         then Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString);
       end;
  UniMainModule.QWork.Close;

  UniMainModule.UCompany.Close;

  PrepareAndShow(Offer, PdfExport);
end;

procedure TReportsMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TReportsMainFrm.UniFormShow(Sender: TObject);
begin
  Width  := UniApplication.ScreenWidth  - 400;
  Height := UniApplication.ScreenHeight - 200;
  Left   := 200;
  Top    := 100;
end;

end.
