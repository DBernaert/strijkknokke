object ReportsMainFrm: TReportsMainFrm
  Left = 0
  Top = 0
  ClientHeight = 848
  ClientWidth = 1191
  Caption = ''
  OnShow = UniFormShow
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  Layout = 'border'
  OnCancel = BtnSaveClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelFooter: TUniContainerPanel
    Left = 0
    Top = 807
    Width = 1191
    Height = 41
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 0
    Layout = 'border'
    object ContainerButtons: TUniContainerPanel
      Left = 1086
      Top = 0
      Width = 105
      Height = 41
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 1
      LayoutConfig.Region = 'east'
      LayoutConfig.Margin = '0 8 0 0'
      object BtnSave: TUniThemeButton
        Left = 0
        Top = 8
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        ScreenMask.Target = Owner
        Images = UniMainModule.ImageList
        ImageIndex = 9
        OnClick = BtnSaveClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object URLFrame: TUniURLFrame
    Left = 0
    Top = 0
    Width = 1191
    Height = 807
    Hint = ''
    LayoutConfig.Region = 'center'
    Align = alClient
    TabOrder = 1
    ParentColor = False
    Color = clBtnFace
  end
  object Invoice: TfrxReport
    Version = '6.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 368
    Top = 88
    Datasets = <
      item
        DataSet = frsInvoices
        DataSetName = 'frsInvoices'
      end
      item
        DataSet = FrxInvoiceLines
        DataSetName = 'FrxInvoiceLines'
      end
      item
        DataSet = frxInvoiceTimeRegistrations
        DataSetName = 'frxInvoiceTimeRegistrations'
      end
      item
        DataSet = frxPurhcases
        DataSetName = 'frxPurchases'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 453.543600000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Width = 261.834570000000000000
          Height = 25.984230000000000000
          Frame.Typ = []
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000017B00
            00003C080200000008768D3C000000017352474200AECE1CE90000000467414D
            410000B18F0BFC61050000000970485973000016250000162501495224F00000
            0B794944415478DAED5D6D96D4380C549F84E124C049184EC27012E0244C9F04
            3809ABD75AB2D99E442A7DD87132A91FFB661F69472E4B65F933973F7FFED089
            13274E74C1A550717EDD207F3C3C3CF01F0F376C55B75F33CCEDD9D0A4352369
            18D2D68CA4199F62240DC9E789C191521C76BE6FDFBE3D3F3F5FAFD7B567C423
            DFBC79F3FEFDFBC7C7C70E5512ABBE7CF9A298F4F1E3C7F73774B067D13C668C
            795B7B86E97AFBF62DDBF9EEDDBB3EA429A67EFFFE5D54660DD2C46C6A0B4AD9
            00FDEDCC4F5EF29E9E9E947F75D54B2F0A79C0C4F30D259C98F45661E230A238
            4C991E306B9842A851A8B3499F3E7DC219647BBE7EFDDA4777C2A4B1FA7CF8F0
            4124B2839D0448B60251F37C504DE0066563F437FEFCF933F916D677C56DD863
            D94F9072B8102E4A7FE6F3E7CF497E9810A64579E0C78F1FA0B798F45661AAB5
            4F71F837668F87809B902D28CCC6D924E62E10CF0C89907643832AD25ADB4939
            AD998355929BB844779090C015610D3D1527DFCFBD0AC511472C4CC00A3B4316
            1ACE0292C670239507B337E7325118C92FC1C5E6B5660EE693FD2C392A044382
            6338F3A29E8A43E9BCECF88AD3CEAC7CA857C549FFE140C6D45A7DCC64882692
            830890C624279D15C755E64B1C59719AFAA220E32BB5DD72D5B44E07D238D9E1
            F24B44279F219AC834311E1299C4A1BFE250222F3BACE274F04541CC235B9897
            8FE4C149BB83E9BB9B5BEB0A89703EB589E2843939A6E2748B1C8137D45D4DEB
            42A6ABEC4C5A5274066F62812B24C259EA268A4351673BA0E2B48B6705CC1133
            053ECCA1620E5BB839B94CD90A2495BA5EAF08BFB1AE727CD2E6E82C37824080
            7943829B3B3055BF95E250C8D90EA838483CCFC1D5930D604CB73439E3F7EFDF
            EC5EAE8561907D335A946960640138963B78491323F957CC18FF2D740975AE9D
            3B01970D74C5777B26A54DD96626CA65AD77C63410128149D90D15279097F554
            1CF62E5775D6A0ED00C423075CDF917DAB489920FBBA85FC737E2387B1520287
            0A3F56E2649B93C635E5875D2E0B5A8BAFDF31936C2D287C2E898C75C25E15DE
            5071C89F97F5549CF27397F78A8327DBDE460527299161C2E572C9FC5CC0A2A3
            6431AEFC1F27CD2B6460C9AEB115B8C0174B9D90453A57AF1E531C6F96BAADE2
            B85E4107531CB0F78B2DEC717B70F96B4D8B77FE0ADD38D7854535254D4FC702
            DB8814BD0ED4FD0EE0AE651E8383296178A2C1D5676CAE38E4718FE3280E27C6
            489DC3EE482BF1E30A1B25BC03DDB2C238581A98BB65485B54EAD89645E49852
            7E53122B8E69184848666A13F787111407CFCB8EA3383AEF82E47672C6F57A9D
            E808848D6224F769859B7AC07E18212D7F786FEECAE1EDD1483C6494710E73EC
            06E62019C5C167B846501C9C9383280ED25787D762EF20871B6361A30C0A02EC
            282E82CCE7F5248D03985F97398A61BA5795DC084CD1415E975CBE0513874114
            87B0CEE9208A83346D208958C474FB54E0876B2DCAA11873CD356F4314A72769
            3C20D517E0C23515E44F60DF419FB603DF98DF3082BC651CC541F2B283288E39
            3A28F7C80094415078E4A24C0C9974EF823481E9A62DEE9FD55F8A0C22F49090
            01A639516DFA4637C5E1A2D8D9CC1BCE745A8EA038C82A6C555F9D81D2A2F8F2
            C71D146F33E936D77D46204DA0FB567EA669119C97498CAD3D60C686A9385C82
            B9246F260EDD1487796633CC58D3DF7804C53187DC83F4D5C96997650A5654C3
            2CD05CDA0B0FF45A400FAA76CAA88787A974A6E2B0E5FAD62A81DE9A3D15E7E9
            864C5E7604C5315F9C5FA22AB3B874E658693C3369DA1169A4F2D65419753930
            A7D511C5216C0A5F118ECE8A836C95540EBEF6541C8E02FAEB3C1C5F93172DFE
            2D01B8F8F054D97F15C7DCC336CEE840313510E10AE3661C9AAD350E697A24B4
            5646A5C9AA14C77C52B0168A9D15872C2116ACE5657B3CC9396503FF2A8E3903
            3ACE67AD948CD47B1F823E7B65369B49DA388A93AC69128A5B9BB3A4B8E2984B
            63B4BE58DE5F7108CBCB16C756A7E27485DEA8AECD2F7AADF30B55E390A6FB68
            6B65D4A72D749670C5216C016471B0BC89E298B5A3952DE047501C7DCDA5E403
            1D85D0C780C8B28B3990469C6C47A4E931BFADE2E86F77290E61072C5E7AC856
            8A03E6657775DCBDE298340D153CF4FF73128BD05743916C1609C2C3284EEB5C
            2C93617915C75C8FA7A5C4612BC5212C2FBB3360F78A43BB0A1E8139D53DBF00
            50CE8E32F827C80D61A087ED88B45732AA12801F8D9AFF7043C5217F5E7604C5
            D9D19484A0E9A59960049AAA370E69C3CE1C5303C521FF62F9B68A03E665D39C
            F711146747ABE313CABFE82628BC16671CD286559CC2B5AA3B2097164D15DF56
            71901F9267F3D11114A7B553C6502E3AAE9D299D8F6267A03B74F880080865F8
            69067358719049D9693BC5E68A439EC5F29E8A238E31CDBDFC271C4B7FDFED03
            9C3F203B09093FE55075E5422D5CD7EB9AF09E2D32496B1DC92E28611F3E2082
            403F0BD24E71085861A0BF8E3D82E210363BC996C84AABF2D80E4E39205F4718
            671EF40E25994E60DF2DB264D62E92BDD0BDB95D3AA6FB74FE24A7EE96E00926
            7D3DA19BE280791997B07BC5418691439D12BA835CF1150BEFF03D9BC846F571
            48D33B9546E268F664A64327150739C1C485545D3992541CC2F232D3E01D280E
            6119DDB0690EDDE29FB9730D8BC2D7774ED811696630B410479D9FFC8D5C08BD
            F95BB27A2A0E55E4ECFB509CF0298F009E6F687121CBB4E9461702F9A45FDE00
            64917E1CD2F4F82FF990B98B1C44E3F28A43E9CFAB77561CF0233C0AF6A13848
            5710F818DB2264A28E4B7BBCA1C512B2488FFCC1E98F184FB36F0356BD0559D4
            CC7F1D61224DF2B2186966E0156664881623DE5CA238E4FF62EA1C9D15072947
            C73E1407793D5538E55DDB67426804203970396961B13603AFE4EA356406141C
            C455298EF915D6124EAA14877279D96E1407990AA55CFABD169F1C42ECA3DC1E
            BBD31DB03BF2DEA431C71A697292C3451ABE601C2604C96E70A5A8521CD0B045
            6CA238C846E435EC46710856D6D848013C0C35C2F5A62E6C4B9A88354E1A32BE
            08772AE5DF0B2C541C8A4ECA6EA2380467002FB127C541F261812B2B912FC322
            C58EB34F17876BAA8FDD17248D0BE462CB49039332B65026D741A7C73DC79543
            D52A4E6C52762BC5A1E8D86A4F8A43CE592BF14BAEDEE2985CBE428DEF9469B4
            B339FC792C1CAEEE48C49A79DB8A34DC8F25EB5C333560AD57236A15874289C3
            868A6332B08842C5A98AC7871B68517128A4AC521CC7D2B461DCDB93946F5D91
            E564AECBE464AE4EDB8B0069B27CC68C49DD2F974B37D2BCE30B3695CDE3D7F1
            1FFC5FA6745A0D74C19BC3962B0EF95B6A5BC5C193C709039EE49C2A7E59CB9A
            903B3B6AD1E713B4F95D7F8197B6439834F660F9ACF0E0D6B6501C722E966FAB
            38E4CFCB76A938D4377E6A37BCF659035E444FA54E92C67E2C079147B6B691E2
            B81287CD15879CC1B857C5F1D6338C5AB94172E6AAAD8C8BE8233A25A4C92C4C
            87260EE7628D14873C63AB1114C735E7BD63C5A1DCD6A95A7640985FE615343D
            D81D187BBB50ABD14D2532B9E5BA9DE2E0151F417190C227EC5B71A8E2A0C722
            AAB6FFCFE19A146C7A2BE88E48A366FD4A7EF1B1A9E280BBEC06511CEABEDDA9
            103EC511148650BBE9DB46C3DD306A75A7EA50E89AA9AE756E1D55CAD8547108
            4B1CC6511C9310C1411447900CA1A64B45E464B0DBE5358393360777FB4C4B66
            6687AD6553D9E0127B5A2B0E0189C3508A8364A387521C0157F87ABDCA6E17E4
            79D9FF2327369B56CC95E374BEF95C36B0306F2ED298B1AAE86D672AFD95C5DA
            A3F9F4F7AE35E5A5255BD4F4FE003F7923F377CA03253D876C46571EC0B34B9D
            DE424C150F2ACE04A658F6ADC997F6646318DD5C41B6B749AD3A5449E03AADB7
            E1D75D24A4993A56BDCD49D3ED14B0FA4CFF2BFF24822806EFF7E8FF89CEC82A
            CE68C0178958743BEF7F3B71E2C4D11487E034679C8F499D38F17A7040C52160
            9FC51E4FA89F3871001C5371483D57D5623FCB891327101C567168B6E022F39D
            B2E233C88CEC8913AF1347569C13274E8C8653714E9C38D10FFF001B1B8D6A0D
            4EA51B0000000049454E44AE426082}
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Architecture & Objects')
          ParentFont = False
        end
        object LblAddress1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAddress1')
        end
        object LblAddress2: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAddress2')
        end
        object LblTelephone: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 102.047310000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblTelephone')
        end
        object LblEmail: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblEmail')
        end
        object LblWeb: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 139.842610000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblWeb')
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Tel:')
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'E-mail:')
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 139.842610000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Web:')
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 166.299320000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 359.055350000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Beste,')
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Top = 400.630180000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Gelieve hierbij te vinden:')
        end
        object LblDocumentType: TfrxMemoView
          AllowVectorExport = True
          Top = 423.307360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Een voorschot-ereloon betreffende prestaties geleverd voor:')
        end
        object LblCustomerName: TfrxMemoView
          AllowVectorExport = True
          Top = 185.196970000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerName')
        end
        object LblCustomerAddress1: TfrxMemoView
          AllowVectorExport = True
          Top = 222.992270000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerAddress1')
        end
        object LblCustomerAddress2: TfrxMemoView
          AllowVectorExport = True
          Top = 241.889920000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerAddress2')
        end
        object LblCustomerVatNumber: TfrxMemoView
          AllowVectorExport = True
          Top = 268.346630000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerVatNumber')
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 298.582870000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Frame.Typ = []
        end
        object LblInvoiceType: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 301.228353780000000000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'ERELOONNOTA')
        end
        object InvoiceNumber: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 301.228353780000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'InvoiceNumber')
          ParentFont = False
        end
        object LblDate: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Top = 336.378170000000000000
          Width = 385.512060000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'LblDate')
          ParentFont = False
        end
        object LblAppelation: TfrxMemoView
          AllowVectorExport = True
          Top = 204.094620000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAppelation')
        end
        object ShapeTitle: TfrxShapeView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 249.448980000000000000
          Height = 34.015770000000000000
          Frame.Typ = []
        end
        object LblTitle: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 7.559060000000000000
          Width = 238.110390000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 532.913730000000000000
        Width = 680.315400000000000000
        DataSet = FrxInvoiceLines
        DataSetName = 'FrxInvoiceLines'
        RowCount = 0
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Width = 514.016080000000000000
          Height = 15.118120000000000000
          DataSet = FrxInvoiceLines
          DataSetName = 'FrxInvoiceLines'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '- [FrxInvoiceLines."Description"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataField = 'Amount'
          DataSet = FrxInvoiceLines
          DataSetName = 'FrxInvoiceLines'
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[FrxInvoiceLines."Amount"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 861.732840000000000000
        Width = 680.315400000000000000
        object LblBank11: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank11')
          ParentFont = False
        end
        object LblBank12: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank12')
          ParentFont = False
        end
        object LblBank13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 56.692950000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank13')
          ParentFont = False
        end
        object LblVat: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblVat')
          ParentFont = False
        end
        object LblBank21: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 18.897650000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank21')
          ParentFont = False
        end
        object LblBank22: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 37.795300000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank22')
          ParentFont = False
        end
        object LblBank23: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 56.692950000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank23')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 226.771800000000000000
        Top = 612.283860000000000000
        Width = 680.315400000000000000
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 7.559060000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<FrxInvoiceLines."Amount">,MasterData2)]')
          ParentFont = False
        end
        object LblTotalen: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'SubTotaal:')
          ParentFont = False
        end
        object LblPaymentLine1: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Gelieve dit bedrag over te schrijven op onderstaand rekeningnumm' +
              'er binnen de 15 dagen na')
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'facturatiedatum met vermelding van het ereloonnota-nummer.')
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Top = 151.181200000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Alvast bedankt!')
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 170.078850000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Met vriendelijke groeten,')
        end
        object LblCompanyName: TfrxMemoView
          AllowVectorExport = True
          Top = 200.315090000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCompanyName')
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 26.456710000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'BTW 21%:')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 415.748300000000000000
          Top = 56.692950000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totaal:')
          ParentFont = False
        end
        object VatAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 26.456710000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VatAmount')
          ParentFont = False
        end
        object GeneralTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 56.692950000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'GeneralTotal')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 680.315400000000000000
        DataSet = frxInvoiceTimeRegistrations
        DataSetName = 'frxInvoiceTimeRegistrations'
        RowCount = 0
        object frxInvoiceTimeRegistrationsREGISTRATIONDATE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataField = 'REGISTRATIONDATE'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."REGISTRATIONDATE"]')
          ParentFont = False
        end
        object frxInvoiceTimeRegistrationsDUTCH: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 117.165430000000000000
          Width = 52.913383390000000000
          Height = 15.118120000000000000
          DataField = 'ENDTIMEVALUE'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,#0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."ENDTIMEVALUE"]')
          ParentFont = False
        end
        object frxInvoiceTimeRegistrationsSALDOVALUE: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          DataField = 'STARTTIMEVALUE'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,#0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."STARTTIMEVALUE"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 173.858380000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DataField = 'SALDOVALUE'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.FormatStr = '#,#0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."SALDOVALUE"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 275.905690000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          DataField = 'DUTCH'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."DUTCH"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 393.071120000000000000
          Width = 226.771800000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRIPTION'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."DESCRIPTION"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 226.771800000000000000
          Width = 45.354360000000000000
          Height = 15.118120000000000000
          DataField = 'HOURLYRATE'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.FormatStr = '#,#0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."HOURLYRATE"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          DataField = 'TOTALAMOUNTLINE'
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceTimeRegistrations."TOTALAMOUNTLINE"]')
          ParentFont = False
        end
      end
      object ReportTitle2: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Top = 34.015770000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          Memo.UTF8W = (
            'Datum')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 34.015770000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Begintijd')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 34.015770000000000000
          Width = 52.913378500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Eindtijd')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'TIJDSBESTEDINGSTABEL')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000000000
          Width = 49.133848500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 275.905690000000000000
          Top = 34.015770000000000000
          Width = 68.031498500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          Memo.UTF8W = (
            'Activiteit')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 34.015770000000000000
          Width = 109.606328500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          Memo.UTF8W = (
            'Omschrijving')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 34.015770000000000000
          Width = 45.354318500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Tarief')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 619.842920000000000000
          Top = 34.015770000000000000
          Width = 52.913378500000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsUnderline]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totaal')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 173.858380000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object frxInvoiceTimeRegistrationsSALDOVALUE1: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 2.645669290000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxInvoiceTimeRegistrations."SALDOVALUE">,MasterData1)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 2.645669290000000000
          Width = 90.708720000000000000
          Height = 15.118120000000000000
          DataSet = frxInvoiceTimeRegistrations
          DataSetName = 'frxInvoiceTimeRegistrations'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            
              '[SUM(<frxInvoiceTimeRegistrations."TOTALAMOUNTLINE">,MasterData1' +
              ')]')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 385.512060000000000000
        Width = 680.315400000000000000
        DataSet = frsInvoices
        DataSetName = 'frsInvoices'
        RowCount = 0
        object frsInvoicesINVOICENUMBER: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 249.448980000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'INVOICENUMBER'
          DataSet = frsInvoices
          DataSetName = 'frsInvoices'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frsInvoices."INVOICENUMBER"]')
        end
        object Memo31: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 582.047620000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'AMOUNT'
          DataSet = frsInvoices
          DataSetName = 'frsInvoices'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frsInvoices."AMOUNT"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = -3.779530000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          DataField = 'INVOICEDATE'
          DataSet = frsInvoices
          DataSetName = 'frsInvoices'
          Frame.Typ = []
          Memo.UTF8W = (
            '[frsInvoices."INVOICEDATE"]')
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 60.472480000000000000
        Top = 302.362400000000000000
        Width = 680.315400000000000000
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 393.071120000000000000
          Top = 34.015770000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsUnderline]
          Frame.Typ = []
          Memo.UTF8W = (
            'Datum')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Samenvatting')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 249.448980000000000000
          Top = 34.015770000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frsInvoices
          DataSetName = 'frsInvoices'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsUnderline]
          Frame.Typ = []
          Memo.UTF8W = (
            'Document')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 34.015770000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsUnderline]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Bedrag')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totaal:')
          ParentFont = False
        end
        object SummaryTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'SummaryTotal')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 30.236240000000000000
        Top = 427.086890000000000000
        Width = 680.315400000000000000
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 11.338590000000000000
          Width = 162.519790000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Openstaand saldo:')
          ParentFont = False
        end
        object SummaryOpen: TfrxMemoView
          AllowVectorExport = True
          Left = 582.047620000000000000
          Top = 11.338590000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'SummaryOpen')
          ParentFont = False
        end
      end
      object HeaderSales: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        object Shape6: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Width = 185.196970000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'AANKOPEN')
          ParentFont = False
        end
      end
      object DataPurhcases: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        DataSet = frxPurhcases
        DataSetName = 'frxPurchases'
        RowCount = 0
        object Memo44: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Width = 56.692950000000010000
          Height = 15.118120000000000000
          DataField = 'DATEPURCHASE'
          DataSet = frxPurhcases
          DataSetName = 'frxPurchases'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxPurchases."DATEPURCHASE"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 559.370440000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRIPTION'
          DataSet = frxPurhcases
          DataSetName = 'frxPurchases'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxPurchases."DESCRIPTION"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          IndexTag = 1
          AllowVectorExport = True
          Left = 619.842920000000000000
          Width = 52.913420000000000000
          Height = 15.118120000000000000
          DataField = 'TOTALSALESPRICE'
          DataSet = frxPurhcases
          DataSetName = 'frxPurchases'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxPurchases."TOTALSALESPRICE"]')
          ParentFont = False
        end
      end
    end
  end
  object InvoiceLinesPrint: TVirtualTable
    Active = True
    FieldDefs = <
      item
        Name = 'Description'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Amount'
        DataType = ftCurrency
      end>
    Left = 368
    Top = 168
    Data = {
      040002000B004465736372697074696F6E01006400000000000600416D6F756E
      740700000000000000000000000000}
  end
  object DsInvoiceLines: TDataSource
    DataSet = InvoiceLinesPrint
    Left = 368
    Top = 224
  end
  object FrxInvoiceLines: TfrxDBDataset
    UserName = 'FrxInvoiceLines'
    CloseDataSource = False
    DataSource = DsInvoiceLines
    BCDToCurrency = False
    Left = 368
    Top = 281
  end
  object PdfExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    OpenAfterExport = False
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Coconne'
    Subject = 'Coconne'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = True
    PDFStandard = psPDFA_2a
    PDFVersion = pv17
    Left = 365
    Top = 358
  end
  object InvoiceTimeRegistrations: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO ZIPCODES'
      '  (ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY)'
      'VALUES'
      '  (:ID, :COMPANYID, :COUNTRYCODE, :ZIPCODE, :CITY)'
      'RETURNING '
      '  ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY')
    SQLDelete.Strings = (
      'DELETE FROM ZIPCODES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE ZIPCODES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, COUNTRYCODE = :COUNTRYCODE, ' +
        'ZIPCODE = :ZIPCODE, CITY = :CITY'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY FROM ZIPCODES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM ZIPCODES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM ZIPCODES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  TIMEREGISTRATIONS.ID,'
      '  TIMEREGISTRATIONS.REGISTRATIONDATE,'
      '  TIMEREGISTRATIONS.STARTTIMEVALUE,'
      '  TIMEREGISTRATIONS.ENDTIMEVALUE,'
      '  TIMEREGISTRATIONS.SALDOVALUE,'
      '  TIMEREGISTRATIONS.HOURLYRATE,'
      
        '  TIMEREGISTRATIONS.HOURLYRATE * TIMEREGISTRATIONS.SALDOVALUE AS' +
        ' TOTALAMOUNTLINE,'
      '  BASICTABLES.DUTCH,'
      '  TIMEREGISTRATIONS.DESCRIPTION'
      'FROM'
      '  TIMEREGISTRATIONS'
      
        '  LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = B' +
        'ASICTABLES.ID)'
      'WHERE'
      '  TIMEREGISTRATIONS.REMOVED = '#39'0'#39
      'ORDER BY'
      '  TIMEREGISTRATIONS.REGISTRATIONDATE')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 368
    Top = 432
  end
  object DsInvoiceTimeRegistrations: TDataSource
    DataSet = InvoiceTimeRegistrations
    Left = 368
    Top = 480
  end
  object frxInvoiceTimeRegistrations: TfrxDBDataset
    UserName = 'frxInvoiceTimeRegistrations'
    CloseDataSource = False
    DataSource = DsInvoiceTimeRegistrations
    BCDToCurrency = False
    Left = 368
    Top = 545
  end
  object ReportsInvoicingOverview: TfrxReport
    Version = '6.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 544
    Top = 88
    Datasets = <
      item
        DataSet = FrxTimeRegistrations
        DataSetName = 'FrxTimeRegistrations'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 132.283550000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object LblReportTile: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 5455915
          HAlign = haCenter
          Memo.UTF8W = (
            'Overzicht facturatie')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 86.929190000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Medewerker:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 105.826840000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Periode:')
          ParentFont = False
        end
        object LblMedewerker: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 86.929190000000000000
          Width = 563.149970000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblMedewerker')
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Width = 261.834570000000000000
          Height = 25.984230000000000000
          Frame.Typ = []
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000017B00
            00003C080200000008768D3C000000017352474200AECE1CE90000000467414D
            410000B18F0BFC61050000000970485973000016250000162501495224F00000
            0B794944415478DAED5D6D96D4380C549F84E124C049184EC27012E0244C9F04
            3809ABD75AB2D99E442A7DD87132A91FFB661F69472E4B65F933973F7FFED089
            13274E74C1A550717EDD207F3C3C3CF01F0F376C55B75F33CCEDD9D0A4352369
            18D2D68CA4199F62240DC9E789C191521C76BE6FDFBE3D3F3F5FAFD7B567C423
            DFBC79F3FEFDFBC7C7C70E5512ABBE7CF9A298F4F1E3C7F73774B067D13C668C
            795B7B86E97AFBF62DDBF9EEDDBB3EA429A67EFFFE5D54660DD2C46C6A0B4AD9
            00FDEDCC4F5EF29E9E9E947F75D54B2F0A79C0C4F30D259C98F45661E230A238
            4C991E306B9842A851A8B3499F3E7DC219647BBE7EFDDA4777C2A4B1FA7CF8F0
            4124B2839D0448B60251F37C504DE0066563F437FEFCF933F916D677C56DD863
            D94F9072B8102E4A7FE6F3E7CF497E9810A64579E0C78F1FA0B798F45661AAB5
            4F71F837668F87809B902D28CCC6D924E62E10CF0C89907643832AD25ADB4939
            AD998355929BB844779090C015610D3D1527DFCFBD0AC511472C4CC00A3B4316
            1ACE0292C670239507B337E7325118C92FC1C5E6B5660EE693FD2C392A044382
            6338F3A29E8A43E9BCECF88AD3CEAC7CA857C549FFE140C6D45A7DCC64882692
            830890C624279D15C755E64B1C59719AFAA220E32BB5DD72D5B44E07D238D9E1
            F24B44279F219AC834311E1299C4A1BFE250222F3BACE274F04541CC235B9897
            8FE4C149BB83E9BB9B5BEB0A89703EB589E2843939A6E2748B1C8137D45D4DEB
            42A6ABEC4C5A5274066F62812B24C259EA268A4351673BA0E2B48B6705CC1133
            053ECCA1620E5BB839B94CD90A2495BA5EAF08BFB1AE727CD2E6E82C37824080
            7943829B3B3055BF95E250C8D90EA838483CCFC1D5930D604CB73439E3F7EFDF
            EC5EAE8561907D335A946960640138963B78491323F957CC18FF2D740975AE9D
            3B01970D74C5777B26A54DD96626CA65AD77C63410128149D90D15279097F554
            1CF62E5775D6A0ED00C423075CDF917DAB489920FBBA85FC737E2387B1520287
            0A3F56E2649B93C635E5875D2E0B5A8BAFDF31936C2D287C2E898C75C25E15DE
            5071C89F97F5549CF27397F78A8327DBDE460527299161C2E572C9FC5CC0A2A3
            6431AEFC1F27CD2B6460C9AEB115B8C0174B9D90453A57AF1E531C6F96BAADE2
            B85E4107531CB0F78B2DEC717B70F96B4D8B77FE0ADD38D7854535254D4FC702
            DB8814BD0ED4FD0EE0AE651E8383296178A2C1D5676CAE38E4718FE3280E27C6
            489DC3EE482BF1E30A1B25BC03DDB2C238581A98BB65485B54EAD89645E49852
            7E53122B8E69184848666A13F787111407CFCB8EA3383AEF82E47672C6F57A9D
            E808848D6224F769859B7AC07E18212D7F786FEECAE1EDD1483C6494710E73EC
            06E62019C5C167B846501C9C9383280ED25787D762EF20871B6361A30C0A02EC
            282E82CCE7F5248D03985F97398A61BA5795DC084CD1415E975CBE0513874114
            87B0CEE9208A83346D208958C474FB54E0876B2DCAA11873CD356F4314A72769
            3C20D517E0C23515E44F60DF419FB603DF98DF3082BC651CC541F2B283288E39
            3A28F7C80094415078E4A24C0C9974EF823481E9A62DEE9FD55F8A0C22F49090
            01A639516DFA4637C5E1A2D8D9CC1BCE745A8EA038C82A6C555F9D81D2A2F8F2
            C71D146F33E936D77D46204DA0FB567EA669119C97498CAD3D60C686A9385C82
            B9246F260EDD1487796633CC58D3DF7804C53187DC83F4D5C96997650A5654C3
            2CD05CDA0B0FF45A400FAA76CAA88787A974A6E2B0E5FAD62A81DE9A3D15E7E9
            864C5E7604C5315F9C5FA22AB3B874E658693C3369DA1169A4F2D65419753930
            A7D511C5216C0A5F118ECE8A836C95540EBEF6541C8E02FAEB3C1C5F93172DFE
            2D01B8F8F054D97F15C7DCC336CEE840313510E10AE3661C9AAD350E697A24B4
            5646A5C9AA14C77C52B0168A9D15872C2116ACE5657B3CC9396503FF2A8E3903
            3ACE67AD948CD47B1F823E7B65369B49DA388A93AC69128A5B9BB3A4B8E2984B
            63B4BE58DE5F7108CBCB16C756A7E27485DEA8AECD2F7AADF30B55E390A6FB68
            6B65D4A72D749670C5216C016471B0BC89E298B5A3952DE047501C7DCDA5E403
            1D85D0C780C8B28B3990469C6C47A4E931BFADE2E86F77290E61072C5E7AC856
            8A03E6657775DCBDE298340D153CF4FF73128BD05743916C1609C2C3284EEB5C
            2C93617915C75C8FA7A5C4612BC5212C2FBB3360F78A43BB0A1E8139D53DBF00
            50CE8E32F827C80D61A087ED88B45732AA12801F8D9AFF7043C5217F5E7604C5
            D9D19484A0E9A59960049AAA370E69C3CE1C5303C521FF62F9B68A03E665D39C
            F711146747ABE313CABFE82628BC16671CD286559CC2B5AA3B2097164D15DF56
            71901F9267F3D11114A7B553C6502E3AAE9D299D8F6267A03B74F880080865F8
            69067358719049D9693BC5E68A439EC5F29E8A238E31CDBDFC271C4B7FDFED03
            9C3F203B09093FE55075E5422D5CD7EB9AF09E2D32496B1DC92E28611F3E2082
            403F0BD24E71085861A0BF8E3D82E210363BC996C84AABF2D80E4E39205F4718
            671EF40E25994E60DF2DB264D62E92BDD0BDB95D3AA6FB74FE24A7EE96E00926
            7D3DA19BE280791997B07BC5418691439D12BA835CF1150BEFF03D9BC846F571
            48D33B9546E268F664A64327150739C1C485545D3992541CC2F232D3E01D280E
            6119DDB0690EDDE29FB9730D8BC2D7774ED811696630B410479D9FFC8D5C08BD
            F95BB27A2A0E55E4ECFB509CF0298F009E6F687121CBB4E9461702F9A45FDE00
            64917E1CD2F4F82FF990B98B1C44E3F28A43E9CFAB77561CF0233C0AF6A13848
            5710F818DB2264A28E4B7BBCA1C512B2488FFCC1E98F184FB36F0356BD0559D4
            CC7F1D61224DF2B2186966E0156664881623DE5CA238E4FF62EA1C9D15072947
            C73E1407793D5538E55DDB67426804203970396961B13603AFE4EA356406141C
            C455298EF915D6124EAA14877279D96E1407990AA55CFABD169F1C42ECA3DC1E
            BBD31DB03BF2DEA431C71A697292C3451ABE601C2604C96E70A5A8521CD0B045
            6CA238C846E435EC46710856D6D848013C0C35C2F5A62E6C4B9A88354E1A32BE
            08772AE5DF0B2C541C8A4ECA6EA2380467002FB127C541F261812B2B912FC322
            C58EB34F17876BAA8FDD17248D0BE462CB49039332B65026D741A7C73DC79543
            D52A4E6C52762BC5A1E8D86A4F8A43CE592BF14BAEDEE2985CBE428DEF9469B4
            B339FC792C1CAEEE48C49A79DB8A34DC8F25EB5C333560AD57236A15874289C3
            868A6332B08842C5A98AC7871B68517128A4AC521CC7D2B461DCDB93946F5D91
            E564AECBE464AE4EDB8B0069B27CC68C49DD2F974B37D2BCE30B3695CDE3D7F1
            1FFC5FA6745A0D74C19BC3962B0EF95B6A5BC5C193C709039EE49C2A7E59CB9A
            903B3B6AD1E713B4F95D7F8197B6439834F660F9ACF0E0D6B6501C722E966FAB
            38E4CFCB76A938D4377E6A37BCF659035E444FA54E92C67E2C079147B6B691E2
            B81287CD15879CC1B857C5F1D6338C5AB94172E6AAAD8C8BE8233A25A4C92C4C
            87260EE7628D14873C63AB1114C735E7BD63C5A1DCD6A95A7640985FE615343D
            D81D187BBB50ABD14D2532B9E5BA9DE2E0151F417190C227EC5B71A8E2A0C722
            AAB6FFCFE19A146C7A2BE88E48A366FD4A7EF1B1A9E280BBEC06511CEABEDDA9
            103EC511148650BBE9DB46C3DD306A75A7EA50E89AA9AE756E1D55CAD8547108
            4B1CC6511C9310C1411447900CA1A64B45E464B0DBE5358393360777FB4C4B66
            6687AD6553D9E0127B5A2B0E0189C3508A8364A387521C0157F87ABDCA6E17E4
            79D9FF2327369B56CC95E374BEF95C36B0306F2ED298B1AAE86D672AFD95C5DA
            A3F9F4F7AE35E5A5255BD4F4FE003F7923F377CA03253D876C46571EC0B34B9D
            DE424C150F2ACE04A658F6ADC997F6646318DD5C41B6B749AD3A5449E03AADB7
            E1D75D24A4993A56BDCD49D3ED14B0FA4CFF2BFF24822806EFF7E8FF89CEC82A
            CE68C0178958743BEF7F3B71E2C4D11487E034679C8F499D38F17A7040C52160
            9FC51E4FA89F3871001C5371483D57D5623FCB891327101C567168B6E022F39D
            B2E233C88CEC8913AF1347569C13274E8C8653714E9C38D10FFF001B1B8D6A0D
            4EA51B0000000049454E44AE426082}
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Architecture & Objects')
          ParentFont = False
        end
        object LblPeriode: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 105.826840000000000000
          Width = 563.149970000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblPeriode')
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 257.008040000000000000
        Width = 680.315400000000000000
        DataSet = FrxTimeRegistrations
        DataSetName = 'FrxTimeRegistrations'
        RowCount = 0
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'REGISTRATIONDATE'
          DataSet = FrxTimeRegistrations
          DataSetName = 'FrxTimeRegistrations'
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[FrxTimeRegistrations."REGISTRATIONDATE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Width = 181.417440000000000000
          Height = 15.118120000000000000
          DataField = 'NAME'
          DataSet = FrxTimeRegistrations
          DataSetName = 'FrxTimeRegistrations'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[FrxTimeRegistrations."NAME"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 109.606370000000000000
          Height = 15.118120000000000000
          DataField = 'DUTCH'
          DataSet = FrxTimeRegistrations
          DataSetName = 'FrxTimeRegistrations'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[FrxTimeRegistrations."DUTCH"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 34.015770000000000000
          Height = 15.118120000000000000
          DataSet = FrxTimeRegistrations
          DataSetName = 'FrxTimeRegistrations'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[FrxTimeRegistrations."KILOMETERS"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692950000000000000
          Height = 15.118120000000000000
          DataSet = FrxTimeRegistrations
          DataSetName = 'FrxTimeRegistrations'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[FrxTimeRegistrations."SALDOVALUE"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 211.653680000000000000
          Height = 15.118120000000000000
          DataField = 'DESCRIPTION'
          DataSet = FrxTimeRegistrations
          DataSetName = 'FrxTimeRegistrations'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[FrxTimeRegistrations."DESCRIPTION"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 211.653680000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = 5455915
          Frame.Color = 5455915
          Frame.Typ = []
        end
        object LblDateCredit: TfrxMemoView
          AllowVectorExport = True
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Datum')
          ParentFont = False
        end
        object LblCustomer: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Project')
          ParentFont = False
        end
        object LblLocation: TfrxMemoView
          AllowVectorExport = True
          Left = 257.008040000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Activiteit')
          ParentFont = False
        end
        object LblCreditAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 585.827150000000000000
          Width = 34.015770000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Km')
          ParentFont = False
        end
        object LblCommissionAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Uren')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Omschrijving')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 559.370440000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 3.779530000000000000
          Width = 317.480520000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          Memo.UTF8W = (
            'Coconne')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000020000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Color = clSilver
          Frame.Color = clSilver
          Frame.Typ = [ftTop]
        end
        object SysMemo2: TfrxSysMemoView
          AllowVectorExport = True
          Top = 3.779530000000020000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 204.094620000000000000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 154.960730000000000000
          Frame.Color = 5455915
          Frame.Typ = []
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 7.559059999999990000
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          Fill.BackColor = 5455915
          Frame.Color = 5455915
          Frame.Typ = []
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 11.338590000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<FrxTimeRegistrations."KILOMETERS">,MasterData2)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 623.622450000000000000
          Top = 11.338590000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<FrxTimeRegistrations."SALDOVALUE">,MasterData2)]')
          ParentFont = False
        end
        object LblTotalen: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 11.338590000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totalen:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 49.133890000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Uren:')
          ParentFont = False
        end
        object LblHours: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 49.133890000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'LblHours')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 68.031540000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Km:')
          ParentFont = False
        end
        object LblKms: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 68.031540000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'LblKms')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 113.385900000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totaal:')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 132.283550000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Btw:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 170.078850000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Algemeen totaal:')
          ParentFont = False
        end
        object LblTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 113.385900000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'LblTotal')
          ParentFont = False
        end
        object LblVat: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 132.283550000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'LblVat')
          ParentFont = False
        end
        object LblGeneralTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 170.078850000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'LblGeneralTotal')
          ParentFont = False
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 98.267780000000000000
          Width = 117.165430000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 162.519790000000000000
          Width = 117.165430000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 49.133890000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'x')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 68.031540000000000000
          Width = 15.118120000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'x')
          ParentFont = False
        end
        object LblHourQuantity: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 49.133890000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          ParentFont = False
        end
        object LblHourlyRate: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 49.133890000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          ParentFont = False
        end
        object LblKmQuantity: TfrxMemoView
          AllowVectorExport = True
          Left = 241.889920000000000000
          Top = 68.031540000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          ParentFont = False
        end
        object LblKmRate: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Top = 68.031540000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          ParentFont = False
        end
      end
    end
  end
  object TimeRegistrations: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO ZIPCODES'
      '  (ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY)'
      'VALUES'
      '  (:ID, :COMPANYID, :COUNTRYCODE, :ZIPCODE, :CITY)'
      'RETURNING '
      '  ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY')
    SQLDelete.Strings = (
      'DELETE FROM ZIPCODES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE ZIPCODES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, COUNTRYCODE = :COUNTRYCODE, ' +
        'ZIPCODE = :ZIPCODE, CITY = :CITY'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY FROM ZIPCODES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM ZIPCODES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM ZIPCODES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  PROJECTS.NAME,'
      '  TIMEREGISTRATIONS.ID,'
      '  TIMEREGISTRATIONS.REGISTRATIONDATE,'
      '  TIMEREGISTRATIONS.SALDOVALUE,'
      '  TIMEREGISTRATIONS.KILOMETERS,'
      '  TIMEREGISTRATIONS.DESCRIPTION,'
      '  BASICTABLES.DUTCH'
      'FROM'
      '  TIMEREGISTRATIONS'
      
        '  RIGHT OUTER JOIN PROJECTS ON (TIMEREGISTRATIONS.PROJECT = PROJ' +
        'ECTS.ID)'
      
        '  LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = B' +
        'ASICTABLES.ID)'
      'WHERE'
      '  TIMEREGISTRATIONS.REMOVED = '#39'0'#39
      'ORDER BY'
      '  TIMEREGISTRATIONS.REGISTRATIONDATE')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 544
    Top = 160
  end
  object DsTimeRegistrations: TDataSource
    DataSet = TimeRegistrations
    Left = 544
    Top = 208
  end
  object FrxTimeRegistrations: TfrxDBDataset
    UserName = 'FrxTimeRegistrations'
    CloseDataSource = False
    DataSource = DsTimeRegistrations
    BCDToCurrency = False
    Left = 544
    Top = 265
  end
  object Invoices: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO INVOICES'
      
        '  (ID, COMPANYID, RELATION, INVOICENUMBER, INVOICEDATE, PAYMENTT' +
        'ERM, DUEDATE, AMOUNT, VATAMOUNT, TOTALAMOUNT, PROJECT, TOTALPAYE' +
        'D, INVOICETYPE, SPLITINVOICE, INVOICESTAGE)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :RELATION, :INVOICENUMBER, :INVOICEDATE, :PA' +
        'YMENTTERM, :DUEDATE, :AMOUNT, :VATAMOUNT, :TOTALAMOUNT, :PROJECT' +
        ', :TOTALPAYED, :INVOICETYPE, :SPLITINVOICE, :INVOICESTAGE)'
      'RETURNING '
      
        '  ID, COMPANYID, RELATION, INVOICENUMBER, INVOICEDATE, PAYMENTTE' +
        'RM, DUEDATE, AMOUNT, VATAMOUNT, TOTALAMOUNT, PROJECT, TOTALPAYED' +
        ', INVOICETYPE, SPLITINVOICE, INVOICESTAGE')
    SQLDelete.Strings = (
      'DELETE FROM INVOICES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE INVOICES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, RELATION = :RELATION, INVOIC' +
        'ENUMBER = :INVOICENUMBER, INVOICEDATE = :INVOICEDATE, PAYMENTTER' +
        'M = :PAYMENTTERM, DUEDATE = :DUEDATE, AMOUNT = :AMOUNT, VATAMOUN' +
        'T = :VATAMOUNT, TOTALAMOUNT = :TOTALAMOUNT, PROJECT = :PROJECT, ' +
        'TOTALPAYED = :TOTALPAYED, INVOICETYPE = :INVOICETYPE, SPLITINVOI' +
        'CE = :SPLITINVOICE, INVOICESTAGE = :INVOICESTAGE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, RELATION, INVOICENUMBER, INVOICEDATE, PAYM' +
        'ENTTERM, DUEDATE, AMOUNT, VATAMOUNT, TOTALAMOUNT, PROJECT, TOTAL' +
        'PAYED, INVOICETYPE, SPLITINVOICE, INVOICESTAGE FROM INVOICES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM INVOICES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM INVOICES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from invoices')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Active = True
    FilterOptions = [foCaseInsensitive]
    Left = 368
    Top = 608
  end
  object DsInvoices: TDataSource
    DataSet = Invoices
    Left = 368
    Top = 656
  end
  object frsInvoices: TfrxDBDataset
    UserName = 'frsInvoices'
    CloseDataSource = False
    DataSource = DsInvoices
    BCDToCurrency = False
    Left = 368
    Top = 712
  end
  object ReportInvoiceList: TfrxReport
    Version = '6.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'begin'
      ''
      'end.')
    Left = 760
    Top = 88
    Datasets = <
      item
        DataSet = frxInvoiceList
        DataSetName = 'frxInvoiceList'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object LblReportTile: TfrxMemoView
          AllowVectorExport = True
          Top = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 5455915
          HAlign = haCenter
          Memo.UTF8W = (
            'Overzicht facturen')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Width = 261.834570000000000000
          Height = 25.984230000000000000
          Frame.Typ = []
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000017B00
            00003C080200000008768D3C000000017352474200AECE1CE90000000467414D
            410000B18F0BFC61050000000970485973000016250000162501495224F00000
            0B794944415478DAED5D6D96D4380C549F84E124C049184EC27012E0244C9F04
            3809ABD75AB2D99E442A7DD87132A91FFB661F69472E4B65F933973F7FFED089
            13274E74C1A550717EDD207F3C3C3CF01F0F376C55B75F33CCEDD9D0A4352369
            18D2D68CA4199F62240DC9E789C191521C76BE6FDFBE3D3F3F5FAFD7B567C423
            DFBC79F3FEFDFBC7C7C70E5512ABBE7CF9A298F4F1E3C7F73774B067D13C668C
            795B7B86E97AFBF62DDBF9EEDDBB3EA429A67EFFFE5D54660DD2C46C6A0B4AD9
            00FDEDCC4F5EF29E9E9E947F75D54B2F0A79C0C4F30D259C98F45661E230A238
            4C991E306B9842A851A8B3499F3E7DC219647BBE7EFDDA4777C2A4B1FA7CF8F0
            4124B2839D0448B60251F37C504DE0066563F437FEFCF933F916D677C56DD863
            D94F9072B8102E4A7FE6F3E7CF497E9810A64579E0C78F1FA0B798F45661AAB5
            4F71F837668F87809B902D28CCC6D924E62E10CF0C89907643832AD25ADB4939
            AD998355929BB844779090C015610D3D1527DFCFBD0AC511472C4CC00A3B4316
            1ACE0292C670239507B337E7325118C92FC1C5E6B5660EE693FD2C392A044382
            6338F3A29E8A43E9BCECF88AD3CEAC7CA857C549FFE140C6D45A7DCC64882692
            830890C624279D15C755E64B1C59719AFAA220E32BB5DD72D5B44E07D238D9E1
            F24B44279F219AC834311E1299C4A1BFE250222F3BACE274F04541CC235B9897
            8FE4C149BB83E9BB9B5BEB0A89703EB589E2843939A6E2748B1C8137D45D4DEB
            42A6ABEC4C5A5274066F62812B24C259EA268A4351673BA0E2B48B6705CC1133
            053ECCA1620E5BB839B94CD90A2495BA5EAF08BFB1AE727CD2E6E82C37824080
            7943829B3B3055BF95E250C8D90EA838483CCFC1D5930D604CB73439E3F7EFDF
            EC5EAE8561907D335A946960640138963B78491323F957CC18FF2D740975AE9D
            3B01970D74C5777B26A54DD96626CA65AD77C63410128149D90D15279097F554
            1CF62E5775D6A0ED00C423075CDF917DAB489920FBBA85FC737E2387B1520287
            0A3F56E2649B93C635E5875D2E0B5A8BAFDF31936C2D287C2E898C75C25E15DE
            5071C89F97F5549CF27397F78A8327DBDE460527299161C2E572C9FC5CC0A2A3
            6431AEFC1F27CD2B6460C9AEB115B8C0174B9D90453A57AF1E531C6F96BAADE2
            B85E4107531CB0F78B2DEC717B70F96B4D8B77FE0ADD38D7854535254D4FC702
            DB8814BD0ED4FD0EE0AE651E8383296178A2C1D5676CAE38E4718FE3280E27C6
            489DC3EE482BF1E30A1B25BC03DDB2C238581A98BB65485B54EAD89645E49852
            7E53122B8E69184848666A13F787111407CFCB8EA3383AEF82E47672C6F57A9D
            E808848D6224F769859B7AC07E18212D7F786FEECAE1EDD1483C6494710E73EC
            06E62019C5C167B846501C9C9383280ED25787D762EF20871B6361A30C0A02EC
            282E82CCE7F5248D03985F97398A61BA5795DC084CD1415E975CBE0513874114
            87B0CEE9208A83346D208958C474FB54E0876B2DCAA11873CD356F4314A72769
            3C20D517E0C23515E44F60DF419FB603DF98DF3082BC651CC541F2B283288E39
            3A28F7C80094415078E4A24C0C9974EF823481E9A62DEE9FD55F8A0C22F49090
            01A639516DFA4637C5E1A2D8D9CC1BCE745A8EA038C82A6C555F9D81D2A2F8F2
            C71D146F33E936D77D46204DA0FB567EA669119C97498CAD3D60C686A9385C82
            B9246F260EDD1487796633CC58D3DF7804C53187DC83F4D5C96997650A5654C3
            2CD05CDA0B0FF45A400FAA76CAA88787A974A6E2B0E5FAD62A81DE9A3D15E7E9
            864C5E7604C5315F9C5FA22AB3B874E658693C3369DA1169A4F2D65419753930
            A7D511C5216C0A5F118ECE8A836C95540EBEF6541C8E02FAEB3C1C5F93172DFE
            2D01B8F8F054D97F15C7DCC336CEE840313510E10AE3661C9AAD350E697A24B4
            5646A5C9AA14C77C52B0168A9D15872C2116ACE5657B3CC9396503FF2A8E3903
            3ACE67AD948CD47B1F823E7B65369B49DA388A93AC69128A5B9BB3A4B8E2984B
            63B4BE58DE5F7108CBCB16C756A7E27485DEA8AECD2F7AADF30B55E390A6FB68
            6B65D4A72D749670C5216C016471B0BC89E298B5A3952DE047501C7DCDA5E403
            1D85D0C780C8B28B3990469C6C47A4E931BFADE2E86F77290E61072C5E7AC856
            8A03E6657775DCBDE298340D153CF4FF73128BD05743916C1609C2C3284EEB5C
            2C93617915C75C8FA7A5C4612BC5212C2FBB3360F78A43BB0A1E8139D53DBF00
            50CE8E32F827C80D61A087ED88B45732AA12801F8D9AFF7043C5217F5E7604C5
            D9D19484A0E9A59960049AAA370E69C3CE1C5303C521FF62F9B68A03E665D39C
            F711146747ABE313CABFE82628BC16671CD286559CC2B5AA3B2097164D15DF56
            71901F9267F3D11114A7B553C6502E3AAE9D299D8F6267A03B74F880080865F8
            69067358719049D9693BC5E68A439EC5F29E8A238E31CDBDFC271C4B7FDFED03
            9C3F203B09093FE55075E5422D5CD7EB9AF09E2D32496B1DC92E28611F3E2082
            403F0BD24E71085861A0BF8E3D82E210363BC996C84AABF2D80E4E39205F4718
            671EF40E25994E60DF2DB264D62E92BDD0BDB95D3AA6FB74FE24A7EE96E00926
            7D3DA19BE280791997B07BC5418691439D12BA835CF1150BEFF03D9BC846F571
            48D33B9546E268F664A64327150739C1C485545D3992541CC2F232D3E01D280E
            6119DDB0690EDDE29FB9730D8BC2D7774ED811696630B410479D9FFC8D5C08BD
            F95BB27A2A0E55E4ECFB509CF0298F009E6F687121CBB4E9461702F9A45FDE00
            64917E1CD2F4F82FF990B98B1C44E3F28A43E9CFAB77561CF0233C0AF6A13848
            5710F818DB2264A28E4B7BBCA1C512B2488FFCC1E98F184FB36F0356BD0559D4
            CC7F1D61224DF2B2186966E0156664881623DE5CA238E4FF62EA1C9D15072947
            C73E1407793D5538E55DDB67426804203970396961B13603AFE4EA356406141C
            C455298EF915D6124EAA14877279D96E1407990AA55CFABD169F1C42ECA3DC1E
            BBD31DB03BF2DEA431C71A697292C3451ABE601C2604C96E70A5A8521CD0B045
            6CA238C846E435EC46710856D6D848013C0C35C2F5A62E6C4B9A88354E1A32BE
            08772AE5DF0B2C541C8A4ECA6EA2380467002FB127C541F261812B2B912FC322
            C58EB34F17876BAA8FDD17248D0BE462CB49039332B65026D741A7C73DC79543
            D52A4E6C52762BC5A1E8D86A4F8A43CE592BF14BAEDEE2985CBE428DEF9469B4
            B339FC792C1CAEEE48C49A79DB8A34DC8F25EB5C333560AD57236A15874289C3
            868A6332B08842C5A98AC7871B68517128A4AC521CC7D2B461DCDB93946F5D91
            E564AECBE464AE4EDB8B0069B27CC68C49DD2F974B37D2BCE30B3695CDE3D7F1
            1FFC5FA6745A0D74C19BC3962B0EF95B6A5BC5C193C709039EE49C2A7E59CB9A
            903B3B6AD1E713B4F95D7F8197B6439834F660F9ACF0E0D6B6501C722E966FAB
            38E4CFCB76A938D4377E6A37BCF659035E444FA54E92C67E2C079147B6B691E2
            B81287CD15879CC1B857C5F1D6338C5AB94172E6AAAD8C8BE8233A25A4C92C4C
            87260EE7628D14873C63AB1114C735E7BD63C5A1DCD6A95A7640985FE615343D
            D81D187BBB50ABD14D2532B9E5BA9DE2E0151F417190C227EC5B71A8E2A0C722
            AAB6FFCFE19A146C7A2BE88E48A366FD4A7EF1B1A9E280BBEC06511CEABEDDA9
            103EC511148650BBE9DB46C3DD306A75A7EA50E89AA9AE756E1D55CAD8547108
            4B1CC6511C9310C1411447900CA1A64B45E464B0DBE5358393360777FB4C4B66
            6687AD6553D9E0127B5A2B0E0189C3508A8364A387521C0157F87ABDCA6E17E4
            79D9FF2327369B56CC95E374BEF95C36B0306F2ED298B1AAE86D672AFD95C5DA
            A3F9F4F7AE35E5A5255BD4F4FE003F7923F377CA03253D876C46571EC0B34B9D
            DE424C150F2ACE04A658F6ADC997F6646318DD5C41B6B749AD3A5449E03AADB7
            E1D75D24A4993A56BDCD49D3ED14B0FA4CFF2BFF24822806EFF7E8FF89CEC82A
            CE68C0178958743BEF7F3B71E2C4D11487E034679C8F499D38F17A7040C52160
            9FC51E4FA89F3871001C5371483D57D5623FCB891327101C567168B6E022F39D
            B2E233C88CEC8913AF1347569C13274E8C8653714E9C38D10FFF001B1B8D6A0D
            4EA51B0000000049454E44AE426082}
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Architecture & Objects')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 215.433210000000000000
        Width = 680.315400000000000000
        DataSet = frxInvoiceList
        DataSetName = 'frxInvoiceList'
        RowCount = 0
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          DataField = 'INVOICENUMBER'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxInvoiceList."INVOICENUMBER"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 64.252010000000000000
          Height = 15.118120000000000000
          DataField = 'INVOICEDATE'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxInvoiceList."INVOICEDATE"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 177.637910000000000000
          Height = 15.118120000000000000
          DataField = 'FullName'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxInvoiceList."FullName"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          DataField = 'VATAMOUNT'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceList."VATAMOUNT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataField = 'TOTALAMOUNT'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceList."TOTALAMOUNT"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 173.858380000000000000
          Height = 15.118120000000000000
          DataField = 'PROJECTNAME'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxInvoiceList."PROJECTNAME"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          DataField = 'AMOUNT'
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxInvoiceList."AMOUNT"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 170.078850000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = 5455915
          Frame.Color = 5455915
          Frame.Typ = []
        end
        object LblDateCredit: TfrxMemoView
          AllowVectorExport = True
          Width = 37.795300000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Nr.')
          ParentFont = False
        end
        object LblCustomer: TfrxMemoView
          AllowVectorExport = True
          Left = 41.574830000000000000
          Width = 60.472480000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Datum')
          ParentFont = False
        end
        object LblLocation: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Klant')
          ParentFont = False
        end
        object LblCreditAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Btw')
          ParentFont = False
        end
        object LblCommissionAmount: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Totaal')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          HAlign = haRight
          Memo.UTF8W = (
            'Basis')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = 5455915
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Fill.BackColor = 5455915
          Memo.UTF8W = (
            'Project')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 34.015770000000000000
        Top = 351.496290000000000000
        Width = 680.315400000000000000
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 3.779530000000000000
          Width = 317.480520000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          Memo.UTF8W = (
            'Coconne')
          ParentFont = False
        end
        object SysMemo1: TfrxSysMemoView
          AllowVectorExport = True
          Left = 597.165740000000000000
          Top = 3.779530000000020000
          Width = 83.149660000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Color = clSilver
          Frame.Color = clSilver
          Frame.Typ = [ftTop]
        end
        object SysMemo2: TfrxSysMemoView
          AllowVectorExport = True
          Top = 3.779530000000020000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATE]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 540.472790000000000000
          Top = 15.118120000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxInvoiceList."VATAMOUNT">,MasterData2)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 612.283860000000000000
          Top = 15.118120000000000000
          Width = 68.031540000000000000
          Height = 15.118120000000000000
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxInvoiceList."TOTALAMOUNT">,MasterData2)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 468.661720000000000000
          Top = 15.118120000000000000
          Width = 68.031496060000000000
          Height = 15.118120000000000000
          DataSet = frxInvoiceList
          DataSetName = 'frxInvoiceList'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxInvoiceList."AMOUNT">,MasterData2)]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 362.834880000000000000
          Top = 15.118120000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Totalen:')
          ParentFont = False
        end
      end
    end
  end
  object InvoiceList: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO ZIPCODES'
      '  (ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY)'
      'VALUES'
      '  (:ID, :COMPANYID, :COUNTRYCODE, :ZIPCODE, :CITY)'
      'RETURNING '
      '  ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY')
    SQLDelete.Strings = (
      'DELETE FROM ZIPCODES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE ZIPCODES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, COUNTRYCODE = :COUNTRYCODE, ' +
        'ZIPCODE = :ZIPCODE, CITY = :CITY'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY FROM ZIPCODES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM ZIPCODES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM ZIPCODES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  INVOICES.ID,'
      '  INVOICES.INVOICENUMBER,'
      '  INVOICES.INVOICEDATE,'
      '  RELATIONS.COMPANYNAME,'
      '  RELATIONS.NAME,'
      '  RELATIONS.FIRSTNAME,'
      '  PROJECTS.NAME AS PROJECTNAME,'
      '  INVOICES.AMOUNT,'
      '  INVOICES.VATAMOUNT,'
      '  INVOICES.TOTALAMOUNT'
      'FROM'
      '  INVOICES'
      
        '  LEFT OUTER JOIN RELATIONS ON (INVOICES.RELATION = RELATIONS.ID' +
        ')'
      '  LEFT OUTER JOIN PROJECTS ON (INVOICES.PROJECT = PROJECTS.ID)'
      'ORDER BY INVOICES.INVOICENUMBER')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = InvoiceListCalcFields
    Left = 760
    Top = 160
    object InvoiceListID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InvoiceListINVOICENUMBER: TIntegerField
      FieldName = 'INVOICENUMBER'
      Required = True
    end
    object InvoiceListINVOICEDATE: TDateField
      FieldName = 'INVOICEDATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object InvoiceListCOMPANYNAME: TWideStringField
      FieldName = 'COMPANYNAME'
      ReadOnly = True
      Size = 50
    end
    object InvoiceListNAME: TWideStringField
      FieldName = 'NAME'
      ReadOnly = True
      Size = 25
    end
    object InvoiceListFIRSTNAME: TWideStringField
      FieldName = 'FIRSTNAME'
      ReadOnly = True
      Size = 25
    end
    object InvoiceListPROJECTNAME: TWideStringField
      FieldName = 'PROJECTNAME'
      ReadOnly = True
      Size = 50
    end
    object InvoiceListAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object InvoiceListVATAMOUNT: TFloatField
      FieldName = 'VATAMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object InvoiceListTOTALAMOUNT: TFloatField
      FieldName = 'TOTALAMOUNT'
      DisplayFormat = '#,##0.00'
    end
    object InvoiceListFullName: TStringField
      FieldKind = fkCalculated
      FieldName = 'FullName'
      Size = 110
      Calculated = True
    end
  end
  object DsInvoiceList: TDataSource
    DataSet = InvoiceList
    Left = 760
    Top = 208
  end
  object frxInvoiceList: TfrxDBDataset
    UserName = 'frxInvoiceList'
    CloseDataSource = False
    DataSource = DsInvoiceList
    BCDToCurrency = False
    Left = 760
    Top = 265
  end
  object Offer: TfrxReport
    Version = '6.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      'procedure LblYourPriceOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if <frxObjectLines."AMOUNT"> = <frxObjectLines."CUSTOMERAMOUNT' +
        '">'
      
        '  then LblYourPrice.Visible := False;                           ' +
        '                                                  '
      'end;'
      ''
      'procedure LblValueYourPriceOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if <frxObjectLines."AMOUNT"> = <frxObjectLines."CUSTOMERAMOUNT' +
        '">'
      '  then LblValueYourPrice.Visible := False;  '
      'end;'
      ''
      'procedure LblYourPrice2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  if <frxObjectLines."AMOUNT"> = <frxObjectLines."CUSTOMERAMOUNT' +
        '">'
      '  then LblYourPrice2.Visible := False;  '
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 984
    Top = 88
    Datasets = <
      item
        DataSet = frsInvoices
        DataSetName = 'frsInvoices'
      end
      item
        DataSet = frxObjectLines
        DataSetName = 'frxObjectLines'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 453.543600000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Width = 261.834570000000000000
          Height = 25.984230000000000000
          Frame.Typ = []
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000017B00
            00003C080200000008768D3C000000017352474200AECE1CE90000000467414D
            410000B18F0BFC61050000000970485973000016250000162501495224F00000
            0B794944415478DAED5D6D96D4380C549F84E124C049184EC27012E0244C9F04
            3809ABD75AB2D99E442A7DD87132A91FFB661F69472E4B65F933973F7FFED089
            13274E74C1A550717EDD207F3C3C3CF01F0F376C55B75F33CCEDD9D0A4352369
            18D2D68CA4199F62240DC9E789C191521C76BE6FDFBE3D3F3F5FAFD7B567C423
            DFBC79F3FEFDFBC7C7C70E5512ABBE7CF9A298F4F1E3C7F73774B067D13C668C
            795B7B86E97AFBF62DDBF9EEDDBB3EA429A67EFFFE5D54660DD2C46C6A0B4AD9
            00FDEDCC4F5EF29E9E9E947F75D54B2F0A79C0C4F30D259C98F45661E230A238
            4C991E306B9842A851A8B3499F3E7DC219647BBE7EFDDA4777C2A4B1FA7CF8F0
            4124B2839D0448B60251F37C504DE0066563F437FEFCF933F916D677C56DD863
            D94F9072B8102E4A7FE6F3E7CF497E9810A64579E0C78F1FA0B798F45661AAB5
            4F71F837668F87809B902D28CCC6D924E62E10CF0C89907643832AD25ADB4939
            AD998355929BB844779090C015610D3D1527DFCFBD0AC511472C4CC00A3B4316
            1ACE0292C670239507B337E7325118C92FC1C5E6B5660EE693FD2C392A044382
            6338F3A29E8A43E9BCECF88AD3CEAC7CA857C549FFE140C6D45A7DCC64882692
            830890C624279D15C755E64B1C59719AFAA220E32BB5DD72D5B44E07D238D9E1
            F24B44279F219AC834311E1299C4A1BFE250222F3BACE274F04541CC235B9897
            8FE4C149BB83E9BB9B5BEB0A89703EB589E2843939A6E2748B1C8137D45D4DEB
            42A6ABEC4C5A5274066F62812B24C259EA268A4351673BA0E2B48B6705CC1133
            053ECCA1620E5BB839B94CD90A2495BA5EAF08BFB1AE727CD2E6E82C37824080
            7943829B3B3055BF95E250C8D90EA838483CCFC1D5930D604CB73439E3F7EFDF
            EC5EAE8561907D335A946960640138963B78491323F957CC18FF2D740975AE9D
            3B01970D74C5777B26A54DD96626CA65AD77C63410128149D90D15279097F554
            1CF62E5775D6A0ED00C423075CDF917DAB489920FBBA85FC737E2387B1520287
            0A3F56E2649B93C635E5875D2E0B5A8BAFDF31936C2D287C2E898C75C25E15DE
            5071C89F97F5549CF27397F78A8327DBDE460527299161C2E572C9FC5CC0A2A3
            6431AEFC1F27CD2B6460C9AEB115B8C0174B9D90453A57AF1E531C6F96BAADE2
            B85E4107531CB0F78B2DEC717B70F96B4D8B77FE0ADD38D7854535254D4FC702
            DB8814BD0ED4FD0EE0AE651E8383296178A2C1D5676CAE38E4718FE3280E27C6
            489DC3EE482BF1E30A1B25BC03DDB2C238581A98BB65485B54EAD89645E49852
            7E53122B8E69184848666A13F787111407CFCB8EA3383AEF82E47672C6F57A9D
            E808848D6224F769859B7AC07E18212D7F786FEECAE1EDD1483C6494710E73EC
            06E62019C5C167B846501C9C9383280ED25787D762EF20871B6361A30C0A02EC
            282E82CCE7F5248D03985F97398A61BA5795DC084CD1415E975CBE0513874114
            87B0CEE9208A83346D208958C474FB54E0876B2DCAA11873CD356F4314A72769
            3C20D517E0C23515E44F60DF419FB603DF98DF3082BC651CC541F2B283288E39
            3A28F7C80094415078E4A24C0C9974EF823481E9A62DEE9FD55F8A0C22F49090
            01A639516DFA4637C5E1A2D8D9CC1BCE745A8EA038C82A6C555F9D81D2A2F8F2
            C71D146F33E936D77D46204DA0FB567EA669119C97498CAD3D60C686A9385C82
            B9246F260EDD1487796633CC58D3DF7804C53187DC83F4D5C96997650A5654C3
            2CD05CDA0B0FF45A400FAA76CAA88787A974A6E2B0E5FAD62A81DE9A3D15E7E9
            864C5E7604C5315F9C5FA22AB3B874E658693C3369DA1169A4F2D65419753930
            A7D511C5216C0A5F118ECE8A836C95540EBEF6541C8E02FAEB3C1C5F93172DFE
            2D01B8F8F054D97F15C7DCC336CEE840313510E10AE3661C9AAD350E697A24B4
            5646A5C9AA14C77C52B0168A9D15872C2116ACE5657B3CC9396503FF2A8E3903
            3ACE67AD948CD47B1F823E7B65369B49DA388A93AC69128A5B9BB3A4B8E2984B
            63B4BE58DE5F7108CBCB16C756A7E27485DEA8AECD2F7AADF30B55E390A6FB68
            6B65D4A72D749670C5216C016471B0BC89E298B5A3952DE047501C7DCDA5E403
            1D85D0C780C8B28B3990469C6C47A4E931BFADE2E86F77290E61072C5E7AC856
            8A03E6657775DCBDE298340D153CF4FF73128BD05743916C1609C2C3284EEB5C
            2C93617915C75C8FA7A5C4612BC5212C2FBB3360F78A43BB0A1E8139D53DBF00
            50CE8E32F827C80D61A087ED88B45732AA12801F8D9AFF7043C5217F5E7604C5
            D9D19484A0E9A59960049AAA370E69C3CE1C5303C521FF62F9B68A03E665D39C
            F711146747ABE313CABFE82628BC16671CD286559CC2B5AA3B2097164D15DF56
            71901F9267F3D11114A7B553C6502E3AAE9D299D8F6267A03B74F880080865F8
            69067358719049D9693BC5E68A439EC5F29E8A238E31CDBDFC271C4B7FDFED03
            9C3F203B09093FE55075E5422D5CD7EB9AF09E2D32496B1DC92E28611F3E2082
            403F0BD24E71085861A0BF8E3D82E210363BC996C84AABF2D80E4E39205F4718
            671EF40E25994E60DF2DB264D62E92BDD0BDB95D3AA6FB74FE24A7EE96E00926
            7D3DA19BE280791997B07BC5418691439D12BA835CF1150BEFF03D9BC846F571
            48D33B9546E268F664A64327150739C1C485545D3992541CC2F232D3E01D280E
            6119DDB0690EDDE29FB9730D8BC2D7774ED811696630B410479D9FFC8D5C08BD
            F95BB27A2A0E55E4ECFB509CF0298F009E6F687121CBB4E9461702F9A45FDE00
            64917E1CD2F4F82FF990B98B1C44E3F28A43E9CFAB77561CF0233C0AF6A13848
            5710F818DB2264A28E4B7BBCA1C512B2488FFCC1E98F184FB36F0356BD0559D4
            CC7F1D61224DF2B2186966E0156664881623DE5CA238E4FF62EA1C9D15072947
            C73E1407793D5538E55DDB67426804203970396961B13603AFE4EA356406141C
            C455298EF915D6124EAA14877279D96E1407990AA55CFABD169F1C42ECA3DC1E
            BBD31DB03BF2DEA431C71A697292C3451ABE601C2604C96E70A5A8521CD0B045
            6CA238C846E435EC46710856D6D848013C0C35C2F5A62E6C4B9A88354E1A32BE
            08772AE5DF0B2C541C8A4ECA6EA2380467002FB127C541F261812B2B912FC322
            C58EB34F17876BAA8FDD17248D0BE462CB49039332B65026D741A7C73DC79543
            D52A4E6C52762BC5A1E8D86A4F8A43CE592BF14BAEDEE2985CBE428DEF9469B4
            B339FC792C1CAEEE48C49A79DB8A34DC8F25EB5C333560AD57236A15874289C3
            868A6332B08842C5A98AC7871B68517128A4AC521CC7D2B461DCDB93946F5D91
            E564AECBE464AE4EDB8B0069B27CC68C49DD2F974B37D2BCE30B3695CDE3D7F1
            1FFC5FA6745A0D74C19BC3962B0EF95B6A5BC5C193C709039EE49C2A7E59CB9A
            903B3B6AD1E713B4F95D7F8197B6439834F660F9ACF0E0D6B6501C722E966FAB
            38E4CFCB76A938D4377E6A37BCF659035E444FA54E92C67E2C079147B6B691E2
            B81287CD15879CC1B857C5F1D6338C5AB94172E6AAAD8C8BE8233A25A4C92C4C
            87260EE7628D14873C63AB1114C735E7BD63C5A1DCD6A95A7640985FE615343D
            D81D187BBB50ABD14D2532B9E5BA9DE2E0151F417190C227EC5B71A8E2A0C722
            AAB6FFCFE19A146C7A2BE88E48A366FD4A7EF1B1A9E280BBEC06511CEABEDDA9
            103EC511148650BBE9DB46C3DD306A75A7EA50E89AA9AE756E1D55CAD8547108
            4B1CC6511C9310C1411447900CA1A64B45E464B0DBE5358393360777FB4C4B66
            6687AD6553D9E0127B5A2B0E0189C3508A8364A387521C0157F87ABDCA6E17E4
            79D9FF2327369B56CC95E374BEF95C36B0306F2ED298B1AAE86D672AFD95C5DA
            A3F9F4F7AE35E5A5255BD4F4FE003F7923F377CA03253D876C46571EC0B34B9D
            DE424C150F2ACE04A658F6ADC997F6646318DD5C41B6B749AD3A5449E03AADB7
            E1D75D24A4993A56BDCD49D3ED14B0FA4CFF2BFF24822806EFF7E8FF89CEC82A
            CE68C0178958743BEF7F3B71E2C4D11487E034679C8F499D38F17A7040C52160
            9FC51E4FA89F3871001C5371483D57D5623FCB891327101C567168B6E022F39D
            B2E233C88CEC8913AF1347569C13274E8C8653714E9C38D10FFF001B1B8D6A0D
            4EA51B0000000049454E44AE426082}
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Architecture & Objects')
          ParentFont = False
        end
        object LblAddress1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAddress1')
        end
        object LblAddress2: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAddress2')
        end
        object LblTelephone: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 102.047310000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblTelephone')
        end
        object LblEmail: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblEmail')
        end
        object LblWeb: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 139.842610000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblWeb')
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Tel:')
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'E-mail:')
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 139.842610000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Web:')
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 166.299320000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 374.173470000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Beste,')
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Top = 400.630180000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Met dit schrijven zijn wij zo vrij een offerte op te stellen voo' +
              'r het door U gevraagde:')
        end
        object LblCustomerName: TfrxMemoView
          AllowVectorExport = True
          Top = 185.196970000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerName')
        end
        object LblCustomerAddress1: TfrxMemoView
          AllowVectorExport = True
          Top = 222.992270000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerAddress1')
        end
        object LblCustomerAddress2: TfrxMemoView
          AllowVectorExport = True
          Top = 241.889920000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerAddress2')
        end
        object LblCustomerVatNumber: TfrxMemoView
          AllowVectorExport = True
          Top = 268.346630000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerVatNumber')
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 298.582870000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Frame.Typ = []
        end
        object LblInvoiceType: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 301.228353780000000000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'OFFERTE')
        end
        object OfferNumber: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 301.228353780000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'OfferNumber')
          ParentFont = False
        end
        object LblDocDate: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 328.819110000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblDate')
          ParentFont = False
        end
        object LblAppelation: TfrxMemoView
          AllowVectorExport = True
          Top = 204.094620000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAppelation')
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 427.086890000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 427.086890000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            'Gekozen object:')
          ParentFont = False
        end
        object LblEndDate: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 347.716760000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblDate')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Top = 328.819110000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Doc-datum:')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 347.716760000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Verval-datum:')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 86.929190000000000000
        Top = 578.268090000000000000
        Width = 680.315400000000000000
        Child = Offer.ChildArticle
        DataSet = frxObjectLines
        DataSetName = 'frxObjectLines'
        RowCount = 0
        Stretched = True
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Width = 514.016080000000000000
          Height = 15.118120000000000000
          StretchMode = smActualHeight
          DataField = 'DESCRIPTION'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."DESCRIPTION"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 22.677180000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataField = 'QUANTITY'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."QUANTITY"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Aantal:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Verkoopprijs particulier:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataField = 'AMOUNT'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."AMOUNT"]')
          ParentFont = False
        end
        object LblYourPrice: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'LblYourPriceOnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Uw prijs:')
          ParentFont = False
        end
        object LblValueYourPrice: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'LblValueYourPriceOnBeforePrint'
          DataField = 'CUSTOMERAMOUNT'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."CUSTOMERAMOUNT"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 68.031540000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Uw totaalprijs')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 68.031540000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            
              '[<frxObjectLines."CUSTOMERAMOUNT"> * <frxObjectLines."QUANTITY">' +
              ']')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 37.795300000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
        object LblYourPrice2: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          OnBeforePrint = 'LblYourPrice2OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 68.031540000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
      end
      object ChildArticle: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 687.874460000000000000
        Width = 680.315400000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 532.913730000000000000
        Width = 680.315400000000000000
        Condition = 'frsInvoices."ID"'
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 147.401670000000000000
        Top = 733.228820000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 680.315400000000000000
          Height = 52.913420000000000000
          Frame.Typ = []
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 34.015770000000000000
          Width = 154.960730000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '- Offerte-prijs particulier:')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 34.015770000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[SUM(<frxObjectLines."TOTALLINE">,MasterData2)]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 49.133890000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[SUM(<frxObjectLines."TOTALLINEVAT">,MasterData2)]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 64.252010000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[SUM(<frxObjectLines."GENERALTOTALLINE">,MasterData2)]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 34.015770000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 49.133890000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'btw')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 64.252010000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/incl btw')
          ParentFont = False
        end
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Top = 90.708720000000000000
          Width = 680.315400000000000000
          Height = 52.913420000000000000
          Frame.Typ = []
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 94.488250000000000000
          Width = 154.960730000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '- Uw offerte-prijs:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 94.488250000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[SUM(<frxObjectLines."TOTALLINECUSTOMER">,MasterData2)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 109.606370000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[SUM(<frxObjectLines."TOTALLINEVATCUSTOMER">,MasterData2)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[SUM(<frxObjectLines."GENERALTOTALLINECUSTOMER">,MasterData2)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 109.606370000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'btw')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 124.724490000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/incl btw')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            'Totaalprijs offerte')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 430.866420000000000000
        Width = 680.315400000000000000
        object LblBank11: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank11')
          ParentFont = False
        end
        object LblBank12: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank12')
          ParentFont = False
        end
        object LblBank13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 56.692950000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank13')
          ParentFont = False
        end
        object LblVat: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblVat')
          ParentFont = False
        end
        object LblBank21: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 18.897650000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank21')
          ParentFont = False
        end
        object LblBank22: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 37.795300000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank22')
          ParentFont = False
        end
        object LblBank23: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 56.692950000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank23')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 389.291590000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 200.315090000000000000
          Width = 680.315400000000000000
          Height = 71.811070000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Bedankt voor uw interesse!'
            
              'Gelieve uw definitieve beslissing per mail aan ons te bevestigen' +
              ' op: info@coconne.be '#233'n steeds het offerte-nummer te vermelden. ' +
              'De bestelling gaat pas in na ontvangst van de gehandtekende vers' +
              'ie van de offerte & betaling van 50% van uw bestelling.')
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Top = 283.464750000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Bel:')
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Top = 302.362400000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Mail:')
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 283.464750000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            '051/50 51 00')
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 302.362400000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'info@coconne.be')
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Top = 336.378170000000000000
          Width = 680.315400000000000000
          Height = 49.133890000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Uw contact:'
            'Charlotte Pattyn Architect / zaakvoerder')
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 132.283550000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Levertermijn')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 151.181200000000000000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Wij trachten een levertermijn te hanteren van 8 weken na betalin' +
              'g voorschot.')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 124.724490000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 7.559060000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Betalingsvoorwaarden:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 26.456710000000000000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '50% bij bestelling - 50% bij vertrek uit ons depot')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 45.354360000000000000
          Width = 593.386210000000000000
          Height = 68.031540000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            
              '(Alle fakturen zijn betaalbaar op vervaldatum. In geval van laat' +
              'tijdige betaling wordt het faktuurbedrag van rechtswege & zonder' +
              ' ingebrekestelling verhoogd met een conventionele schadevergoedi' +
              'ng van 10%, te vermeerderen met de vertragingsintresten van 1% p' +
              'er maand.'
            
              'In geval van betwisting zijn uitsluitend de rechtenbanken van Ko' +
              'rtrijk bevoegd.)')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
      end
    end
  end
  object ObjectLines: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_OBJECTLINES_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO OBJECTLINES'
      
        '  (ID, COMPANYID, OBJECT, QUANTITY, AMOUNT, VATAMOUNT, TOTALAMOU' +
        'NT, CUSTOMERAMOUNT, CUSTOMERVATAMOUNT, CUSTOMERTOTALAMOUNT, DESC' +
        'RIPTION)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :OBJECT, :QUANTITY, :AMOUNT, :VATAMOUNT, :TO' +
        'TALAMOUNT, :CUSTOMERAMOUNT, :CUSTOMERVATAMOUNT, :CUSTOMERTOTALAM' +
        'OUNT, :DESCRIPTION)'
      'RETURNING '
      
        '  ID, COMPANYID, OBJECT, QUANTITY, AMOUNT, VATAMOUNT, TOTALAMOUN' +
        'T, CUSTOMERAMOUNT, CUSTOMERVATAMOUNT, CUSTOMERTOTALAMOUNT')
    SQLDelete.Strings = (
      'DELETE FROM OBJECTLINES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE OBJECTLINES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, OBJECT = :OBJECT, QUANTITY =' +
        ' :QUANTITY, AMOUNT = :AMOUNT, VATAMOUNT = :VATAMOUNT, TOTALAMOUN' +
        'T = :TOTALAMOUNT, CUSTOMERAMOUNT = :CUSTOMERAMOUNT, CUSTOMERVATA' +
        'MOUNT = :CUSTOMERVATAMOUNT, CUSTOMERTOTALAMOUNT = :CUSTOMERTOTAL' +
        'AMOUNT, DESCRIPTION = :DESCRIPTION'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, OBJECT, QUANTITY, AMOUNT, VATAMOUNT, TOTAL' +
        'AMOUNT, CUSTOMERAMOUNT, CUSTOMERVATAMOUNT, CUSTOMERTOTALAMOUNT, ' +
        'DESCRIPTION FROM OBJECTLINES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM OBJECTLINES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM OBJECTLINES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from objectlines')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = ObjectLinesCalcFields
    Left = 984
    Top = 160
    object ObjectLinesID: TLargeintField
      FieldName = 'ID'
    end
    object ObjectLinesCOMPANYID: TLargeintField
      FieldName = 'COMPANYID'
      Required = True
    end
    object ObjectLinesOBJECT: TLargeintField
      FieldName = 'OBJECT'
      Required = True
    end
    object ObjectLinesQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Required = True
    end
    object ObjectLinesAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ObjectLinesVATAMOUNT: TFloatField
      FieldName = 'VATAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ObjectLinesTOTALAMOUNT: TFloatField
      FieldName = 'TOTALAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ObjectLinesDESCRIPTION: TWideMemoField
      FieldName = 'DESCRIPTION'
      BlobType = ftWideMemo
    end
    object ObjectLinesCUSTOMERAMOUNT: TFloatField
      FieldName = 'CUSTOMERAMOUNT'
    end
    object ObjectLinesCUSTOMERVATAMOUNT: TFloatField
      FieldName = 'CUSTOMERVATAMOUNT'
    end
    object ObjectLinesCUSTOMERTOTALAMOUNT: TFloatField
      FieldName = 'CUSTOMERTOTALAMOUNT'
    end
    object ObjectLinesTOTALLINECUSTOMER: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'TOTALLINECUSTOMER'
      Calculated = True
    end
    object ObjectLinesTOTALLINEVATCUSTOMER: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'TOTALLINEVATCUSTOMER'
      Calculated = True
    end
    object ObjectLinesGENERALTOTALLINECUSTOMER: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'GENERALTOTALLINECUSTOMER'
      Calculated = True
    end
    object ObjectLinesTOTALLINE: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'TOTALLINE'
      Calculated = True
    end
    object ObjectLinesTOTALLINEVAT: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'TOTALLINEVAT'
      Calculated = True
    end
    object ObjectLinesGENERALTOTALLINE: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'GENERALTOTALLINE'
      Calculated = True
    end
  end
  object DsObjectLines: TDataSource
    DataSet = ObjectLines
    Left = 984
    Top = 208
  end
  object frxObjectLines: TfrxDBDataset
    UserName = 'frxObjectLines'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ID=ID'
      'COMPANYID=COMPANYID'
      'OBJECT=OBJECT'
      'QUANTITY=QUANTITY'
      'AMOUNT=AMOUNT'
      'VATAMOUNT=VATAMOUNT'
      'TOTALAMOUNT=TOTALAMOUNT'
      'DESCRIPTION=DESCRIPTION'
      'CUSTOMERAMOUNT=CUSTOMERAMOUNT'
      'CUSTOMERVATAMOUNT=CUSTOMERVATAMOUNT'
      'CUSTOMERTOTALAMOUNT=CUSTOMERTOTALAMOUNT'
      'TOTALLINECUSTOMER=TOTALLINECUSTOMER'
      'TOTALLINEVATCUSTOMER=TOTALLINEVATCUSTOMER'
      'GENERALTOTALLINECUSTOMER=GENERALTOTALLINECUSTOMER'
      'TOTALLINE=TOTALLINE'
      'TOTALLINEVAT=TOTALLINEVAT'
      'GENERALTOTALLINE=GENERALTOTALLINE')
    DataSource = DsObjectLines
    BCDToCurrency = False
    Left = 984
    Top = 264
  end
  object Purchases: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO INVOICES'
      
        '  (ID, COMPANYID, RELATION, INVOICENUMBER, INVOICEDATE, PAYMENTT' +
        'ERM, DUEDATE, AMOUNT, VATAMOUNT, TOTALAMOUNT, PROJECT, TOTALPAYE' +
        'D, INVOICETYPE, SPLITINVOICE, INVOICESTAGE)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :RELATION, :INVOICENUMBER, :INVOICEDATE, :PA' +
        'YMENTTERM, :DUEDATE, :AMOUNT, :VATAMOUNT, :TOTALAMOUNT, :PROJECT' +
        ', :TOTALPAYED, :INVOICETYPE, :SPLITINVOICE, :INVOICESTAGE)'
      'RETURNING '
      
        '  ID, COMPANYID, RELATION, INVOICENUMBER, INVOICEDATE, PAYMENTTE' +
        'RM, DUEDATE, AMOUNT, VATAMOUNT, TOTALAMOUNT, PROJECT, TOTALPAYED' +
        ', INVOICETYPE, SPLITINVOICE, INVOICESTAGE')
    SQLDelete.Strings = (
      'DELETE FROM INVOICES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE INVOICES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, RELATION = :RELATION, INVOIC' +
        'ENUMBER = :INVOICENUMBER, INVOICEDATE = :INVOICEDATE, PAYMENTTER' +
        'M = :PAYMENTTERM, DUEDATE = :DUEDATE, AMOUNT = :AMOUNT, VATAMOUN' +
        'T = :VATAMOUNT, TOTALAMOUNT = :TOTALAMOUNT, PROJECT = :PROJECT, ' +
        'TOTALPAYED = :TOTALPAYED, INVOICETYPE = :INVOICETYPE, SPLITINVOI' +
        'CE = :SPLITINVOICE, INVOICESTAGE = :INVOICESTAGE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, RELATION, INVOICENUMBER, INVOICEDATE, PAYM' +
        'ENTTERM, DUEDATE, AMOUNT, VATAMOUNT, TOTALAMOUNT, PROJECT, TOTAL' +
        'PAYED, INVOICETYPE, SPLITINVOICE, INVOICESTAGE FROM INVOICES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM INVOICES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM INVOICES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from projectpurchases')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 456
    Top = 608
  end
  object DsPurchases: TDataSource
    DataSet = Purchases
    Left = 456
    Top = 656
  end
  object frxPurhcases: TfrxDBDataset
    UserName = 'frxPurchases'
    CloseDataSource = False
    DataSource = DsPurchases
    BCDToCurrency = False
    Left = 456
    Top = 712
  end
  object InvoiceObjects: TfrxReport
    Version = '6.3'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42333.649778981500000000
    ReportOptions.LastChange = 42770.852921539300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      ''
      ''
      'procedure LblYourPrice2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      ''
      'end.')
    Left = 984
    Top = 352
    Datasets = <
      item
        DataSet = frsInvoices
        DataSetName = 'frsInvoices'
      end
      item
        DataSet = frxObjectLines
        DataSetName = 'frxObjectLines'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Frame.Typ = []
        Height = 476.220780000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Width = 261.834570000000000000
          Height = 25.984230000000000000
          Frame.Typ = []
          Picture.Data = {
            0A54504E474F626A65637489504E470D0A1A0A0000000D494844520000017B00
            00003C080200000008768D3C000000017352474200AECE1CE90000000467414D
            410000B18F0BFC61050000000970485973000016250000162501495224F00000
            0B794944415478DAED5D6D96D4380C549F84E124C049184EC27012E0244C9F04
            3809ABD75AB2D99E442A7DD87132A91FFB661F69472E4B65F933973F7FFED089
            13274E74C1A550717EDD207F3C3C3CF01F0F376C55B75F33CCEDD9D0A4352369
            18D2D68CA4199F62240DC9E789C191521C76BE6FDFBE3D3F3F5FAFD7B567C423
            DFBC79F3FEFDFBC7C7C70E5512ABBE7CF9A298F4F1E3C7F73774B067D13C668C
            795B7B86E97AFBF62DDBF9EEDDBB3EA429A67EFFFE5D54660DD2C46C6A0B4AD9
            00FDEDCC4F5EF29E9E9E947F75D54B2F0A79C0C4F30D259C98F45661E230A238
            4C991E306B9842A851A8B3499F3E7DC219647BBE7EFDDA4777C2A4B1FA7CF8F0
            4124B2839D0448B60251F37C504DE0066563F437FEFCF933F916D677C56DD863
            D94F9072B8102E4A7FE6F3E7CF497E9810A64579E0C78F1FA0B798F45661AAB5
            4F71F837668F87809B902D28CCC6D924E62E10CF0C89907643832AD25ADB4939
            AD998355929BB844779090C015610D3D1527DFCFBD0AC511472C4CC00A3B4316
            1ACE0292C670239507B337E7325118C92FC1C5E6B5660EE693FD2C392A044382
            6338F3A29E8A43E9BCECF88AD3CEAC7CA857C549FFE140C6D45A7DCC64882692
            830890C624279D15C755E64B1C59719AFAA220E32BB5DD72D5B44E07D238D9E1
            F24B44279F219AC834311E1299C4A1BFE250222F3BACE274F04541CC235B9897
            8FE4C149BB83E9BB9B5BEB0A89703EB589E2843939A6E2748B1C8137D45D4DEB
            42A6ABEC4C5A5274066F62812B24C259EA268A4351673BA0E2B48B6705CC1133
            053ECCA1620E5BB839B94CD90A2495BA5EAF08BFB1AE727CD2E6E82C37824080
            7943829B3B3055BF95E250C8D90EA838483CCFC1D5930D604CB73439E3F7EFDF
            EC5EAE8561907D335A946960640138963B78491323F957CC18FF2D740975AE9D
            3B01970D74C5777B26A54DD96626CA65AD77C63410128149D90D15279097F554
            1CF62E5775D6A0ED00C423075CDF917DAB489920FBBA85FC737E2387B1520287
            0A3F56E2649B93C635E5875D2E0B5A8BAFDF31936C2D287C2E898C75C25E15DE
            5071C89F97F5549CF27397F78A8327DBDE460527299161C2E572C9FC5CC0A2A3
            6431AEFC1F27CD2B6460C9AEB115B8C0174B9D90453A57AF1E531C6F96BAADE2
            B85E4107531CB0F78B2DEC717B70F96B4D8B77FE0ADD38D7854535254D4FC702
            DB8814BD0ED4FD0EE0AE651E8383296178A2C1D5676CAE38E4718FE3280E27C6
            489DC3EE482BF1E30A1B25BC03DDB2C238581A98BB65485B54EAD89645E49852
            7E53122B8E69184848666A13F787111407CFCB8EA3383AEF82E47672C6F57A9D
            E808848D6224F769859B7AC07E18212D7F786FEECAE1EDD1483C6494710E73EC
            06E62019C5C167B846501C9C9383280ED25787D762EF20871B6361A30C0A02EC
            282E82CCE7F5248D03985F97398A61BA5795DC084CD1415E975CBE0513874114
            87B0CEE9208A83346D208958C474FB54E0876B2DCAA11873CD356F4314A72769
            3C20D517E0C23515E44F60DF419FB603DF98DF3082BC651CC541F2B283288E39
            3A28F7C80094415078E4A24C0C9974EF823481E9A62DEE9FD55F8A0C22F49090
            01A639516DFA4637C5E1A2D8D9CC1BCE745A8EA038C82A6C555F9D81D2A2F8F2
            C71D146F33E936D77D46204DA0FB567EA669119C97498CAD3D60C686A9385C82
            B9246F260EDD1487796633CC58D3DF7804C53187DC83F4D5C96997650A5654C3
            2CD05CDA0B0FF45A400FAA76CAA88787A974A6E2B0E5FAD62A81DE9A3D15E7E9
            864C5E7604C5315F9C5FA22AB3B874E658693C3369DA1169A4F2D65419753930
            A7D511C5216C0A5F118ECE8A836C95540EBEF6541C8E02FAEB3C1C5F93172DFE
            2D01B8F8F054D97F15C7DCC336CEE840313510E10AE3661C9AAD350E697A24B4
            5646A5C9AA14C77C52B0168A9D15872C2116ACE5657B3CC9396503FF2A8E3903
            3ACE67AD948CD47B1F823E7B65369B49DA388A93AC69128A5B9BB3A4B8E2984B
            63B4BE58DE5F7108CBCB16C756A7E27485DEA8AECD2F7AADF30B55E390A6FB68
            6B65D4A72D749670C5216C016471B0BC89E298B5A3952DE047501C7DCDA5E403
            1D85D0C780C8B28B3990469C6C47A4E931BFADE2E86F77290E61072C5E7AC856
            8A03E6657775DCBDE298340D153CF4FF73128BD05743916C1609C2C3284EEB5C
            2C93617915C75C8FA7A5C4612BC5212C2FBB3360F78A43BB0A1E8139D53DBF00
            50CE8E32F827C80D61A087ED88B45732AA12801F8D9AFF7043C5217F5E7604C5
            D9D19484A0E9A59960049AAA370E69C3CE1C5303C521FF62F9B68A03E665D39C
            F711146747ABE313CABFE82628BC16671CD286559CC2B5AA3B2097164D15DF56
            71901F9267F3D11114A7B553C6502E3AAE9D299D8F6267A03B74F880080865F8
            69067358719049D9693BC5E68A439EC5F29E8A238E31CDBDFC271C4B7FDFED03
            9C3F203B09093FE55075E5422D5CD7EB9AF09E2D32496B1DC92E28611F3E2082
            403F0BD24E71085861A0BF8E3D82E210363BC996C84AABF2D80E4E39205F4718
            671EF40E25994E60DF2DB264D62E92BDD0BDB95D3AA6FB74FE24A7EE96E00926
            7D3DA19BE280791997B07BC5418691439D12BA835CF1150BEFF03D9BC846F571
            48D33B9546E268F664A64327150739C1C485545D3992541CC2F232D3E01D280E
            6119DDB0690EDDE29FB9730D8BC2D7774ED811696630B410479D9FFC8D5C08BD
            F95BB27A2A0E55E4ECFB509CF0298F009E6F687121CBB4E9461702F9A45FDE00
            64917E1CD2F4F82FF990B98B1C44E3F28A43E9CFAB77561CF0233C0AF6A13848
            5710F818DB2264A28E4B7BBCA1C512B2488FFCC1E98F184FB36F0356BD0559D4
            CC7F1D61224DF2B2186966E0156664881623DE5CA238E4FF62EA1C9D15072947
            C73E1407793D5538E55DDB67426804203970396961B13603AFE4EA356406141C
            C455298EF915D6124EAA14877279D96E1407990AA55CFABD169F1C42ECA3DC1E
            BBD31DB03BF2DEA431C71A697292C3451ABE601C2604C96E70A5A8521CD0B045
            6CA238C846E435EC46710856D6D848013C0C35C2F5A62E6C4B9A88354E1A32BE
            08772AE5DF0B2C541C8A4ECA6EA2380467002FB127C541F261812B2B912FC322
            C58EB34F17876BAA8FDD17248D0BE462CB49039332B65026D741A7C73DC79543
            D52A4E6C52762BC5A1E8D86A4F8A43CE592BF14BAEDEE2985CBE428DEF9469B4
            B339FC792C1CAEEE48C49A79DB8A34DC8F25EB5C333560AD57236A15874289C3
            868A6332B08842C5A98AC7871B68517128A4AC521CC7D2B461DCDB93946F5D91
            E564AECBE464AE4EDB8B0069B27CC68C49DD2F974B37D2BCE30B3695CDE3D7F1
            1FFC5FA6745A0D74C19BC3962B0EF95B6A5BC5C193C709039EE49C2A7E59CB9A
            903B3B6AD1E713B4F95D7F8197B6439834F660F9ACF0E0D6B6501C722E966FAB
            38E4CFCB76A938D4377E6A37BCF659035E444FA54E92C67E2C079147B6B691E2
            B81287CD15879CC1B857C5F1D6338C5AB94172E6AAAD8C8BE8233A25A4C92C4C
            87260EE7628D14873C63AB1114C735E7BD63C5A1DCD6A95A7640985FE615343D
            D81D187BBB50ABD14D2532B9E5BA9DE2E0151F417190C227EC5B71A8E2A0C722
            AAB6FFCFE19A146C7A2BE88E48A366FD4A7EF1B1A9E280BBEC06511CEABEDDA9
            103EC511148650BBE9DB46C3DD306A75A7EA50E89AA9AE756E1D55CAD8547108
            4B1CC6511C9310C1411447900CA1A64B45E464B0DBE5358393360777FB4C4B66
            6687AD6553D9E0127B5A2B0E0189C3508A8364A387521C0157F87ABDCA6E17E4
            79D9FF2327369B56CC95E374BEF95C36B0306F2ED298B1AAE86D672AFD95C5DA
            A3F9F4F7AE35E5A5255BD4F4FE003F7923F377CA03253D876C46571EC0B34B9D
            DE424C150F2ACE04A658F6ADC997F6646318DD5C41B6B749AD3A5449E03AADB7
            E1D75D24A4993A56BDCD49D3ED14B0FA4CFF2BFF24822806EFF7E8FF89CEC82A
            CE68C0178958743BEF7F3B71E2C4D11487E034679C8F499D38F17A7040C52160
            9FC51E4FA89F3871001C5371483D57D5623FCB891327101C567168B6E022F39D
            B2E233C88CEC8913AF1347569C13274E8C8653714E9C38D10FFF001B1B8D6A0D
            4EA51B0000000049454E44AE426082}
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 26.456710000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Architecture & Objects')
          ParentFont = False
        end
        object LblAddress1: TfrxMemoView
          AllowVectorExport = True
          Top = 60.472480000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAddress1')
        end
        object LblAddress2: TfrxMemoView
          AllowVectorExport = True
          Top = 79.370130000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAddress2')
        end
        object LblTelephone: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 102.047310000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblTelephone')
        end
        object LblEmail: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 120.944960000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblEmail')
        end
        object LblWeb: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 139.842610000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblWeb')
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 102.047310000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Tel:')
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Top = 120.944960000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'E-mail:')
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Top = 139.842610000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Web:')
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Top = 166.299320000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Top = 393.071120000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Beste,')
        end
        object LblDescription: TfrxMemoView
          AllowVectorExport = True
          Top = 419.527830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Met dit schrijven zijn wij zo vrij een offerte op te stellen voo' +
              'r het door U gevraagde:')
        end
        object LblCustomerName: TfrxMemoView
          AllowVectorExport = True
          Top = 185.196970000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerName')
        end
        object LblCustomerAddress1: TfrxMemoView
          AllowVectorExport = True
          Top = 222.992270000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerAddress1')
        end
        object LblCustomerAddress2: TfrxMemoView
          AllowVectorExport = True
          Top = 241.889920000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerAddress2')
        end
        object LblCustomerVatNumber: TfrxMemoView
          AllowVectorExport = True
          Top = 268.346630000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblCustomerVatNumber')
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Top = 298.582870000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Frame.Typ = []
        end
        object LblInvoiceType: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 301.228353780000000000
          Width = 260.787570000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'FAKTUUR')
        end
        object InvoiceNumber: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 301.228353780000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'OfferNumber')
          ParentFont = False
        end
        object LblDocDate: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 328.819110000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblDate')
          ParentFont = False
        end
        object LblAppelation: TfrxMemoView
          AllowVectorExport = True
          Top = 204.094620000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'LblAppelation')
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Top = 445.984540000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 445.984540000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            'Gekozen object:')
          ParentFont = False
        end
        object LblEndDate: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 347.716760000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblDate')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Top = 328.819110000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Doc-datum:')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Top = 347.716760000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Verval-datum:')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 366.614410000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Referentie offerte:')
          ParentFont = False
        end
        object LblReferenceOffer: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 366.614410000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'LblReferenceOffer')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Height = 75.590600000000000000
        Top = 600.945270000000000000
        Width = 680.315400000000000000
        Child = InvoiceObjects.ChildArticle
        DataSet = frxObjectLines
        DataSetName = 'frxObjectLines'
        RowCount = 0
        Stretched = True
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Width = 514.016080000000000000
          Height = 15.118120000000000000
          StretchMode = smActualHeight
          DataField = 'DESCRIPTION'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."DESCRIPTION"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 22.677180000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataField = 'QUANTITY'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.FormatStr = '%g'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."QUANTITY"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 22.677180000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Aantal:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Prijs:')
          ParentFont = False
        end
        object LblValueYourPrice: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataField = 'CUSTOMERAMOUNT'
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            '[frxObjectLines."CUSTOMERAMOUNT"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Uw totaalprijs')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            
              '[<frxObjectLines."CUSTOMERAMOUNT"> * <frxObjectLines."QUANTITY">' +
              ']')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 37.795300000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
      end
      object ChildArticle: TfrxChild
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 699.213050000000000000
        Width = 680.315400000000000000
        ToNRows = 0
        ToNRowsMode = rmCount
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 555.590910000000000000
        Width = 680.315400000000000000
        Condition = 'frsInvoices."ID"'
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 94.488250000000000000
        Top = 744.567410000000000000
        Width = 680.315400000000000000
        object LblTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            'LblTotal')
          ParentFont = False
        end
        object LblTotalVat: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 52.913420000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            'LblTotalVat')
          ParentFont = False
        end
        object LblGeneralTotal: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858380000000000000
          Top = 68.031540000000000000
          Width = 151.181200000000000000
          Height = 15.118120000000000000
          DataSet = frxObjectLines
          DataSetName = 'frxObjectLines'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Color = clNone
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Memo.UTF8W = (
            'LblGeneralTotal')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 37.795300000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/excl btw')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 52.913420000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'btw')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 68.031540000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            #8364'/incl btw')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Fill.BackColor = clBlack
          Frame.Typ = []
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 3.779530000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold, fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            'Totaalprijs factuur')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 15.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      PrintOnPreviousPage = True
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 83.149660000000000000
        Top = 377.953000000000000000
        Width = 680.315400000000000000
        object LblBank11: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank11')
          ParentFont = False
        end
        object LblBank12: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 37.795300000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank12')
          ParentFont = False
        end
        object LblBank13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 56.692950000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank13')
          ParentFont = False
        end
        object LblVat: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 18.897650000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblVat')
          ParentFont = False
        end
        object LblBank21: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 18.897650000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank21')
          ParentFont = False
        end
        object LblBank22: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 37.795300000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank22')
          ParentFont = False
        end
        object LblBank23: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 56.692950000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'LblBank23')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Frame.Typ = []
        Height = 336.378170000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object LblPaymentTerms: TfrxMemoView
          AllowVectorExport = True
          Top = 181.417440000000000000
          Width = 680.315400000000000000
          Height = 41.574830000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Gelieve dit bedrag over te schrijven op onderstaand rekeningnumm' +
              'er binnen de 15 dagen na facturatiedatum met vermelding van het ' +
              'factuurnummer.')
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 132.283550000000000000
          Width = 139.842610000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Levertermijn')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 151.181200000000000000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Wij trachten een levertermijn te hanteren van 8 weken na betalin' +
              'g voorschot.')
          ParentFont = False
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Top = 124.724490000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Top = 7.559060000000000000
          Width = 211.653680000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '- Betalingsvoorwaarden:')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 26.456710000000000000
          Width = 657.638220000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '50% bij bestelling - 50% bij vertrek uit ons depot')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 22.677180000000000000
          Top = 45.354360000000000000
          Width = 593.386210000000000000
          Height = 68.031540000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsItalic]
          Frame.Typ = []
          Memo.UTF8W = (
            
              '(Alle facturen zijn betaalbaar op vervaldatum. In geval van laat' +
              'tijdige betaling wordt het factuurbedrag van rechtswege & zonder' +
              ' ingebrekestelling verhoogd met een conventionele schadevergoedi' +
              'ng van 10%, te vermeerderen met de vertragingsintresten van 1% p' +
              'er maand.'
            
              'In geval van betwisting zijn uitsluitend de rechtenbanken van Ko' +
              'rtrijk bevoegd.)')
          ParentFont = False
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = []
          Diagonal = True
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Top = 241.889920000000000000
          Width = 680.315400000000000000
          Height = 83.149660000000000000
          Frame.Typ = []
          Memo.UTF8W = (
            'Alvast bedankt!'
            ''
            'Met vriendelijke groeten,'
            ''
            'Coconne')
        end
      end
    end
  end
end
