object VwInvoiceDetailsFrm: TVwInvoiceDetailsFrm
  Left = 0
  Top = 0
  ClientHeight = 321
  ClientWidth = 481
  Caption = 'Detail factuur'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnCancel = BtnSaveClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LblTotalAmount: TUniLabel
    Left = 16
    Top = 16
    Width = 68
    Height = 13
    Hint = ''
    Caption = 'Totaalbedrag:'
    ParentFont = False
    TabOrder = 0
  end
  object UniLabel1: TUniLabel
    Left = 16
    Top = 40
    Width = 35
    Height = 13
    Hint = ''
    Caption = 'Datum:'
    ParentFont = False
    TabOrder = 1
  end
  object GridInvoiceLines: TUniDBGrid
    Left = 16
    Top = 88
    Width = 449
    Height = 177
    Hint = ''
    DataSource = DsInvoiceLines
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    TabOrder = 2
    TabStop = False
    Columns = <
      item
        Flex = 1
        FieldName = 'DESCRIPTION'
        Title.Caption = 'OMSCHRIJVING'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'AMOUNT'
        Title.Alignment = taRightJustify
        Title.Caption = 'BEDRAG'
        Width = 85
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object UniLabel2: TUniLabel
    Left = 16
    Top = 64
    Width = 56
    Height = 13
    Hint = ''
    Caption = 'Detaillijnen:'
    ParentFont = False
    TabOrder = 3
  end
  object LblTotalAmountValue: TUniLabel
    Left = 296
    Top = 16
    Width = 169
    Height = 13
    Hint = ''
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblTotalAmountValue'
    ParentFont = False
    TabOrder = 4
  end
  object LblInvoiceDateValue: TUniLabel
    Left = 296
    Top = 40
    Width = 169
    Height = 13
    Hint = ''
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblInvoiceDateValue'
    ParentFont = False
    TabOrder = 5
  end
  object BtnSave: TUniThemeButton
    Left = 360
    Top = 280
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Sluiten'
    ParentFont = False
    TabOrder = 6
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnSaveClick
    ButtonTheme = uctSecondary
  end
  object InvoiceLines: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_INVOICELINES_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO INVOICELINES'
      '  (ID, INVOICE, DESCRIPTION, AMOUNT)'
      'VALUES'
      '  (:ID, :INVOICE, :DESCRIPTION, :AMOUNT)'
      'RETURNING '
      '  ID, INVOICE, DESCRIPTION, AMOUNT')
    SQLDelete.Strings = (
      'DELETE FROM INVOICELINES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE INVOICELINES'
      'SET'
      
        '  ID = :ID, INVOICE = :INVOICE, DESCRIPTION = :DESCRIPTION, AMOU' +
        'NT = :AMOUNT'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, INVOICE, DESCRIPTION, AMOUNT FROM INVOICELINES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM INVOICELINES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM INVOICELINES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from invoicelines')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 236
    Top = 117
    object InvoiceLinesID: TLargeintField
      FieldName = 'ID'
    end
    object InvoiceLinesINVOICE: TLargeintField
      FieldName = 'INVOICE'
      Required = True
    end
    object InvoiceLinesDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Size = 100
    end
    object InvoiceLinesAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00'
    end
  end
  object DsInvoiceLines: TDataSource
    DataSet = InvoiceLines
    Left = 232
    Top = 176
  end
end
