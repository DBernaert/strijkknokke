unit InvoicingMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, Data.DB, MemDS, DBAccess, IBC, uniButton,
  UniThemeButton, uniEdit, uniBasicGrid, uniDBGrid, Vcl.Imaging.pngimage, uniMultiItem, uniComboBox, uniBitBtn,
  uniMenuButton, Vcl.Menus, uniMainMenu, uniDBNavigator;

type
  TInvoicingMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageInvoices: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerButtonsFiles: TUniContainerPanel;
    Invoices: TIBCQuery;
    InvoicesID: TLargeintField;
    InvoicesINVOICENUMBER: TIntegerField;
    InvoicesINVOICEDATE: TDateField;
    InvoicesDUEDATE: TDateField;
    InvoicesTOTALAMOUNT: TFloatField;
    InvoicesPROJECTNAME: TWideStringField;
    InvoicesRELATIONTYPE: TIntegerField;
    InvoicesCOMPANYNAME: TWideStringField;
    InvoicesRELATIONNAME: TWideStringField;
    InvoicesRELATIONFIRSTNAME: TWideStringField;
    InvoicesDisplayName: TStringField;
    InvoicesTOTALPAYED: TFloatField;
    InvoicesOPENAMOUNT: TCurrencyField;
    InvoicesAMOUNT: TFloatField;
    DsInvoices: TDataSource;
    GridInvoices: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    BtnBookPayment: TUniThemeButton;
    BtnModifyInvoiceNumber: TUniThemeButton;
    BtnDeleteInvoice: TUniThemeButton;
    BtnMailInvoice: TUniThemeButton;
    BtnPrintInvoice: TUniThemeButton;
    BtnViewInvoice: TUniThemeButton;
    InvoicesINVOICESTAGE: TIntegerField;
    ImageCreated: TUniImage;
    ImageMailed: TUniImage;
    ImageReminder1: TUniImage;
    ImageReminder2: TUniImage;
    ImageReminder3: TUniImage;
    ComboSearch: TUniComboBox;
    BtnSearch: TUniThemeButton;
    BtnChangeStatus: TUniMenuButton;
    PopupMenuStatus: TUniPopupMenu;
    BtnStatusCreated: TUniMenuItem;
    BtnStatusMailed: TUniMenuItem;
    BtnStatusRappel1: TUniMenuItem;
    BtnStatusRappel2: TUniMenuItem;
    BtnStatusRappel3: TUniMenuItem;
    NavigatorInvoices: TUniDBNavigator;
    procedure EditSearchChange(Sender: TObject);
    procedure InvoicesCalcFields(DataSet: TDataSet);
    procedure BtnBookPaymentClick(Sender: TObject);
    procedure BtnModifyInvoiceNumberClick(Sender: TObject);
    procedure BtnDeleteInvoiceClick(Sender: TObject);
    procedure BtnMailInvoiceClick(Sender: TObject);
    procedure BtnPrintInvoiceClick(Sender: TObject);
    procedure BtnViewInvoiceClick(Sender: TObject);
    procedure GridInvoicesColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
    procedure GridInvoicesColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
      Attribs: TUniCellAttribs; var Result: string);
    procedure GridInvoicesFieldImage(const Column: TUniDBGridColumn; const AField: TField; var OutImage: TGraphic;
      var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
    procedure BtnSearchClick(Sender: TObject);
    procedure UniFrameCreate(Sender: TObject);
    procedure BtnStatusCreatedClick(Sender: TObject);
    procedure GridInvoicesDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
  private
    { Private declarations }
    procedure Open_invoices;
    procedure CallBackBookPayment(Sender: TComponent; AResult: Integer);
    procedure CallBackChangeInvoiceNumber(Sender: TComponent; AResult:Integer; AText: string);
    procedure CallBackMailing(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Main, ServerModule, BookInvoicePayment, UniFSConfirm, MailDocument, ReportsMain, PrintInvoice,
     VwInvoiceDetails;



{ TInvoicingMainFrm }

procedure TInvoicingMainFrm.BtnBookPaymentClick(Sender: TObject);
begin
 if (Invoices.Active) and (not Invoices.fieldbyname('Id').isNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With BookInvoicePaymentFrm do begin
      Initialize_payment(Invoices.fieldbyname('Id').AsInteger);
      ShowModal(CallBackBookPayment);
    end;
  end;
end;

procedure TInvoicingMainFrm.BtnDeleteInvoiceClick(Sender: TObject);
begin
 if (not Invoices.Active) or (Invoices.FieldByName('Id').IsNull)
  then exit;

  MainForm.Confirm.Question('Verwijderen factuur?', 'Bent u zeker dat u deze factuur wenst te verwijderen?', 'fa fa-exclamation-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes
      then begin
             UniMainModule.UInvoiceLines.Close;
             UniMainModule.UInvoiceLines.SQL.Clear;
             UniMainModule.UInvoiceLines.SQL.Add('Select * from invoicelines where Invoice=' + Invoices.FieldByName('Id').asString);
             UniMainModule.UInvoiceLines.Open;
             UniMainModule.UInvoiceLines.First;
             while not UniMainModule.UInvoiceLines.Eof
             do UniMainModule.UInvoiceLines.Delete;
             Try
               UniMainModule.UpdTrInvoiceLines.Commit;
               UniMainModule.UInvoiceLines.Close;
             Except
               UniMainModule.UpdTrInvoiceLines.Rollback;
               UniMainModule.UInvoiceLines.Close;
               UniMainModule.show_error('Fout bij het verwijderen van de factuur!');
               Exit;
             End;

             UniMainModule.UInvoices.Close;
             UniMainModule.UInvoices.SQL.Clear;
             UniMainModule.UInvoices.SQL.Add('Select * from invoices where id=' + Invoices.FieldByName('Id').asString);
             UniMainModule.UInvoices.Open;
             UniMainModule.UInvoices.Delete;
             Try
               UniMainModule.UpdTrInvoices.Commit;
               UniMainModule.UInvoices.Close;
             Except
               UniMainModule.UpdTrInvoices.Rollback;
               UniMainModule.UInvoices.Close;
               UniMainModule.show_error('Fout bij het verwijderen van de factuur!');
               Exit;
             End;
             Invoices.Refresh;
           end;
    end);
end;

procedure TInvoicingMainFrm.BtnMailInvoiceClick(Sender: TObject);
begin
  if (not Invoices.Active) or (Invoices.FieldByName('Id').IsNull)
  then exit;

  with MailDocumentFrm
  do begin
       Start_module_mail_invoice(Invoices.FieldByName('Id').AsInteger);
       ShowModal(CallBackMailing);
     end;
end;

procedure TInvoicingMainFrm.BtnModifyInvoiceNumberClick(Sender: TObject);
begin
  if (not Invoices.Active) or (Invoices.FieldByName('Id').isNull)
  then exit;

  Prompt('Nieuw factuurnummer', '', mtInformation, mbOKCancel, CallBackChangeInvoiceNumber);
end;

procedure TInvoicingMainFrm.BtnPrintInvoiceClick(Sender: TObject);
var ExtraTitle: string;
begin
  if (not Invoices.Active) or (Invoices.FieldByName('Id').IsNull)
  then exit;

  ExtraTitle := '';
  case Invoices.FieldByName('INVOICESTAGE').AsInteger of
    2: ExtraTitle := 'Herinnering';
    3: ExtraTitle := '2de en laatste herinnering';
    4: ExtraTitle := 'Originele factuur';
  end;

  with PrintInvoiceFrm
  do begin
       Start_module(Invoices.FieldByName('Id').AsInteger, ExtraTitle);
       ShowModal();
     end;
end;

procedure TInvoicingMainFrm.BtnSearchClick(Sender: TObject);
begin
  Open_invoices;
end;

procedure TInvoicingMainFrm.BtnStatusCreatedClick(Sender: TObject);
begin
  if (not Invoices.Active) or (Invoices.FieldByName('Id').IsNull)
  then exit;

  UniMainModule.UInvoices.Close;
  UniMainModule.UInvoices.SQL.Clear;
  UniMainModule.UInvoices.SQL.Add('Select * from invoices where id=' + Invoices.FieldByName('Id').asString);
  UniMainModule.UInvoices.Open;
  UniMainModule.UInvoices.Edit;
  UniMainModule.UInvoices.FieldByName('InvoiceStage').AsInteger := (Sender as TUniMenuItem).Tag;

  Try
    UniMainModule.UInvoices.Post;
    UniMainModule.UpdTrInvoices.Commit;
    UniMainModule.UInvoices.Close;
    Invoices.Refresh;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       UniMainModule.UpdTrInvoices.Rollback;
       UniMainModule.UInvoices.Close;
       UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
     end;
  end;
end;

procedure TInvoicingMainFrm.CallBackMailing(Sender: TComponent; AResult: Integer);
var currentinvoice: longint;
begin
  Currentinvoice := Invoices.FieldByName('Id').AsInteger;
  Open_invoices;
  Invoices.Locate('Id', Currentinvoice, []);
end;

procedure TInvoicingMainFrm.CallBackBookPayment(Sender: TComponent; AResult: Integer);
begin
  if (UniMainModule.Result_dbAction <> 0) and (Invoices.Active)
  then Invoices.Refresh;
end;

procedure TInvoicingMainFrm.CallBackChangeInvoiceNumber(Sender: TComponent; AResult: Integer; AText: string);
begin
  if AResult = mrOK then begin
    Try
      StrToInt(AText);
    Except
      UniMainModule.show_warning('Het opgegeven factuurnummer is niet geldig.');
      Exit;
    End;

    UniMainModule.UInvoices.Close;
    UniMainModule.UInvoices.SQL.Clear;
    UniMainModule.UInvoices.SQL.Add('Select * from invoices where id=' + Invoices.FieldByName('Id').asString);
    UniMainModule.UInvoices.Open;
    UniMainModule.UInvoices.Edit;
    UniMainModule.UInvoices.FieldByName('InvoiceNumber').AsInteger := StrToInt(AText);

    Try
      UniMainModule.UInvoices.Post;
      UniMainModule.UpdTrInvoices.Commit;
      UniMainModule.UInvoices.Close;
      Invoices.Refresh;
    Except on E: EDataBaseError
    do begin
         UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
         UniMainModule.UpdTrInvoices.Rollback;
         UniMainModule.UInvoices.Close;
         UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
       end;
    end;
  end;
end;

procedure TInvoicingMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' DISPLAYNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
    ' PROJECTNAME LIKE ''%' + EditSearch.Text + '%''';
  Invoices.Filter := Filter;
end;

procedure TInvoicingMainFrm.GridInvoicesColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(UpperCase(Column.FieldName), 'OPENAMOUNT')
  then begin
         if Column.AuxValue = NULL
         then Column.AuxValue := 0.0;
         Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
       end;
end;

procedure TInvoicingMainFrm.GridInvoicesColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
  Attribs: TUniCellAttribs; var Result: string);
var F: currency;
begin
  if SameText(UpperCase(Column.FieldName), 'OPENAMOUNT')
  then begin
         F := Column.AuxValue;
         if UpperCase(Column.FieldName) = 'OPENAMOUNT'
         then Result := FormatCurr('#,##0.00 �', F);
         Attribs.Color := $00B36D00;
         Attribs.Font.Color := ClWhite;
         Attribs.Font.Style := [];
       end;
  Column.AuxValue := NULL;
end;

procedure TInvoicingMainFrm.GridInvoicesDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
  Attribs: TUniCellAttribs);
begin
  if (Invoices.fieldbyname('DueDate').AsDateTime < Date) and (Invoices.fieldbyname('OpenAmount').AsCurrency <> 0) and
     (UpperCase(Column.FieldName) = 'OPENAMOUNT')
  then begin
         Attribs.Color := $003738C6;
         Attribs.Font.Style := [];
         Attribs.Font.Color := ClWhite;
       end;
end;

procedure TInvoicingMainFrm.GridInvoicesFieldImage(const Column: TUniDBGridColumn; const AField: TField;
  var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(UpperCase(AField.FieldName), 'INVOICESTAGE') then
  begin DoNotDispose := True; // we provide an static image so do not free it.
    Case AField.AsInteger of
     0: OutImage := ImageCreated.Picture.Graphic;
     1: OutImage := ImageMailed.Picture.Graphic;
     2: OutImage := ImageReminder1.Picture.Graphic;
     3: OutImage := ImageReminder2.Picture.Graphic;
     4: OutImage := ImageReminder3.Picture.Graphic;
     5: OutImage := MainForm.ImageGreen.Picture.Graphic;
    End;
  end;
end;

procedure TInvoicingMainFrm.InvoicesCalcFields(DataSet: TDataSet);
begin
  if Invoices.FieldByName('RelationType').AsInteger = 0
  then Invoices.FieldByName('DisplayName').AsString := Invoices.FieldByName('CompanyName').AsString
  else Invoices.FieldByName('DisplayName').AsString := Trim(Invoices.FieldByName('RelationName').AsString + ' ' +
                                                            Invoices.FieldByName('RelationFirstName').AsString);

  Invoices.FieldByName('OpenAmount').AsCurrency :=
  Invoices.FieldByName('TotalAmount').AsCurrency - Invoices.FieldByName('TotalPayed').AsCurrency;
end;

procedure TInvoicingMainFrm.Open_invoices;
var SQL: string;
begin
  Invoices.Close;
  Invoices.SQL.Clear;
  SQL := 'SELECT INVOICES.ID, INVOICES.INVOICESTAGE, INVOICES.INVOICENUMBER, INVOICES.INVOICEDATE, INVOICES.DUEDATE, '
    + 'INVOICES.TOTALAMOUNT, INVOICES.AMOUNT, INVOICES.TOTALPAYED, PROJECTS.NAME AS PROJECTNAME, '
    + 'RELATIONS.RELATIONTYPE, RELATIONS.COMPANYNAME, RELATIONS.NAME AS RELATIONNAME, '
    + 'RELATIONS.FIRSTNAME AS RELATIONFIRSTNAME ' + 'FROM INVOICES ' +
    'LEFT OUTER JOIN RELATIONS ON (INVOICES.RELATION = RELATIONS.ID) ' +
    'LEFT OUTER JOIN PROJECTS ON (INVOICES.PROJECT = PROJECTS.ID) ' +
    'WHERE INVOICES.COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' ';

  case ComboSearch.ItemIndex of
  0: SQL := SQL + ' AND INVOICES.INVOICESTAGE <> 5 ';
  1: SQL := SQL + ' AND INVOICES.INVOICESTAGE = 5 ';
  end;

  SQL := SQL + 'ORDER BY INVOICES.INVOICENUMBER';
  Invoices.SQL.Add(SQL);
  Invoices.Open;
  Invoices.Last;
end;

procedure TInvoicingMainFrm.Start_module;
begin
  ComboSearch.ItemIndex := 2;
  Open_invoices;
  EditSearch.SetFocus;
end;

procedure TInvoicingMainFrm.UniFrameCreate(Sender: TObject);
begin
  BtnChangeStatus.JSInterface.JSConfig('menuAlign', ['tr-br?']);
end;

procedure TInvoicingMainFrm.BtnViewInvoiceClick(Sender: TObject);
begin
  if (not Invoices.Active) or (Invoices.FieldByName('Id').IsNull)
  then exit;

  with VwInvoiceDetailsFrm
  do begin
       Start_module(Invoices.FieldByName('Id').AsInteger);
       ShowModal();
     end;
end;

end.
