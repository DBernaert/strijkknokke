object InvoicesAddFrm: TInvoicesAddFrm
  Left = 0
  Top = 0
  ClientHeight = 385
  ClientWidth = 1097
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditAmountToInvoice
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LblProjectTitle: TUniLabel
    Left = 16
    Top = 16
    Width = 38
    Height = 13
    Hint = ''
    Caption = 'Project:'
    ParentFont = False
    TabOrder = 0
  end
  object LblProjectName: TUniLabel
    Left = 184
    Top = 16
    Width = 89
    Height = 13
    Hint = ''
    Caption = 'LblProjectName'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 1
  end
  object LblTitleOpenAmountProject: TUniLabel
    Left = 16
    Top = 48
    Width = 137
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Openstaand bedrag project:'
    ParentFont = False
    TabOrder = 2
  end
  object LblOpenAmountProject: TUniLabel
    Left = 184
    Top = 48
    Width = 345
    Height = 13
    Hint = ''
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblOpenAmountProject'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 3
  end
  object LblAmountToInvoice: TUniLabel
    Left = 16
    Top = 80
    Width = 280
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Te factureren bedrag  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 4
  end
  object EditAmountToInvoice: TUniFormattedNumberEdit
    Left = 184
    Top = 80
    Width = 345
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.CurrencySignPos = cpsRight
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    Alignment = taRightJustify
    TabOrder = 5
    SelectOnFocus = True
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object LblDescriptionInvoice: TUniLabel
    Left = 16
    Top = 112
    Width = 238
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Omschrijving  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 6
  end
  object EditDescriptionInvoice: TUniEdit
    Left = 184
    Top = 112
    Width = 345
    Hint = ''
    Text = ''
    TabOrder = 7
  end
  object LblInvoiceDate: TUniLabel
    Left = 16
    Top = 144
    Width = 244
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Factuurdatum  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 8
  end
  object EditInvoiceDate: TUniDateTimePicker
    Left = 184
    Top = 144
    Width = 345
    Hint = ''
    DateTime = 43505.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 9
  end
  object LblPaymentTerm: TUniLabel
    Left = 16
    Top = 176
    Width = 291
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Betalingstermijn factuur  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 10
  end
  object EditPaymentTerm: TUniFormattedNumberEdit
    Left = 184
    Top = 176
    Width = 345
    Hint = ''
    Alignment = taRightJustify
    TabOrder = 11
    DecimalPrecision = 0
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object LblDocumentType: TUniLabel
    Left = 16
    Top = 208
    Width = 251
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Type document  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 12
  end
  object ComboDocumentType: TUniComboBox
    Left = 184
    Top = 208
    Width = 345
    Hint = ''
    Text = 'ComboDocumentType'
    Items.Strings = (
      'Voorschot-ereloon'
      'Eindafschrift-ereloon')
    ItemIndex = 0
    TabOrder = 13
    IconItems = <>
  end
  object CheckboxIncludeRegistrations: TUniCheckBox
    Left = 184
    Top = 240
    Width = 289
    Height = 17
    Hint = ''
    Checked = True
    Caption = 'Invoegen overzicht registraties'
    ParentFont = False
    TabOrder = 14
  end
  object LblStartDateRegistrations: TUniLabel
    Left = 16
    Top = 272
    Width = 290
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Begindatum registraties  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 15
  end
  object EditStartDateRegistrations: TUniDateTimePicker
    Left = 184
    Top = 272
    Width = 345
    Hint = ''
    DateTime = 43505.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 16
  end
  object LblEndDateRegistrations: TUniLabel
    Left = 16
    Top = 304
    Width = 284
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Einddatum registraties  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 17
  end
  object EditEndDateRegistrations: TUniDateTimePicker
    Left = 184
    Top = 304
    Width = 345
    Hint = ''
    DateTime = 43505.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 18
  end
  object LblNonInvoicedPurchases: TUniLabel
    Left = 544
    Top = 16
    Width = 52
    Height = 13
    Hint = ''
    Caption = 'Aankopen:'
    ParentFont = False
    TabOrder = 19
  end
  object BtnChangeStatus: TUniThemeButton
    Left = 1056
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Selecteren facturatie'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    ParentFont = False
    TabStop = False
    TabOrder = 20
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnChangeStatusClick
    ButtonTheme = uctPrimary
  end
  object GridPurchases: TUniDBGrid
    Left = 545
    Top = 50
    Width = 536
    Height = 279
    Hint = ''
    DataSource = DsOpenPurchases
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    TabOrder = 21
    OnDblClick = BtnChangeStatusClick
    OnFieldImage = GridPurchasesFieldImage
    OnDrawColumnCell = GridPurchasesDrawColumnCell
    Columns = <
      item
        FieldName = 'Selected'
        Title.Alignment = taCenter
        Title.Caption = 'FACT.'
        Width = 64
        Alignment = taCenter
        ImageOptions.Visible = True
        ImageOptions.Width = 12
        ImageOptions.Height = 12
      end
      item
        Flex = 1
        FieldName = 'Description'
        Title.Caption = 'OMSCHRIJVING'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'Quantity'
        Title.Alignment = taRightJustify
        Title.Caption = '#'
        Width = 50
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'Price'
        Title.Alignment = taRightJustify
        Title.Caption = 'PRIJS'
        Width = 105
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'TotalPrice'
        Title.Alignment = taRightJustify
        Title.Caption = 'TOTAAL'
        Width = 105
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object BtnSave: TUniThemeButton
    Left = 864
    Top = 344
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Aanmaken'
    ParentFont = False
    TabOrder = 22
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 976
    Top = 344
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 23
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object OpenPurchases: TVirtualTable
    Active = True
    FieldDefs = <
      item
        Name = 'PurchaseId'
        DataType = ftLargeint
      end
      item
        Name = 'Selected'
        DataType = ftInteger
      end
      item
        Name = 'DatePurchase'
        DataType = ftDate
      end
      item
        Name = 'Description'
        DataType = ftString
        Size = 100
      end
      item
        Name = 'Quantity'
        DataType = ftInteger
      end
      item
        Name = 'Price'
        DataType = ftCurrency
      end
      item
        Name = 'TotalPrice'
        DataType = ftCurrency
      end
      item
        Name = 'Invoiced'
        DataType = ftInteger
      end>
    Left = 928
    Top = 176
    Data = {
      040008000A00507572636861736549641900000000000000080053656C656374
      656403000000000000000C004461746550757263686173650900000000000000
      0B004465736372697074696F6E010064000000000008005175616E7469747903
      000000000000000500507269636507000000000000000A00546F74616C507269
      636507000000000000000800496E766F69636564030000000000000000000000
      0000}
  end
  object DsOpenPurchases: TDataSource
    DataSet = OpenPurchases
    Left = 928
    Top = 232
  end
end
