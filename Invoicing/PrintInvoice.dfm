object PrintInvoiceFrm: TPrintInvoiceFrm
  Left = 0
  Top = 0
  ClientHeight = 161
  ClientWidth = 561
  Caption = 'Afdrukken factuur'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = CheckboxIncludeRegistrations
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object CheckboxIncludeRegistrations: TUniCheckBox
    Left = 16
    Top = 16
    Width = 289
    Height = 17
    Hint = ''
    Checked = True
    Caption = 'Invoegen overzicht registraties'
    ParentFont = False
    TabOrder = 0
  end
  object LblStartDateRegistrations: TUniLabel
    Left = 32
    Top = 48
    Width = 290
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Begindatum registraties  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 1
  end
  object EditStartDateRegistrations: TUniDateTimePicker
    Left = 200
    Top = 48
    Width = 345
    Hint = ''
    DateTime = 43505.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 2
  end
  object LblEndDateRegistrations: TUniLabel
    Left = 32
    Top = 80
    Width = 284
    Height = 13
    Hint = ''
    TextConversion = txtHTML
    Caption = 'Einddatum registraties  <font color="#cd0015">*</font>:'
    ParentFont = False
    TabOrder = 3
  end
  object EditEndDateRegistrations: TUniDateTimePicker
    Left = 200
    Top = 80
    Width = 345
    Hint = ''
    DateTime = 43505.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 4
  end
  object BtnSave: TUniThemeButton
    Left = 328
    Top = 120
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Afdrukken'
    ParentFont = False
    TabOrder = 5
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 440
    Top = 120
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 6
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
end
