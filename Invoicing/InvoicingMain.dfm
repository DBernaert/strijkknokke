object InvoicingMainFrm: TInvoicingMainFrm
  Left = 0
  Top = 0
  Width = 1306
  Height = 888
  OnCreate = UniFrameCreate
  Layout = 'border'
  LayoutConfig.Margin = '8 8 8 4'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1306
    Height = 48
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerTitle: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 1306
      Height = 48
      Hint = ''
      ParentColor = False
      Align = alClient
      TabOrder = 0
      TabStop = False
      LayoutConfig.Region = 'center'
      object LblTitle: TUniLabel
        Left = 40
        Top = 0
        Width = 130
        Height = 33
        Hint = ''
        Caption = 'Factuurlijst'
        ParentFont = False
        Font.Height = -27
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
            '= "lfp-pagetitle";'#13#10'}')
        TabOrder = 1
      end
      object ImageInvoices: TUniImage
        Left = 0
        Top = 0
        Width = 32
        Height = 32
        Hint = ''
        AutoSize = True
        Url = 'images/Invoice-32-black.png'
      end
    end
  end
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 48
    Width = 1306
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerSearch: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 633
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 265
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        OnChange = EditSearchChange
      end
      object ComboSearch: TUniComboBox
        Left = 272
        Top = 0
        Width = 233
        Hint = ''
        Text = 'ComboSearch'
        Items.Strings = (
          'Openstaande facturen'
          'Betaalde facturen'
          'Alle facturen')
        ItemIndex = 0
        TabOrder = 2
        IconItems = <>
      end
      object BtnSearch: TUniThemeButton
        Left = 512
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Zoeken'
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 39
        OnClick = BtnSearchClick
        ButtonTheme = uctPrimary
      end
    end
    object ContainerButtonsFiles: TUniContainerPanel
      Left = 898
      Top = 0
      Width = 408
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object BtnViewInvoice: TUniThemeButton
        Left = 1
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Zien'
        ParentFont = False
        TabStop = False
        TabOrder = 1
        Default = True
        Images = UniMainModule.ImageList
        ImageIndex = 7
        OnClick = BtnViewInvoiceClick
        ButtonTheme = uctDefault
      end
      object BtnChangeStatus: TUniMenuButton
        Left = 112
        Top = 0
        Width = 169
        Height = 25
        Hint = ''
        DropdownMenu = PopupMenuStatus
        Caption = 'Status aanpassen'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
      end
      object NavigatorInvoices: TUniDBNavigator
        Left = 288
        Top = 0
        Width = 121
        Height = 25
        Hint = ''
        DataSource = DsInvoices
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        IconSet = icsFontAwesome
        TabStop = False
        TabOrder = 3
      end
    end
  end
  object GridInvoices: TUniDBGrid
    Left = 0
    Top = 73
    Width = 1306
    Height = 790
    Hint = ''
    DataSource = DsInvoices
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 0 8 0'
    Align = alClient
    TabOrder = 2
    ParentColor = False
    Color = clBtnFace
    Summary.Align = taBottom
    Summary.Enabled = True
    Summary.GrandTotalAlign = taBottom
    OnDblClick = BtnViewInvoiceClick
    OnFieldImage = GridInvoicesFieldImage
    OnDrawColumnCell = GridInvoicesDrawColumnCell
    OnColumnSummary = GridInvoicesColumnSummary
    OnColumnSummaryResult = GridInvoicesColumnSummaryResult
    Columns = <
      item
        FieldName = 'INVOICESTAGE'
        Title.Caption = ' '
        Width = 40
        Alignment = taCenter
        ImageOptions.Visible = True
        ImageOptions.Width = 12
        ImageOptions.Height = 12
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'INVOICENUMBER'
        Title.Alignment = taRightJustify
        Title.Caption = 'NR.'
        Width = 65
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'INVOICEDATE'
        Title.Caption = 'DATUM'
        Width = 120
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'DUEDATE'
        Title.Caption = 'VERVALDATUM'
        Width = 120
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'DisplayName'
        Title.Caption = 'NAAM'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'PROJECTNAME'
        Title.Caption = 'PROJECT'
        Width = 100
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'AMOUNT'
        Title.Alignment = taRightJustify
        Title.Caption = 'EXCL. BTW'
        Width = 110
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'TOTALAMOUNT'
        Title.Alignment = taRightJustify
        Title.Caption = 'INCL. BTW'
        Width = 110
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'TOTALPAYED'
        Title.Alignment = taRightJustify
        Title.Caption = 'BETAALD'
        Width = 110
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'OPENAMOUNT'
        Title.Alignment = taRightJustify
        Title.Caption = 'OPENSTAAND'
        Width = 110
        ShowSummary = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 863
    Width = 1306
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 3
    LayoutConfig.Region = 'south'
    object BtnBookPayment: TUniThemeButton
      Left = 0
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Betaling'
      ParentFont = False
      TabOrder = 1
      Images = UniMainModule.ImageList
      ImageIndex = 10
      OnClick = BtnBookPaymentClick
      ButtonTheme = uctDefault
    end
    object BtnModifyInvoiceNumber: TUniThemeButton
      Left = 112
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Factuurnr.'
      ParentFont = False
      TabOrder = 2
      Images = UniMainModule.ImageList
      ImageIndex = 11
      OnClick = BtnModifyInvoiceNumberClick
      ButtonTheme = uctDefault
    end
    object BtnDeleteInvoice: TUniThemeButton
      Left = 224
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Verwijderen'
      ParentFont = False
      TabOrder = 3
      Images = UniMainModule.ImageList
      ImageIndex = 12
      OnClick = BtnDeleteInvoiceClick
      ButtonTheme = uctDefault
    end
    object BtnMailInvoice: TUniThemeButton
      Left = 336
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Mailen'
      ParentFont = False
      TabOrder = 4
      Images = UniMainModule.ImageList
      ImageIndex = 34
      OnClick = BtnMailInvoiceClick
      ButtonTheme = uctDefault
    end
    object BtnPrintInvoice: TUniThemeButton
      Left = 448
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Afdrukken'
      ParentFont = False
      TabOrder = 5
      Default = True
      Images = UniMainModule.ImageList
      ImageIndex = 29
      OnClick = BtnPrintInvoiceClick
      ButtonTheme = uctDefault
    end
  end
  object ImageCreated: TUniImage
    Left = 104
    Top = 352
    Width = 13
    Height = 13
    Hint = ''
    Visible = False
    AutoSize = True
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000000D0000
      000D080600000072EBE47C0000000473424954080808087C0864880000006849
      44415478DA63646060D007E2BD40FC880113C803711910CF451664846ACA07E2
      242C9AD601B11C104F03E279A468AA02E27E205E05C4F389D52401556702C46E
      40BC9F90262E206685B23381F839102F24A4091980D47C18D5442D4DB8D21E32
      1005E21A902600D85F280EAF990D270000000049454E44AE426082}
    Transparent = True
  end
  object ImageMailed: TUniImage
    Left = 104
    Top = 384
    Width = 13
    Height = 13
    Hint = ''
    Visible = False
    AutoSize = True
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000000D0000
      000D080600000072EBE47C0000000473424954080808087C0864880000009849
      44415478DA636420033002713C399AFE02F12C20BE41847A0D204E03697A02C4
      AF813817888FE0D16003C493815814A4E91C10FB02F11A20EE03E2D558348402
      7111108700F16698262320E605E25540BC1BAA1906408A5D81380C883F83D423
      6B020156A8FFBE00713B105702310FC81F40FC1BAA0643130CE400B10F106F01
      E22968723835E103E46B7A0CC43D24682A213B45900C00114B2164ED4DFB2100
      00000049454E44AE426082}
    Transparent = True
  end
  object ImageReminder1: TUniImage
    Left = 104
    Top = 445
    Width = 13
    Height = 13
    Hint = ''
    Visible = False
    AutoSize = True
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000000D0000
      000D080600000072EBE47C0000000473424954080808087C0864880000006649
      44415478DA6364C00E7880981B885F629364C4222605C4DB80781D103711A349
      0D885700F13E20FE448C269093F6037112102B01B13EB1367100F10F20F62745
      130C8C6A22479319106700B11C100B01F1050648EA58834F930010CBA389BD02
      E2E7C802009CF4150EF5373C740000000049454E44AE426082}
    Transparent = True
  end
  object ImageReminder2: TUniImage
    Left = 104
    Top = 477
    Width = 13
    Height = 13
    Hint = ''
    Visible = False
    AutoSize = True
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000000D0000
      000D080600000072EBE47C0000000473424954080808087C086488000000BC49
      44415478DA6364C0044C40AC0CC4FF80F81E10FF4757C088C67704E23E20BE03
      D5AC00C459407C1297262920DE02C49E40FC122A260BC49B81D81288BF63D394
      0AC47F81781E9AED53817819101FC5E53C6C6026D4A093C46A1204E2FD406C0A
      C4BF89D1C402C4EBA1B6ACC7177A30C00CC44B80F80C10F7120A7298D85C0648
      1CB530E050800EA600F16720AEC4E56E744D5D40CC01C479F8420759530610F7
      00F11A06481242067540FC049B26508A10C561F84D20FE01E30000DB39210E42
      ED497F0000000049454E44AE426082}
    Transparent = True
  end
  object ImageReminder3: TUniImage
    Left = 104
    Top = 509
    Width = 13
    Height = 13
    Hint = ''
    Visible = False
    AutoSize = True
    Picture.Data = {
      0954506E67496D61676589504E470D0A1A0A0000000D494844520000000D0000
      000D080600000072EBE47C0000000473424954080808087C086488000000C749
      44415478DA6364C0044C40AC0CC43F81F8111679064634BE35104F06E2BB40CC
      01C482401C0DC40F71690229D80FC45E40FC0C2A6605C49540EC8B4B9307102B
      00F10C34DBCF03B1213EE7A1030120DE0DC4A6C468E203627320AE03E23E205E
      4F8CA635406C09C40F803814C98F4439CF13881BA001F297584D2030171A38A7
      B1696A05E239407C1F4DD342209E08C4E7B0690A82BA3F1688FF40C540CEEA01
      621B20FE87CB79A50C901470158885819805889318D09213363F81928F22107F
      440F351800007227210EA1B528A70000000049454E44AE426082}
    Transparent = True
  end
  object Invoices: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  INVOICES.ID,'
      '  INVOICES.INVOICESTAGE,'
      '  INVOICES.INVOICENUMBER,'
      '  INVOICES.INVOICEDATE,'
      '  INVOICES.DUEDATE,'
      '  INVOICES.AMOUNT,'
      '  INVOICES.TOTALAMOUNT,'
      '  INVOICES.TOTALPAYED,'
      '  PROJECTS.NAME AS PROJECTNAME,'
      '  RELATIONS.RELATIONTYPE,'
      '  RELATIONS.COMPANYNAME,'
      '  RELATIONS.NAME AS RELATIONNAME,'
      '  RELATIONS.FIRSTNAME AS RELATIONFIRSTNAME'
      'FROM'
      '  INVOICES'
      
        '  LEFT OUTER JOIN RELATIONS ON (INVOICES.RELATION = RELATIONS.ID' +
        ')'
      '  LEFT OUTER JOIN PROJECTS ON (INVOICES.PROJECT = PROJECTS.ID)')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = InvoicesCalcFields
    Left = 648
    Top = 216
    object InvoicesID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InvoicesINVOICENUMBER: TIntegerField
      FieldName = 'INVOICENUMBER'
      Required = True
    end
    object InvoicesINVOICEDATE: TDateField
      FieldName = 'INVOICEDATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object InvoicesDUEDATE: TDateField
      FieldName = 'DUEDATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object InvoicesTOTALAMOUNT: TFloatField
      FieldName = 'TOTALAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesPROJECTNAME: TWideStringField
      FieldName = 'PROJECTNAME'
      ReadOnly = True
      Size = 50
    end
    object InvoicesRELATIONTYPE: TIntegerField
      FieldName = 'RELATIONTYPE'
      ReadOnly = True
    end
    object InvoicesCOMPANYNAME: TWideStringField
      FieldName = 'COMPANYNAME'
      ReadOnly = True
      Size = 50
    end
    object InvoicesRELATIONNAME: TWideStringField
      FieldName = 'RELATIONNAME'
      ReadOnly = True
      Size = 25
    end
    object InvoicesRELATIONFIRSTNAME: TWideStringField
      FieldName = 'RELATIONFIRSTNAME'
      ReadOnly = True
      Size = 25
    end
    object InvoicesDisplayName: TStringField
      FieldKind = fkCalculated
      FieldName = 'DisplayName'
      Size = 51
      Calculated = True
    end
    object InvoicesTOTALPAYED: TFloatField
      FieldName = 'TOTALPAYED'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesOPENAMOUNT: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'OPENAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
      Calculated = True
    end
    object InvoicesAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesINVOICESTAGE: TIntegerField
      FieldName = 'INVOICESTAGE'
      Required = True
    end
  end
  object DsInvoices: TDataSource
    DataSet = Invoices
    Left = 648
    Top = 272
  end
  object PopupMenuStatus: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 648
    Top = 336
    object BtnStatusCreated: TUniMenuItem
      Caption = 'Status wijzigen naar gemaakt'
      ImageIndex = 19
      OnClick = BtnStatusCreatedClick
    end
    object BtnStatusMailed: TUniMenuItem
      Tag = 1
      Caption = 'Status wijzigen naar gemaild'
      ImageIndex = 19
      OnClick = BtnStatusCreatedClick
    end
    object BtnStatusRappel1: TUniMenuItem
      Tag = 2
      Caption = 'Status wijzigen naar rappel 1'
      ImageIndex = 19
      OnClick = BtnStatusCreatedClick
    end
    object BtnStatusRappel2: TUniMenuItem
      Tag = 3
      Caption = 'Status wijzigen naar rappel 2'
      ImageIndex = 19
      OnClick = BtnStatusCreatedClick
    end
    object BtnStatusRappel3: TUniMenuItem
      Tag = 4
      Caption = 'Status wijzigen naar rappel 3'
      ImageIndex = 19
      OnClick = BtnStatusCreatedClick
    end
  end
end
