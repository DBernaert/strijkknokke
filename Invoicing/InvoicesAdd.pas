unit InvoicesAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniBasicGrid, uniDBGrid, uniButton, UniThemeButton, uniCheckBox, uniMultiItem, uniComboBox,
  uniDateTimePicker, uniEdit, uniGUIBaseClasses, uniLabel, Data.DB, MemDS, VirtualTable, uniPanel, Vcl.Imaging.pngimage,
  uniImage, uniDBNavigator;

type
  TInvoicesAddFrm = class(TUniForm)
    OpenPurchases: TVirtualTable;
    DsOpenPurchases: TDataSource;
    LblProjectTitle: TUniLabel;
    LblProjectName: TUniLabel;
    LblTitleOpenAmountProject: TUniLabel;
    LblOpenAmountProject: TUniLabel;
    LblAmountToInvoice: TUniLabel;
    EditAmountToInvoice: TUniFormattedNumberEdit;
    LblDescriptionInvoice: TUniLabel;
    EditDescriptionInvoice: TUniEdit;
    LblInvoiceDate: TUniLabel;
    EditInvoiceDate: TUniDateTimePicker;
    LblPaymentTerm: TUniLabel;
    EditPaymentTerm: TUniFormattedNumberEdit;
    LblDocumentType: TUniLabel;
    ComboDocumentType: TUniComboBox;
    CheckboxIncludeRegistrations: TUniCheckBox;
    LblStartDateRegistrations: TUniLabel;
    EditStartDateRegistrations: TUniDateTimePicker;
    LblEndDateRegistrations: TUniLabel;
    EditEndDateRegistrations: TUniDateTimePicker;
    LblNonInvoicedPurchases: TUniLabel;
    BtnChangeStatus: TUniThemeButton;
    GridPurchases: TUniDBGrid;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnChangeStatusClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure GridPurchasesFieldImage(const Column: TUniDBGridColumn; const AField: TField; var OutImage: TGraphic;
      var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
    procedure GridPurchasesDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
      Attribs: TUniCellAttribs);
  private
    { Private declarations }
    Id_Project: longint;
    RelationId: longint;
    InvoiceNumber: longint;
    SubTotal, VatAmount, TotalAmount: currency;
    SplitInvoices: boolean;
    SplitInvoiceAddress: integer;
    procedure Generate_invoice;
    procedure Book_invoice;
    procedure CallBackPrintInvoice(Sender: TComponent; AResult: Integer);
    procedure CallBackSelectionSplitInvoice(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_create_invoice(ProjectId: longint);
  end;

function InvoicesAddFrm: TInvoicesAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ReportsMain, frxClass, UniFSConfirm, DateUtils, ServerModule, Main, SelectSplitInvoice;

function InvoicesAddFrm: TInvoicesAddFrm;
begin
  Result := TInvoicesAddFrm(UniMainModule.GetFormInstance(TInvoicesAddFrm));
end;

{ TInvoicesAddFrm }

procedure TInvoicesAddFrm.Book_invoice;
begin
  UniMainModule.UInvoices.Close;
  UniMainModule.UInvoices.SQL.Clear;
  UniMainModule.UInvoices.SQL.Add('Select first 0 * from invoices');
  UniMainModule.UInvoices.Open;
  UniMainModule.UInvoices.Append;
  UniMainModule.UInvoices.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UniMainModule.UInvoices.FieldByName('Relation').AsInteger := RelationId;
  UniMainModule.UInvoices.FieldByName('Project').AsInteger := Id_Project;
  UniMainModule.UInvoices.FieldByName('InvoiceNumber').AsInteger := InvoiceNumber;
  UniMainModule.UInvoices.FieldByName('InvoiceDate').AsDateTime := EditInvoiceDate.DateTime;
  UniMainModule.UInvoices.FieldByName('PaymentTerm').AsInteger := Trunc(EditPaymentTerm.Value);
  UniMainModule.UInvoices.FieldByName('InvoiceType').AsInteger := ComboDocumentType.ItemIndex;
  UniMainModule.UInvoices.FieldByName('DueDate').AsDateTime := IncDay(UniMainModule.UInvoices.FieldByName('InvoiceDate').AsDateTime,
    UniMainModule.UInvoices.FieldByName('PaymentTerm').AsInteger);
  UniMainModule.UInvoices.FieldByName('SplitInvoice').AsInteger := SplitInvoiceAddress;
  UniMainModule.UInvoices.FieldByName('Amount').AsCurrency := SubTotal;
  UniMainModule.UInvoices.FieldByName('VatAmount').AsCurrency := VatAmount;
  UniMainModule.UInvoices.FieldByName('TotalAmount').AsCurrency := TotalAmount;
  UniMainModule.UInvoices.FieldByName('TotalPayed').AsCurrency  := 0;
  UniMainModule.UInvoices.FieldByName('InvoiceStage').AsInteger := 0;
  Try
    UniMainModule.UInvoices.Post;
    UniMainModule.UpdTrInvoices.Commit;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrInvoices.Rollback;
      UniMainModule.UInvoices.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;

  UniMainModule.UInvoiceLines.Close;
  UniMainModule.UInvoiceLines.SQL.Clear;
  UniMainModule.UInvoiceLines.SQL.Add('Select first 0 * from invoicelines');
  UniMainModule.UInvoiceLines.Open;
  UniMainModule.UInvoiceLines.Append;
  UniMainModule.UInvoiceLines.FieldByName('Description').AsString := EditDescriptionInvoice.Text;
  UniMainModule.UInvoiceLines.FieldByName('Amount').AsCurrency    := EditAmountToInvoice.Value;
  UniMainModule.UInvoiceLines.FieldByName('Invoice').AsInteger    := UniMainModule.UInvoices.FieldByName('Id').AsInteger;
  UniMainModule.UInvoiceLines.Post;

  // Check for selected purchases
  OpenPurchases.First;
  while not OpenPurchases.Eof do
  begin
    if OpenPurchases.FieldByName('Selected').AsInteger = 1 then
    begin
        UniMainModule.UInvoiceLines.Append;
        UniMainModule.UInvoiceLines.FieldByName('Description').AsString := OpenPurchases.FieldByName('Description').AsString;
        UniMainModule.UInvoiceLines.FieldByName('Amount').AsCurrency    := OpenPurchases.FieldByName('TotalPrice').AsCurrency;
        UniMainModule.UInvoiceLines.FieldByName('Invoice').AsInteger    := UniMainModule.UInvoices.FieldByName('Id').AsInteger;
        UniMainModule.UInvoiceLines.Post;
    end;
    OpenPurchases.Next;
  end;
  OpenPurchases.First;

  Try
    UniMainModule.UpdTrInvoiceLines.Commit;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrInvoiceLines.Rollback;
      UniMainModule.UInvoiceLines.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;

  //Change status of the purchases
  OpenPurchases.First;
  while not OpenPurchases.Eof do
  begin
    if OpenPurchases.FieldByName('Selected').AsInteger = 1 then
    begin
        UniMainModule.UProjectPurchases.Close;
        UniMainModule.UProjectPurchases.SQL.Clear;
        UniMainModule.UProjectPurchases.SQL.Add('Select * from projectpurchases where Id=' + OpenPurchases.FieldByName('PurchaseId').AsString);
        UniMainModule.UProjectPurchases.Open;
        Try
          UniMainModule.UProjectPurchases.Edit;
          UniMainModule.UProjectPurchases.FieldByName('Invoiced').AsInteger := 1;
          UniMainModule.UProjectPurchases.Post;
          UniMainModule.UpdTrProjectPurchases.Commit;
          UniMainModule.UProjectPurchases.Close;
        Except
          UniMainModule.UProjectPurchases.Cancel;
          UniMainModule.UpdTrProjectPurchases.Rollback;
          UniMainModule.UProjectPurchases.Close;
          UniMainModule.show_error('Fout bij het opslaan van de gegevens.');
        End;
    end;
    OpenPurchases.Next;
  end;
  OpenPurchases.First;

  UniMainModule.UInvoices.Close;
  UniMainModule.UInvoiceLines.Close;

  UniMainModule.UCompany.FieldByName('InvoiceSequence').AsInteger :=
  UniMainModule.UCompany.FieldByName('InvoiceSequence').AsInteger + 1;
  Try
    UniMainModule.UCompany.Post;
    UniMainModule.UpdTrCompany.Commit;
    UniMainModule.UCompany.Close;
    Close;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrCompany.Rollback;
      UniMainModule.UCompany.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  End;
end;

procedure TInvoicesAddFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TInvoicesAddFrm.BtnChangeStatusClick(Sender: TObject);
begin
  if (OpenPurchases.RecordCount <> 0) and (OpenPurchases.FieldByName('Invoiced').AsInteger = 0) then
  begin
    OpenPurchases.Edit;
    if OpenPurchases.FieldByName('Selected').AsInteger = 0 then
      OpenPurchases.FieldByName('Selected').AsInteger := 1
    else
      OpenPurchases.FieldByName('Selected').AsInteger := 0;
    OpenPurchases.Post;
  end;
end;

procedure TInvoicesAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if EditAmountToInvoice.IsBlank then
  begin
    UniMainModule.show_warning('Het te factureren bedrag is een verplichte ingave.');
    EditAmountToInvoice.SetFocus;
    Exit;
  end;

  if EditAmountToInvoice.Value = 0 then
  begin
    UniMainModule.show_warning('Het te factureren bedrag mag niet 0 zijn.');
    EditAmountToInvoice.SetFocus;
    Exit;
  end;

  if Trim(EditDescriptionInvoice.Text) = '' then
  begin
    UniMainModule.show_warning('De omschrijving is een verplichte ingave.');
    EditDescriptionInvoice.SetFocus;
    Exit;
  end;

  if EditInvoiceDate.IsBlank then
  begin
    UniMainModule.show_warning('De factuurdatum is een verplichte ingave.');
    EditInvoiceDate.SetFocus;
    Exit;
  end;

  if EditPaymentTerm.IsBlank then
  begin
    UniMainModule.show_warning('De betalingstermijn is een verplichte ingave.');
    EditPaymentTerm.SetFocus;
    Exit;
  end;

  if EditPaymentTerm.Value < 0 then
  begin
    UniMainModule.show_warning('De betalingstermijn kan niet negatief zijn.');
    EditPaymentTerm.SetFocus;
    Exit;
  end;

  if EditStartDateRegistrations.IsBlank then
  begin
    UniMainModule.show_warning('De begindatum van de registraties is een verplichte ingave.');
    EditStartDateRegistrations.SetFocus;
    Exit;
  end;

  if EditEndDateRegistrations.IsBlank then
  begin
    UniMainModule.show_warning('De einddatum van de registraties is een verplichte ingave.');
    EditEndDateRegistrations.SetFocus;
    Exit;
  end;

  if EditEndDateRegistrations.DateTime < EditStartDateRegistrations.DateTime then
  begin
    UniMainModule.show_warning('De einddatum kan niet kleiner zijn dan de begindatum.');
    EditEndDateRegistrations.SetFocus;
    Exit;
  end;

  if ComboDocumentType.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('Het documenttype is een verplichte ingave.');
    ComboDocumentType.SetFocus;
    Exit;
  end;

  SplitInvoiceAddress := 0;
  //Check of het om een facturatie gaat op een custom adres
  if SplitInvoices = True
  then begin
         UniMainModule.Result_dbAction := 0;
         with SelectSplitInvoiceFrm
         do begin
              Start_module(Id_Project);
              ShowModal(CallBackSelectionSplitInvoice);
            end;
       end
  else Generate_invoice;
end;

procedure TInvoicesAddFrm.CallBackSelectionSplitInvoice(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
         SplitInvoiceAddress := UniMainModule.Result_dbAction;
         Generate_invoice;
       end;
end;

procedure TInvoicesAddFrm.CallBackPrintInvoice(Sender: TComponent; AResult: Integer);
begin
   MainForm.Confirm.Question('Inboeken factuur?', 'Wenst u de factuur in te boeken?', 'fa fa-exclamation-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes
      then Book_invoice
      else begin
             UniMainModule.UCompany.Cancel;
             UniMainModule.UCompany.Close;
           end;
    end);
end;

procedure TInvoicesAddFrm.Generate_invoice;
var Memo: TfrxMemoView;
    SQL:  string;
    Totaal, Openstaand: currency;
begin
  With ReportsMainFrm do
  begin
    UniMainModule.UCompany.Close;
    UniMainModule.UCompany.SQL.Clear;
    UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
    UniMainModule.UCompany.Open;
    Try
      UniMainModule.UCompany.Edit;
    Except
      UniMainModule.show_error('Kan geen nieuw factuurnummer opvragen, bedrijfsfiche in gebruik door een andere gebruiker.');
      UniMainModule.UCompany.Close;
      exit;
    End;

    //Check for the title
    (Invoice.FindObject('ShapeTitle') as TfrxShapeView).Visible := False;
    (Invoice.FindObject('LblTitle') as TfrxMemoView).Visible    := True;

    InvoiceNumber := UniMainModule.UCompany.FieldByName('InvoiceSequence').AsInteger;

    // Header
    Memo := Invoice.FindObject('LblAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Address').AsString);

    Memo := Invoice.FindObject('LblAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Zipcode').AsString + ' ' + UniMainModule.UCompany.FieldByName('City').AsString);

    Memo := Invoice.FindObject('LblTelephone') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Phone').AsString);

    Memo := Invoice.FindObject('LblEmail') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Email').AsString);

    Memo := Invoice.FindObject('LblWeb') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Website').AsString);

    // Central
    Memo := Invoice.FindObject('LblDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('City').AsString + ', ' + FormatDateTime('dd/mm/yyyy', EditInvoiceDate.DateTime));

    Memo := Invoice.FindObject('LblDocumentType') as TfrxMemoView;
    Memo.Memo.Clear;
    case ComboDocumentType.ItemIndex of
      0:
        Memo.Memo.Add('Een voorschot-ereloon betreffende prestaties geleverd voor: "' + LblProjectName.Caption + '"');
      1:
        Memo.Memo.Add('Een eindafschrift-ereloon betreffende prestaties geleverd voor: "' + LblProjectName.Caption + '"');
    end;

    Memo := Invoice.FindObject('LblPaymentLine1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add('Gelieve dit bedrag over te schrijven op onderstaand rekeningnummer binnen de ' + IntToStr(Trunc(EditPaymentTerm.Value)) + ' dagen na');

    Memo := Invoice.FindObject('LblCompanyName') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Name').AsString);

    // Footer
    Memo := Invoice.FindObject('LblBank11') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount').AsString);

    Memo := Invoice.FindObject('LblBank12') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban').AsString);

    Memo := Invoice.FindObject('LblBank13') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic').AsString);

    Memo := Invoice.FindObject('LblVat') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.UCompany.FieldByName('VatNumber').AsString);

    Memo := Invoice.FindObject('LblBank21') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount2').AsString);

    Memo := Invoice.FindObject('LblBank22') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban2').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban2').AsString);

    Memo := Invoice.FindObject('LblBank23') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic2').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic2').AsString);

    // Get Customer details
    if SplitInvoices = False
    then begin
           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select relation from projects where Id=' + IntToStr(Id_Project));
           UniMainModule.QWork.Open;
           RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;
           UniMainModule.QWork.Close;

           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select * from relations where Id=' + IntToStr(RelationId));
           UniMainModule.QWork.Open;

           Memo := Invoice.FindObject('LblCustomerName') as TfrxMemoView;
           Memo.Memo.Clear;
           Case UniMainModule.QWork.FieldByName('RelationType').AsInteger of
             0:
               begin
                 Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyName').AsString);
               end;
             1:
               begin
                 if not UniMainModule.QWork.FieldByName('Appelation').isNull then
                 begin
                   UniMainModule.QWork2.Close;
                   UniMainModule.QWork2.SQL.Clear;
                   UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('Appelation').AsString);
                   UniMainModule.QWork2.Open;
                   Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('Name').AsString + ' ' +
                     UniMainModule.QWork.FieldByName('FirstName').AsString));
                   UniMainModule.QWork2.Close;
                 end
                 else
                   Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
               end;
           End;

           Memo := Invoice.FindObject('LblAppelation') as TfrxMemoView;
           Memo.Memo.Clear;
           if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
           then begin
                  if (Trim(UniMainModule.QWork.FieldByName('Name').AsString) <> '') or
                     (Trim(UniMainModule.QWork.FieldByName('FirstName').asString) <> '')
                  then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
                end;

           Memo := Invoice.FindObject('LblCustomerAddress1') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Address').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumber').AsString));

           Memo := Invoice.FindObject('LblCustomerAddress2') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' + UniMainModule.QWork.FieldByName('City').AsString));

           Memo := Invoice.FindObject('LblCustomerVatNumber') as TfrxMemoView;
           Memo.Memo.Clear;
           if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0 then
           begin
             if Trim(UniMainModule.QWork.FieldByName('VatNumber').AsString) <> '' then
               Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString);
           end;
           UniMainModule.QWork.Close;
         end
    else begin
           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select * from projects where Id=' + IntToStr(Id_Project));
           UniMainModule.QWork.Open;

           RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;

           Memo := Invoice.FindObject('LblCustomerName') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoiceAddress = 1
           then begin
                  Case UniMainModule.QWork.FieldByName('RelationTypeCustom1').AsInteger of
                  0: Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyNameCustom1').AsString);
                  1: begin
                       if not UniMainModule.QWork.FieldByName('AppelationCustom1').isNull
                       then begin
                              UniMainModule.QWork2.Close;
                              UniMainModule.QWork2.SQL.Clear;
                              UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('AppelationCustom1').AsString);
                              UniMainModule.QWork2.Open;
                              Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('NameCustom1').AsString + ' ' +
                              UniMainModule.QWork.FieldByName('FirstNameCustom1').AsString));
                              UniMainModule.QWork2.Close;
                            end
                      else Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('NameCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom1').AsString));
                     end;
                  End;
                end
           else if SplitInvoiceAddress = 2
                then begin
                       Case UniMainModule.QWork.FieldByName('RelationTypeCustom2').AsInteger of
                       0: Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyNameCustom2').AsString);
                       1: begin
                            if not UniMainModule.QWork.FieldByName('AppelationCustom2').isNull
                            then begin
                                   UniMainModule.QWork2.Close;
                                   UniMainModule.QWork2.SQL.Clear;
                                   UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('AppelationCustom2').AsString);
                                   UniMainModule.QWork2.Open;
                                   Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('NameCustom2').AsString + ' ' +
                                   UniMainModule.QWork.FieldByName('FirstNameCustom2').AsString));
                                   UniMainModule.QWork2.Close;
                                 end
                            else Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('NameCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom2').AsString));
                          end;
                       End;
                     end;


           Memo := Invoice.FindObject('LblAppelation') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoiceAddress = 1
           then begin
                  if UniMainModule.QWork.FieldByName('RelationTypeCustom1').AsInteger = 0
                  then begin
                         if (Trim(UniMainModule.QWork.FieldByName('NameCustom1').AsString) <> '') or
                            (Trim(UniMainModule.QWork.FieldByName('FirstNameCustom1').asString) <> '')
                         then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('NameCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom1').AsString));
                       end;
                end
           else if SplitInvoiceAddress = 2
                then begin
                       if UniMainModule.QWork.FieldByName('RelationTypeCustom2').AsInteger = 0
                       then begin
                              if (Trim(UniMainModule.QWork.FieldByName('NameCustom2').AsString) <> '') or
                                 (Trim(UniMainModule.QWork.FieldByName('FirstNameCustom2').asString) <> '')
                              then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('NameCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom2').AsString));
                            end;
                     end;

           Memo := Invoice.FindObject('LblCustomerAddress1') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoiceAddress = 1
           then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('AddressCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumberCustom1').AsString))
           else if SplitInvoiceAddress = 2
                then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('AddressCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumberCustom2').AsString));

           Memo := Invoice.FindObject('LblCustomerAddress2') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoiceAddress = 1
           then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCodeCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('CityCustom1').AsString))
           else if SplitInvoiceAddress = 2
                then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCodeCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('CityCustom2').AsString));

           Memo := Invoice.FindObject('LblCustomerVatNumber') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoiceAddress = 1
           then begin
                  if UniMainModule.QWork.FieldByName('RelationTypeCustom1').AsInteger = 0
                  then begin
                         if Trim(UniMainModule.QWork.FieldByName('VatNumberCustom1').AsString) <> ''
                         then Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumberCustom1').AsString);
                       end;
                end
           else if SplitInvoiceAddress = 2
                then begin
                       if UniMainModule.QWork.FieldByName('RelationTypeCustom2').AsInteger = 0
                       then begin
                              if Trim(UniMainModule.QWork.FieldByName('VatNumberCustom2').AsString) <> ''
                              then Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumberCustom2').AsString);
                            end;
                     end;
           UniMainModule.QWork.Close;
         end;

    // Get sequence number invoice
    Memo := Invoice.FindObject('InvoiceNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(IntToStr(InvoiceNumber));

    // Fill the invoice articles
    InvoiceLinesPrint.First;
    while not InvoiceLinesPrint.Eof do
      InvoiceLinesPrint.Delete;

    InvoiceLinesPrint.Append;
    InvoiceLinesPrint.FieldByName('Description').AsString := EditDescriptionInvoice.Text;
    InvoiceLinesPrint.FieldByName('Amount').AsCurrency := EditAmountToInvoice.Value;
    InvoiceLinesPrint.Post;

    // Check for selected purchases
    OpenPurchases.First;
    while not OpenPurchases.Eof do
    begin
      if OpenPurchases.FieldByName('Selected').AsInteger = 1 then
      begin
        InvoiceLinesPrint.Append;
        InvoiceLinesPrint.FieldByName('Description').AsString := OpenPurchases.FieldByName('Description').AsString;
        InvoiceLinesPrint.FieldByName('Amount').AsCurrency := OpenPurchases.FieldByName('TotalPrice').AsCurrency;
        InvoiceLinesPrint.Post;
      end;
      OpenPurchases.Next;
    end;
    OpenPurchases.First;

    SubTotal := 0;

    InvoiceLinesPrint.First;
    while not InvoiceLinesPrint.Eof do
    begin
      SubTotal := SubTotal + InvoiceLinesPrint.FieldByName('Amount').AsCurrency;
      InvoiceLinesPrint.Next;
    end;

    VatAmount := SubTotal / 100 * 21;
    TotalAmount := SubTotal + VatAmount;

    Memo := Invoice.FindObject('VatAmount') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', VatAmount));

    Memo := Invoice.FindObject('GeneralTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', TotalAmount));

    if TotalAmount < 0
    then begin
           Memo := Invoice.FindObject('LblInvoiceType') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add('CREDITNOTA');

           Memo := Invoice.FindObject('LblDocumentType') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add('Een creditnota betreffende prestaties geleverd voor:');
         end;

    InvoiceTimeRegistrations.Close;
    InvoiceTimeRegistrations.SQL.Clear;
    SQL := 'SELECT TIMEREGISTRATIONS.ID, TIMEREGISTRATIONS.REGISTRATIONDATE, TIMEREGISTRATIONS.STARTTIMEVALUE, TIMEREGISTRATIONS.ENDTIMEVALUE, ' +
           'TIMEREGISTRATIONS.SALDOVALUE, TIMEREGISTRATIONS.HOURLYRATE, TIMEREGISTRATIONS.HOURLYRATE * TIMEREGISTRATIONS.SALDOVALUE AS TOTALAMOUNTLINE, ' +
           'BASICTABLES.DUTCH, TIMEREGISTRATIONS.DESCRIPTION ' +
           'FROM TIMEREGISTRATIONS ' +
           'LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = BASICTABLES.ID) ' +
           'WHERE TIMEREGISTRATIONS.REMOVED = ''0'' AND PROJECT=' + IntToStr(Id_Project);

    SQL := SQL + ' and RegistrationDate >=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EditStartDateRegistrations.DateTime));
    SQL := SQL + ' and RegistrationDate <=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EditEndDateRegistrations.DateTime));

    SQL := SQL + ' ORDER BY TIMEREGISTRATIONS.REGISTRATIONDATE';
    InvoiceTimeRegistrations.SQL.Add(SQL);
    InvoiceTimeRegistrations.Open;

    Purchases.Close;
    Purchases.SQL.Clear;
    SQL := 'SELECT * from projectpurchases where project=' + IntToStr(Id_Project) +
           ' and invoiced=1 order by datepurchase';
    Purchases.SQL.Add(SQL);
    Purchases.Open;

    //Get the total amount of the project
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select SUM(SALDOVALUE * HOURLYRATE) AS AMOUNT from timeregistrations where project=' + IntToStr(Id_Project) +
                                ' and removed=''0''');
    UniMainModule.QWork.Open;
    Totaal := UniMainModule.QWork.FieldByName('Amount').AsCurrency;
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select sum(SalesPrice) as TOTALAMOUNT from Projectpurchases where Project=' + IntToStr(Id_Project) +
                                ' and Invoiced=1');
    UniMainModule.QWork.Open;
    Totaal := Totaal + UniMainModule.QWork.Fieldbyname('TotalAmount').asCurrency;
    UniMainModule.QWork.Close;

    Memo := Invoice.FindObject('SummaryTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Totaal));


    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('select sum(amount) AS TOTALAMOUNT from invoices where project=' + IntToStr(Id_Project));
    UniMainModule.QWork.Open;
    Openstaand := Totaal - UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency;
    UniMainModule.QWork.Close;

    Memo := Invoice.FindObject('SummaryOpen') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Openstaand));

    Invoices.Close;
    Invoices.SQL.Clear;
    Invoices.SQL.Add('Select * from invoices where Project=' + IntToStr(Id_Project) + ' order by InvoiceNumber');
    Invoices.Open;

    Invoice.Pages[2].Visible := CheckboxIncludeRegistrations.Checked;

    Prepare_invoice;
    ShowModal(CallBackPrintInvoice);
  end;
end;

procedure TInvoicesAddFrm.GridPurchasesDrawColumnCell(Sender: TObject; ACol, ARow: Integer; Column: TUniDBGridColumn;
  Attribs: TUniCellAttribs);
begin
  if (OpenPurchases.FieldByName('Invoiced').asInteger  = 1)
  then Attribs.Style.Style := 'text-decoration: line-through';
end;

procedure TInvoicesAddFrm.GridPurchasesFieldImage(const Column: TUniDBGridColumn; const AField: TField;
  var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(UpperCase(AField.FieldName), 'SELECTED') then
  begin
    DoNotDispose := True; // we provide an static image so do not free it.
    Case AField.AsInteger of
      0: OutImage := MainForm.ImageEmpty.Picture.Graphic;
      1: OutImage := MainForm.ImageGreen.Picture.Graphic;
    End;
  end;
end;

procedure TInvoicesAddFrm.Start_create_invoice(ProjectId: longint);
var
  TotalInvoiced: currency;
  TotalActivities: currency;
  TotalPurchases: currency;
begin
  Id_Project := ProjectId;
  Caption := 'Aanmaken factuur';
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select id, name, splitinvoices, paymentterm, projecttype, senddetailledreport from projects where id=' + IntToStr(ProjectId));
  UniMainModule.QWork.Open;
  LblProjectName.Text := UniMainModule.QWork.FieldByName('Name').AsString;
  EditPaymentTerm.Value := UniMainModule.QWork.FieldByName('Paymentterm').AsInteger;
  if UniMainModule.QWork.Fieldbyname('SplitInvoices').AsInteger = 1
  then SplitInvoices := True
  else SplitInvoices := False;

  if UniMainModule.QWork.FieldByName('Senddetailledreport').AsInteger = 1
  then CheckboxIncludeRegistrations.Checked := True
  else CheckboxIncludeRegistrations.Checked := False;

  // Get the open amount of the project
  UniMainModule.QWork2.Close;
  UniMainModule.QWork2.SQL.Clear;
  UniMainModule.QWork2.SQL.Add('select sum(amount) AS TOTALAMOUNT from invoices where project=' + IntToStr(ProjectId));
  UniMainModule.QWork2.Open;
  TotalInvoiced := UniMainModule.QWork2.FieldByName('TotalAmount').AsCurrency;
  UniMainModule.QWork2.Close;
  UniMainModule.QWork2.SQL.Clear;
  UniMainModule.QWork2.SQL.Add('Select SUM(SALDOVALUE * HOURLYRATE) AS AMOUNT from timeregistrations where project=' + IntToStr(ProjectId) +
    ' and removed=''0''');
  UniMainModule.QWork2.Open;
  TotalActivities := UniMainModule.QWork2.FieldByName('Amount').AsCurrency;
  UniMainModule.QWork2.Close;

  //Get the total amount of purchases
  UniMainModule.QWork2.Close;
  UniMainModule.QWork2.SQL.Clear;
  UniMainModule.QWork2.SQL.Add('SELECT sum(TOTALSALESPRICE) AS TOTALPURCHASES FROM PROJECTPURCHASES PUR WHERE PUR.PROJECT = ' + IntToStr(ProjectId));
  UniMainModule.QWork2.Open;
  TotalPurchases := UniMainModule.QWork2.FieldByName('TotalPurchases').AsCurrency;
  UniMainModule.QWork2.Close;

  LblOpenAmountProject.Caption := FormatCurr('#,##0.00 �', TotalActivities + TotalPurchases - TotalInvoiced);
  EditAmountToInvoice.Value    := TotalActivities + TotalPurchases - TotalInvoiced;

  if UniMainModule.QWork.FieldByName('Projecttype').AsInteger = 1 then
  begin
    CheckboxIncludeRegistrations.Visible := False;
    LblStartDateRegistrations.Visible := False;
    EditStartDateRegistrations.Visible := False;
    LblEndDateRegistrations.Visible := False;
    EditEndDateRegistrations.Visible := False;
  end;
  UniMainModule.QWork.Close;

  EditInvoiceDate.DateTime := Date;
  //Search the first registration for this project
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select first 1 * from timeregistrations where project=' + IntToStr(ProjectId) + ' order by registrationdate asc');
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.RecordCount = 1
  then EditStartDateRegistrations.DateTime := UniMainModule.QWork.FieldByName('Registrationdate').AsDateTime
  else EditStartDateRegistrations.DateTime := Date;
  UniMainModule.QWork.Close;
  EditEndDateRegistrations.DateTime   := Date;
  ComboDocumentType.ItemIndex := 0;

  // Fill the open purchases
  OpenPurchases.First;
  while not OpenPurchases.Eof do
    OpenPurchases.Next;

  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from projectpurchases where project=' + IntToStr(ProjectId) + ' order by invoiced, description');
  UniMainModule.QWork.Open;
  UniMainModule.QWork.First;
  while not UniMainModule.QWork.Eof do
  begin
    OpenPurchases.Append;
    OpenPurchases.FieldByName('PurchaseId').AsInteger := UniMainModule.QWork.FieldByName('Id').AsInteger;
    OpenPurchases.FieldByName('Selected').AsInteger := 0;
    OpenPurchases.FieldByName('DatePurchase').AsDateTime := UniMainModule.QWork.FieldByName('DatePurchase').AsDateTime;
    OpenPurchases.FieldByName('Description').AsString := UniMainModule.QWork.FieldByName('Description').AsString;
    OpenPurchases.FieldByName('Quantity').AsInteger := UniMainModule.QWork.FieldByName('Quantity').AsInteger;
    OpenPurchases.FieldByName('Price').AsCurrency := UniMainModule.QWork.FieldByName('SalesPrice').AsCurrency;
    OpenPurchases.FieldByName('TotalPrice').AsCurrency := UniMainModule.QWork.FieldByName('TotalSalesPrice').AsCurrency;
    OpenPurchases.FieldByName('Invoiced').AsInteger := UniMainModule.QWork.FieldByName('Invoiced').AsInteger;
    OpenPurchases.Post;
    UniMainModule.QWork.Next;
  end;
  UniMainModule.QWork.Close;
  OpenPurchases.First;
end;

procedure TInvoicesAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
