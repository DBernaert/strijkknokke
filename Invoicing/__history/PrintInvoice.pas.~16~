unit PrintInvoice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniPanel, uniDateTimePicker, uniLabel, uniGUIBaseClasses,
  uniCheckBox;

type
  TPrintInvoiceFrm = class(TUniForm)
    CheckboxIncludeRegistrations: TUniCheckBox;
    LblStartDateRegistrations: TUniLabel;
    EditStartDateRegistrations: TUniDateTimePicker;
    LblEndDateRegistrations: TUniLabel;
    EditEndDateRegistrations: TUniDateTimePicker;
    PanelFooter: TUniContainerPanel;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
    DocumentId:   longint;
    DocumentType: integer;
  public
    { Public declarations }
    procedure Start_module(InvoiceId: longint; ExtraTitle: string);
  end;

function PrintInvoiceFrm: TPrintInvoiceFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, frxClass, ReportsMain;

function PrintInvoiceFrm: TPrintInvoiceFrm;
begin
  Result := TPrintInvoiceFrm(UniMainModule.GetFormInstance(TPrintInvoiceFrm));
end;

{ TPrintInvoiceFrm }

procedure TPrintInvoiceFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TPrintInvoiceFrm.BtnSaveClick(Sender: TObject);
var Memo: TfrxMemoView;
    SQL:  string;

    InvoiceNumber: longint;
    Id_Project:    longint;
    RelationId:    longint;
    SubTotal, VatAmount, TotalAmount: currency;
    SplitInvoice:  integer;
    Totaal, Openstaand: currency;
begin
  With ReportsMainFrm do
  begin
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select * from invoices where id=' + IntToStr(DocumentId));
    UniMainModule.QWork.Open;

    InvoiceNumber := UniMainModule.QWork.FieldByName('InvoiceNumber').AsInteger;
    Id_Project    := UniMainModule.QWork.FieldByName('Project').AsInteger;
    SplitInvoice  := UniMainModule.QWork.FieldByName('SplitInvoice').AsInteger;

    UniMainModule.UCompany.Close;
    UniMainModule.UCompany.SQL.Clear;
    UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
    UniMainModule.UCompany.Open;

    // Header
    Memo := Invoice.FindObject('LblAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Address').AsString);

    Memo := Invoice.FindObject('LblAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Zipcode').AsString + ' ' + UniMainModule.UCompany.FieldByName('City').AsString);

    Memo := Invoice.FindObject('LblTelephone') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Phone').AsString);

    Memo := Invoice.FindObject('LblEmail') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Email').AsString);

    Memo := Invoice.FindObject('LblWeb') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Website').AsString);

    // Central
    Memo := Invoice.FindObject('LblDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('City').AsString + ', ' + FormatDateTime('dd/mm/yyyy', UniMainModule.QWork.FieldByName('InvoiceDate').AsDateTime));

    Memo := Invoice.FindObject('LblDocumentType') as TfrxMemoView;
    Memo.Memo.Clear;
    Case UniMainModule.QWork.FieldByName('InvoiceType').AsInteger of
     0: Memo.Memo.Add('Een voorschot-ereloon betreffende prestaties geleverd voor:');
     1: Memo.Memo.Add('Een eindafschrift-ereloon betreffende prestaties geleverd voor:');
    end;

    Memo := Invoice.FindObject('LblPaymentLine1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add('Gelieve dit bedrag over te schrijven op onderstaand rekeningnummer binnen de ' + UniMainModule.QWork.FieldByName('PaymentTerm').asString + ' dagen na');

    Memo := Invoice.FindObject('LblCompanyName') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Name').AsString);

    // Footer
    Memo := Invoice.FindObject('LblBank11') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount').AsString);

    Memo := Invoice.FindObject('LblBank12') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban').AsString);

    Memo := Invoice.FindObject('LblBank13') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic').AsString);

    Memo := Invoice.FindObject('LblVat') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.UCompany.FieldByName('VatNumber').AsString);

    Memo := Invoice.FindObject('LblBank21') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount2').AsString);

    Memo := Invoice.FindObject('LblBank22') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban2').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban2').AsString);

    Memo := Invoice.FindObject('LblBank23') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic2').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic2').AsString);

    // Get Customer details
    if SplitInvoice = 0
    then begin
           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select relation from projects where Id=' + IntToStr(Id_Project));
           UniMainModule.QWork.Open;
           RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;
           UniMainModule.QWork.Close;

           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select * from relations where Id=' + IntToStr(RelationId));
           UniMainModule.QWork.Open;

           Memo := Invoice.FindObject('LblCustomerName') as TfrxMemoView;
           Memo.Memo.Clear;
           Case UniMainModule.QWork.FieldByName('RelationType').AsInteger of
             0:
               begin
                 Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyName').AsString);
               end;
             1:
               begin
                 if not UniMainModule.QWork.FieldByName('Appelation').isNull then
                 begin
                   UniMainModule.QWork2.Close;
                   UniMainModule.QWork2.SQL.Clear;
                   UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('Appelation').AsString);
                   UniMainModule.QWork2.Open;
                   Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('Name').AsString + ' ' +
                     UniMainModule.QWork.FieldByName('FirstName').AsString));
                   UniMainModule.QWork2.Close;
                 end
                 else
                   Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
               end;
           End;

           Memo := Invoice.FindObject('LblAppelation') as TfrxMemoView;
           Memo.Memo.Clear;
           if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
           then begin
                  if (Trim(UniMainModule.QWork.FieldByName('Name').AsString) <> '') or
                     (Trim(UniMainModule.QWork.FieldByName('FirstName').asString) <> '')
                  then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
                end;

           Memo := Invoice.FindObject('LblCustomerAddress1') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Address').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumber').AsString));

           Memo := Invoice.FindObject('LblCustomerAddress2') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' + UniMainModule.QWork.FieldByName('City').AsString));

           Memo := Invoice.FindObject('LblCustomerVatNumber') as TfrxMemoView;
           Memo.Memo.Clear;
           if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0 then
           begin
             if Trim(UniMainModule.QWork.FieldByName('VatNumber').AsString) <> '' then
               Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString);
           end;
           UniMainModule.QWork.Close;
         end
    else begin
           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select * from projects where Id=' + IntToStr(Id_Project));
           UniMainModule.QWork.Open;
           Memo := Invoice.FindObject('LblCustomerName') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoice = 1
           then begin
                  Case UniMainModule.QWork.FieldByName('RelationTypeCustom1').AsInteger of
                  0: Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyNameCustom1').AsString);
                  1: begin
                       if not UniMainModule.QWork.FieldByName('AppelationCustom1').isNull
                       then begin
                              UniMainModule.QWork2.Close;
                              UniMainModule.QWork2.SQL.Clear;
                              UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('AppelationCustom1').AsString);
                              UniMainModule.QWork2.Open;
                              Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('NameCustom1').AsString + ' ' +
                              UniMainModule.QWork.FieldByName('FirstNameCustom1').AsString));
                              UniMainModule.QWork2.Close;
                            end
                      else Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('NameCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom1').AsString));
                     end;
                  End;
                end
           else if SplitInvoice = 2
                then begin
                       Case UniMainModule.QWork.FieldByName('RelationTypeCustom2').AsInteger of
                       0: Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyNameCustom2').AsString);
                       1: begin
                            if not UniMainModule.QWork.FieldByName('AppelationCustom2').isNull
                            then begin
                                   UniMainModule.QWork2.Close;
                                   UniMainModule.QWork2.SQL.Clear;
                                   UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('AppelationCustom2').AsString);
                                   UniMainModule.QWork2.Open;
                                   Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('NameCustom2').AsString + ' ' +
                                   UniMainModule.QWork.FieldByName('FirstNameCustom2').AsString));
                                   UniMainModule.QWork2.Close;
                                 end
                            else Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('NameCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom2').AsString));
                          end;
                       End;
                     end;


           Memo := Invoice.FindObject('LblAppelation') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoice = 1
           then begin
                  if UniMainModule.QWork.FieldByName('RelationTypeCustom1').AsInteger = 0
                  then begin
                         if (Trim(UniMainModule.QWork.FieldByName('NameCustom1').AsString) <> '') or
                            (Trim(UniMainModule.QWork.FieldByName('FirstNameCustom1').asString) <> '')
                         then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('NameCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom1').AsString));
                       end;
                end
           else if SplitInvoice = 2
                then begin
                       if UniMainModule.QWork.FieldByName('RelationTypeCustom2').AsInteger = 0
                       then begin
                              if (Trim(UniMainModule.QWork.FieldByName('NameCustom2').AsString) <> '') or
                                 (Trim(UniMainModule.QWork.FieldByName('FirstNameCustom2').asString) <> '')
                              then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('NameCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstNameCustom2').AsString));
                            end;
                     end;

           Memo := Invoice.FindObject('LblCustomerAddress1') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoice = 1
           then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('AddressCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumberCustom1').AsString))
           else if SplitInvoice = 2
                then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('AddressCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumberCustom2').AsString));

           Memo := Invoice.FindObject('LblCustomerAddress2') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoice = 1
           then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCodeCustom1').AsString + ' ' + UniMainModule.QWork.FieldByName('CityCustom1').AsString))
           else if SplitInvoice = 2
                then Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCodeCustom2').AsString + ' ' + UniMainModule.QWork.FieldByName('CityCustom2').AsString));

           Memo := Invoice.FindObject('LblCustomerVatNumber') as TfrxMemoView;
           Memo.Memo.Clear;
           if SplitInvoice = 1
           then begin
                  if UniMainModule.QWork.FieldByName('RelationTypeCustom1').AsInteger = 0
                  then begin
                         if Trim(UniMainModule.QWork.FieldByName('VatNumberCustom1').AsString) <> ''
                         then Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumberCustom1').AsString);
                       end;
                end
           else if SplitInvoice = 2
                then begin
                       if UniMainModule.QWork.FieldByName('RelationTypeCustom2').AsInteger = 0
                       then begin
                              if Trim(UniMainModule.QWork.FieldByName('VatNumberCustom2').AsString) <> ''
                              then Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumberCustom2').AsString);
                            end;
                     end;
           UniMainModule.QWork.Close;
         end;

    // Get sequence number invoice
    Memo := Invoice.FindObject('InvoiceNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(IntToStr(InvoiceNumber));

    // Fill the invoice articles
    InvoiceLinesPrint.First;
    while not InvoiceLinesPrint.Eof
    do InvoiceLinesPrint.Delete;

    UniMainModule.UInvoiceLines.Close;
    UniMainModule.UInvoiceLines.SQL.Clear;
    UniMainModule.UInvoiceLines.SQL.Add('Select * from invoicelines where invoice=' + IntToStr(DocumentId));
    UniMainModule.UInvoiceLines.Open;
    UniMainModule.UInvoiceLines.First;
    while not UniMainModule.UInvoiceLines.EOF
    do begin
         InvoiceLinesPrint.Append;
         InvoiceLinesPrint.FieldByName('Description').AsString := UniMainModule.UInvoiceLines.FieldByName('Description').AsString;
         InvoiceLinesPrint.FieldByName('Amount').AsCurrency    := UniMainModule.UInvoiceLines.FieldByName('Amount').AsCurrency;
         InvoiceLinesPrint.Post;
         UniMainModule.UInvoiceLines.Next;
       end;
    UniMainModule.UInvoiceLines.Close;

    SubTotal := 0;

    InvoiceLinesPrint.First;
    while not InvoiceLinesPrint.Eof do
    begin
      SubTotal := SubTotal + InvoiceLinesPrint.FieldByName('Amount').AsCurrency;
      InvoiceLinesPrint.Next;
    end;

    VatAmount := SubTotal / 100 * 21;
    TotalAmount := SubTotal + VatAmount;

    Memo := Invoice.FindObject('VatAmount') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', VatAmount));

    Memo := Invoice.FindObject('GeneralTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', TotalAmount));

    if TotalAmount < 0
    then begin
           Memo := Invoice.FindObject('LblInvoiceType') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add('CREDITNOTA');

           Memo := Invoice.FindObject('LblDocumentType') as TfrxMemoView;
           Memo.Memo.Clear;
           Memo.Memo.Add('Een creditnota betreffende prestaties geleverd voor:');
         end;


    InvoiceTimeRegistrations.Close;
    InvoiceTimeRegistrations.SQL.Clear;
    SQL := 'SELECT TIMEREGISTRATIONS.ID, TIMEREGISTRATIONS.REGISTRATIONDATE, TIMEREGISTRATIONS.STARTTIMEVALUE, TIMEREGISTRATIONS.ENDTIMEVALUE, ' +
           'TIMEREGISTRATIONS.SALDOVALUE, BASICTABLES.DUTCH, TIMEREGISTRATIONS.DESCRIPTION ' +
           'FROM TIMEREGISTRATIONS ' +
           'LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = BASICTABLES.ID) ' +
           'WHERE TIMEREGISTRATIONS.REMOVED = ''0'' AND PROJECT=' + IntToStr(Id_Project);

    SQL := SQL + ' and RegistrationDate >=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EditStartDateRegistrations.DateTime));
    SQL := SQL + ' and RegistrationDate <=' + QuotedStr(FormatDateTime('yyyy-mm-dd', EditEndDateRegistrations.DateTime));

    SQL := SQL + ' ORDER BY TIMEREGISTRATIONS.REGISTRATIONDATE';
    InvoiceTimeRegistrations.SQL.Add(SQL);
    InvoiceTimeRegistrations.Open;

    //Get the total amount of the project
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select SUM(SALDOVALUE * HOURLYRATE) AS AMOUNT from timeregistrations where project=' + IntToStr(Id_Project) +
                                ' and removed=''0''');
    UniMainModule.QWork.Open;
    Totaal := UniMainModule.QWork.FieldByName('Amount').AsCurrency;
    UniMainModule.QWork.Close;
    Memo := Invoice.FindObject('SummaryTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Totaal));


    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('select sum(amount) AS TOTALAMOUNT from invoices where project=' + IntToStr(Id_Project));
    UniMainModule.QWork.Open;
    Openstaand := Totaal - UniMainModule.QWork.FieldByName('TotalAmount').AsCurrency;
    UniMainModule.QWork.Close;
    Memo := Invoice.FindObject('SummaryOpen') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Openstaand));

    Invoices.Close;
    Invoices.SQL.Clear;
    Invoices.SQL.Add('Select * from invoices where Project=' + IntToStr(Id_Project) + ' order by InvoiceNumber');
    Invoices.Open;

    Invoice.Pages[2].Visible := CheckboxIncludeRegistrations.Checked;
    Prepare_invoice;
  end;
end;

procedure TPrintInvoiceFrm.Start_module(InvoiceId: longint; ExtraTitle: string);
var ProjectId:  longint;
begin
  DocumentType := 1;
  DocumentId   := InvoiceId;

  //Get the id of the project
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select project from invoices where id=' + IntToStr(InvoiceId));
  UniMainModule.QWork.Open;
  ProjectId := UniMainModule.QWork.FieldByName('Project').AsInteger;
  UniMainModule.QWork.Close;

  //Get the flag for sending details
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select senddetailledreport from projects where id=' + IntToStr(ProjectId));
  UniMainModule.QWork.Open;

  if UniMainModule.QWork.FieldByName('Senddetailledreport').AsInteger = 1
  then CheckboxIncludeRegistrations.Checked := True
  else CheckboxIncludeRegistrations.Checked := False;

  UniMainModule.QWork.Close;

  //Search the first registration for this project
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select first 1 * from timeregistrations where project=' + IntToStr(ProjectId) + ' order by registrationdate asc');
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.RecordCount = 1
  then EditStartDateRegistrations.DateTime := UniMainModule.QWork.FieldByName('Registrationdate').AsDateTime
  else EditStartDateRegistrations.DateTime := Date;
  UniMainModule.QWork.Close;
  EditEndDateRegistrations.DateTime   := Date;
end;

procedure TPrintInvoiceFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
