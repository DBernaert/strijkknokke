unit BookInvoicePayment;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniEdit, uniButton, UniThemeButton, uniPanel;

type
  TBookInvoicePaymentFrm = class(TUniForm)
    EditAmount: TUniFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    IdInvoice: longint;
  public
    { Public declarations }
    procedure Initialize_payment(Invoice: longint);
  end;

function BookInvoicePaymentFrm: TBookInvoicePaymentFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function BookInvoicePaymentFrm: TBookInvoicePaymentFrm;
begin
  Result := TBookInvoicePaymentFrm(UniMainModule.GetFormInstance(TBookInvoicePaymentFrm));
end;

{ TBookInvoicePaymentFrm }

procedure TBookInvoicePaymentFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  Close;
end;

procedure TBookInvoicePaymentFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  if (EditAmount.IsBlank) or (EditAmount.Value = 0)
  then begin
         UniMainModule.show_warning('Het bedrag is een verplichte ingave.');
         EditAmount.SetFocus;
         Exit;
       end;

  UniMainModule.UInvoices.Close;
  UniMainModule.UInvoices.SQL.Clear;
  UniMainModule.UInvoices.SQL.Add('Select * from Invoices where Id=' + IntToStr(IdInvoice));
  UniMainModule.UInvoices.Open;
  Try
    UniMainModule.UInvoices.Edit;
    UniMainModule.UInvoices.FieldByName('TotalPayed').AsCurrency :=
    UniMainModule.UInvoices.FieldByName('TotalPayed').AsCurrency + EditAmount.Value;

    if UniMainModule.UInvoices.Fieldbyname('TotalPayed').AsCurrency =
       UniMainModule.UInvoices.FieldByName('TotalAmount').AsCurrency
    then UniMainModule.UInvoices.FieldByName('Invoicestage').AsInteger := 5
    else UniMainModule.UInvoices.FieldByName('Invoicestage').AsInteger := 1;

    UniMainModule.UInvoices.Post;
    UniMainModule.UpdTrInvoices.Commit;
    UniMainModule.UInvoices.Close;
    UniMainModule.Result_dbAction := 1;
    Close;
  Except
    UniMainModule.UInvoices.Cancel;
    UniMainModule.UpdTrInvoices.Rollback;
    UniMainModule.show_error('Fout bij het inboeken van de betaling.');
    UniMainModule.UInvoices.Close;
    Close;
  End;
end;

procedure TBookInvoicePaymentFrm.Initialize_payment(Invoice: longint);
begin
  Caption := 'Inboeken betaling';
  IdInvoice        := Invoice;
  EditAmount.Value := 0;
end;

procedure TBookInvoicePaymentFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
