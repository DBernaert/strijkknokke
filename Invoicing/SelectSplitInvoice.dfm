object SelectSplitInvoiceFrm: TSelectSplitInvoiceFrm
  Left = 0
  Top = 0
  ClientHeight = 225
  ClientWidth = 569
  Caption = 'Selectie opgesplitse facturatie'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LblSelection: TUniLabel
    Left = 16
    Top = 16
    Width = 374
    Height = 13
    Hint = ''
    Caption = 
      'Selecteer hieronder de gegevens die u wenst te gebruiken voor de' +
      ' facturatie:'
    TabOrder = 0
  end
  object PanelAddress1: TUniPanel
    Left = 16
    Top = 40
    Width = 265
    Height = 129
    Hint = ''
    TabOrder = 1
    BorderStyle = ubsSolid
    Caption = ''
    object LblCompanyName1: TUniLabel
      Left = 8
      Top = 8
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblCompanyName1'
      TabOrder = 1
    end
    object LblName1: TUniLabel
      Left = 8
      Top = 24
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblName1'
      TabOrder = 2
    end
    object LblAdddress1: TUniLabel
      Left = 8
      Top = 40
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblAdddress1'
      TabOrder = 3
    end
    object LblAdddress21: TUniLabel
      Left = 8
      Top = 56
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblAdddress21'
      TabOrder = 4
    end
    object LblVat1: TUniLabel
      Left = 8
      Top = 72
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblVat1'
      TabOrder = 5
    end
    object BtnAddress1: TUniThemeButton
      Left = 80
      Top = 96
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Selecteren'
      ParentFont = False
      TabStop = False
      TabOrder = 6
      Images = UniMainModule.ImageList
      ImageIndex = 8
      OnClick = BtnAddress1Click
      ButtonTheme = uctPrimary
    end
  end
  object PanelAddress2: TUniPanel
    Left = 288
    Top = 40
    Width = 265
    Height = 129
    Hint = ''
    TabOrder = 2
    BorderStyle = ubsSolid
    Caption = ''
    object LblCompanyName2: TUniLabel
      Left = 8
      Top = 8
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblCompanyName2'
      TabOrder = 1
    end
    object LblName2: TUniLabel
      Left = 8
      Top = 24
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblName2'
      TabOrder = 2
    end
    object LblAdddress2: TUniLabel
      Left = 8
      Top = 40
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblAdddress2'
      TabOrder = 3
    end
    object LblAdddress22: TUniLabel
      Left = 8
      Top = 56
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblAdddress22'
      TabOrder = 4
    end
    object LblVat2: TUniLabel
      Left = 8
      Top = 72
      Width = 249
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblVat2'
      TabOrder = 5
    end
    object BtnAddress2: TUniThemeButton
      Left = 75
      Top = 96
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Selecteren'
      ParentFont = False
      TabStop = False
      TabOrder = 6
      Images = UniMainModule.ImageList
      ImageIndex = 8
      OnClick = BtnAddress2Click
      ButtonTheme = uctPrimary
    end
  end
  object BtnCancel: TUniThemeButton
    Left = 448
    Top = 184
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 3
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
end
