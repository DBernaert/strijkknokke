object LoginFrm: TLoginFrm
  Left = 0
  Top = 0
  ClientHeight = 563
  ClientWidth = 419
  Caption = 'WaveDesk Coconne login'
  OnShow = UniLoginFormShow
  Color = clWhite
  BorderStyle = bsNone
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditLogin
  ClientEvents.UniEvents.Strings = (
    
      'window.beforeInit=function window.beforeInit(sender, config)'#13#10'{'#13 +
      #10'  config.baseCls='#39'frmLogin'#39';'#13#10'}')
  Layout = 'border'
  OnSubmit = BtnLoginClick
  OnCancel = BtnCancelClick
  OnCreate = UniLoginFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ContainerBody: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 419
    Height = 563
    Hint = ''
    ParentColor = False
    Color = clWhite
    Align = alClient
    TabOrder = 0
    LayoutConfig.Region = 'center'
    object ImageLogo: TUniImage
      Left = 9
      Top = 8
      Width = 400
      Height = 329
      Hint = ''
      AutoSize = True
      Url = 'images/Logo.png'
    end
    object LblCopyright: TUniLabel
      Left = 119
      Top = 360
      Width = 182
      Height = 13
      Hint = ''
      Caption = #169' 2019 Copyright  Adm-Concept'
      ParentFont = False
      Font.Style = [fsBold]
      TabOrder = 2
    end
    object LblLogin: TUniLabel
      Left = 33
      Top = 448
      Width = 25
      Height = 13
      Hint = ''
      TextConversion = txtHTML
      Caption = 'Login'
      ParentFont = False
      TabOrder = 3
    end
    object EditLogin: TUniEdit
      Left = 121
      Top = 448
      Width = 265
      Hint = ''
      Text = ''
      TabOrder = 4
    end
    object LblPassWord: TUniLabel
      Left = 33
      Top = 480
      Width = 61
      Height = 13
      Hint = ''
      TextConversion = txtHTML
      Caption = 'Wachtwoord'
      ParentFont = False
      TabOrder = 5
    end
    object EditPassWord: TUniEdit
      Left = 121
      Top = 480
      Width = 265
      Hint = ''
      PasswordChar = '$'
      Text = ''
      TabOrder = 6
    end
    object LblVersion: TUniLabel
      Left = 129
      Top = 381
      Width = 161
      Height = 13
      Hint = ''
      Alignment = taCenter
      AutoSize = False
      Caption = 'LblVersion'
      ParentFont = False
      TabOrder = 7
    end
    object BtnLogin: TUniLabel
      Left = 242
      Top = 528
      Width = 30
      Height = 13
      Cursor = crHandPoint
      Hint = ''
      Caption = 'Login'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 8
      OnClick = BtnLoginClick
    end
    object BtnCancel: TUniLabel
      Left = 322
      Top = 528
      Width = 58
      Height = 13
      Cursor = crHandPoint
      Hint = ''
      Caption = 'Annuleren'
      ParentFont = False
      Font.Style = [fsBold, fsUnderline]
      TabOrder = 9
      OnClick = BtnCancelClick
    end
  end
end
