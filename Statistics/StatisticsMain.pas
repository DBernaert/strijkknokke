unit StatisticsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniGUIBaseClasses, uniPanel, Vcl.Imaging.pngimage, uniImage,
  uniLabel;

type
  TStatisticsMainFrm = class(TUniForm)
    ImageReportInvoices: TUniImage;
    LblListInvoices: TUniLabel;
    UniLabel1: TUniLabel;
    BtnListInvoices: TUniThemeButton;
    BtnSave: TUniThemeButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnListInvoicesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function StatisticsMainFrm: TStatisticsMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, PrintInvoiceList;

function StatisticsMainFrm: TStatisticsMainFrm;
begin
  Result := TStatisticsMainFrm(UniMainModule.GetFormInstance(TStatisticsMainFrm));
end;

procedure TStatisticsMainFrm.BtnListInvoicesClick(Sender: TObject);
begin
  with PrintInvoiceListFrm
  do begin
       Start_module;
       ShowModal();
     end;
end;

procedure TStatisticsMainFrm.BtnSaveClick(Sender: TObject);
begin
  Close;
end;

procedure TStatisticsMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
