unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniTreeView, uniTreeMenu, uniImage, uniLabel, uniGUIBaseClasses, uniPanel,
  uniGUIFrame, UniFSConfirm, Vcl.Menus, uniMainMenu, uniButton, Vcl.Imaging.pngimage;

type
  TMainForm = class(TUniForm)
    TreeMenuItems: TUniMenuItems;
    BtnRegistrationManagement: TUniMenuItem;
    BtnInvoicing: TUniMenuItem;
    BtnReports: TUniMenuItem;
    BtnSettings: TUniMenuItem;
    Confirm: TUniFSConfirm;
    BtnCountries: TUniMenuItem;
    BtnUsers: TUniMenuItem;
    BtnZipCodes: TUniMenuItem;
    BtnGeneralSettings: TUniMenuItem;
    BtnMailTemplates: TUniMenuItem;
    BtnBasicTables: TUniMenuItem;
    BtnTimeManagementAssistents: TUniMenuItem;
    ContainerHeader: TUniContainerPanel;
    ContainerHeaderLeft: TUniContainerPanel;
    ContainerHeaderRight: TUniContainerPanel;
    BtnInfo: TUniImage;
    BtnExit: TUniImage;
    ContainerBody: TUniContainerPanel;
    UniPanel1: TUniPanel;
    TreeMenuAdministratie: TUniTreeMenu;
    Container: TUniPanel;
    BtnChat: TUniImage;
    BtnCompanySettings: TUniImage;
    PanelUser: TUniPanel;
    ImageAbbreviation: TUniImage;
    LblUserName: TUniLabel;
    LblCompanyName: TUniLabel;
    LblAbbreviation: TUniLabel;
    ImageAvatar: TUniImage;
    ImageSeparator1: TUniImage;
    ImageSeparator2: TUniImage;
    ImageEmpty: TUniImage;
    ImageGreen: TUniImage;
    ImageRed: TUniImage;
    ImageProfile: TUniImage;
    BtnCustomers: TUniMenuItem;
    BtnProjects: TUniMenuItem;
    BtnObjects: TUniMenuItem;
    BtnTimeRegistration: TUniMenuItem;
    BtnManagementObjects: TUniMenuItem;
    BtnClickUpProject: TUniImage;
    ImageSeparator3: TUniImage;
    LblModuleName: TUniLabel;
    BtnOrders: TUniMenuItem;
    procedure BtnExitClick(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
    procedure BtnChatClick(Sender: TObject);
    procedure ImageDropDownUserClick(Sender: TObject);
    procedure BtnInfoClick(Sender: TObject);
    procedure BtnCountriesClick(Sender: TObject);
    procedure BtnUsersClick(Sender: TObject);
    procedure BtnCompanySettingsClick(Sender: TObject);
    procedure BtnReportsClick(Sender: TObject);
    procedure BtnZipCodesClick(Sender: TObject);
    procedure BtnProjectsClick(Sender: TObject);
    procedure BtnCustomersClick(Sender: TObject);
    procedure BtnGeneralSettingsClick(Sender: TObject);
    procedure BtnMailTemplatesClick(Sender: TObject);
    procedure BtnInvoicingClick(Sender: TObject);
    procedure BtnTimeRegistrationClick(Sender: TObject);
    procedure BtnRegistrationManagementClick(Sender: TObject);
    procedure BtnBasicTablesClick(Sender: TObject);
    procedure BtnTimeManagementAssistentsClick(Sender: TObject);
    procedure BtnObjectsClick(Sender: TObject);
    procedure ImageLogoClick(Sender: TObject);
    procedure BtnManagementObjectsClick(Sender: TObject);
    procedure BtnClickUpProjectClick(Sender: TObject);
    procedure BtnOrdersClick(Sender: TObject);
  private
    { Private declarations }
    ActiveFrame: TUniFrame;
    procedure Open_dashboard;
    procedure Load_info;
    procedure CallBackInfo(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
  end;

function MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication, DashBoard, ProfileMain, Info, CountriesMain, UsersMain, Company,
  ZipCodesMain, ProjectsMain, CustomersMain, GeneralSettingsMain, MailTemplatesMain, InvoicingMain,
  TimeRegistrationsMain, RegistrationManagementMain, BasicMain, StatisticsMain, TimeOverviewManagement,
  CoconneObjectsMain, ObjectsManagementMain, OrdersMain;

function MainForm: TMainForm;
begin
  Result := TMainForm(UniMainModule.GetFormInstance(TMainForm));
end;

procedure TMainForm.BtnBasicTablesClick(Sender: TObject);
begin
  with BasicMainFrm
  do begin
       ShowModal();
     end;
end;

procedure TMainForm.BtnChatClick(Sender: TObject);
begin
  UniSession.AddJS('window.fcWidget.open()');
end;

procedure TMainForm.BtnClickUpProjectClick(Sender: TObject);
begin
  UniSession.AddJS('window.open(''https://app.clickup.com/2191651/d/t?p=2291781&c=2852447&s=-1'', ''_blank'')');
end;

procedure TMainForm.BtnCompanySettingsClick(Sender: TObject);
begin
  With CompanyFrm do begin
    InitializeModule;
    ShowModal(CallBackInfo);
  end;
end;

procedure TMainForm.BtnCountriesClick(Sender: TObject);
begin
  with CountriesMainFrm
  do begin
       Start_module;
       ShowModal();
     end;
end;

procedure TMainForm.BtnCustomersClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TCustomersMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TCustomersMainFrm.Create(Self);
  With (ActiveFrame as TCustomersMainFrm)
  do begin
       Parent := Container;
       Start_module;
     end;
end;

procedure TMainForm.BtnExitClick(Sender: TObject);
begin
  UniSession.Terminate();
end;

procedure TMainForm.BtnGeneralSettingsClick(Sender: TObject);
begin
  with GeneralSettingsMainFrm
  do begin
       Start_module;
       ShowModal();
     end;
end;

procedure TMainForm.BtnInfoClick(Sender: TObject);
begin
  InfoFrm.ShowModal();
end;

procedure TMainForm.BtnInvoicingClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TInvoicingMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TInvoicingMainFrm.Create(Self);
  With (ActiveFrame as TInvoicingMainFrm) do begin
    Parent := Container;
    Start_module;
  end;
end;

procedure TMainForm.BtnMailTemplatesClick(Sender: TObject);
begin
  with MailTemplatesMainFrm
  do begin
       Start_module;
       ShowModal();
     end;
end;

procedure TMainForm.BtnManagementObjectsClick(Sender: TObject);
begin
  with ObjectsManagementMainFrm
  do begin
       Start_Objects_Management_module;
       ShowModal();
     end;
end;

procedure TMainForm.BtnObjectsClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TCoconneObjectsMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TCoconneObjectsMainFrm.Create(Self);
  With (ActiveFrame as TCoconneObjectsMainFrm) do begin
    Parent := Container;
    Start_module;
  end;
end;

procedure TMainForm.BtnOrdersClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TOrdersMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TOrdersMainFrm.Create(Self);
  With (ActiveFrame as TOrdersMainFrm)
  do begin
       Parent := Container;
       Start_module;
     end;
end;

procedure TMainForm.BtnProjectsClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TProjectsMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TProjectsMainFrm.Create(Self);
  With (ActiveFrame as TProjectsMainFrm) do begin
    Parent := Container;
    Start_module;
  end;
end;

procedure TMainForm.BtnRegistrationManagementClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TRegistrationManagementMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TRegistrationManagementMainFrm.Create(Self);
  With (ActiveFrame as TRegistrationManagementMainFrm) do begin
    Parent := Container;
    Start_module;
  end;
end;

procedure TMainForm.BtnReportsClick(Sender: TObject);
begin
  StatisticsMainFrm.ShowModal();
end;

procedure TMainForm.BtnTimeManagementAssistentsClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TTimeOverviewManagementFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TTimeOverviewManagementFrm.Create(Self);
  With (ActiveFrame as TTimeOverviewManagementFrm)
  do begin
    Parent := Container;
    Start_module;
  end;
end;

procedure TMainForm.BtnTimeRegistrationClick(Sender: TObject);
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TTimeRegistrationsMainFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TTimeRegistrationsMainFrm.Create(Self);
  With (ActiveFrame as TTimeRegistrationsMainFrm)
  do begin
    Parent := Container;
    Start_module;
  end;
end;

procedure TMainForm.BtnUsersClick(Sender: TObject);
begin
  with UsersMainFrm
  do begin
       Start_module;
       ShowModal();
     end;
end;

procedure TMainForm.BtnZipCodesClick(Sender: TObject);
begin
  With ZipcodesMainFrm
  do begin
       Start_module;
       ShowModal();
     end;
end;

procedure TMainForm.CallBackInfo(Sender: TComponent; AResult: Integer);
begin
  Load_info;
end;

procedure TMainForm.ImageDropDownUserClick(Sender: TObject);
begin
  with ProfileMainFrm do begin
    Start_module;
    ShowModal(CallBackInfo);
  end;
end;

procedure TMainForm.ImageLogoClick(Sender: TObject);
begin
  UniSession.AddJS('window.open(''https://www.wavedesk.be'', ''_blank'')');
end;

procedure TMainForm.Open_dashboard;
begin
  if (ActiveFrame <> nil) and (ActiveFrame.ClassType = TDashboardFrm)
  then exit;

  if (ActiveFrame <> nil)
  then FreeAndNil(ActiveFrame);

  ActiveFrame := TDashboardFrm.Create(Self);
  With (ActiveFrame as TDashboardFrm)
  do begin
       Parent := Container;
     end;
end;

procedure TMainForm.UniFormShow(Sender: TObject);
begin
  Load_info;
  Open_dashboard;
end;

procedure TMainForm.Load_info;
begin
  LblCompanyName.Caption   := UniMainModule.Company_name;
  LblUserName.Caption      := Trim(UniMainModule.User_name + ' ' + UniMainModule.User_firstname);
  LblAbbreviation.Caption  := Copy(UniMainModule.User_name, 1, 1) + Copy(UniMainModule.User_firstname, 1, 1);

  if UniMainModule.ShowAvatar = True
  then Try
         ImageAbbreviation.Visible := False;
         LblAbbreviation.Visible   := False;
         ImageAvatar.Visible       := True;
         if UniMainModule.Avatar.Graphic.Empty = False
         then begin
                ImageAvatar.Picture.Assign(UniMainModule.Avatar);
                ImageAvatar.JSInterface.JSCall('addCls', ['avatar']);
              end;
       Except
         ImageAvatar.Visible       := False;
         ImageAbbreviation.Visible := True;
         LblAbbreviation.Visible   := True;
       End
  else begin
         ImageAvatar.Visible       := False;
         ImageAbbreviation.Visible := True;
         LblAbbreviation.Visible   := True;
       end;
end;

initialization
  RegisterAppFormClass(TMainForm);

end.
