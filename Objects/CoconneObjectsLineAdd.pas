unit CoconneObjectsLineAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, Data.DB, MemDS, uniDBEdit, uniEdit, uniGUIBaseClasses, uniMemo, uniDBMemo,
  uniButton, UniThemeButton, uniBasicGrid, uniDBGrid, uniLabel;

type
  TCoconneObjectsLineAddFrm = class(TUniForm)
    EditDescription: TUniDBMemo;
    EditQuantity: TUniDBNumberEdit;
    EditAmount: TUniDBFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    GridObjectDefinitions: TUniDBGrid;
    ObjectDefinitions: TIBCQuery;
    DsObjectDefinitions: TDataSource;
    BtnInsert: TUniThemeButton;
    LblTotaalInclReduction: TUniLabel;
    LblTotaalInclReductionValue: TUniLabel;
    EditCustomerPrice: TUniDBFormattedNumberEdit;
    EditTitle: TUniDBEdit;
    procedure BtnCancelClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnInsertClick(Sender: TObject);
    procedure EditAmountExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_insert_objectline;
    procedure Start_edit_objectline;
  end;

function CoconneObjectsLineAddFrm: TCoconneObjectsLineAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule;

function CoconneObjectsLineAddFrm: TCoconneObjectsLineAddFrm;
begin
  Result := TCoconneObjectsLineAddFrm(UniMainModule.GetFormInstance(TCoconneObjectsLineAddFrm));
end;

{ TCoconneObjectsLineAddFrm }

procedure TCoconneObjectsLineAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.UObjectLines.Cancel;
  Close;
end;

procedure TCoconneObjectsLineAddFrm.BtnInsertClick(Sender: TObject);
begin
  if (ObjectDefinitions.Active) and (not ObjectDefinitions.FieldByName('Id').IsNull)
  then begin
         UniMainModule.UObjectLines.Fieldbyname('Title').AsString :=
         ObjectDefinitions.Fieldbyname('TitleDutch').asString;
         UniMainModule.UObjectLines.FieldByName('Description').AsString :=
         ObjectDefinitions.FieldByName('TemplateDutch').AsString;
       end;
end;

procedure TCoconneObjectsLineAddFrm.BtnSaveClick(Sender: TObject);
var totalprice: currency;
    vatamount:  currency;
begin
  BtnSave.SetFocus;

  if Trim(UniMainModule.UObjectLines.FieldByName('Description').AsString) = ''
  then begin
         UniMainModule.Show_warning('De omschrijving is een verplichte ingave.');
         EditDescription.SetFocus;
         Exit;
       end;

  if UniMainModule.UObjectLines.FieldByName('Amount').IsNull
  then begin
         UniMainModule.Show_warning('De stukprijs particulier is een verplichte ingave.');
         EditAmount.Setfocus;
         Exit;
       end;

  if UniMainModule.UObjectLines.FieldByName('CustomerAmount').IsNull
  then begin
         UniMainModule.Show_warning('Uw prijs is een verplichte ingave.');
         EditCustomerPrice.SetFocus;
         Exit;
       end;

  if UniMainModule.UObjectLines.FieldByName('Quantity').IsNull
  then begin
         UniMainModule.Show_warning('Het aantal is een verplichte ingave.');
         EditQuantity.Setfocus;
         Exit;
       end;

  //Calculate totals
  totalprice := UniMainModule.UObjectLines.FieldByName('Amount').AsCurrency;
  vatamount  := totalprice / 100 * 21;
  UniMainModule.UObjectLines.FieldByName('VatAmount').AsCurrency :=
  UniMainModule.UObjectLines.FieldByName('Quantity').AsInteger * vatamount;
  totalprice := totalprice + vatamount;
  totalprice := UniMainModule.UObjectLines.FieldByName('Quantity').AsInteger * totalprice;

  UniMainModule.UObjectLines.FieldByName('TotalAmount').AsCurrency := totalprice;

  //Calculate totals
  totalprice := UniMainModule.UObjectLines.FieldByName('CustomerAmount').AsCurrency;
  vatamount  := totalprice / 100 * 21;
  UniMainModule.UObjectLines.FieldByName('CustomerVatAmount').AsCurrency :=
  UniMainModule.UObjectLines.FieldByName('Quantity').AsInteger * vatamount;
  totalprice := totalprice + vatamount;
  totalprice := UniMainModule.UObjectLines.FieldByName('Quantity').AsInteger * totalprice;

  UniMainModule.UObjectLines.FieldByName('CustomerTotalAmount').AsCurrency := totalprice;

  Try
    UniMainModule.UObjectLines.Post;
    UniMainModule.UpdTrObjectLines.Commit;
    UniMainModule.Result_dbAction := UniMainModule.UObjectLines.FieldByName('Id').AsInteger;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrObjectLines.Rollback;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;
  Close;
end;

procedure TCoconneObjectsLineAddFrm.EditAmountExit(Sender: TObject);
var totaalmetkorting:    currency;
begin
  if (not UniMainModule.UObjectLines.FieldByName('Amount').IsNull) and
     (not UniMainModule.UObjectLines.FieldByName('Quantity').IsNull) and
     (not UniMainModule.UObjectLines.FieldByName('CustomerAmount').IsNull)
  then try
         if not UniMainModule.UObjectLines.FieldByName('CustomerAmount').isNull
         then try
                totaalmetkorting := UniMainModule.UObjectLines.FieldByName('CustomerAmount').AsCurrency *
                                    UniMainModule.UObjectLines.FieldByName('Quantity').AsCurrency;
                LblTotaalInclReductionValue.Caption := FormatCurr('#,##0.00 �', totaalmetkorting);
                LblTotaalInclReductionValue.Visible := True;
              except
                LblTotaalInclReductionValue.Visible := False;
              end
         else LblTotaalInclReductionValue.Visible   := False;

       except
         LblTotaalInclReductionValue.Visible := False;
       end
  else begin
         LblTotaalInclReductionValue.Visible := False;
       end;
end;

procedure TCoconneObjectsLineAddFrm.Start_edit_objectline;
var SQL: string;
begin
  Caption := 'Wijzigen object';
  ObjectDefinitions.Close;
  SQL := 'Select * from objectdefinitions where companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by titledutch';
  ObjectDefinitions.SQL.Clear;
  ObjectDefinitions.SQL.Add(SQL);
  ObjectDefinitions.Open;
  EditAmountExit(nil);
end;

procedure TCoconneObjectsLineAddFrm.Start_insert_objectline;
var SQL: string;
begin
  Caption := 'Toevoegen object';
  ObjectDefinitions.Close;
  SQL := 'Select * from objectdefinitions where companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by titledutch';
  ObjectDefinitions.SQL.Clear;
  ObjectDefinitions.SQL.Add(SQL);
  ObjectDefinitions.Open;
end;

procedure TCoconneObjectsLineAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
