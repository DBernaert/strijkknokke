unit ObjectsInvoiceAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniRadioButton, uniGUIBaseClasses, uniLabel, uniEdit, uniButton, UniThemeButton,
  uniDateTimePicker;

type
  TObjectsInvoiceAddFrm = class(TUniForm)
    LblInvoiceType: TUniLabel;
    RadiobuttonAdvance: TUniRadioButton;
    RadiobuttonSaldo: TUniRadioButton;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditPercentageToInvoice: TUniFormattedNumberEdit;
    EditDateInvoice: TUniDateTimePicker;
    RadiobuttonInvoice: TUniRadioButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
    Id_Objects: longint;
    RelationId: longint;
    InvoiceNumber: longint;
    Totaal, Btw, TotaalInclusief: currency;
    procedure Generate_invoice;
    procedure Book_invoice;
    procedure CallBackPrintInvoice(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_create_objects_invoice(ObjectsId: longint);
  end;

function ObjectsInvoiceAddFrm: TObjectsInvoiceAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ReportsMain, frxClass, Main, UniFSConfirm, ServerModule, Db;

function ObjectsInvoiceAddFrm: TObjectsInvoiceAddFrm;
begin
  Result := TObjectsInvoiceAddFrm(UniMainModule.GetFormInstance(TObjectsInvoiceAddFrm));
end;

procedure TObjectsInvoiceAddFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TObjectsInvoiceAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if EditDateInvoice.IsBlank
  then begin
         UniMainModule.Show_warning('De factuurdatum is een verplichte ingave.');
         EditDateInvoice.SetFocus;
         Exit;
       end;

  if EditPercentageToInvoice.IsBlank then
  begin
    UniMainModule.show_warning('Het percentage te factureren is een verplichte ingave.');
    EditPercentageToInvoice.SetFocus;
    Exit;
  end;

  if EditPercentageToInvoice.Value = 0 then
  begin
    UniMainModule.show_warning('Het percentage te factureren mag niet 0 zijn.');
    EditPercentageToInvoice.SetFocus;
    Exit;
  end;

  Generate_invoice;
end;

procedure TObjectsInvoiceAddFrm.Generate_invoice;
var Memo: TfrxMemoView;
begin
  With ReportsMainFrm do
  begin
    UniMainModule.UCompany.Close;
    UniMainModule.UCompany.SQL.Clear;
    UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
    UniMainModule.UCompany.Open;
    Try
      UniMainModule.UCompany.Edit;
    Except
      UniMainModule.show_error('Kan geen nieuw factuurnummer opvragen, bedrijfsfiche in gebruik door een andere gebruiker.');
      UniMainModule.UCompany.Close;
      exit;
    End;

    InvoiceNumber := UniMainModule.UCompany.FieldByName('InvoiceSequenceObjects').AsInteger;

    // Header
    Memo := InvoiceObjects.FindObject('LblAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Address').AsString);

    Memo := InvoiceObjects.FindObject('LblAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Zipcode').AsString + ' ' + UniMainModule.UCompany.FieldByName('City').AsString);

    Memo := InvoiceObjects.FindObject('LblTelephone') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Phone').AsString);

    Memo := InvoiceObjects.FindObject('LblEmail') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Email').AsString);

    Memo := InvoiceObjects.FindObject('LblWeb') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Website').AsString);

    // Central
    Memo := InvoiceObjects.FindObject('LblDocDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', EditDateInvoice.DateTime));

    Memo := InvoiceObjects.FindObject('LblEndDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', IncMonth(EditDateInvoice.DateTime, 1)));

    Memo := InvoiceObjects.FindObject('LblInvoiceType') as TfrxMemoView;
    Memo.Memo.Clear;
    if RadiobuttonAdvance.Checked
    then Memo.Memo.Add('Voorschotfactuur')
    else if RadiobuttonSaldo.Checked
         then Memo.Memo.Add('Saldofactuur')
         else if RadiobuttonInvoice.Checked
              then Memo.Memo.Add('Factuur');

    Memo := InvoiceObjects.FindObject('LblDescription') as TfrxMemoView;
    Memo.Memo.Clear;
    if RadiobuttonAdvance.Checked
    then Memo.Memo.Add('Met dit schrijven zijn wij zo vrij een voorschotfactuur op te stellen voor het door U gevraagde:')
    else if RadiobuttonSaldo.Checked
         then Memo.Memo.Add('Met dit schrijven zijn wij zo vrij een saldofactuur op te stellen voor het door U gevraagde:')
         else if RadiobuttonInvoice.Checked
              then Memo.Memo.Add('Met dit schrijven zijn wij zo vrij een factuur op te stellen voor het door U gevraagde:');

    // Footer
    Memo := InvoiceObjects.FindObject('LblBank11') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount').AsString);

    Memo := InvoiceObjects.FindObject('LblBank12') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban').AsString);

    Memo := InvoiceObjects.FindObject('LblBank13') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic').AsString);

    Memo := InvoiceObjects.FindObject('LblVat') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.UCompany.FieldByName('VatNumber').AsString);

    Memo := InvoiceObjects.FindObject('LblBank21') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount2').AsString);

    Memo := InvoiceObjects.FindObject('LblBank22') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban2').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban2').AsString);

    Memo := InvoiceObjects.FindObject('LblBank23') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic2').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic2').AsString);

    // Get Customer details
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select relation from objects where Id=' + IntToStr(Id_Objects));
    UniMainModule.QWork.Open;
    RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;
    UniMainModule.QWork.Close;

    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select * from relations where Id=' + IntToStr(RelationId));
    UniMainModule.QWork.Open;

    Memo := InvoiceObjects.FindObject('LblCustomerName') as TfrxMemoView;
    Memo.Memo.Clear;
    Case UniMainModule.QWork.FieldByName('RelationType').AsInteger of
      0:
        begin
          Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyName').AsString);
        end;
      1:
        begin
          if not UniMainModule.QWork.FieldByName('Appelation').isNull then
          begin
            UniMainModule.QWork2.Close;
            UniMainModule.QWork2.SQL.Clear;
            UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('Appelation').AsString);
            UniMainModule.QWork2.Open;
            Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('Name').AsString + ' ' +
            UniMainModule.QWork.FieldByName('FirstName').AsString));
            UniMainModule.QWork2.Close;
          end
          else
            Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
          end;
        End;

    Memo := InvoiceObjects.FindObject('LblAppelation') as TfrxMemoView;
    Memo.Memo.Clear;
    if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
    then begin
           if (Trim(UniMainModule.QWork.FieldByName('Name').AsString) <> '') or
              (Trim(UniMainModule.QWork.FieldByName('FirstName').asString) <> '')
           then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
         end;

    Memo := InvoiceObjects.FindObject('LblCustomerAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Address').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumber').AsString));

    Memo := InvoiceObjects.FindObject('LblCustomerAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' + UniMainModule.QWork.FieldByName('City').AsString));

    Memo := InvoiceObjects.FindObject('LblCustomerVatNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0 then
    begin
      if Trim(UniMainModule.QWork.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString);
    end;
    UniMainModule.QWork.Close;

    // Get sequence number invoice
    Memo := InvoiceObjects.FindObject('InvoiceNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(IntToStr(InvoiceNumber));

    ObjectLines.Close;
    ObjectLines.SQL.Clear;
    ObjectLines.SQL.Add('Select * from ObjectLines where Object=' + IntToStr(Id_Objects));
    ObjectLines.Open;

    //Calculate the totals
    Totaal := 0;
    ObjectLines.First;
    while not ObjectLines.Eof
    do begin
         Totaal := Totaal + (ObjectLines.FieldByName('CustomerAmount').AsCurrency * ObjectLines.FieldByName('Quantity').AsInteger);
         ObjectLines.Next;
       end;

    ObjectLines.First;

    Totaal := Totaal / 2;
    Btw    := Totaal / 100 * 21;
    TotaalInclusief := Totaal + Btw;

    Memo := InvoiceObjects.FindObject('LblTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Totaal));

    Memo := InvoiceObjects.FindObject('LblTotalVat') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Btw));

    Memo := InvoiceObjects.FindObject('LblGeneralTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', TotaalInclusief));

    Prepare_invoice_objects;
    ShowModal(CallBackPrintInvoice);
  end;
end;

procedure TObjectsInvoiceAddFrm.CallBackPrintInvoice(Sender: TComponent; AResult: Integer);
begin
   MainForm.Confirm.Question('Inboeken factuur?', 'Wenst u de factuur in te boeken?', 'fa fa-exclamation-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes
      then Book_invoice
      else begin
             UniMainModule.UCompany.Cancel;
             UniMainModule.UCompany.Close;
           end;
    end);
end;

procedure TObjectsInvoiceAddFrm.Book_invoice;
begin
  UniMainModule.UObjectInvoices.Close;
  UniMainModule.UObjectInvoices.SQL.Clear;
  UniMainModule.UObjectInvoices.SQL.Add('Select first 0 * from ObjectInvoices');
  UniMainModule.UObjectInvoices.Open;
  UniMainModule.UObjectInvoices.Append;
  UniMainModule.UObjectInvoices.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UniMainModule.UObjectInvoices.FieldByName('Relation').AsInteger := RelationId;
  UniMainModule.UObjectInvoices.FieldByName('Object').AsInteger := Id_Objects;
  UniMainModule.UObjectInvoices.FieldByName('InvoiceNumber').AsInteger := InvoiceNumber;
  UniMainModule.UObjectInvoices.FieldByName('InvoiceDate').AsDateTime := EditDateInvoice.DateTime;
  UniMainModule.UObjectInvoices.FieldByName('PaymentTerm').AsInteger := 30;
  if RadiobuttonAdvance.Checked
  then UniMainModule.UObjectInvoices.FieldByName('InvoiceType').AsInteger := 0
  else if RadiobuttonSaldo.Checked
       then UniMainModule.UObjectInvoices.Fieldbyname('InvoiceType').AsInteger := 1
       else if RadiobuttonInvoice.Checked
            then UniMainModule.UObjectInvoices.FieldByName('InvoiceType').AsInteger := 2;
                                                                                         
  UniMainModule.UObjectInvoices.FieldByName('DueDate').AsDateTime := IncMonth(EditDateInvoice.DateTime, 1);
  UniMainModule.UObjectInvoices.FieldByName('Amount').AsCurrency := Totaal;
  UniMainModule.UObjectInvoices.FieldByName('VatAmount').AsCurrency := Btw;
  UniMainModule.UObjectInvoices.FieldByName('TotalAmount').AsCurrency := TotaalInclusief;
  UniMainModule.UObjectInvoices.FieldByName('TotalPayed').AsCurrency  := 0;
  UniMainModule.UObjectInvoices.FieldByName('InvoiceStage').AsInteger := 0;
  UniMainModule.UObjectInvoices.FieldByName('InvoicePercentage').AsCurrency := EditPercentageToInvoice.Value;

  Try
    UniMainModule.UObjectInvoices.Post;
    UniMainModule.UpdTrObjectInvoices.Commit;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrObjectInvoices.Rollback;
      UniMainModule.UObjectInvoices.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;

  UniMainModule.UObjectInvoices.Close;

  UniMainModule.UCompany.FieldByName('InvoiceSequenceObjects').AsInteger :=
  UniMainModule.UCompany.FieldByName('InvoiceSequenceObjects').AsInteger + 1;
  Try
    UniMainModule.UCompany.Post;
    UniMainModule.UpdTrCompany.Commit;
    UniMainModule.UCompany.Close;
    Close;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrCompany.Rollback;
      UniMainModule.UCompany.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  End;
end;

procedure TObjectsInvoiceAddFrm.Start_create_objects_invoice(ObjectsId: longint);
begin
  Id_Objects := ObjectsId;
  Caption := 'Aanmaken factuur';
end;

procedure TObjectsInvoiceAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
