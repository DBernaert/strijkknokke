unit CoconneObjectsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, uniDBNavigator, uniEdit, uniButton,
  UniThemeButton, uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC;

type
  TCoconneObjectsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageObjects: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorObjects: TUniDBNavigator;
    GridObjects: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    GridInvoices: TUniDBGrid;
    Objects: TIBCQuery;
    DsObjects: TDataSource;
    ObjectsID: TLargeintField;
    ObjectsOFFERDATE: TDateField;
    ObjectsAMOUNT: TFloatField;
    ObjectsVATAMOUNT: TFloatField;
    ObjectsTOTALAMOUNT: TFloatField;
    ObjectsRELATIONTYPE: TIntegerField;
    ObjectsCOMPANYNAME: TWideStringField;
    ObjectsRELATIONNAME: TWideStringField;
    ObjectsRELATIONFIRSTNAME: TWideStringField;
    BtnOffer: TUniThemeButton;
    ObjectsOFFERNUMBER: TIntegerField;
    BtnInvoice: TUniThemeButton;
    Invoices: TIBCQuery;
    DsInvoices: TDataSource;
    InvoicesID: TLargeintField;
    InvoicesCOMPANYID: TLargeintField;
    InvoicesRELATION: TLargeintField;
    InvoicesINVOICENUMBER: TIntegerField;
    InvoicesINVOICEDATE: TDateField;
    InvoicesPAYMENTTERM: TIntegerField;
    InvoicesDUEDATE: TDateField;
    InvoicesAMOUNT: TFloatField;
    InvoicesVATAMOUNT: TFloatField;
    InvoicesTOTALAMOUNT: TFloatField;
    InvoicesOBJECT: TLargeintField;
    InvoicesTOTALPAYED: TFloatField;
    InvoicesINVOICETYPE: TIntegerField;
    InvoicesINVOICEPERCENTAGE: TFloatField;
    InvoicesINVOICESTAGE: TIntegerField;
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnOfferClick(Sender: TObject);
    procedure BtnInvoiceClick(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackInsertUpdateObject(Sender: TComponent; AResult: Integer);
    procedure CallBackGenerateInvoice(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses CoconneObjectsAdd, MainModule, Main, UniFSConfirm, ReportsMain, ObjectsInvoiceAdd;



{ TCoconneObjectsMainFrm }

procedure TCoconneObjectsMainFrm.BtnAddClick(Sender: TObject);
begin
 UniMainModule.Result_dbAction := 0;
 with CoconneObjectsAddFrm
 do begin
      Initialize_insert;
      ShowModal(CallBackInsertUpdateObject);
    end;
end;

procedure TCoconneObjectsMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Objects.Active) or (Objects.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen offerte?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UObjectLines.Close;
          UniMainModule.UObjectLines.SQL.Clear;
          UniMainModule.UObjectLines.SQL.Add('Select * from ObjectLines where Object=' + Objects.FieldByName('Id').asString);
          UniMainModule.UObjectLines.Open;
          UniMainModule.UObjectLines.First;
          while not UniMainModule.UObjectLines.Eof
          do UniMainModule.UObjectLines.Delete;

          Try
            if UniMainModule.UpdTrObjectLines.Active
            then UniMainModule.UpdTrObjectLines.Commit;
            UniMainModule.UObjectLines.Close;
          Except
            UniMainModule.UpdTrObjectLines.Rollback;
            UniMainModule.UObjectLines.Close;
            UniMainModule.Show_warning('Fout bij het verwijderen.');
          End;

          UniMainModule.UObjects.Close;
          UniMainModule.UObjects.SQL.Clear;
          UniMainModule.UObjects.SQL.Add('Select * from objects where id=' + Objects.fieldbyname('Id').asString);
          UniMainModule.UObjects.Open;
          UniMainModule.UObjects.Delete;
          if UniMainModule.UpdTrObjects.Active
          then UniMainModule.UpdTrObjects.Commit;
          UniMainModule.UObjects.Close;
        Except
          on E: EDataBaseError do
          begin
            if UniMainModule.UpdTrObjects.Active
            then UniMainModule.UpdTrObjects.Rollback;
            UniMainModule.UObjects.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Objects.Refresh;
      end;
    end);
end;

procedure TCoconneObjectsMainFrm.BtnInvoiceClick(Sender: TObject);
begin
  if (Objects.Active = false) or (Objects.FieldByName('Id').IsNull)
  then exit;

  With ObjectsInvoiceAddFrm do
  begin
    Start_create_objects_invoice(Objects.FieldByName('Id').AsInteger, Objects.FieldByName('OfferNumber').AsInteger);
    ShowModal(CallBackGenerateInvoice);
  end;
end;

procedure TCoconneObjectsMainFrm.CallBackGenerateInvoice(Sender: TComponent; AResult: Integer);
begin
  if Invoices.Active
  then Invoices.Refresh;
end;

procedure TCoconneObjectsMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Objects.Active) and (not Objects.FieldByName('Id').IsNull)
  then begin
         with CoconneObjectsAddFrm
         do begin
              Initialize_edit(Objects.fieldbyname('Id').AsInteger);
              ShowModal(CallBackInsertUpdateObject);
            end;
       end;
end;

procedure TCoconneObjectsMainFrm.BtnOfferClick(Sender: TObject);
begin
  if (Objects.Active) and (not Objects.FieldByName('Id').IsNull)
  then begin
         with ReportsMainFrm
         do begin
              Prepare_offer(Objects.FieldByName('Id').AsInteger);
              ShowModal();
            end;
       end;
end;

procedure TCoconneObjectsMainFrm.CallBackInsertUpdateObject(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if Objects.Active then begin
      Objects.Refresh;
      Objects.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCoconneObjectsMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' RELATIONNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' RELATIONFIRSTNAME LIKE ''%' +
            EditSearch.Text + '%''' + ' OR ' + ' COMPANYNAME LIKE ''%' + EditSearch.Text + '%''';
  Objects.Filter := Filter;
end;

procedure TCoconneObjectsMainFrm.Start_module;
var SQL: string;
begin
  Objects.Close;
  Objects.SQL.Clear;
  SQL := 'SELECT OBJECTS.ID, OBJECTS.OFFERDATE, OBJECTS.AMOUNT, OBJECTS.VATAMOUNT, OBJECTS.TOTALAMOUNT, OBJECTS.OFFERNUMBER, ' +
         'RELATIONS.RELATIONTYPE, RELATIONS.COMPANYNAME, RELATIONS.NAME AS RELATIONNAME, RELATIONS.FIRSTNAME AS RELATIONFIRSTNAME ' +
         'FROM OBJECTS ' +
         'LEFT OUTER JOIN RELATIONS ON (OBJECTS.RELATION = RELATIONS.ID) ' +
         'WHERE OBJECTS.COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'ORDER BY OBJECTS.OFFERNUMBER ASC';
  Objects.SQL.Add(SQL);
  Objects.Open;
  Objects.Last;
  Invoices.Open;
  EditSearch.SetFocus;
end;

end.
