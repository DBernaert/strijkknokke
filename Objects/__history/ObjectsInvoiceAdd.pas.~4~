unit ObjectsInvoiceAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniRadioButton, uniGUIBaseClasses, uniLabel, uniEdit, uniButton, UniThemeButton,
  uniDateTimePicker;

type
  TObjectsInvoiceAddFrm = class(TUniForm)
    LblInvoiceType: TUniLabel;
    RadiobuttonAdvance: TUniRadioButton;
    RadiobuttonSaldo: TUniRadioButton;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditPercentageToInvoice: TUniFormattedNumberEdit;
    EditDateInvoice: TUniDateTimePicker;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
    Id_Objects: longint;
    RelationId: longint;
    InvoiceNumber: longint;
    procedure Generate_invoice;
  public
    { Public declarations }
    procedure Start_create_objects_invoice(ObjectsId: longint);
  end;

function ObjectsInvoiceAddFrm: TObjectsInvoiceAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ReportsMain, frxClass;

function ObjectsInvoiceAddFrm: TObjectsInvoiceAddFrm;
begin
  Result := TObjectsInvoiceAddFrm(UniMainModule.GetFormInstance(TObjectsInvoiceAddFrm));
end;

procedure TObjectsInvoiceAddFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TObjectsInvoiceAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if EditDateInvoice.IsBlank
  then begin
         UniMainModule.Show_warning('De factuurdatum is een verplichte ingave.');
         EditDateInvoice.SetFocus;
         Exit;
       end;

  if EditPercentageToInvoice.IsBlank then
  begin
    UniMainModule.show_warning('Het percentage te factureren is een verplichte ingave.');
    EditPercentageToInvoice.SetFocus;
    Exit;
  end;

  if EditPercentageToInvoice.Value = 0 then
  begin
    UniMainModule.show_warning('Het percentage te factureren mag niet 0 zijn.');
    EditPercentageToInvoice.SetFocus;
    Exit;
  end;

  Generate_invoice;
end;

procedure TObjectsInvoiceAddFrm.Generate_invoice;
var Memo: TfrxMemoView;
    Totaal, Btw, TotaalInclusief: currency;
begin
  With ReportsMainFrm do
  begin
    UniMainModule.UCompany.Close;
    UniMainModule.UCompany.SQL.Clear;
    UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
    UniMainModule.UCompany.Open;
    Try
      UniMainModule.UCompany.Edit;
    Except
      UniMainModule.show_error('Kan geen nieuw factuurnummer opvragen, bedrijfsfiche in gebruik door een andere gebruiker.');
      UniMainModule.UCompany.Close;
      exit;
    End;

    InvoiceNumber := UniMainModule.UCompany.FieldByName('InvoiceSequenceObjects').AsInteger;

    // Header
    Memo := InvoiceObjects.FindObject('LblAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Address').AsString);

    Memo := InvoiceObjects.FindObject('LblAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Zipcode').AsString + ' ' + UniMainModule.UCompany.FieldByName('City').AsString);

    Memo := InvoiceObjects.FindObject('LblTelephone') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Phone').AsString);

    Memo := InvoiceObjects.FindObject('LblEmail') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Email').AsString);

    Memo := InvoiceObjects.FindObject('LblWeb') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Website').AsString);

    // Central
    Memo := InvoiceObjects.FindObject('LblDocDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', EditDateInvoice.DateTime));

    Memo := InvoiceObjects.FindObject('LblEndDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', IncMonth(EditDateInvoice.DateTime, 1)));

    Memo := InvoiceObjects.FindObject('LblInvoiceType') as TfrxMemoView;
    Memo.Memo.Clear;
    if RadiobuttonAdvance.Checked
    then Memo.Memo.Add('VOORSCHOTFAKTUUR')
    else if RadiobuttonSaldo.Checked
         then Memo.Memo.Add('SALDOFAKTUUR');

{    Memo := Invoice.FindObject('LblPaymentLine1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add('Gelieve dit bedrag over te schrijven op onderstaand rekeningnummer binnen de ' + IntToStr(Trunc(EditPaymentTerm.Value)) + ' dagen na');

    Memo := Invoice.FindObject('LblCompanyName') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Name').AsString);}

    // Footer
    Memo := InvoiceObjects.FindObject('LblBank11') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount').AsString);

    Memo := InvoiceObjects.FindObject('LblBank12') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban').AsString);

    Memo := InvoiceObjects.FindObject('LblBank13') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic').AsString);

    Memo := InvoiceObjects.FindObject('LblVat') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.UCompany.FieldByName('VatNumber').AsString);

    Memo := InvoiceObjects.FindObject('LblBank21') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount2').AsString);

    Memo := InvoiceObjects.FindObject('LblBank22') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban2').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban2').AsString);

    Memo := InvoiceObjects.FindObject('LblBank23') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic2').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic2').AsString);

    // Get Customer details
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select relation from objects where Id=' + IntToStr(Id_Objects));
    UniMainModule.QWork.Open;
    RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;
    UniMainModule.QWork.Close;

    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select * from relations where Id=' + IntToStr(RelationId));
    UniMainModule.QWork.Open;

    Memo := InvoiceObjects.FindObject('LblCustomerName') as TfrxMemoView;
    Memo.Memo.Clear;
    Case UniMainModule.QWork.FieldByName('RelationType').AsInteger of
      0:
        begin
          Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyName').AsString);
        end;
      1:
        begin
          if not UniMainModule.QWork.FieldByName('Appelation').isNull then
          begin
            UniMainModule.QWork2.Close;
            UniMainModule.QWork2.SQL.Clear;
            UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('Appelation').AsString);
            UniMainModule.QWork2.Open;
            Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('Name').AsString + ' ' +
            UniMainModule.QWork.FieldByName('FirstName').AsString));
            UniMainModule.QWork2.Close;
          end
          else
            Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
          end;
        End;

    Memo := InvoiceObjects.FindObject('LblAppelation') as TfrxMemoView;
    Memo.Memo.Clear;
    if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
    then begin
           if (Trim(UniMainModule.QWork.FieldByName('Name').AsString) <> '') or
              (Trim(UniMainModule.QWork.FieldByName('FirstName').asString) <> '')
           then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
         end;

    Memo := InvoiceObjects.FindObject('LblCustomerAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Address').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumber').AsString));

    Memo := InvoiceObjects.FindObject('LblCustomerAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' + UniMainModule.QWork.FieldByName('City').AsString));

    Memo := InvoiceObjects.FindObject('LblCustomerVatNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0 then
    begin
      if Trim(UniMainModule.QWork.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString);
    end;
    UniMainModule.QWork.Close;

    // Get sequence number invoice
    Memo := InvoiceObjects.FindObject('InvoiceNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(IntToStr(InvoiceNumber));

    ObjectLines.Close;
    ObjectLines.SQL.Clear;
    ObjectLines.SQL.Add('Select * from ObjectLines where Object=' + IntToStr(Id_Objects));
    ObjectLines.Open;

    //Calculate the totals
    Totaal := 0;
    ObjectLines.First;
    while not ObjectLines.Eof
    do begin
         Totaal := Totaal + (ObjectLines.FieldByName('CustomerAmount').AsCurrency * ObjectLines.FieldByName('Quantity').AsInteger);
         ObjectLines.Next;
       end;

    ObjectLines.First;

    Totaal := Totaal / 2;
    Btw    := Totaal / 100 * 21;
    TotaalInclusief := Totaal + Btw;

    Memo := InvoiceObjects.FindObject('LblTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Totaal));

    Memo := InvoiceObjects.FindObject('LblTotalVat') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', Btw));

    Memo := InvoiceObjects.FindObject('LblGeneralTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', TotaalInclusief));

    Prepare_invoice_objects;
    ShowModal(); //CallBackPrintInvoice);
  end;
end;

procedure TObjectsInvoiceAddFrm.Start_create_objects_invoice(ObjectsId: longint);
begin
  Id_Objects := ObjectsId;
  Caption := 'Aanmaken factuur';
end;

procedure TObjectsInvoiceAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
