object CoconneObjectsLineAddFrm: TCoconneObjectsLineAddFrm
  Left = 0
  Top = 0
  ClientHeight = 385
  ClientWidth = 945
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditDescription
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditDescription: TUniDBMemo
    Left = 424
    Top = 48
    Width = 505
    Height = 145
    Hint = ''
    DataField = 'DESCRIPTION'
    DataSource = UniMainModule.DsUObjectLines
    TabOrder = 1
    FieldLabel = 'Omschrijving <font color="#cd0015">*</font>'
    FieldLabelWidth = 130
  end
  object EditQuantity: TUniDBNumberEdit
    Left = 424
    Top = 264
    Width = 505
    Height = 22
    Hint = ''
    DataField = 'QUANTITY'
    DataSource = UniMainModule.DsUObjectLines
    TabOrder = 4
    SelectOnFocus = True
    FieldLabel = 'Aantal <font color="#cd0015">*</font>'
    FieldLabelWidth = 200
    DecimalSeparator = ','
    OnExit = EditAmountExit
  end
  object EditAmount: TUniDBFormattedNumberEdit
    Left = 424
    Top = 200
    Width = 505
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    DataField = 'AMOUNT'
    DataSource = UniMainModule.DsUObjectLines
    TabOrder = 2
    SelectOnFocus = True
    FieldLabel = 'Stukprijs particulier (excl. btw)<font color="#cd0015">*</font>'
    FieldLabelWidth = 200
    DecimalSeparator = ','
    ThousandSeparator = '.'
    OnExit = EditAmountExit
  end
  object BtnSave: TUniThemeButton
    Left = 712
    Top = 344
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 5
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 824
    Top = 344
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 6
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object GridObjectDefinitions: TUniDBGrid
    Left = 16
    Top = 16
    Width = 369
    Height = 241
    Hint = ''
    DataSource = DsObjectDefinitions
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    TabOrder = 7
    TabStop = False
    OnDblClick = BtnInsertClick
    Columns = <
      item
        Flex = 1
        FieldName = 'TITLEDUTCH'
        Title.Caption = 'OMSCHRIJVING'
        Width = 64
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object BtnInsert: TUniThemeButton
    Left = 392
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Invoegen'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 8
    Images = UniMainModule.ImageList
    ImageIndex = 10
    OnClick = BtnInsertClick
    ButtonTheme = uctDefault
  end
  object LblTotaalInclReduction: TUniLabel
    Left = 424
    Top = 304
    Width = 82
    Height = 13
    Hint = ''
    Caption = 'Uw totaalprijs:'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 9
  end
  object LblTotaalInclReductionValue: TUniLabel
    Left = 560
    Top = 304
    Width = 369
    Height = 13
    Hint = ''
    Visible = False
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'LblTotaalValue'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 10
  end
  object EditCustomerPrice: TUniDBFormattedNumberEdit
    Left = 424
    Top = 232
    Width = 505
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    DataField = 'CUSTOMERAMOUNT'
    DataSource = UniMainModule.DsUObjectLines
    TabOrder = 3
    SelectOnFocus = True
    FieldLabel = 'Uw prijs (excl. btw)<font color="#cd0015">*</font>'
    FieldLabelWidth = 200
    DecimalSeparator = ','
    ThousandSeparator = '.'
    OnExit = EditAmountExit
  end
  object EditTitle: TUniDBEdit
    Left = 424
    Top = 16
    Width = 505
    Height = 22
    Hint = ''
    DataField = 'TITLE'
    DataSource = UniMainModule.DsUObjectLines
    TabOrder = 0
    FieldLabel = 'Titel'
    FieldLabelWidth = 130
  end
  object ObjectDefinitions: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from objectdefinitions order by titledutch')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 184
    Top = 56
  end
  object DsObjectDefinitions: TDataSource
    DataSet = ObjectDefinitions
    Left = 184
    Top = 120
  end
end
