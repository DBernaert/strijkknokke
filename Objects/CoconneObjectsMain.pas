unit CoconneObjectsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, uniDBNavigator, uniEdit, uniButton,
  UniThemeButton, uniBasicGrid, uniDBGrid, Data.DB, MemDS, DBAccess, IBC;

type
  TCoconneObjectsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageObjects: TUniImage;
    Objects: TIBCQuery;
    DsObjects: TDataSource;
    ObjectsID: TLargeintField;
    ObjectsOFFERDATE: TDateField;
    ObjectsAMOUNT: TFloatField;
    ObjectsVATAMOUNT: TFloatField;
    ObjectsTOTALAMOUNT: TFloatField;
    ObjectsRELATIONTYPE: TIntegerField;
    ObjectsCOMPANYNAME: TWideStringField;
    ObjectsRELATIONNAME: TWideStringField;
    ObjectsRELATIONFIRSTNAME: TWideStringField;
    ObjectsOFFERNUMBER: TIntegerField;
    InvoicesObjects: TIBCQuery;
    DsInvoicesObjects: TDataSource;
    InvoicesObjectsID: TLargeintField;
    InvoicesObjectsCOMPANYID: TLargeintField;
    InvoicesObjectsRELATION: TLargeintField;
    InvoicesObjectsINVOICENUMBER: TIntegerField;
    InvoicesObjectsINVOICEDATE: TDateField;
    InvoicesObjectsPAYMENTTERM: TIntegerField;
    InvoicesObjectsDUEDATE: TDateField;
    InvoicesObjectsAMOUNT: TFloatField;
    InvoicesObjectsVATAMOUNT: TFloatField;
    InvoicesObjectsTOTALAMOUNT: TFloatField;
    InvoicesObjectsOBJECT: TLargeintField;
    InvoicesObjectsTOTALPAYED: TFloatField;
    InvoicesObjectsINVOICETYPE: TIntegerField;
    InvoicesObjectsINVOICEPERCENTAGE: TFloatField;
    InvoicesObjectsINVOICESTAGE: TIntegerField;
    ContainerBodyLeft: TUniContainerPanel;
    ContainerBodyRight: TUniContainerPanel;
    ContainerFilterLeftBody: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorObjects: TUniDBNavigator;
    ContainerFilterRightBody: TUniContainerPanel;
    UniContainerPanel3: TUniContainerPanel;
    UniDBNavigator1: TUniDBNavigator;
    GridObjects: TUniDBGrid;
    GridInvoices: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    BtnOffer: TUniThemeButton;
    UniContainerPanel1: TUniContainerPanel;
    BtnInvoice: TUniThemeButton;
    BtnPrintInvoice: TUniThemeButton;
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnOfferClick(Sender: TObject);
    procedure BtnInvoiceClick(Sender: TObject);
    procedure BtnPrintInvoiceClick(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackInsertUpdateObject(Sender: TComponent; AResult: Integer);
    procedure CallBackGenerateInvoice(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses CoconneObjectsAdd, MainModule, Main, UniFSConfirm, ReportsMain, ObjectsInvoiceAdd, frxClass;



{ TCoconneObjectsMainFrm }

procedure TCoconneObjectsMainFrm.BtnAddClick(Sender: TObject);
begin
 UniMainModule.Result_dbAction := 0;
 with CoconneObjectsAddFrm
 do begin
      Initialize_insert;
      ShowModal(CallBackInsertUpdateObject);
    end;
end;

procedure TCoconneObjectsMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Objects.Active) or (Objects.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen offerte?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UObjectLines.Close;
          UniMainModule.UObjectLines.SQL.Clear;
          UniMainModule.UObjectLines.SQL.Add('Select * from ObjectLines where Object=' + Objects.FieldByName('Id').asString);
          UniMainModule.UObjectLines.Open;
          UniMainModule.UObjectLines.First;
          while not UniMainModule.UObjectLines.Eof
          do UniMainModule.UObjectLines.Delete;

          Try
            if UniMainModule.UpdTrObjectLines.Active
            then UniMainModule.UpdTrObjectLines.Commit;
            UniMainModule.UObjectLines.Close;
          Except
            UniMainModule.UpdTrObjectLines.Rollback;
            UniMainModule.UObjectLines.Close;
            UniMainModule.Show_warning('Fout bij het verwijderen.');
          End;

          UniMainModule.UObjects.Close;
          UniMainModule.UObjects.SQL.Clear;
          UniMainModule.UObjects.SQL.Add('Select * from objects where id=' + Objects.fieldbyname('Id').asString);
          UniMainModule.UObjects.Open;
          UniMainModule.UObjects.Delete;
          if UniMainModule.UpdTrObjects.Active
          then UniMainModule.UpdTrObjects.Commit;
          UniMainModule.UObjects.Close;
        Except
          on E: EDataBaseError do
          begin
            if UniMainModule.UpdTrObjects.Active
            then UniMainModule.UpdTrObjects.Rollback;
            UniMainModule.UObjects.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Objects.Refresh;
      end;
    end);
end;

procedure TCoconneObjectsMainFrm.BtnInvoiceClick(Sender: TObject);
begin
  if (Objects.Active = false) or (Objects.FieldByName('Id').IsNull)
  then exit;

  With ObjectsInvoiceAddFrm do
  begin
    Start_create_objects_invoice(Objects.FieldByName('Id').AsInteger, Objects.FieldByName('OfferNumber').AsInteger);
    ShowModal(CallBackGenerateInvoice);
  end;
end;

procedure TCoconneObjectsMainFrm.CallBackGenerateInvoice(Sender: TComponent; AResult: Integer);
begin
  if InvoicesObjects.Active
  then InvoicesObjects.Refresh;
end;

procedure TCoconneObjectsMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Objects.Active) and (not Objects.FieldByName('Id').IsNull)
  then begin
         with CoconneObjectsAddFrm
         do begin
              Initialize_edit(Objects.fieldbyname('Id').AsInteger);
              ShowModal(CallBackInsertUpdateObject);
            end;
       end;
end;

procedure TCoconneObjectsMainFrm.BtnOfferClick(Sender: TObject);
begin
  if (Objects.Active) and (not Objects.FieldByName('Id').IsNull)
  then begin
         with ReportsMainFrm
         do begin
              Prepare_offer(Objects.FieldByName('Id').AsInteger);
              ShowModal();
            end;
       end;
end;

procedure TCoconneObjectsMainFrm.BtnPrintInvoiceClick(Sender: TObject);
var Memo: TfrxMemoView;
    InvoiceNumber: longint;
    RelationId: longint;
begin
  if (not InvoicesObjects.Active) or (InvoicesObjects.FieldByName('Id').isNull)
  then exit;

  InvoiceNumber := InvoicesObjects.FieldByName('InvoiceNumber').AsInteger;

  With ReportsMainFrm do
  begin
    UniMainModule.UCompany.Close;
    UniMainModule.UCompany.SQL.Clear;
    UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
    UniMainModule.UCompany.Open;

    // Header
    Memo := InvoiceObjects.FindObject('LblAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Address').AsString);

    Memo := InvoiceObjects.FindObject('LblAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Zipcode').AsString + ' ' + UniMainModule.UCompany.FieldByName('City').AsString);

    Memo := InvoiceObjects.FindObject('LblTelephone') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Phone').AsString);

    Memo := InvoiceObjects.FindObject('LblEmail') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Email').AsString);

    Memo := InvoiceObjects.FindObject('LblWeb') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('Website').AsString);

    // Central
    Memo := InvoiceObjects.FindObject('LblDocDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', InvoicesObjects.FieldByName('InvoiceDate').AsDateTime));

    Memo := InvoiceObjects.FindObject('LblEndDate') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatDateTime('dd/mm/yyyy', InvoicesObjects.FieldByName('DueDate').AsDateTime));

    Memo := InvoiceObjects.FindObject('LblReferenceOffer') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Objects.FieldByName('OfferNumber').AsString);

    Memo := InvoiceObjects.FindObject('LblInvoiceType') as TfrxMemoView;
    Memo.Memo.Clear;
    case InvoicesObjects.FieldByName('InvoiceType').AsInteger of
    0: Memo.Memo.Add('Voorschotfactuur');
    1: Memo.Memo.Add('Saldofactuur');
    2: Memo.Memo.Add('Factuur');
    end;

    Memo := InvoiceObjects.FindObject('LblDescription') as TfrxMemoView;
    Memo.Memo.Clear;
    case InvoicesObjects.FieldByName('InvoiceType').AsInteger of
    0: Memo.Memo.Add('Met dit schrijven zijn wij zo vrij een voorschotfactuur op te stellen voor het door U gevraagde:');
    1: Memo.Memo.Add('Met dit schrijven zijn wij zo vrij een saldofactuur op te stellen voor het door U gevraagde:');
    2: Memo.Memo.Add('Met dit schrijven zijn wij zo vrij een factuur op te stellen voor het door U gevraagde:');
    end;


    Memo := InvoiceObjects.FindObject('LblPaymentTerms') as TfrxMemoView;
    Memo.Memo.Clear;
    case InvoicesObjects.FieldByName('InvoiceType').AsInteger of
    0: Memo.Memo.Add('Gelieve deze factuur te betalen op onderstaande vermelde rekening binnen een periode van 15 dagen. ' +
                     'U dient er rekening mee te houden dat de bestelling pas begint te lopen bij het betalen van dit voorschotbedrag.');
    1: Memo.Memo.Add('Gelieve deze factuur te betalen op onderstaande vermelde rekening binnen een periode van 15 dagen.');
    2: Memo.Memo.Add('Gelieve deze factuur te betalen op onderstaande vermelde rekening binnen een periode van 15 dagen.');
    end;

    // Footer
    Memo := InvoiceObjects.FindObject('LblBank11') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount').AsString);

    Memo := InvoiceObjects.FindObject('LblBank12') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban').AsString);

    Memo := InvoiceObjects.FindObject('LblBank13') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic').AsString);

    Memo := InvoiceObjects.FindObject('LblVat') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.UCompany.FieldByName('VatNumber').AsString);

    Memo := InvoiceObjects.FindObject('LblBank21') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(UniMainModule.UCompany.FieldByName('BankAccount2').AsString);

    Memo := InvoiceObjects.FindObject('LblBank22') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Iban2').AsString) <> '' then
      Memo.Memo.Add('IBAN: ' + UniMainModule.UCompany.FieldByName('Iban2').AsString);

    Memo := InvoiceObjects.FindObject('LblBank23') as TfrxMemoView;
    Memo.Memo.Clear;
    if Trim(UniMainModule.UCompany.FieldByName('Bic2').AsString) <> '' then
      Memo.Memo.Add('BIC: ' + UniMainModule.UCompany.FieldByName('Bic2').AsString);

    // Get Customer details
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select relation from objects where Id=' + Objects.FieldByName('Id').AsString);
    UniMainModule.QWork.Open;
    RelationId := UniMainModule.QWork.FieldByName('Relation').AsInteger;
    UniMainModule.QWork.Close;

    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select * from relations where Id=' + IntToStr(RelationId));
    UniMainModule.QWork.Open;

    Memo := InvoiceObjects.FindObject('LblCustomerName') as TfrxMemoView;
    Memo.Memo.Clear;
    Case UniMainModule.QWork.FieldByName('RelationType').AsInteger of
      0:
        begin
          Memo.Memo.Add(UniMainModule.QWork.FieldByName('CompanyName').AsString);
        end;
      1:
        begin
          if not UniMainModule.QWork.FieldByName('Appelation').isNull then
          begin
            UniMainModule.QWork2.Close;
            UniMainModule.QWork2.SQL.Clear;
            UniMainModule.QWork2.SQL.Add('Select * from basictables where id=' + UniMainModule.QWork.FieldByName('Appelation').AsString);
            UniMainModule.QWork2.Open;
            Memo.Memo.Add(Trim(UniMainModule.QWork2.FieldByName('Dutch').AsString + ' ' + UniMainModule.QWork.FieldByName('Name').AsString + ' ' +
            UniMainModule.QWork.FieldByName('FirstName').AsString));
            UniMainModule.QWork2.Close;
          end
          else
            Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
          end;
        End;

    Memo := InvoiceObjects.FindObject('LblAppelation') as TfrxMemoView;
    Memo.Memo.Clear;
    if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0
    then begin
           if (Trim(UniMainModule.QWork.FieldByName('Name').AsString) <> '') or
              (Trim(UniMainModule.QWork.FieldByName('FirstName').asString) <> '')
           then Memo.Memo.Add('T.a.v. ' + Trim(UniMainModule.QWork.FieldByName('Name').AsString + ' ' + UniMainModule.QWork.FieldByName('FirstName').AsString));
         end;

    Memo := InvoiceObjects.FindObject('LblCustomerAddress1') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('Address').AsString + ' ' + UniMainModule.QWork.FieldByName('HouseNumber').AsString));

    Memo := InvoiceObjects.FindObject('LblCustomerAddress2') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(Trim(UniMainModule.QWork.FieldByName('ZipCode').AsString + ' ' + UniMainModule.QWork.FieldByName('City').AsString));

    Memo := InvoiceObjects.FindObject('LblCustomerVatNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    if UniMainModule.QWork.FieldByName('RelationType').AsInteger = 0 then
    begin
      if Trim(UniMainModule.QWork.FieldByName('VatNumber').AsString) <> '' then
      Memo.Memo.Add('BTW: ' + UniMainModule.QWork.FieldByName('VatNumber').AsString);
    end;
    UniMainModule.QWork.Close;

    // Get sequence number invoice
    Memo := InvoiceObjects.FindObject('InvoiceNumber') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(IntToStr(InvoiceNumber));

    ObjectLines.Close;
    ObjectLines.SQL.Clear;
    ObjectLines.SQL.Add('Select * from ObjectLines where Object=' + Objects.fieldbyname('Id').AsString);
    ObjectLines.Open;

    //Calculate the totals
    Memo := InvoiceObjects.FindObject('LblTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', InvoicesObjects.FieldByName('Amount').AsCurrency));

    Memo := InvoiceObjects.FindObject('LblTotalVat') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', InvoicesObjects.FieldByName('VatAmount').AsCurrency));

    Memo := InvoiceObjects.FindObject('LblGeneralTotal') as TfrxMemoView;
    Memo.Memo.Clear;
    Memo.Memo.Add(FormatCurr('#,##0.00 �', InvoicesObjects.FieldByName('TotalAmount').AsCurrency));

    Prepare_invoice_objects;
    ShowModal();
  end;

end;

procedure TCoconneObjectsMainFrm.CallBackInsertUpdateObject(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if Objects.Active then begin
      Objects.Refresh;
      Objects.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCoconneObjectsMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' RELATIONNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' RELATIONFIRSTNAME LIKE ''%' +
            EditSearch.Text + '%''' + ' OR ' + ' COMPANYNAME LIKE ''%' + EditSearch.Text + '%''';
  Objects.Filter := Filter;
end;

procedure TCoconneObjectsMainFrm.Start_module;
var SQL: string;
begin
  Objects.Close;
  Objects.SQL.Clear;
  SQL := 'SELECT OBJECTS.ID, OBJECTS.OFFERDATE, OBJECTS.AMOUNT, OBJECTS.VATAMOUNT, OBJECTS.TOTALAMOUNT, OBJECTS.OFFERNUMBER, ' +
         'RELATIONS.RELATIONTYPE, RELATIONS.COMPANYNAME, RELATIONS.NAME AS RELATIONNAME, RELATIONS.FIRSTNAME AS RELATIONFIRSTNAME ' +
         'FROM OBJECTS ' +
         'LEFT OUTER JOIN RELATIONS ON (OBJECTS.RELATION = RELATIONS.ID) ' +
         'WHERE OBJECTS.COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' ' +
         'ORDER BY OBJECTS.OFFERNUMBER ASC';
  Objects.SQL.Add(SQL);
  Objects.Open;
  Objects.Last;
  InvoicesObjects.Open;
  EditSearch.SetFocus;
end;

end.
