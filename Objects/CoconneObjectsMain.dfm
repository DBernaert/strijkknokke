object CoconneObjectsMainFrm: TCoconneObjectsMainFrm
  Left = 0
  Top = 0
  Width = 1374
  Height = 851
  Layout = 'border'
  LayoutConfig.Margin = '8 8 8 4'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1374
    Height = 49
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    Layout = 'border'
    LayoutConfig.Region = 'north'
    ExplicitWidth = 1151
    object ContainerTitle: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 1374
      Height = 49
      Hint = ''
      ParentColor = False
      Align = alClient
      TabOrder = 0
      TabStop = False
      LayoutConfig.Region = 'center'
      ExplicitWidth = 1151
      object LblTitle: TUniLabel
        Left = 40
        Top = 0
        Width = 290
        Height = 33
        Hint = ''
        Caption = 'Beheer Coconne Objects'
        ParentFont = False
        Font.Color = clBlack
        Font.Height = -27
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
            '= "lfp-pagetitle";'#13#10'}')
        TabOrder = 1
      end
      object ImageObjects: TUniImage
        Left = 0
        Top = 0
        Width = 32
        Height = 32
        Hint = ''
        AutoSize = True
        Url = 'images/Objects-32-black.png'
      end
    end
  end
  object ContainerBodyLeft: TUniContainerPanel
    Left = 8
    Top = 72
    Width = 449
    Height = 705
    Hint = ''
    ParentColor = False
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'center'
    object ContainerFilterLeftBody: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 449
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Region = 'north'
      ExplicitTop = 8
      ExplicitWidth = 481
      object ContainerSearch: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 273
        Height = 25
        Hint = ''
        ParentColor = False
        Align = alLeft
        TabOrder = 1
        LayoutConfig.Region = 'west'
        object EditSearch: TUniEdit
          Left = 0
          Top = 0
          Width = 265
          Hint = ''
          Text = ''
          TabOrder = 1
          CheckChangeDelay = 500
          ClearButton = True
          OnChange = EditSearchChange
        end
      end
      object ContainerFilterRight: TUniContainerPanel
        Left = 329
        Top = 0
        Width = 120
        Height = 25
        Hint = ''
        ParentColor = False
        Align = alRight
        TabOrder = 2
        LayoutConfig.Region = 'east'
        LayoutConfig.Margin = '0 8 0 0'
        ExplicitLeft = 361
        object NavigatorObjects: TUniDBNavigator
          Left = 0
          Top = 0
          Width = 121
          Height = 25
          Hint = ''
          DataSource = DsObjects
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
          IconSet = icsFontAwesome
          TabStop = False
          TabOrder = 1
        end
      end
    end
    object GridObjects: TUniDBGrid
      Left = 16
      Top = 40
      Width = 305
      Height = 233
      Hint = ''
      DataSource = DsObjects
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      WebOptions.FetchAll = True
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      Images = UniMainModule.ImageList
      ForceFit = True
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 8 8 0'
      TabOrder = 2
      OnDblClick = BtnModifyClick
      Columns = <
        item
          FieldName = 'OFFERNUMBER'
          Title.Alignment = taRightJustify
          Title.Caption = 'NR.'
          Width = 60
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'OFFERDATE'
          Title.Caption = 'DATUM'
          Width = 80
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'RELATIONNAME'
          Title.Caption = 'NAAM'
          Width = 154
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'RELATIONFIRSTNAME'
          Title.Caption = 'VOORNAAM'
          Width = 154
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          Flex = 1
          FieldName = 'COMPANYNAME'
          Title.Caption = 'BEDRIJFSNAAM'
          Width = 304
          ReadOnly = True
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'TOTALAMOUNT'
          Title.Alignment = taRightJustify
          Title.Caption = 'TOTAAL'
          Width = 85
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end>
    end
    object ContainerFooter: TUniContainerPanel
      Left = 0
      Top = 680
      Width = 449
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alBottom
      TabOrder = 3
      LayoutConfig.Region = 'south'
      ExplicitTop = 826
      ExplicitWidth = 1151
      object BtnAdd: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Toevoegen'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddClick
        ButtonTheme = uctDefault
      end
      object BtnModify: TUniThemeButton
        Left = 112
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Wijzigen'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyClick
        ButtonTheme = uctDefault
      end
      object BtnDelete: TUniThemeButton
        Left = 224
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Verwijderen'
        ParentFont = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteClick
        ButtonTheme = uctDefault
      end
      object BtnOffer: TUniThemeButton
        Left = 336
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Offerte'
        ParentFont = False
        TabOrder = 4
        Images = UniMainModule.ImageList
        ImageIndex = 29
        OnClick = BtnOfferClick
        ButtonTheme = uctDefault
      end
    end
  end
  object ContainerBodyRight: TUniContainerPanel
    Left = 480
    Top = 72
    Width = 817
    Height = 705
    Hint = ''
    ParentColor = False
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'east'
    object ContainerFilterRightBody: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 817
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alTop
      TabOrder = 1
      Layout = 'border'
      LayoutConfig.Region = 'north'
      ExplicitTop = 8
      ExplicitWidth = 609
      object UniContainerPanel3: TUniContainerPanel
        Left = 697
        Top = 0
        Width = 120
        Height = 25
        Hint = ''
        ParentColor = False
        Align = alRight
        TabOrder = 1
        LayoutConfig.Region = 'east'
        ExplicitLeft = 489
        object UniDBNavigator1: TUniDBNavigator
          Left = 0
          Top = 0
          Width = 121
          Height = 25
          Hint = ''
          DataSource = DsInvoicesObjects
          VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
          IconSet = icsFontAwesome
          TabStop = False
          TabOrder = 1
        end
      end
    end
    object GridInvoices: TUniDBGrid
      Left = 0
      Top = 32
      Width = 809
      Height = 305
      Hint = ''
      DataSource = DsInvoicesObjects
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
      WebOptions.Paged = False
      WebOptions.FetchAll = True
      LoadMask.Enabled = False
      LoadMask.Message = 'Loading data...'
      Images = UniMainModule.ImageList
      ForceFit = True
      LayoutConfig.Region = 'center'
      LayoutConfig.Margin = '8 0 8 0'
      TabOrder = 2
      Columns = <
        item
          FieldName = 'INVOICENUMBER'
          Title.Alignment = taRightJustify
          Title.Caption = 'NR.'
          Width = 65
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'INVOICEDATE'
          Title.Caption = 'DATUM'
          Width = 120
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'DUEDATE'
          Title.Caption = 'VERVALDATUM'
          Width = 120
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'TOTALAMOUNT'
          Title.Caption = 'INCL. BTW'
          Width = 110
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'TOTALPAYED'
          Title.Alignment = taRightJustify
          Title.Caption = 'BETAALD'
          Width = 110
          Menu.MenuEnabled = False
          Menu.ColumnHideable = False
        end
        item
          FieldName = 'INVOICEPERCENTAGE'
          Title.Alignment = taRightJustify
          Title.Caption = '%'
          Width = 70
        end>
    end
    object UniContainerPanel1: TUniContainerPanel
      Left = 0
      Top = 680
      Width = 817
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alBottom
      TabOrder = 3
      LayoutConfig.Region = 'south'
      ExplicitTop = 826
      ExplicitWidth = 1151
      object BtnInvoice: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Factuur'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnInvoiceClick
        ButtonTheme = uctDefault
      end
      object BtnPrintInvoice: TUniThemeButton
        Left = 112
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Afdrukken'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 29
        OnClick = BtnPrintInvoiceClick
        ButtonTheme = uctDefault
      end
    end
  end
  object Objects: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  OBJECTS.ID,'
      '  OBJECTS.OFFERDATE,'
      '  OBJECTS.AMOUNT,'
      '  OBJECTS.VATAMOUNT,'
      '  OBJECTS.TOTALAMOUNT,'
      '  OBJECTS.OFFERNUMBER,'
      '  RELATIONS.RELATIONTYPE,'
      '  RELATIONS.COMPANYNAME,'
      '  RELATIONS.NAME AS RELATIONNAME,'
      '  RELATIONS.FIRSTNAME AS RELATIONFIRSTNAME'
      'FROM'
      '  OBJECTS'
      '  LEFT OUTER JOIN RELATIONS ON (OBJECTS.RELATION = RELATIONS.ID)'
      'ORDER BY OBJECTS.OFFERNUMBER DESC')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 296
    Top = 272
    object ObjectsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object ObjectsOFFERDATE: TDateField
      FieldName = 'OFFERDATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object ObjectsAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ObjectsVATAMOUNT: TFloatField
      FieldName = 'VATAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ObjectsTOTALAMOUNT: TFloatField
      FieldName = 'TOTALAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ObjectsRELATIONTYPE: TIntegerField
      FieldName = 'RELATIONTYPE'
      ReadOnly = True
    end
    object ObjectsCOMPANYNAME: TWideStringField
      FieldName = 'COMPANYNAME'
      ReadOnly = True
      Size = 50
    end
    object ObjectsRELATIONNAME: TWideStringField
      FieldName = 'RELATIONNAME'
      ReadOnly = True
      Size = 25
    end
    object ObjectsRELATIONFIRSTNAME: TWideStringField
      FieldName = 'RELATIONFIRSTNAME'
      ReadOnly = True
      Size = 25
    end
    object ObjectsOFFERNUMBER: TIntegerField
      FieldName = 'OFFERNUMBER'
    end
  end
  object DsObjects: TDataSource
    DataSet = Objects
    Left = 296
    Top = 328
  end
  object InvoicesObjects: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from ObjectInvoices'
      'order by invoicenumber')
    MasterFields = 'ID'
    DetailFields = 'OBJECT'
    MasterSource = DsObjects
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 944
    Top = 256
    ParamData = <
      item
        DataType = ftLargeint
        Name = 'ID'
        ParamType = ptInput
        Value = 4
      end>
    object InvoicesObjectsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object InvoicesObjectsCOMPANYID: TLargeintField
      FieldName = 'COMPANYID'
      Required = True
    end
    object InvoicesObjectsRELATION: TLargeintField
      FieldName = 'RELATION'
      Required = True
    end
    object InvoicesObjectsINVOICENUMBER: TIntegerField
      FieldName = 'INVOICENUMBER'
      Required = True
    end
    object InvoicesObjectsINVOICEDATE: TDateField
      FieldName = 'INVOICEDATE'
      Required = True
    end
    object InvoicesObjectsPAYMENTTERM: TIntegerField
      FieldName = 'PAYMENTTERM'
      Required = True
    end
    object InvoicesObjectsDUEDATE: TDateField
      FieldName = 'DUEDATE'
      Required = True
    end
    object InvoicesObjectsAMOUNT: TFloatField
      FieldName = 'AMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesObjectsVATAMOUNT: TFloatField
      FieldName = 'VATAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesObjectsTOTALAMOUNT: TFloatField
      FieldName = 'TOTALAMOUNT'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesObjectsOBJECT: TLargeintField
      FieldName = 'OBJECT'
    end
    object InvoicesObjectsTOTALPAYED: TFloatField
      FieldName = 'TOTALPAYED'
      DisplayFormat = '#,##0.00 '#8364
    end
    object InvoicesObjectsINVOICETYPE: TIntegerField
      FieldName = 'INVOICETYPE'
      Required = True
    end
    object InvoicesObjectsINVOICEPERCENTAGE: TFloatField
      FieldName = 'INVOICEPERCENTAGE'
      DisplayFormat = '#,##0.00 %'
    end
    object InvoicesObjectsINVOICESTAGE: TIntegerField
      FieldName = 'INVOICESTAGE'
      Required = True
    end
  end
  object DsInvoicesObjects: TDataSource
    DataSet = InvoicesObjects
    Left = 944
    Top = 312
  end
end
