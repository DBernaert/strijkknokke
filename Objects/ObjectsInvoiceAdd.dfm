object ObjectsInvoiceAddFrm: TObjectsInvoiceAddFrm
  Left = 0
  Top = 0
  ClientHeight = 241
  ClientWidth = 481
  Caption = 'uni'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditDateInvoice
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LblInvoiceType: TUniLabel
    Left = 16
    Top = 56
    Width = 66
    Height = 13
    Hint = ''
    Caption = 'Type factuur:'
    TabOrder = 1
  end
  object RadiobuttonAdvance: TUniRadioButton
    Left = 32
    Top = 80
    Width = 113
    Height = 17
    Hint = ''
    Checked = True
    Caption = 'Voorschotfactuur'
    TabOrder = 2
  end
  object RadiobuttonSaldo: TUniRadioButton
    Left = 32
    Top = 104
    Width = 113
    Height = 17
    Hint = ''
    Caption = 'Saldofactuur'
    TabOrder = 3
  end
  object BtnSave: TUniThemeButton
    Left = 248
    Top = 200
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Aanmaken'
    ParentFont = False
    TabOrder = 6
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 360
    Top = 200
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 7
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object EditPercentageToInvoice: TUniFormattedNumberEdit
    Left = 16
    Top = 160
    Width = 449
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = '%'
    TabOrder = 5
    Value = 50.000000000000000000
    SelectOnFocus = True
    FieldLabel = 'Percentage te factureren *'
    FieldLabelWidth = 150
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object EditDateInvoice: TUniDateTimePicker
    Left = 16
    Top = 16
    Width = 449
    Hint = ''
    DateTime = 43641.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 0
    FieldLabel = 'Datum factuur *'
  end
  object RadiobuttonInvoice: TUniRadioButton
    Left = 32
    Top = 128
    Width = 113
    Height = 17
    Hint = ''
    Caption = 'Factuur'
    TabOrder = 4
  end
end
