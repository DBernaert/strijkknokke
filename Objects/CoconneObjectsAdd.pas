unit CoconneObjectsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox, Data.DB,
  DBAccess, IBC, MemDS, uniDateTimePicker, uniDBDateTimePicker, uniBasicGrid, uniDBGrid, uniLabel, uniButton,
  UniThemeButton, uniPanel, uniRadioGroup, uniDBRadioGroup, uniEdit, uniDBEdit;

type
  TCoconneObjectsAddFrm = class(TUniForm)
    UObjects: TIBCQuery;
    UpdTrUObjects: TIBCTransaction;
    DsUObjects: TDataSource;
    EditRelation: TUniDBLookupComboBox;
    Relations: TIBCQuery;
    RelationsID: TLargeintField;
    RelationsFULLNAME: TWideStringField;
    RelationsCOMPANYNAME: TWideStringField;
    RelationsRELATIONTYPE: TIntegerField;
    RelationsDISPLAYNAME: TStringField;
    DsRelations: TDataSource;
    EditOfferDate: TUniDBDateTimePicker;
    GridObjectLines: TUniDBGrid;
    LblObjects: TUniLabel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    BtnAddCustomer: TUniThemeButton;
    BtnModifyCustomer: TUniThemeButton;
    ObjectLines: TIBCQuery;
    DsObjectLines: TDataSource;
    ObjectLinesID: TLargeintField;
    ObjectLinesCOMPANYID: TLargeintField;
    ObjectLinesOBJECT: TLargeintField;
    ObjectLinesQUANTITY: TIntegerField;
    ObjectLinesAMOUNT: TFloatField;
    ObjectLinesVATAMOUNT: TFloatField;
    ObjectLinesTOTALAMOUNT: TFloatField;
    ObjectLinesDESCRIPTION: TWideMemoField;
    ObjectLinesCUSTOMERAMOUNT: TFloatField;
    ObjectLinesCUSTOMERVATAMOUNT: TFloatField;
    ObjectLinesCUSTOMERTOTALAMOUNT: TFloatField;
    RadiogroupDelivery: TUniDBRadioGroup;
    EditPriceDelivery: TUniDBFormattedNumberEdit;
    EditOfferNumber: TUniDBNumberEdit;
    ObjectLinesTITLE: TWideStringField;
    procedure RelationsCalcFields(DataSet: TDataSet);
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnAddCustomerClick(Sender: TObject);
    procedure BtnModifyCustomerClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
    function  Save_Object: boolean;
    procedure Open_object_lines;
    procedure CallBackInsertUpdateRelation(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateObject(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Initialize_insert;
    procedure Initialize_edit(Id: longint);
  end;

function CoconneObjectsAddFrm: TCoconneObjectsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, ServerModule, CustomersAdd, CoconneObjectsLineAdd, Main, UniFSConfirm;

function CoconneObjectsAddFrm: TCoconneObjectsAddFrm;
begin
  Result := TCoconneObjectsAddFrm(UniMainModule.GetFormInstance(TCoconneObjectsAddFrm));
end;

{ TCoconneObjectsAddFrm }

procedure TCoconneObjectsAddFrm.BtnAddClick(Sender: TObject);
begin
  if Save_Object
  then begin
         Open_object_lines;
         UObjects.Edit;
         UniMainModule.UObjectLines.Close;
         UniMainModule.UObjectLines.SQL.Clear;
         UniMainModule.UObjectLines.SQL.Add('Select first 0 * from objectlines');
         UniMainModule.UObjectLines.Open;
         UniMainModule.UObjectLines.Append;
         UniMainModule.UObjectLines.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
         UniMainModule.UObjectLines.FieldByName('Object').AsInteger    := UObjects.FieldByName('Id').AsInteger;
         UniMainModule.UObjectLines.FieldByName('Quantity').AsInteger  := 1;
         UniMainModule.Result_dbAction := 0;
         With CoconneObjectsLineAddFrm
         do begin
              Start_insert_objectline;
              ShowModal(CallBackInsertUpdateObject);
            end;
       end;
end;

procedure TCoconneObjectsAddFrm.BtnAddCustomerClick(Sender: TObject);
begin
  with CustomersAddFrm
  do begin
       Initialize_insert;
       ShowModal(CallBackInsertUpdateRelation);
     end;
end;

procedure TCoconneObjectsAddFrm.CallBackInsertUpdateObject(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0
  then begin
    if ObjectLines.Active then
    begin
      ObjectLines.Refresh;
      ObjectLines.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCoconneObjectsAddFrm.CallBackInsertUpdateRelation(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Relations.Active then
    begin
      Relations.Refresh;
      Relations.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCoconneObjectsAddFrm.BtnCancelClick(Sender: TObject);
begin
  UObjects.Cancel;
  Close;
end;

procedure TCoconneObjectsAddFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not ObjectLines.Active) or (ObjectLines.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen Object?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UObjectLines.Close;
          UniMainModule.UObjectLines.SQL.Clear;
          UniMainModule.UObjectLines.SQL.Add('Select * from objectlines where id=' + QuotedStr(ObjectLines.fieldbyname('Id').asString));
          UniMainModule.UObjectLines.Open;
          UniMainModule.UObjectLines.Delete;
          UniMainModule.UpdTrObjectLines.Commit;
          UniMainModule.UObjectLines.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrObjectLines.Rollback;
            UniMainModule.UObjects.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        ObjectLines.Refresh;
      end;
    end);
end;

procedure TCoconneObjectsAddFrm.BtnModifyClick(Sender: TObject);
begin
  if (ObjectLines.Active) and (not ObjectLines.FieldByName('Id').IsNull)
  then begin
         UniMainModule.UObjectLines.Close;
         UniMainModule.UObjectLines.SQL.Clear;
         UniMainModule.UObjectLines.SQL.Add('Select * from objectlines where id=' + ObjectLines.FieldByName('Id').asString);
         UniMainModule.UObjectLines.Open;
         Try
           UniMainModule.UObjectLines.Edit;
           with CoconneObjectsLineAddFrm
           do begin
                Start_edit_objectline;
                ShowModal(CallBackInsertUpdateObject);
              end;
         Except
           UniMainModule.UObjectLines.Cancel;
           UniMainModule.UObjectLines.Close;
           UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
         End;
       end;
end;

procedure TCoconneObjectsAddFrm.BtnModifyCustomerClick(Sender: TObject);
begin
  if UObjects.FieldByName('Relation').IsNull
  then exit;

  with CustomersAddFrm
  do begin
       Initialize_edit(UObjects.FieldByName('Relation').AsInteger);
       ShowModal(CallBackInsertUpdateRelation);
     end;
end;

procedure TCoconneObjectsAddFrm.BtnSaveClick(Sender: TObject);
begin
 if Save_object
 then Close;
end;

procedure TCoconneObjectsAddFrm.Initialize_edit(Id: longint);
begin
  Caption  := 'Wijzigen Objects offerte';

  Open_reference_tables;

  UObjects.Close;
  UObjects.SQL.Clear;
  UObjects.SQL.Add('Select * from objects where id=' + IntToStr(Id));
  UObjects.Open;
  Try
    UObjects.Edit;
    Open_object_lines;
    EditOfferNumber.Visible := True;
  Except
    UObjects.Cancel;
    UObjects.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Close;
    Exit;
  End;
end;

procedure TCoconneObjectsAddFrm.Initialize_insert;
begin
  Caption  := 'Toevoegen Objects offerte';

  Open_reference_tables;

  UObjects.Close;
  UObjects.SQL.Clear;
  UObjects.SQL.Add('Select first 0 * from objects');
  UObjects.Open;
  UObjects.Append;
  UObjects.FieldByName('CompanyId').AsInteger      := UniMainModule.Company_id;
  UObjects.FieldByName('OfferDate').AsDateTime     := Date;
  UObjects.FieldByName('Amount').AsCurrency        := 0;
  UObjects.FieldByName('VatAmount').AsCurrency     := 0;
  UObjects.FieldByName('TotalAmount').AsCurrency   := 0;
  UObjects.FieldByName('DeliveryType').AsInteger   := 0;
  UObjects.FieldByName('DeliveryPrice').AsCurrency := 0;
end;

procedure TCoconneObjectsAddFrm.Open_object_lines;
begin
  ObjectLines.Close;
  ObjectLines.SQL.Clear;
  ObjectLines.SQL.Add('Select * from ObjectLines where Object=' + UObjects.fieldbyname('Id').asString);
  ObjectLines.Open;
end;

procedure TCoconneObjectsAddFrm.Open_reference_tables;
begin
  Relations.Close;
  Relations.SQL.Clear;
  Relations.SQL.Add('Select ID, NAME || '' '' || FIRSTNAME AS FULLNAME, COMPANYNAME, RELATIONTYPE from relations' + ' where companyid=' +
    IntToStr(UniMainModule.Company_id) + ' and relationmodule=2 and removed=''0''');
  Relations.Open;
end;

procedure TCoconneObjectsAddFrm.RelationsCalcFields(DataSet: TDataSet);
begin
 case Relations.FieldByName('RelationType').AsInteger of
  0: Relations.FieldByName('DisplayName').AsString := Relations.FieldByName('CompanyName').AsString;
  1: Relations.FieldByName('DisplayName').AsString := Relations.FieldByName('FullName').AsString;
  end;
end;

function TCoconneObjectsAddFrm.Save_Object: boolean;
var amount, vatamount, totalamount: currency;
begin
  Result := False;
  BtnSave.SetFocus;

  if UObjects.FieldByName('OFFERDATE').IsNull
  then begin
         UniMainModule.Show_warning('De datum van de offerte is een verplichte ingave.');
         EditOfferDate.SetFocus;
         Exit;
       end;

  if UObjects.FieldByName('Relation').isNull then
  begin
    UniMainModule.show_warning('De klant is een verplichte ingave.');
    EditRelation.SetFocus;
    exit;
  end;

  if ObjectLines.Active
  then begin
         Amount      := 0;
         Vatamount   := 0;
         Totalamount := 0;
         ObjectLines.First;
         while not ObjectLines.Eof
         do begin
              Amount      := Amount      + (ObjectLines.FieldByName('CustomerAmount').AsCurrency * ObjectLines.Fieldbyname('Quantity').AsInteger);
              vatamount   := Vatamount   + (ObjectLines.FieldByName('CustomerAmount').AsCurrency /100 * 21 * ObjectLines.FieldByName('Quantity').AsInteger);
              TotalAmount := TotalAmount + ObjectLines.FieldByName('CustomerTotalAmount').AsCurrency;
              ObjectLines.Next;
            end;

         UObjects.FieldByName('Amount').AsCurrency := Amount;
         UObjects.FieldByName('VatAmount').AsCurrency := vatAmount;
         UObjects.FieldByName('TotalAmount').AsCurrency := TotalAmount;
       end;

  if UObjects.State = dsInsert
  then begin
         UniMainModule.UCompany.Close;
         UniMainModule.UCompany.SQL.Clear;
         UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
         UniMainModule.UCompany.Open;
         UniMainModule.UCompany.Edit;
         UObjects.FieldByName('OfferNumber').AsInteger := UniMainModule.UCompany.FieldByName('OfferSequenceObjects').AsInteger;
         UniMainModule.UCompany.FieldByName('OfferSequenceObjects').AsInteger :=
         UniMainModule.UCompany.FieldByName('OfferSequenceObjects').AsInteger + 1;
         UniMainModule.UCompany.Post;
         Try
           UniMainModule.UpdTrCompany.Commit;
           UniMainModule.UCompany.Close;
         Except
           UniMainModule.UpdTrCompany.Rollback;
           UniMainModule.UCompany.Close;
           UniMainModule.Show_error('Fout bij het opslaan van de gegevens.');
           Exit;
         end;
       end;

  Try
    UObjects.Post;
    UpdTrUObjects.Commit;
    UniMainModule.Result_dbAction := UObjects.FieldByName('Id').AsInteger;
    Result := True;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UpdTrUObjects.Rollback;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
      Result := False;
    end;
  end;
end;


procedure TCoconneObjectsAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
