unit Login;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIRegClasses, uniGUIForm, uniEdit, uniLabel, uniImage, uniGUIBaseClasses, uniButton, UniThemeButton,
  jpeg, uniPanel, Vcl.Imaging.pngimage;

type
  TLoginFrm = class(TUniLoginForm)
    ContainerBody: TUniContainerPanel;
    ImageLogo: TUniImage;
    LblCopyright: TUniLabel;
    LblLogin: TUniLabel;
    EditLogin: TUniEdit;
    LblPassWord: TUniLabel;
    EditPassWord: TUniEdit;
    LblVersion: TUniLabel;
    BtnLogin: TUniLabel;
    BtnCancel: TUniLabel;
    procedure BtnCancelClick(Sender: TObject);
    procedure UniLoginFormShow(Sender: TObject);
    procedure BtnLoginClick(Sender: TObject);
    procedure UniLoginFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function LoginFrm: TLoginFrm;

implementation

{$R *.dfm}

uses
  uniGUIVars, MainModule, uniGUIApplication, ServerModule, Db, Utils, Main;

function LoginFrm: TLoginFrm;
begin
  Result := TLoginFrm(UniMainModule.GetFormInstance(TLoginFrm));
end;

procedure TLoginFrm.BtnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
  UniSession.Terminate('<script>window.location.href = "https://www.strijkknokke.be";</script>');
end;

procedure TLoginFrm.BtnLoginClick(Sender: TObject);
begin
  Try
    UniMainModule.DbModule.Connected := False;
    UniMainModule.DbModule.Connected := true;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message);
       MessageDlg('Fout bij het connecteren aan de Strijk Knokke database.', mtError, [mbOK]);
       ModalResult := mrCancel;
       UniSession.Terminate('<script>window.location.href = "https://www.strijkknokke.be";</script>');
     end;
  end;

  if Trim(EditLogin.Text) = ''
  then begin
         EditLogin.SetFocus;
         UniMainModule.show_warning('Gelieve uw login te in te geven.');
         Exit;
       end;

  if Trim(EditPassWord.Text) = ''
  then begin
         EditPassWord.SetFocus;
         UniMainModule.show_warning('Gelieve uw wachtwoord in te geven.');
         Exit;
       end;

  //Search the user
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select * from users where email=' + QuotedStr(EditLogin.Text) +
                              ' and removed=''0''');
  UniMainModule.QWork.Open;

  if UniMainModule.QWork.RecordCount = 0
  then begin
         UniMainModule.QWork.Close;
         UniMainModule.show_error('Ongeldige login.');
         Exit;
       end;


  if UpperCase(Utils.Decrypt(UniMainModule.QWork.FieldByName('PASSWORD').asString, 35462)) <>
     UpperCase(EditPassWord.Text)
  then begin
         UniMainModule.QWork.Close;
         UniMainModule.show_error('Ongeldige login.');
         Exit;
       end;


  //Load avatar image
  if not UniMainModule.QWork.Fieldbyname('Photo').isNull
  then try
         UniMainModule.Avatar.Assign(UniMainModule.QWork.FieldByName('Photo'));
         UniMainModule.ShowAvatar := True;
       except
         UniMainModule.ShowAvatar := False;
       end
  else UniMainModule.ShowAvatar := False;

  //Set cookies
  UniApplication.Cookies.SetCookie('_CoconneLogin', EditLogin.Text, Date + 60.0);

  UniMainModule.ProductId         := 4; //Coconne module
  UniMainModule.User_id           := UniMainModule.QWork.FieldByName('ID').AsInteger;
  UniMainModule.User_name         := UniMainModule.QWork.FieldByName('NAME').AsString;
  UniMainModule.User_firstname    := UniMainModule.QWork.FieldByName('FIRSTNAME').AsString;
  UniMainModule.User_email        := UniMainModule.QWork.FieldByName('EMAIL').AsString;
  UniMainModule.User_hourlyRate   := UniMainModule.QWork.FieldByName('HourlyRate').AsCurrency;
  UniMainModule.Company_id        := UniMainModule.QWork.FieldByName('COMPANYID').AsInteger;

  if UniMainModule.QWork.FieldByName('AdminUser').AsInteger = 1
  then UniMainModule.User_admin := True
  else UniMainModule.User_admin := False;

  UniMainModule.QWork.Close;

  //Get the company name
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select name, kilometerAllowance from companyprofile where Id=' + IntToStr(UniMainModule.Company_id));
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.RecordCount = 1
  then begin
         UniMainModule.Company_name := UniMainModule.QWork.FieldByName('Name').AsString;
         UniMainModule.KilometerAllowance := UniMainModule.QWork.FieldByName('KilometerAllowance').AsCurrency;
       end
  else begin
         UniMainModule.Company_name := '';
         UniMainModule.KilometerAllowance := 0;
       end;
  UniMainModule.QWork.Close;

  ModalResult := mrOk;
end;

procedure TLoginFrm.UniLoginFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TLoginFrm.UniLoginFormShow(Sender: TObject);
var tempvalue: string;
begin
  tempvalue := UniApplication.Cookies.Values['_CoconneLogin'];
  if Trim(Tempvalue) <> ''
  then begin
         EditLogin.Text := TempValue;
         ActiveControl  := EditPassWord;
       end;
  LblVersion.Caption := 'Release ' + Utils.GetSoftwareVersionFormatted;
end;

initialization
  RegisterAppFormClass(TLoginFrm);

end.
