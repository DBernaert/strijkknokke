unit CustomersMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, Data.DB, MemDS, DBAccess, IBC, uniButton,
  UniThemeButton, uniEdit, uniBasicGrid, uniDBGrid, uniDBNavigator;

type
  TCustomersMainFrm = class(TUniFrame)
    Relations: TIBCQuery;
    DsRelations: TDataSource;
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageOffers: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    GridRelations: TUniDBGrid;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorCustomers: TUniDBNavigator;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    procedure EditSearchChange(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure GridRelationsFieldImage(const Column: TUniDBGridColumn; const AField: TField; var OutImage: TGraphic;
      var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
  private
    { Private declarations }
    procedure CallBackInsertUpdateRelation(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, CustomersAdd, Main, UniFSConfirm;



{ TCustomersMainFrm }

procedure TCustomersMainFrm.BtnAddClick(Sender: TObject);
begin
 UniMainModule.Result_dbAction := 0;
  With CustomersAddFrm do
  begin
    if Initialize_insert = True
    then ShowModal(CallBackInsertUpdateRelation);
  end;
end;

procedure TCustomersMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Relations.Active) or (Relations.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen klant?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.URelations.Close;
          UniMainModule.URelations.SQL.Clear;
          UniMainModule.URelations.SQL.Add('Select * from relations where id=' + QuotedStr(Relations.fieldbyname('Id').asString));
          UniMainModule.URelations.Open;
          UniMainModule.URelations.Edit;
          UniMainModule.URelations.fieldbyname('Removed').asString := '1';
          UniMainModule.URelations.Post;
          UniMainModule.UpdTrRelations.Commit;
          UniMainModule.URelations.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrRelations.Rollback;
            UniMainModule.URelations.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Relations.Refresh;
      end;
    end);
end;

procedure TCustomersMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Relations.Active) and (not Relations.fieldbyname('Id').isNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With CustomersAddFrm do
    begin
      if Initialize_edit(Relations.fieldbyname('Id').AsInteger) = True
      then ShowModal(CallBackInsertUpdateRelation)
      else UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    end;
  end;
end;

procedure TCustomersMainFrm.CallBackInsertUpdateRelation(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Relations.Active then
    begin
      Relations.Refresh;
      Relations.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TCustomersMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' FULLNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' COMPANYNAME LIKE ''%' +
    EditSearch.Text + '%''' + ' OR ' + ' CITY LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' PHONE LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
    ' MOBILE LIKE ''%' + EditSearch.Text + '%''';
  Relations.Filter := Filter;
end;

procedure TCustomersMainFrm.GridRelationsFieldImage(const Column: TUniDBGridColumn; const AField: TField;
  var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(UpperCase(AField.FieldName), 'INVOICENEEDED') then
  begin DoNotDispose := True; // we provide an static image so do not free it.
    Case AField.AsInteger of
     0: OutImage := MainForm.ImageEmpty.Picture.Graphic;
     1: OutImage := MainForm.ImageGreen.Picture.Graphic;
    End;
  end;

  if SameText(UpperCase(AField.FieldName), 'CONTACTEMAILMOBILE') then
  begin DoNotDispose := True; // we provide an static image so do not free it.
    Case AField.AsInteger of
     0: OutImage := MainForm.ImageRed.Picture.Graphic;
     1: OutImage := MainForm.ImageGreen.Picture.Graphic;
    End;
  end;
end;

procedure TCustomersMainFrm.Start_module;
begin
  Relations.Close;
  Relations.SQL.Clear;
  Relations.SQL.Add('Select ID, coalesce(NAME,'''') || '' '' || coalesce(FIRSTNAME,'''') AS FULLNAME, COMPANYNAME, CITY, PHONE, MOBILE, ' +
                    'INVOICENEEDED, CONTACTEMAILMOBILE ' +
                    'from relations ' + ' where companyId=' +
                    IntToStr(UniMainModule.Company_id) + ' and Removed=''0''' + ' order by Name, Firstname');
  Relations.Open;
  EditSearch.SetFocus;
end;

end.
