unit CustomersAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniMemo, uniDBMemo, uniCheckBox, uniDBCheckBox, uniDBComboBox, uniDBLookupComboBox,
  uniEdit, uniDBEdit, uniMultiItem, uniComboBox, uniPanel, uniPageControl, uniGUIBaseClasses, DBAccess, IBC, Data.DB,
  MemDS, uniButton, UniThemeButton;

type
  TCustomersAddFrm = class(TUniForm)
    URelations: TIBCQuery;
    DsURelations: TDataSource;
    UpdTrRelations: TIBCTransaction;
    Countries: TIBCQuery;
    DsCountries: TDataSource;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    Languages: TIBCQuery;
    DsLanguages: TDataSource;
    CompanyTypes: TIBCQuery;
    DsCompanyTypes: TDataSource;
    Sectors: TIBCQuery;
    DsSectors: TDataSource;
    VatRegimes: TIBCQuery;
    DsVatRegimes: TDataSource;
    Appelations: TIBCQuery;
    DsAppelations: TDataSource;
    PageControlCompanies: TUniPageControl;
    TabSheetGeneral: TUniTabSheet;
    ComboRelationType: TUniComboBox;
    EditCompanyName: TUniDBEdit;
    EditAppelation: TUniDBLookupComboBox;
    EditName: TUniDBEdit;
    EditFirstName: TUniDBEdit;
    EditAddres: TUniDBEdit;
    EditHousenumber: TUniDBEdit;
    EditZipCode: TUniDBEdit;
    EditCity: TUniDBEdit;
    EditRegion: TUniDBEdit;
    ComboCountryCode: TUniDBLookupComboBox;
    EditMobile: TUniDBEdit;
    EditPhone: TUniDBEdit;
    EditEmail: TUniDBEdit;
    EditFax: TUniDBEdit;
    EditWebsite: TUniDBEdit;
    EditVatNumber: TUniDBEdit;
    EditIban: TUniDBEdit;
    EditBic: TUniDBEdit;
    ComboCompanyType: TUniDBLookupComboBox;
    ComboVatRegime: TUniDBLookupComboBox;
    ComboSector: TUniDBLookupComboBox;
    ComboAccountManager: TUniDBLookupComboBox;
    ComboLanguage: TUniDBLookupComboBox;
    CheckboxMarketing: TUniDBCheckBox;
    EditRemarks: TUniDBMemo;
    EditInvoiceEmail: TUniDBEdit;
    EditNisNumber: TUniDBEdit;
    TabSheetContact: TUniTabSheet;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    CheckBoxInvoiceNeeded: TUniDBCheckBox;
    CheckboxContactEmailMobile: TUniDBCheckBox;
    EditContactEmail1: TUniDBEdit;
    EditContactEmail2: TUniDBEdit;
    EditContactEmail3: TUniDBEdit;
    EditContactEmail4: TUniDBEdit;
    EditContactMobile1: TUniDBEdit;
    EditContactMobile2: TUniDBEdit;
    EditContactMobile3: TUniDBEdit;
    EditContactMobile4: TUniDBEdit;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure EditCityExit(Sender: TObject);
    procedure EditZipCodeExit(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure CallBackSelectZipCode(Sender: TComponent; AResult: Integer);
    procedure CallBackSelectCity(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Initialize_insert: boolean;
    function Initialize_edit(Id: longint): boolean;
  end;

function CustomersAddFrm: TCustomersAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule, ZipCodesSelect;

function CustomersAddFrm: TCustomersAddFrm;
begin
  Result := TCustomersAddFrm(UniMainModule.GetFormInstance(TCustomersAddFrm));
end;

{ TCustomersAddFrm }

procedure TCustomersAddFrm.BtnCancelClick(Sender: TObject);
begin
  URelations.Cancel;
  URelations.Close;
  Close;
end;

procedure TCustomersAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if ComboRelationType.ItemIndex = -1
  then begin
         UniMainModule.show_warning('Type is een verplichte ingave.');
         ComboRelationType.SetFocus;
         Exit;
       end;

  if (ComboRelationType.ItemIndex = 0) and (Trim(URelations.FieldByName('COMPANYNAME').AsString) = '')
  then begin
         UniMainModule.show_warning('De bedrijfsnaam is een verplichte ingave.');
         EditCompanyName.SetFocus;
         Exit;
       end;

  if (ComboRelationType.ItemIndex = 1) and (Trim(URelations.FieldByName('NAME').AsString) = '')
  then begin
         UniMainModule.show_warning('De naam is een verplichte ingave.');
         EditName.SetFocus;
         Exit;
       end;

  URelations.FieldByName('RelationType').AsInteger   :=  ComboRelationType.ItemIndex;

  Try
    URelations.Post;
    UpdTrRelations.Commit;
    UniMainModule.Result_dbAction := URelations.FieldByName('Id').AsInteger;
    URelations.Close;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       UpdTrRelations.Rollback;
       URelations.Close;
       UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
     end;
  end;
  Close;
end;

procedure TCustomersAddFrm.CallBackSelectCity(Sender: TComponent; AResult: Integer);
begin
  if Trim(UniMainModule.Result_dbAction_string) <> ''
  then URelations.FieldByName('City').AsString := UniMainModule.Result_dbAction_string;
end;

procedure TCustomersAddFrm.CallBackSelectZipCode(Sender: TComponent; AResult: Integer);
begin
  if Trim(UniMainModule.Result_dbAction_string) <> ''
  then URelations.FieldByName('ZipCode').AsString := UniMainModule.Result_dbAction_string;
end;

procedure TCustomersAddFrm.EditCityExit(Sender: TObject);
var Zipcode: string;
begin
  if (Trim(EditCity.Text) <> '') and (Trim(EditZipCode.Text) = '')
  then begin
         Zipcode := UniMainModule.Search_zipcode(EditCity.Text);
         if Zipcode = '0'
         then exit;

         if Zipcode = '+1'
         then begin
                UniMainModule.Result_dbAction_string := '';
                With ZipCodesSelectFrm do begin
                  Init_zipcode_selection(EditCity.Text);
                  ShowModal(CallBackSelectZipCode);
                end;
              end
         else URelations.FieldByName('ZipCode').AsString := Zipcode;
       end;
end;

procedure TCustomersAddFrm.EditZipCodeExit(Sender: TObject);
var city: string;
begin
  if (Trim(EditZipcode.Text) <> '') and (Trim(EditCity.Text) = '')
  then begin
         city := UniMainModule.Search_city(UpperCase(EditZipCode.Text));
         if city = '0'
         then exit;

         if city = '+1'
         then begin
                UniMainModule.Result_dbAction_string := '';
                With ZipCodesSelectFrm do begin
                  Init_city_selection(EditZipCode.Text);
                  ShowModal(CallBackSelectCity);
                end;
              end
         else URelations.FieldByName('City').AsString := City;
       end;
end;

function TCustomersAddFrm.Initialize_edit(Id: longint): boolean;
begin
  Caption  := 'Wijzigen klant';
  Try
    Open_reference_tables;

    URelations.Close;
    URelations.SQL.Clear;
    URelations.SQL.Add('Select * from relations where id=' + IntToStr(Id));
    URelations.Open;
    URelations.Edit;
    ComboRelationType.ItemIndex := URelations.FieldByName('RelationType').AsInteger;
    Result := True;
  Except
    URelations.Cancel;
    URelations.Close;
    Result := False;
  End;
end;

function TCustomersAddFrm.Initialize_insert: boolean;
begin
  Caption  := 'Toevoegen klant';
  Try
    Open_reference_tables;

    URelations.Close;
    URelations.SQL.Clear;
    URelations.SQL.Add('Select first 0 * from relations');
    URelations.Open;
    URelations.Append;
    URelations.FieldByName('CompanyId').AsInteger      := UniMainModule.Company_id;
    URelations.FieldByName('Removed').AsString         := '0';
    URelations.FieldByName('RelationType').AsInteger   :=  0;
    URelations.FieldByName('CountryCode').AsString     := 'BEL';
    URelations.FieldByName('Marketing').AsInteger      := 1;
    URelations.FieldByName('InvoiceNeeded').AsInteger  := 0;
    URelations.FieldByName('ContactEmailMobile').AsInteger := 1;
    URelations.FieldByName('AccountManager').AsInteger := UniMainModule.User_id;
    ComboRelationType.ItemIndex                        := 1;
    Result := True;
  Except
    URelations.Close;
    Result := False;
  End;
end;

procedure TCustomersAddFrm.Open_reference_tables;
begin
  Countries.Close;
  Countries.SQL.Clear;
  Countries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by dutch');
  Countries.Open;

  Users.Close;
  Users.SQL.Clear;
  Users.SQL.Add('Select ID, NAME || '' '' || FIRSTNAME AS "USERFULLNAME" from users ' +
                'where COMPANYID=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by USERFULLNAME');
  Users.Open;

  Languages.Close;
  Languages.SQL.Clear;
  Languages.SQL.Add('Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
                    ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
                    ' and TABLEID=3 ' +
                    ' and REMOVED=''0''' +
                    ' order by DUTCH');
  Languages.Open;

  CompanyTypes.Close;
  CompanyTypes.SQL.Clear;
  CompanyTypes.SQL.Add('Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
                       ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
                       ' and TABLEID=4 ' +
                       ' and REMOVED=''0''' +
                       ' order by DUTCH');
  CompanyTypes.Open;

  Sectors.Close;
  Sectors.SQL.Clear;
  Sectors.SQL.Add('Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
                  ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
                  ' and TABLEID=5 ' +
                  ' and REMOVED=''0''' +
                  ' order by DUTCH');
  Sectors.Open;

  VatRegimes.Close;
  VatRegimes.SQL.Clear;
  VatRegimes.SQL.Add('Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
                  ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
                  ' and TABLEID=7 ' +
                  ' and REMOVED=''0''' +
                  ' order by DUTCH');
  VatRegimes.Open;

  Appelations.Close;
  Appelations.SQL.Clear;
  Appelations.SQL.Add('Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
                  ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
                  ' and TABLEID=1 ' +
                  ' and REMOVED=''0''' +
                  ' order by DUTCH');
  Appelations.Open;
end;

procedure TCustomersAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TCustomersAddFrm.UniFormReady(Sender: TObject);
begin
  EditRemarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

end.
