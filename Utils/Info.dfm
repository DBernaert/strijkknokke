object InfoFrm: TInfoFrm
  Left = 0
  Top = 0
  ClientHeight = 497
  ClientWidth = 641
  Caption = 'Info'
  OnShow = UniFormShow
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnSubmit = BtnOkClick
  OnCancel = BtnOkClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LblCopyright: TUniLabel
    Left = 131
    Top = 368
    Width = 379
    Height = 13
    Hint = ''
    Alignment = taCenter
    AutoSize = False
    Caption = 'Copyright 2019 Strijk Knokke'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 0
  end
  object LblDevelopment: TUniLabel
    Left = 131
    Top = 384
    Width = 379
    Height = 13
    Hint = ''
    Alignment = taCenter
    AutoSize = False
    Caption = 'Development by Adm-Concept'
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 1
  end
  object LblVersion: TUniLabel
    Left = 131
    Top = 413
    Width = 379
    Height = 13
    Hint = ''
    Alignment = taCenter
    AutoSize = False
    Caption = 'LblVersion'
    ParentFont = False
    Font.Color = clGray
    TabOrder = 2
  end
  object BtnOk: TUniThemeButton
    Left = 520
    Top = 456
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Sluiten'
    ParentFont = False
    TabOrder = 3
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnOkClick
    ButtonTheme = uctSecondary
  end
  object ImageWaveDesk: TUniImage
    Left = 121
    Top = 16
    Width = 400
    Height = 329
    Hint = ''
    AutoSize = True
    Url = 'images/Logo.png'
  end
end
