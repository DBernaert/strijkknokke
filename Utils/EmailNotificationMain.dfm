object EmailNotificationMainFrm: TEmailNotificationMainFrm
  Left = 0
  Top = 0
  ClientHeight = 129
  ClientWidth = 577
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = CheckBoxYesNo
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object CheckBoxYesNo: TUniCheckBox
    Left = 16
    Top = 16
    Width = 545
    Height = 17
    Hint = ''
    Caption = ''
    ParentFont = False
    Font.Style = [fsBold]
    TabOrder = 0
    FieldLabel = 'Ik wens release notes te ontvangen via mail'
    FieldLabelWidth = 250
  end
  object EditLanguage: TUniComboBox
    Left = 16
    Top = 48
    Width = 545
    Hint = ''
    Text = 'EditLanguage'
    Items.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    TabOrder = 1
    ForceSelection = True
    FieldLabel = 'Taal release notes <font color="#cd0015">*</font>'
    FieldLabelWidth = 250
    IconItems = <>
  end
  object BtnSave: TUniThemeButton
    Left = 344
    Top = 88
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 2
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 456
    Top = 88
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 3
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object Email: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_MAILINGLIST_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO MAILINGLIST'
      '  (ID, EMAILADDRESS, PRODUCTID, LANGUAGE)'
      'VALUES'
      '  (:ID, :EMAILADDRESS, :PRODUCTID, :LANGUAGE)'
      'RETURNING '
      '  ID, EMAILADDRESS, PRODUCTID, LANGUAGE')
    SQLDelete.Strings = (
      'DELETE FROM MAILINGLIST'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE MAILINGLIST'
      'SET'
      
        '  ID = :ID, EMAILADDRESS = :EMAILADDRESS, PRODUCTID = :PRODUCTID' +
        ', LANGUAGE = :LANGUAGE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, EMAILADDRESS, PRODUCTID, LANGUAGE FROM MAILINGLIST'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM MAILINGLIST'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM MAILINGLIST'
      ''
      ') q')
    Connection = UniMainModule.DbAdmin
    UpdateTransaction = UpdTrEmail
    SQL.Strings = (
      'Select * from mailinglist')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 112
    Top = 21
  end
  object DsEmail: TDataSource
    DataSet = Email
    Left = 176
    Top = 24
  end
  object UpdTrEmail: TIBCTransaction
    DefaultConnection = UniMainModule.DbAdmin
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 312
    Top = 31
  end
end
