unit Info;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniLabel, uniGUIBaseClasses, uniImage, uniButton, UniThemeButton, uniPanel,
  Vcl.Imaging.pngimage;

type
  TInfoFrm = class(TUniForm)
    LblCopyright: TUniLabel;
    LblDevelopment: TUniLabel;
    LblVersion: TUniLabel;
    BtnOk: TUniThemeButton;
    ImageWaveDesk: TUniImage;
    procedure BtnOkClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

function InfoFrm: TInfoFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Utils;

function InfoFrm: TInfoFrm;
begin
  Result := TInfoFrm(UniMainModule.GetFormInstance(TInfoFrm));
end;

procedure TInfoFrm.BtnOkClick(Sender: TObject);
begin
  Close;
end;

procedure TInfoFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TInfoFrm.UniFormShow(Sender: TObject);
begin
  LblVersion.Caption := 'Release ' + Utils.GetSoftwareVersionFormatted;
end;

end.
