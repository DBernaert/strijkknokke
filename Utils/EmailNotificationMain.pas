unit EmailNotificationMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, Data.DB, MemDS, uniMultiItem, uniComboBox, uniGUIBaseClasses, uniCheckBox,
  uniButton, UniThemeButton, uniPanel;

type
  TEmailNotificationMainFrm = class(TUniForm)
    Email: TIBCQuery;
    DsEmail: TDataSource;
    UpdTrEmail: TIBCTransaction;
    CheckBoxYesNo: TUniCheckBox;
    EditLanguage: TUniComboBox;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_notification_edit;
  end;

function EmailNotificationMainFrm: TEmailNotificationMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule;

function EmailNotificationMainFrm: TEmailNotificationMainFrm;
begin
  Result := TEmailNotificationMainFrm(UniMainModule.GetFormInstance(TEmailNotificationMainFrm));
end;

{ TEmailNotificationMainFrm }

procedure TEmailNotificationMainFrm.BtnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TEmailNotificationMainFrm.BtnSaveClick(Sender: TObject);
begin
  if EditLanguage.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('De taal is een verplichte ingave.');
    EditLanguage.SetFocus;
    Exit;
  end;

  Email.Close;
  Email.SQL.Clear;
  Email.SQL.Add('Select * from mailinglist where productid=' + IntToStr(UniMainModule.ProductId) + ' and emailaddress=' + QuotedStr(UniMainModule.User_email));
  Email.Open;
  if CheckBoxYesNo.Checked then
  begin
    if Email.RecordCount = 1 then
    begin
      Email.Edit;
      Email.FieldByName('Language').AsInteger := EditLanguage.ItemIndex;
      Email.Post;
    end
    else
    begin
      Email.Append;
      Email.FieldByName('ProductId').AsInteger := UniMainModule.ProductId;
      Email.FieldByName('EmailAddress').AsString := UniMainModule.User_email;
      Email.FieldByName('Language').AsInteger := EditLanguage.ItemIndex;
      Email.Post;
    end;
  end
  else
  begin
    if Email.RecordCount = 1 then
    begin
      Email.Delete;
    end
    else
    begin
      Email.Close;
      Close;
      Exit;
    end;
  end;

  Try
    UpdTrEmail.Commit;
    Email.Close;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UpdTrEmail.Rollback;
      Email.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;
  Close;
end;

procedure TEmailNotificationMainFrm.Init_notification_edit;
begin
  Caption := 'Release notes via mail';
  Email.Close;
  Email.SQL.Clear;
  Email.SQL.Add('Select * from mailinglist where productid=' + IntToStr(UniMainModule.ProductId) + ' and emailaddress=' + QuotedStr(UniMainModule.User_email));
  Email.Open;
  if Email.RecordCount = 0 then
  begin
    CheckBoxYesNo.Checked := False;
    EditLanguage.ItemIndex := 0;
  end
  else
  begin
    CheckBoxYesNo.Checked := True;
    EditLanguage.ItemIndex := Email.FieldByName('Language').AsInteger;
  end;
  Email.Close;
end;

procedure TEmailNotificationMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
