unit TimeRegistrationsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, Data.DB, MemDS, uniMemo, uniDBMemo, uniDBEdit, uniMultiItem, uniComboBox,
  uniDBComboBox, uniDBLookupComboBox, uniEdit, uniGUIBaseClasses, uniDateTimePicker, uniDBDateTimePicker, uniButton,
  UniThemeButton, uniPanel;

type
  TTimeRegistrationsAddFrm = class(TUniForm)
    UTimeRegistrations: TIBCQuery;
    DsUTimeRegistrations: TDataSource;
    UpdTrTimeRegistrations: TIBCTransaction;
    Projects: TIBCQuery;
    DsProjects: TDataSource;
    Activities: TIBCQuery;
    DsActivities: TDataSource;
    EditRegistrationDate: TUniDBDateTimePicker;
    EditRegistrationStartTime: TUniDBFormattedNumberEdit;
    EditRegistrationEndTime: TUniDBFormattedNumberEdit;
    EditKilometers: TUniDBFormattedNumberEdit;
    EditProject: TUniDBLookupComboBox;
    EditActivity: TUniDBLookupComboBox;
    EditDescription: TUniDBEdit;
    EditInternalRemarks: TUniDBMemo;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables(new: boolean);
  public
    { Public declarations }
    procedure Initialize_insert;
    procedure Initialize_edit(Id: longint);
  end;

function TimeRegistrationsAddFrm: TTimeRegistrationsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule;

function TimeRegistrationsAddFrm: TTimeRegistrationsAddFrm;
begin
  Result := TTimeRegistrationsAddFrm(UniMainModule.GetFormInstance(TTimeRegistrationsAddFrm));
end;

{ TTimeRegistrationsAddFrm }

procedure TTimeRegistrationsAddFrm.BtnCancelClick(Sender: TObject);
begin
  UTimeRegistrations.Cancel;
  UTimeRegistrations.Close;
  Close;
end;

procedure TTimeRegistrationsAddFrm.BtnSaveClick(Sender: TObject);
var decimalvalue: currency;
begin
  BtnSave.SetFocus;

  if UTimeRegistrations.FieldByName('REGISTRATIONDATE').IsNull then
  begin
    UniMainModule.show_warning('De datum is een verplichte ingave.');
    EditRegistrationDate.SetFocus;
    Exit;
  end;

  if UTimeRegistrations.FieldByName('STARTTIMEVALUE').IsNull then
  begin
    UniMainModule.show_warning('De begintijd is een verplichte ingave.');
    EditRegistrationStartTime.SetFocus;
    Exit;
  end;

  if UTimeRegistrations.FieldByName('ENDTIMEVALUE').IsNull then
  begin
    UniMainModule.show_warning('De eindtijd is een verplichte ingave.');
    EditRegistrationEndTime.SetFocus;
    Exit;
  end;

  if UTimeRegistrations.FieldByName('KILOMETERS').IsNull then
  begin
    UniMainModule.show_warning('Het aantal kilometer is een verplichte ingave.');
    EditKilometers.SetFocus;
    Exit;
  end;

  if UTimeRegistrations.FieldByName('PROJECT').IsNull then
  begin
    UniMainModule.show_warning('Het project is een verplichte ingave.');
    EditProject.SetFocus;
    Exit;
  end;

  if UTimeRegistrations.FieldByName('ACTIVITY').IsNull then
  begin
    UniMainModule.show_warning('De activiteit is een verplichte ingave.');
    EditActivity.SetFocus;
    Exit;
  end;

  if Trim(UTimeRegistrations.FieldByName('DESCRIPTION').asString) = '' then
  begin
    UniMainModule.show_warning('De omschrijving is een verplichte ingave.');
    EditDescription.SetFocus;
    Exit;
  end;

  //Check entry of timeslots
  //Check StartTimeValue
  if (UTimeRegistrations.FieldByName('StartTimeValue').AsCurrency < 1) or
     (UTimeRegistrations.Fieldbyname('StartTimeValue').asCurrency > 24)
  then begin
         UniMainModule.Show_warning('De waarde voor begintijd is ongeldig.');
         EditRegistrationStartTime.SetFocus;
         Exit;
       end;

  decimalvalue := UTimeRegistrations.FieldByName('StartTimeValue').AsCurrency -
                  trunc(UTimeRegistrations.FieldByName('StartTimeValue').AsCurrency);

  if (decimalvalue <> 0) and (decimalvalue <> 0.25) and (decimalvalue <> 0.50) and (decimalvalue <> 0.75)
  then begin
         UniMainModule.Show_warning('De waarde voor begintijd is ongeldig.');
         EditRegistrationStartTime.SetFocus;
         Exit;
       end;

  //Check EndTimeValue
  if (UTimeRegistrations.FieldByName('EndTimeValue').AsCurrency < 1) or
     (UTimeRegistrations.Fieldbyname('EndTimeValue').asCurrency > 24)
  then begin
         UniMainModule.Show_warning('De waarde voor eindtijd is ongeldig.');
         EditRegistrationEndTime.SetFocus;
         Exit;
       end;

  decimalvalue := UTimeRegistrations.FieldByName('EndTimeValue').AsCurrency -
                  trunc(UTimeRegistrations.FieldByName('EndTimeValue').AsCurrency);

  if (decimalvalue <> 0) and (decimalvalue <> 0.25) and (decimalvalue <> 0.50) and (decimalvalue <> 0.75)
  then begin
         UniMainModule.Show_warning('De waarde voor eindtijd is ongeldig.');
         EditRegistrationEndTime.SetFocus;
         Exit;
       end;


  UTimeRegistrations.FieldByName('SaldoValue').AsCurrency := UTimeRegistrations.FieldByName('EndTimeValue').AsCurrency -
    UTimeRegistrations.FieldByName('StartTimeValue').AsCurrency;

  // Find the hourly rate
  if Projects.Locate('Id', UTimeRegistrations.FieldByName('Project').AsInteger, []) then
    UTimeRegistrations.FieldByName('HourlyRate').AsCurrency := Projects.FieldByName('HourlyRate').AsCurrency
  else
  begin
    UniMainModule.show_warning('Uurtarief niet teruggevonden.');
    Exit;
  end;

  Try
    UTimeRegistrations.Post;
    UpdTrTimeRegistrations.Commit;
    UniMainModule.Result_dbAction := UTimeRegistrations.FieldByName('Id').AsInteger;
    UTimeRegistrations.Close;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UpdTrTimeRegistrations.Rollback;
      UTimeRegistrations.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;
  Close;

end;

procedure TTimeRegistrationsAddFrm.Initialize_edit(Id: longint);
begin
  Caption := 'Wijzigen tijdsregistratie';

  Open_reference_tables(false);

  UTimeRegistrations.Close;
  UTimeRegistrations.SQL.Clear;
  UTimeRegistrations.SQL.Add('Select * from timeregistrations where id=' + IntToStr(Id));
  UTimeRegistrations.Open;
  Try
    UTimeRegistrations.Edit;
  Except
    UTimeRegistrations.Cancel;
    UTimeRegistrations.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Close;
    Exit;
  End;
end;

procedure TTimeRegistrationsAddFrm.Initialize_insert;
begin
  Caption := 'Toevoegen tijdsregistratie';
  Open_reference_tables(true);
  UTimeRegistrations.Close;
  UTimeRegistrations.SQL.Clear;
  UTimeRegistrations.SQL.Add('Select first 0 * from timeregistrations');
  UTimeRegistrations.Open;
  UTimeRegistrations.Append;
  UTimeRegistrations.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UTimeRegistrations.FieldByName('UserId').AsInteger := UniMainModule.User_id;
  UTimeRegistrations.FieldByName('Removed').asString := '0';
  UTimeRegistrations.FieldByName('REGISTRATIONDATE').AsDateTime := Date;
  UTimeRegistrations.FieldByName('KILOMETERS').AsInteger := 0;
end;

procedure TTimeRegistrationsAddFrm.Open_reference_tables(new: boolean);
var SQL: string;
begin
  Projects.Close;
  Projects.SQL.Clear;
  SQL := 'SELECT PROJECTS.ID, PROJECTS.NAME, PROJECTMEMBERS.HOURLYRATE, PROJECTMEMBERS.USERID ' + 'FROM PROJECTMEMBERS ' +
         'LEFT OUTER JOIN PROJECTS ON (PROJECTMEMBERS.PROJECT = PROJECTS.ID) ' + 'WHERE PROJECTMEMBERS.USERID=' + IntToStr(UniMainModule.User_id) + ' ' +
         'AND PROJECTS.REMOVED=' + QuotedStr('0');
  if new = true
  then SQL := SQL + ' AND PROJECTS.STATUS=0';
  SQL := SQL + ' ORDER BY PROJECTS.NAME';
  Projects.SQL.Add(SQL);
  Projects.Open;

  Activities.Close;
  Activities.SQL.Clear;
  Activities.SQL.Add('Select id, dutch from basictables where companyid=' + IntToStr(UniMainModule.Company_id) +
                     ' and removed=''0''  and TableId=2000 order by dutch');
  Activities.Open;
end;

procedure TTimeRegistrationsAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TTimeRegistrationsAddFrm.UniFormReady(Sender: TObject);
begin
  EditInternalRemarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

end.
