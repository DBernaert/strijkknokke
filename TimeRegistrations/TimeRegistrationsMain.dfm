object TimeRegistrationsMainFrm: TTimeRegistrationsMainFrm
  Left = 0
  Top = 0
  Width = 1234
  Height = 907
  Layout = 'border'
  LayoutConfig.Margin = '8 8 8 4'
  ParentAlignmentControl = False
  AlignmentControl = uniAlignmentClient
  TabOrder = 0
  ParentColor = False
  ParentBackground = False
  object ContainerHeader: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 1234
    Height = 49
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    TabStop = False
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerTitle: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 1234
      Height = 49
      Hint = ''
      ParentColor = False
      Align = alClient
      TabOrder = 0
      TabStop = False
      LayoutConfig.Region = 'center'
      object LblTitle: TUniLabel
        Left = 40
        Top = 0
        Width = 176
        Height = 33
        Hint = ''
        Caption = 'Tijdsregistratie'
        ParentFont = False
        Font.Color = clBlack
        Font.Height = -27
        ClientEvents.UniEvents.Strings = (
          
            'beforeInit=function beforeInit(sender, config)'#13#10'{'#13#10'  config.cls ' +
            '= "lfp-pagetitle";'#13#10'}')
        TabOrder = 1
      end
      object ImageTimeRegistration: TUniImage
        Left = 0
        Top = 0
        Width = 32
        Height = 32
        Hint = ''
        AutoSize = True
        Url = 'images/TimeRegistration-32-black.png'
      end
    end
  end
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 49
    Width = 1234
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'north'
    object ContainerSearch: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 273
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 265
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        OnChange = EditSearchChange
      end
    end
    object ContainerButtonsFiles: TUniContainerPanel
      Left = 1114
      Top = 0
      Width = 120
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object UniDBNavigator1: TUniDBNavigator
        Left = 0
        Top = 0
        Width = 121
        Height = 25
        Hint = ''
        DataSource = DsTimeRegistrations
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        IconSet = icsFontAwesome
        TabStop = False
        TabOrder = 1
      end
    end
  end
  object GridTimeRegistrations: TUniDBGrid
    Left = 0
    Top = 74
    Width = 1234
    Height = 808
    Hint = ''
    DataSource = DsTimeRegistrations
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    Grouping.FieldName = 'REGISTRATIONDATE'
    Grouping.Enabled = True
    Grouping.Collapsible = True
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    Images = UniMainModule.ImageList
    ForceFit = True
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 0 8 0'
    Align = alClient
    TabOrder = 2
    Summary.Enabled = True
    Summary.GrandTotalAlign = taBottom
    OnColumnSort = GridTimeRegistrationsColumnSort
    OnDblClick = BtnModifyClick
    OnColumnSummary = GridTimeRegistrationsColumnSummary
    OnColumnSummaryResult = GridTimeRegistrationsColumnSummaryResult
    Columns = <
      item
        FieldName = 'REGISTRATIONDATE'
        Title.Caption = 'DATUM'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'STARTTIMEVALUE'
        Title.Alignment = taRightJustify
        Title.Caption = 'BEGINTIJD'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'ENDTIMEVALUE'
        Title.Alignment = taRightJustify
        Title.Caption = 'EINDTIJD'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'SALDOVALUE'
        Title.Alignment = taRightJustify
        Title.Caption = 'SALDO'
        Title.Font.Style = [fsBold]
        Width = 100
        Font.Style = [fsBold]
        ShowSummary = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'PROJECTNAME'
        Title.Caption = 'PROJECT'
        Width = 304
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'ACTIVITY'
        Title.Caption = 'ACTIVITEIT'
        Width = 304
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'DESCRIPTION'
        Title.Caption = 'OMSCHRIJVING'
        Width = 304
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 882
    Width = 1234
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 3
    LayoutConfig.Region = 'south'
    object BtnAdd: TUniThemeButton
      Left = 0
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Toevoegen'
      ParentFont = False
      TabOrder = 1
      Images = UniMainModule.ImageList
      ImageIndex = 10
      OnClick = BtnAddClick
      ButtonTheme = uctDefault
    end
    object BtnModify: TUniThemeButton
      Left = 112
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Wijzigen'
      ParentFont = False
      TabOrder = 2
      Images = UniMainModule.ImageList
      ImageIndex = 11
      OnClick = BtnModifyClick
      ButtonTheme = uctDefault
    end
    object BtnDelete: TUniThemeButton
      Left = 224
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Verwijderen'
      ParentFont = False
      TabOrder = 3
      Images = UniMainModule.ImageList
      ImageIndex = 12
      OnClick = BtnDeleteClick
      ButtonTheme = uctDefault
    end
    object BtnPrintOverview: TUniThemeButton
      Left = 336
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      Caption = 'Overzicht'
      ParentFont = False
      TabOrder = 4
      Images = UniMainModule.ImageList
      ImageIndex = 29
      OnClick = BtnPrintOverviewClick
      ButtonTheme = uctDefault
    end
  end
  object TimeRegistrations: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO TIMEREGISTRATIONS'
      
        '  (ID, REGISTRATIONDATE, DESCRIPTION, INTERNALREMARKS, STATUS, S' +
        'TARTTIMEVALUE, ENDTIMEVALUE)'
      'VALUES'
      
        '  (:ID, :REGISTRATIONDATE, :DESCRIPTION, :INTERNALREMARKS, :STAT' +
        'US, :STARTTIMEVALUE, :ENDTIMEVALUE)'
      'RETURNING '
      
        '  ID, REGISTRATIONDATE, DESCRIPTION, STATUS, STARTTIMEVALUE, END' +
        'TIMEVALUE')
    SQLDelete.Strings = (
      'DELETE FROM TIMEREGISTRATIONS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE TIMEREGISTRATIONS'
      'SET'
      
        '  ID = :ID, REGISTRATIONDATE = :REGISTRATIONDATE, DESCRIPTION = ' +
        ':DESCRIPTION, INTERNALREMARKS = :INTERNALREMARKS, STATUS = :STAT' +
        'US, STARTTIMEVALUE = :STARTTIMEVALUE, ENDTIMEVALUE = :ENDTIMEVAL' +
        'UE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, REGISTRATIONDATE, DESCRIPTION, INTERNALREMARKS, STATU' +
        'S, STARTTIMEVALUE, ENDTIMEVALUE FROM TIMEREGISTRATIONS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM TIMEREGISTRATIONS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM TIMEREGISTRATIONS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  TIMEREGISTRATIONS.ID,'
      '  TIMEREGISTRATIONS.REGISTRATIONDATE,'
      '  TIMEREGISTRATIONS.STARTTIMEVALUE,'
      '  TIMEREGISTRATIONS.ENDTIMEVALUE,'
      '  TIMEREGISTRATIONS.SALDOVALUE,'
      '  TIMEREGISTRATIONS.DESCRIPTION,'
      '  TIMEREGISTRATIONS.INTERNALREMARKS,'
      '  PROJECTS.NAME AS PROJECTNAME,'
      '  BASICTABLES.DUTCH AS ACTIVITY'
      'FROM'
      '  TIMEREGISTRATIONS'
      
        '  LEFT OUTER JOIN PROJECTS ON (TIMEREGISTRATIONS.PROJECT = PROJE' +
        'CTS.ID)'
      
        '  LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = B' +
        'ASICTABLES.ID)')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 616
    Top = 208
    object TimeRegistrationsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object TimeRegistrationsREGISTRATIONDATE: TDateField
      FieldName = 'REGISTRATIONDATE'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object TimeRegistrationsPROJECTNAME: TWideStringField
      FieldName = 'PROJECTNAME'
      ReadOnly = True
      Size = 50
    end
    object TimeRegistrationsACTIVITY: TWideStringField
      FieldName = 'ACTIVITY'
      ReadOnly = True
      Size = 50
    end
    object TimeRegistrationsDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Size = 50
    end
    object TimeRegistrationsINTERNALREMARKS: TBlobField
      FieldName = 'INTERNALREMARKS'
    end
    object TimeRegistrationsSTARTTIMEVALUE: TFloatField
      FieldName = 'STARTTIMEVALUE'
      DisplayFormat = '#,##0.00'
    end
    object TimeRegistrationsENDTIMEVALUE: TFloatField
      FieldName = 'ENDTIMEVALUE'
      DisplayFormat = '#,##0.00'
    end
    object TimeRegistrationsSALDOVALUE: TFloatField
      FieldName = 'SALDOVALUE'
      DisplayFormat = '#,##0.00'
    end
  end
  object DsTimeRegistrations: TDataSource
    DataSet = TimeRegistrations
    Left = 616
    Top = 264
  end
end
