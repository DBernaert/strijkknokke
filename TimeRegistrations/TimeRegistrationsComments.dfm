object TimeRegistrationCommentsFrm: TTimeRegistrationCommentsFrm
  Left = 0
  Top = 0
  ClientHeight = 217
  ClientWidth = 497
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnCancel = BtnSaveClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MemoRemarks: TUniDBMemo
    Left = 16
    Top = 16
    Width = 465
    Height = 145
    Hint = ''
    DataField = 'INTERNALREMARKS'
    DataSource = DsComments
    ReadOnly = True
    TabOrder = 0
    TabStop = False
  end
  object BtnSave: TUniThemeButton
    Left = 376
    Top = 176
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Sluiten'
    ParentFont = False
    TabOrder = 1
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnSaveClick
    ButtonTheme = uctSecondary
  end
  object Comments: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO TIMEREGISTRATIONS'
      
        '  (ID, REGISTRATIONDATE, DESCRIPTION, INTERNALREMARKS, STATUS, S' +
        'TARTTIMEVALUE, ENDTIMEVALUE)'
      'VALUES'
      
        '  (:ID, :REGISTRATIONDATE, :DESCRIPTION, :INTERNALREMARKS, :STAT' +
        'US, :STARTTIMEVALUE, :ENDTIMEVALUE)'
      'RETURNING '
      
        '  ID, REGISTRATIONDATE, DESCRIPTION, STATUS, STARTTIMEVALUE, END' +
        'TIMEVALUE')
    SQLDelete.Strings = (
      'DELETE FROM TIMEREGISTRATIONS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE TIMEREGISTRATIONS'
      'SET'
      
        '  ID = :ID, REGISTRATIONDATE = :REGISTRATIONDATE, DESCRIPTION = ' +
        ':DESCRIPTION, INTERNALREMARKS = :INTERNALREMARKS, STATUS = :STAT' +
        'US, STARTTIMEVALUE = :STARTTIMEVALUE, ENDTIMEVALUE = :ENDTIMEVAL' +
        'UE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, REGISTRATIONDATE, DESCRIPTION, INTERNALREMARKS, STATU' +
        'S, STARTTIMEVALUE, ENDTIMEVALUE FROM TIMEREGISTRATIONS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM TIMEREGISTRATIONS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM TIMEREGISTRATIONS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT ID, INTERNALREMARKS FROM TIMEREGISTRATIONS')
    MasterFields = 'ID'
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 240
    Top = 40
  end
  object DsComments: TDataSource
    DataSet = Comments
    Left = 240
    Top = 96
  end
end
