unit TimeRegistrationsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniButton, UniThemeButton, uniEdit, uniImage, uniLabel, uniGUIBaseClasses, uniPanel,
  Data.DB, MemDS, DBAccess, IBC, uniBasicGrid, uniDBGrid;

type
  TTimeRegistrationsMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageTimeRegistration: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerButtonsFiles: TUniContainerPanel;
    TimeRegistrations: TIBCQuery;
    TimeRegistrationsID: TLargeintField;
    TimeRegistrationsREGISTRATIONDATE: TDateField;
    TimeRegistrationsPROJECTNAME: TWideStringField;
    TimeRegistrationsACTIVITY: TWideStringField;
    TimeRegistrationsDESCRIPTION: TWideStringField;
    TimeRegistrationsINTERNALREMARKS: TBlobField;
    TimeRegistrationsSTARTTIMEVALUE: TFloatField;
    TimeRegistrationsENDTIMEVALUE: TFloatField;
    TimeRegistrationsSALDOVALUE: TFloatField;
    DsTimeRegistrations: TDataSource;
    GridTimeRegistrations: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    BtnPrintOverview: TUniThemeButton;
    procedure EditSearchChange(Sender: TObject);
    procedure GridTimeRegistrationsColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
    procedure GridTimeRegistrationsColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
      Attribs: TUniCellAttribs; var Result: string);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
  private
    { Private declarations }
    procedure Open_registrations;
    procedure CallBackInsertUpdateRegistration(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Main, UniFSConfirm;



{ TTimeRegistrationsMainFrm }

procedure TTimeRegistrationsMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  With TimeRegistrationsAddFrm do
  begin
    Initialize_insert;
    ShowModal(CallBackInsertUpdateRegistration);
  end;
end;

procedure TTimeRegistrationsMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not TimeRegistrations.Active) or (TimeRegistrations.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen registratie?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UTimeRegistrations.Close;
          UniMainModule.UTimeRegistrations.SQL.Clear;
          UniMainModule.UTimeRegistrations.SQL.Add('Select * from timeregistrations where id=' + QuotedStr(TimeRegistrations.fieldbyname('Id').asString));
          UniMainModule.UTimeRegistrations.Open;
          UniMainModule.UTimeRegistrations.Edit;
          UniMainModule.UTimeRegistrations.fieldbyname('Removed').asString := '1';
          UniMainModule.UTimeRegistrations.Post;
          UniMainModule.UpdTrRegistrations.Commit;
          UniMainModule.UTimeRegistrations.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrRegistrations.Rollback;
            UniMainModule.UTimeRegistrations.Close;
            MainForm.Show_Message('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        TimeRegistrations.Refresh;
      end;
    end);
end;

procedure TTimeRegistrationsMainFrm.CallBackInsertUpdateRegistration(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if TimeRegistrations.Active then
    begin
      TimeRegistrations.Refresh;
      TimeRegistrations.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TTimeRegistrationsMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  inherited;
  Filter := ' PROJECTNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' ACTIVITY LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' DESCRIPTION LIKE ''%' +
              EditSearch.Text + '%''';
  TimeRegistrations.Filter := Filter;
end;

procedure TTimeRegistrationsMainFrm.GridTimeRegistrationsColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  if SameText(UpperCase(Column.FieldName), 'SALDOVALUE') then
  begin
    if Column.AuxValue = NULL then
      Column.AuxValue := 0.0;
    Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
  end;
end;

procedure TTimeRegistrationsMainFrm.GridTimeRegistrationsColumnSummaryResult(Column: TUniDBGridColumn;
  GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
var F: currency;
begin
  inherited;
  if SameText(UpperCase(Column.FieldName), 'SALDOVALUE') then
  begin
    F := Column.AuxValue;
    Result := FormatCurr('#,##0.00', F);
    Attribs.Color := $00855000;
    Attribs.Font.Color := ClWhite;
    Attribs.Font.Style := [fsBold];
  end;
  Column.AuxValue := NULL;
end;

procedure TTimeRegistrationsMainFrm.Open_registrations;
var SQL: string;
begin
  TimeRegistrations.Close;
  TimeRegistrations.SQL.Clear;
  SQL := 'SELECT TIMEREGISTRATIONS.ID, TIMEREGISTRATIONS.REGISTRATIONDATE, TIMEREGISTRATIONS.STARTTIMEVALUE, ' +
    'TIMEREGISTRATIONS.ENDTIMEVALUE, TIMEREGISTRATIONS.SALDOVALUE, TIMEREGISTRATIONS.DESCRIPTION, ' + 'TIMEREGISTRATIONS.INTERNALREMARKS, ' +
    'PROJECTS.NAME AS PROJECTNAME, ' + 'BASICTABLES.DUTCH AS ACTIVITY ' + 'FROM ' + 'TIMEREGISTRATIONS ' +
    'LEFT OUTER JOIN PROJECTS ON (TIMEREGISTRATIONS.PROJECT = PROJECTS.ID) ' + 'LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = BASICTABLES.ID) ' +
    'WHERE TIMEREGISTRATIONS.USERID=' + IntToStr(UniMainModule.User_id) + ' ' + 'AND TIMEREGISTRATIONS.REMOVED=''0'' ';

  SQL := SQL + 'ORDER BY REGISTRATIONDATE DESC, STARTTIMEVALUE DESC';
  TimeRegistrations.SQL.Add(SQL);
  TimeRegistrations.Open;
end;

procedure TTimeRegistrationsMainFrm.Start_module;
begin
  Open_registrations;
  EditSearch.SetFocus;
end;

end.

 //
procedure TTimeRegistrationMainFrm.BtnModifyClick(Sender: TObject);
begin
  inherited;
  if (TimeRegistrations.Active) and (not TimeRegistrations.fieldbyname('Id').isNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With TimeRegistrationsAddFrm do
    begin
      Initialize_edit(TimeRegistrations.fieldbyname('Id').AsInteger);
      ShowModal(CallBackInsertUpdateRegistration);
    end;
  end;
end;

procedure TTimeRegistrationMainFrm.BtnPrintOverviewInvoicingClick(Sender: TObject);
begin
  inherited;
  With ParamsReportOverviewInvoiceFrm do
  begin
    Start_module;
    ShowModal;
  end;
end;
