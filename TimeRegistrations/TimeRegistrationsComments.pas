unit TimeRegistrationsComments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniMemo, uniDBMemo, Data.DB, MemDS, DBAccess, IBC, uniButton,
  UniThemeButton, uniPanel;

type
  TTimeRegistrationCommentsFrm = class(TUniForm)
    Comments: TIBCQuery;
    DsComments: TDataSource;
    MemoRemarks: TUniDBMemo;
    BtnSave: TUniThemeButton;
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module(TimeRegistration: longint);
  end;

function TimeRegistrationCommentsFrm: TTimeRegistrationCommentsFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function TimeRegistrationCommentsFrm: TTimeRegistrationCommentsFrm;
begin
  Result := TTimeRegistrationCommentsFrm(UniMainModule.GetFormInstance(TTimeRegistrationCommentsFrm));
end;

{ TTimeRegistrationCommentsFrm }

procedure TTimeRegistrationCommentsFrm.BtnSaveClick(Sender: TObject);
begin
  Close;
end;

procedure TTimeRegistrationCommentsFrm.Start_module(TimeRegistration: longint);
begin
  Caption := 'Opmerkingen registratie';
  Comments.Close;
  Comments.SQL.Clear;
  Comments.SQL.Add('Select id, internalremarks from timeregistrations where id=' + IntToStr(TimeRegistration));
  Comments.Open;
end;

procedure TTimeRegistrationCommentsFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
