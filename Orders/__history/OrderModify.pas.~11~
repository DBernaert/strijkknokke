unit OrderModify;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, uniGUIBaseClasses, uniBasicGrid, uniDBGrid, DBAccess, IBC, MemDS, uniButton,
  UniThemeButton, uniSpinEdit, uniEdit, uniPanel, uniDBEdit;

type
  TOrderModifyFrm = class(TUniForm)
    UOrderItems: TIBCQuery;
    DsUOrderItems: TDataSource;
    UpdTrOrderItems: TIBCTransaction;
    UniDBGrid1: TUniDBGrid;
    UOrderItemsID: TLargeintField;
    UOrderItemsORDERID: TLargeintField;
    UOrderItemsPRICEITEM: TLargeintField;
    UOrderItemsQUANTITY: TIntegerField;
    UOrderItemsITEMSEQUENCE: TIntegerField;
    Prices: TIBCQuery;
    DsPrices: TDataSource;
    UOrderItemsPRICEDESCRIPTION: TStringField;
    PricesID: TLargeintField;
    PricesCATEGORY: TWideStringField;
    PricesDESCRIPTION: TWideStringField;
    PricesSEQUENCE: TIntegerField;
    PricesNUMBEROFMINUTES: TFloatField;
    UOrderItemsPRICECATEGORY: TStringField;
    UOrderItemsNUMBEROFMINUTES: TCurrencyField;
    UOrderItemsTOTALAMOUNT: TCurrencyField;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    UniHiddenPanel1: TUniHiddenPanel;
    TemplateEditQuantity: TUniSpinEdit;
    UOrderItemsEXTRAMINUTES: TFloatField;
    TemplateExtraMinutes: TUniFormattedNumberEdit;
    EditAmountExternalIroning: TUniDBFormattedNumberEdit;
    EditAmountTransportation: TUniDBFormattedNumberEdit;
    EditExtraMinutes: TUniDBFormattedNumberEdit;
    procedure BtnCancelClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UOrderItemsCalcFields(DataSet: TDataSet);
    procedure UniDBGrid1ColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
    procedure UniDBGrid1ColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
      Attribs: TUniCellAttribs; var Result: string);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    function Initialize_edit(OrderId: longint): boolean;
  end;

function OrderModifyFrm: TOrderModifyFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function OrderModifyFrm: TOrderModifyFrm;
begin
  Result := TOrderModifyFrm(UniMainModule.GetFormInstance(TOrderModifyFrm));
end;

{ TOrderModifyFrm }

procedure TOrderModifyFrm.BtnCancelClick(Sender: TObject);
begin
  UOrderItems.Cancel;
  UniMainModule.UOrders.Cancel;
  UniMainModule.UOrders.Close;
  Close;
end;

procedure TOrderModifyFrm.BtnSaveClick(Sender: TObject);
var Total: currency;
    Minutes: currency;
begin
  BtnSave.SetFocus;

  if UniMainModule.UOrders.FieldByName('AMOUNTEXTERNALIRONING').IsNull
  then begin
         UniMainModule.Show_warning('Kostprijs mangelen is een verplichte ingave.');
         EditAmountExternalIroning.SetFocus;
         Exit;
       end;

  if UniMainModule.UOrders.FieldByName('AMOUNTTRANSPORTATION').IsNull
  then begin
         UniMainModule.Show_warning('Kostprijs vervoer is een verplichte ingave.');
         EditAmountTransportation.SetFocus;
         Exit;
       end;

  if UniMainModule.UOrders.FieldByName('EXTRAMINUTES').IsNull
  then begin
         UniMainModule.Show_warning('Extra minuten is een verplichte ingave.');
         EditAmountTransportation.SetFocus;
         Exit;
       end;

  if UpdTrOrderItems.Active
  then try
         UpdTrOrderItems.Commit;
         Total := 0;
         Minutes := 0;
         UOrderItems.First;
         while not UOrderItems.Eof
         do begin
              Total := Total + UOrderItems.FieldByName('TotalAmount').AsCurrency;
              Minutes := Minutes + (UOrderItems.FieldByName('NumberOfMinutes').AsCurrency *
                                    UOrderItems.FieldByName('Quantity').AsInteger) +
                                    UOrderItems.FieldByName('ExtraMinutes').AsCurrency;
              UOrderItems.Next;
            end;

         UniMainModule.UOrders.FieldByName('TotalAmount').AsCurrency := Total;
         UniMainModule.UOrders.FieldByName('TotalMinutes').AsCurrency := Minutes;
         UniMainModule.UOrders.Post;
         UOrderItems.Close;
         UniMainModule.UpdTrOrders.Commit;
         UniMainModule.Result_dbAction := UniMainModule.UOrders.FieldByName('Id').AsInteger;
         UniMainModule.UOrders.Close;
       except
         UpdTrOrderItems.Rollback;
         if UniMainModule.UpdTrOrders.Active
         then UniMainModule.UpdTrOrders.Rollback;
         UniMainModule.UOrders.Cancel;
         UniMainModule.UOrders.Close;
       end
  else begin
         UniMainModule.UOrders.Cancel;
         UniMainModule.UOrders.Close;
       end;
  Close;
end;

function TOrderModifyFrm.Initialize_edit(OrderId: longint): boolean;
begin
  Caption  := 'Wijzigen strijkopdracht';
  Open_reference_tables;
  Try
    UniMainModule.UOrders.Close;
    UniMainModule.UOrders.SQL.Clear;
    UniMainModule.UOrders.SQL.Add('Select * from orders where Id=' + IntToStr(OrderId));
    UniMainModule.UOrders.Open;
    UniMainModule.UOrders.Edit;
    UOrderItems.Close;
    UOrderItems.SQL.Clear;
    UOrderItems.SQL.Add('Select * from orderitems where orderid=' + IntToStr(OrderId) + ' order by itemsequence');
    UOrderItems.Open;
    Result := True;
  Except
    if UniMainModule.UOrders.Active
    then UniMainModule.UOrders.Cancel;
    UOrderItems.Cancel;
    UOrderItems.Close;
    UniMainModule.UOrders.Close;
    Result := False;
  End;
end;

procedure TOrderModifyFrm.Open_reference_tables;
begin
  Prices.Open;
end;

procedure TOrderModifyFrm.UniDBGrid1ColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
  if SameText(UpperCase(Column.FieldName), 'TOTALAMOUNT')
  then begin
         if Column.AuxValue = NULL
         then Column.AuxValue := 0.0;
         Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
       end;
end;

procedure TOrderModifyFrm.UniDBGrid1ColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
  Attribs: TUniCellAttribs; var Result: string);
var F: currency;
begin
  if SameText(UpperCase(Column.FieldName), 'TOTALAMOUNT')
  then begin
         F := Column.AuxValue;
         Result := FormatCurr('#,##0.00 �', F);
       end;

  Attribs.Color := $00B36D00;
  Attribs.Font.Color := ClWhite;
  Attribs.Font.Style := [fsBold];
  Column.AuxValue := NULL;
end;

procedure TOrderModifyFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TOrderModifyFrm.UOrderItemsCalcFields(DataSet: TDataSet);
begin
  UOrderItems.Fieldbyname('TotalAmount').asCurrency :=
  (UOrderItems.FieldByName('Quantity').AsInteger * UniMainModule.MinutePrice *
   UOrderItems.FieldByName('NumberOfMinutes').AsCurrency) +
  (UOrderItems.FieldByName('ExtraMinutes').AsInteger * UniMainModule.MinutePrice);
end;

end.
