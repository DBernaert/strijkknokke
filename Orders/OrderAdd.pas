unit OrderAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniButton, UniThemeButton, uniMultiItem, uniComboBox, uniDBComboBox,
  uniDBLookupComboBox, uniDateTimePicker, uniDBDateTimePicker, uniEdit, uniDBEdit, uniMemo, uniDBMemo, Data.DB, MemDS,
  DBAccess, IBC;

type
  TOrderAddFrm = class(TUniForm)
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditRelation: TUniDBLookupComboBox;
    EditDateArrival: TUniDBDateTimePicker;
    EditDueDate: TUniDBDateTimePicker;
    EditNumberOfItems: TUniDBNumberEdit;
    EditRemarks: TUniDBMemo;
    Relations: TIBCQuery;
    RelationsID: TLargeintField;
    RelationsFULLNAME: TWideStringField;
    RelationsCOMPANYNAME: TWideStringField;
    RelationsRELATIONTYPE: TIntegerField;
    RelationsDISPLAYNAME: TStringField;
    DsRelations: TDataSource;
    BtnAddCustomer: TUniThemeButton;
    BtnModifyCustomer: TUniThemeButton;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure RelationsCalcFields(DataSet: TDataSet);
    procedure UniFormReady(Sender: TObject);
    procedure BtnAddCustomerClick(Sender: TObject);
    procedure BtnModifyCustomerClick(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure CallBackInsertUpdateRelation(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    function Initialize_insert: boolean;
    function Initialize_edit(Id: longint): boolean;
  end;

function OrderAddFrm: TOrderAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, DateUtils, ServerModule, CustomersAdd;

function OrderAddFrm: TOrderAddFrm;
begin
  Result := TOrderAddFrm(UniMainModule.GetFormInstance(TOrderAddFrm));
end;

{ TOrderAddFrm }

procedure TOrderAddFrm.BtnAddCustomerClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  With CustomersAddFrm do
  begin
    if Initialize_insert = True
    then ShowModal(CallBackInsertUpdateRelation);
  end;
end;

procedure TOrderAddFrm.CallBackInsertUpdateRelation(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Relations.Active then
    begin
      Relations.Refresh;
      Relations.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TOrderAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.UOrders.Cancel;
  UniMainModule.UOrders.Close;
  Close;
end;

procedure TOrderAddFrm.BtnModifyCustomerClick(Sender: TObject);
begin
  if (not UniMainModule.UOrders.Fieldbyname('Relation').IsNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With CustomersAddFrm do
    begin
      if Initialize_edit(UniMainModule.UOrders.fieldbyname('Relation').AsInteger) = True
      then ShowModal(CallBackInsertUpdateRelation)
      else UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    end;
  end;
end;

procedure TOrderAddFrm.BtnSaveClick(Sender: TObject);
var insertmode: boolean;
begin
  if UniMainModule.UOrders.State = dsInsert
  then insertmode := True
  else insertmode := False;

  BtnSave.SetFocus;

  if UniMainModule.UOrders.FieldByName('Relation').IsNull
  then begin
         UniMainModule.show_warning('De klant is een verplichte ingave.');
         EditRelation.SetFocus;
         Exit;
       end;

  if UniMainModule.UOrders.FieldByName('DateArrival').IsNull
  then begin
         UniMainModule.Show_warning('De datum van aankomst is een verplichte ingave.');
         EditDateArrival.SetFocus;
         Exit;
       end;

  if UniMainModule.UOrders.FieldByName('DueDate').IsNull
  then begin
         UniMainModule.Show_warning('De opleveringsdatum is een verplichte ingave.');
         EditDueDate.SetFocus;
         Exit;
       end;

  if UniMainModule.UOrders.FieldByName('NumberOfItems').IsNull
  then begin
         UniMainModule.Show_warning('Het aantal stuks is een verplichte ingave.');
         EditNumberOfItems.SetFocus;
         Exit;
       end;

  Try
    UniMainModule.UOrders.Post;
    UniMainModule.UpdTrOrders.Commit;

    //Aanmaken werklijst
    if insertmode = True
    then begin
           UniMainModule.QWork.Close;
           UniMainModule.QWork.SQL.Clear;
           UniMainModule.QWork.SQL.Add('Select * from prices order by sequence');
           UniMainModule.QWork.Open;

           UniMainModule.UOrderItems.Close;
           UniMainModule.UOrderItems.SQL.Clear;
           UniMainModule.UOrderItems.SQL.Add('Select first 0 * from orderitems');
           UniMainModule.UOrderItems.Open;

           UniMainModule.QWork.First;
           while not UniMainModule.QWork.Eof
           do begin
                UniMainModule.UOrderItems.Append;
                UniMainModule.UOrderItems.FieldByName('OrderId').AsInteger := UniMainModule.UOrders.FieldByName('Id').AsInteger;
                UniMainModule.UOrderItems.FieldByName('PriceItem').AsInteger := UniMainModule.QWork.FieldByName('Id').AsInteger;
                UniMainModule.UOrderItems.FieldByName('Quantity').AsInteger := 0;
                UniMainModule.UOrderItems.FieldByName('Extraminutes').AsCurrency := 0;
                UniMainModule.UOrderItems.FieldByName('ItemSequence').AsInteger := UniMainModule.QWork.FieldByName('Sequence').AsInteger;
                UniMainModule.UOrderItems.Post;
                UniMainModule.QWork.Next;
              end;
           UniMainModule.QWork.Close;
         end;

         Try
           UniMainModule.UpdTrOrderItems.Commit;
           UniMainModule.UOrderItems.Close;
         Except on E: EDataBaseError
         do begin
              UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
              UniMainModule.UpdTrOrderItems.Rollback;
              UniMainModule.UOrderItems.Close;
              UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
            end;
         End;


    UniMainModule.Result_dbAction := UniMainModule.UOrders.FieldByName('Id').AsInteger;
    UniMainModule.UOrders.Close;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       UniMainModule.UpdTrOrders.Rollback;
       UniMainModule.UOrders.Close;
       UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
     end;
  end;
  Close;
end;

function TOrderAddFrm.Initialize_edit(Id: longint): boolean;
begin

end;

procedure TOrderAddFrm.Open_reference_tables;
begin
  Relations.Close;
  Relations.SQL.Clear;
  Relations.SQL.Add('Select ID, coalesce(NAME, '''') || '' '' || coalesce(FIRSTNAME, '''') AS FULLNAME, COMPANYNAME, RELATIONTYPE from relations' + ' where companyid=' +
                     IntToStr(UniMainModule.Company_id) + ' and removed=''0''');
  Relations.Open;
end;

procedure TOrderAddFrm.RelationsCalcFields(DataSet: TDataSet);
begin
 case Relations.FieldByName('RelationType').AsInteger of
  0: Relations.FieldByName('DisplayName').AsString := Relations.FieldByName('CompanyName').AsString;
  1: Relations.FieldByName('DisplayName').AsString := Relations.FieldByName('FullName').AsString;
  end;
end;

function TOrderAddFrm.Initialize_insert: boolean;
begin
  Caption  := 'Toevoegen strijkopdracht';

  Open_reference_tables;

  Try
    //Open_reference_tables;
    UniMainModule.UOrders.Close;
    UniMainModule.UOrders.SQL.Clear;
    UniMainModule.UOrders.SQL.Add('Select first 0 * from orders');
    UniMainModule.UOrders.Open;
    UniMainModule.UOrders.Append;
    UniMainModule.UOrders.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
    UniMainModule.UOrders.FieldByName('DateArrival').AsDateTime := Date;
    UniMainModule.UOrders.FieldByName('DueDate').AsDateTime     := IncDay(Date, 3);
    UniMainModule.UOrders.FieldByName('NumberOfItems').AsInteger := 0;
    UniMainModule.UOrders.FieldByName('Status').AsInteger := 0;
    UniMainModule.UOrders.FieldByName('TotalMinutes').AsInteger := 0;
    UniMainModule.UOrders.FieldByName('TotalAmount').AsCurrency := 0;
    UniMainModule.UOrders.FieldByName('AmountExternalIroning').AsCurrency := 0;
    UniMainModule.UOrders.FieldByName('AmountTransportation').AsCurrency := 0;
    UniMainModule.UOrders.FieldByName('ExtraMinutes').AsCurrency := 0;
    Result := True;
  Except
    UniMainModule.UOrders.Close;
    Result := False;
  End;
end;

procedure TOrderAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TOrderAddFrm.UniFormReady(Sender: TObject);
begin
  EditRemarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

end.
