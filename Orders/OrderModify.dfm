object OrderModifyFrm: TOrderModifyFrm
  Left = 0
  Top = 0
  ClientHeight = 665
  ClientWidth = 768
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object UniDBGrid1: TUniDBGrid
    Left = 16
    Top = 56
    Width = 737
    Height = 513
    Hint = ''
    ClicksToEdit = 1
    RowEditor = True
    DataSource = DsUOrderItems
    WebOptions.Paged = False
    LoadMask.Message = 'Loading data...'
    TabOrder = 0
    Summary.Align = taBottom
    Summary.Enabled = True
    Summary.GrandTotalAlign = taBottom
    OnColumnSummary = UniDBGrid1ColumnSummary
    OnColumnSummaryResult = UniDBGrid1ColumnSummaryResult
    Columns = <
      item
        FieldName = 'PRICECATEGORY'
        Title.Caption = 'Categorie'
        Width = 150
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'PRICEDESCRIPTION'
        Title.Caption = 'Omschrijving'
        Width = 304
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'NUMBEROFMINUTES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Basis minuten'
        Width = 80
        ReadOnly = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'QUANTITY'
        Title.Alignment = taRightJustify
        Title.Caption = 'Aantal'
        Width = 64
        Editor = TemplateEditQuantity
      end
      item
        FieldName = 'EXTRAMINUTES'
        Title.Alignment = taRightJustify
        Title.Caption = 'Extra minuten'
        Width = 90
        Editor = TemplateExtraMinutes
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'TOTALAMOUNT'
        Title.Alignment = taRightJustify
        Title.Caption = 'Totaal'
        Width = 85
        ReadOnly = True
        ShowSummary = True
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object BtnSave: TUniThemeButton
    Left = 536
    Top = 624
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 4
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 648
    Top = 624
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 5
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object UniHiddenPanel1: TUniHiddenPanel
    Left = 352
    Top = 232
    Width = 161
    Height = 73
    Hint = ''
    Visible = True
    object TemplateEditQuantity: TUniSpinEdit
      Left = 16
      Top = 8
      Width = 121
      Hint = ''
      TabOrder = 1
      SelectOnFocus = True
    end
    object TemplateExtraMinutes: TUniFormattedNumberEdit
      Left = 16
      Top = 40
      Width = 121
      Hint = ''
      TabOrder = 2
      SelectOnFocus = True
      DecimalSeparator = ','
      ThousandSeparator = '.'
    end
  end
  object EditAmountExternalIroning: TUniDBFormattedNumberEdit
    Left = 16
    Top = 584
    Width = 241
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.CurrencySignPos = cpsRight
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'AMOUNTEXTERNALIRONING'
    DataSource = UniMainModule.DsUOrders
    TabOrder = 1
    SelectOnFocus = True
    FieldLabel = 'Kostprijs mangelen *'
    FieldLabelWidth = 130
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object EditAmountTransportation: TUniDBFormattedNumberEdit
    Left = 264
    Top = 584
    Width = 241
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.CurrencySignPos = cpsRight
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'AMOUNTTRANSPORTATION'
    DataSource = UniMainModule.DsUOrders
    TabOrder = 2
    SelectOnFocus = True
    FieldLabel = 'Kostprijs vervoer *'
    FieldLabelWidth = 130
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object EditExtraMinutes: TUniDBFormattedNumberEdit
    Left = 512
    Top = 584
    Width = 241
    Height = 22
    Hint = ''
    DataField = 'EXTRAMINUTES'
    DataSource = UniMainModule.DsUOrders
    TabOrder = 3
    SelectOnFocus = True
    FieldLabel = 'Extra minuten *'
    FieldLabelWidth = 150
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object BtnFilter: TUniThemeButton
    Left = 16
    Top = 16
    Width = 89
    Height = 25
    Hint = ''
    Caption = 'Filteren'
    TabStop = False
    TabOrder = 7
    Images = UniMainModule.ImageList
    ImageIndex = 39
    OnClick = BtnFilterClick
    ButtonTheme = uctDefault
  end
  object UOrderItems: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_ORDERITEMS_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO ORDERITEMS'
      '  (ID, ORDERID, PRICEITEM, QUANTITY, ITEMSEQUENCE, EXTRAMINUTES)'
      'VALUES'
      
        '  (:ID, :ORDERID, :PRICEITEM, :QUANTITY, :ITEMSEQUENCE, :EXTRAMI' +
        'NUTES)'
      'RETURNING '
      '  ID, ORDERID, PRICEITEM, QUANTITY, ITEMSEQUENCE, EXTRAMINUTES')
    SQLDelete.Strings = (
      'DELETE FROM ORDERITEMS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE ORDERITEMS'
      'SET'
      
        '  ID = :ID, ORDERID = :ORDERID, PRICEITEM = :PRICEITEM, QUANTITY' +
        ' = :QUANTITY, ITEMSEQUENCE = :ITEMSEQUENCE, EXTRAMINUTES = :EXTR' +
        'AMINUTES'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, ORDERID, PRICEITEM, QUANTITY, ITEMSEQUENCE, EXTRAMINU' +
        'TES FROM ORDERITEMS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM ORDERITEMS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM ORDERITEMS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTrOrderItems
    SQL.Strings = (
      'Select * from orderitems')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    OnCalcFields = UOrderItemsCalcFields
    Left = 512
    Top = 80
    object UOrderItemsID: TLargeintField
      FieldName = 'ID'
    end
    object UOrderItemsORDERID: TLargeintField
      FieldName = 'ORDERID'
      Required = True
    end
    object UOrderItemsPRICEITEM: TLargeintField
      FieldName = 'PRICEITEM'
      Required = True
    end
    object UOrderItemsQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Required = True
    end
    object UOrderItemsITEMSEQUENCE: TIntegerField
      FieldName = 'ITEMSEQUENCE'
      Required = True
    end
    object UOrderItemsPRICEDESCRIPTION: TStringField
      FieldKind = fkLookup
      FieldName = 'PRICEDESCRIPTION'
      LookupDataSet = Prices
      LookupKeyFields = 'ID'
      LookupResultField = 'DESCRIPTION'
      KeyFields = 'PRICEITEM'
      Size = 50
      Lookup = True
    end
    object UOrderItemsPRICECATEGORY: TStringField
      FieldKind = fkLookup
      FieldName = 'PRICECATEGORY'
      LookupDataSet = Prices
      LookupKeyFields = 'ID'
      LookupResultField = 'CATEGORY'
      KeyFields = 'PRICEITEM'
      Size = 50
      Lookup = True
    end
    object UOrderItemsNUMBEROFMINUTES: TCurrencyField
      FieldKind = fkLookup
      FieldName = 'NUMBEROFMINUTES'
      LookupDataSet = Prices
      LookupKeyFields = 'ID'
      LookupResultField = 'NUMBEROFMINUTES'
      KeyFields = 'PRICEITEM'
      DisplayFormat = '0.00'
      Lookup = True
    end
    object UOrderItemsTOTALAMOUNT: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'TOTALAMOUNT'
      Calculated = True
    end
    object UOrderItemsEXTRAMINUTES: TFloatField
      FieldName = 'EXTRAMINUTES'
      DisplayFormat = '0.00'
    end
  end
  object DsUOrderItems: TDataSource
    DataSet = UOrderItems
    Left = 512
    Top = 128
  end
  object UpdTrOrderItems: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 512
    Top = 176
  end
  object Prices: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_COMPANYPROFILE_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COMPANYPROFILE'
      
        '  (ID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAIL, WEBSITE,' +
        ' VATNUMBER, BANKACCOUNT, IBAN, BIC, BANKACCOUNT2, IBAN2, BIC2, L' +
        'OGO, INVOICESEQUENCE, KILOMETERALLOWANCE, INVOICESEQUENCEOBJECTS' +
        ', MAILTEMPLATEOFFEROBJECTS, MAILTEMPLATEPREINVOICEOBJECTS, MAILT' +
        'EMPLATEFINALINVOICEOBJECTS, MAILTEMPLATEGENERALINVOICE, MAILTEMP' +
        'LATEREMINDER1, MAILTEMPLATEREMINDER2, MAILTEMPLATEREMINDER3, OFF' +
        'ERSEQUENCEOBJECTS)'
      'VALUES'
      
        '  (:ID, :NAME, :ADDRESS, :ZIPCODE, :CITY, :PHONE, :FAX, :EMAIL, ' +
        ':WEBSITE, :VATNUMBER, :BANKACCOUNT, :IBAN, :BIC, :BANKACCOUNT2, ' +
        ':IBAN2, :BIC2, :LOGO, :INVOICESEQUENCE, :KILOMETERALLOWANCE, :IN' +
        'VOICESEQUENCEOBJECTS, :MAILTEMPLATEOFFEROBJECTS, :MAILTEMPLATEPR' +
        'EINVOICEOBJECTS, :MAILTEMPLATEFINALINVOICEOBJECTS, :MAILTEMPLATE' +
        'GENERALINVOICE, :MAILTEMPLATEREMINDER1, :MAILTEMPLATEREMINDER2, ' +
        ':MAILTEMPLATEREMINDER3, :OFFERSEQUENCEOBJECTS)'
      'RETURNING '
      
        '  ID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAIL, WEBSITE, ' +
        'VATNUMBER, BANKACCOUNT, IBAN, BIC, BANKACCOUNT2, IBAN2, BIC2, IN' +
        'VOICESEQUENCE, KILOMETERALLOWANCE, INVOICESEQUENCEOBJECTS, OFFER' +
        'SEQUENCEOBJECTS')
    SQLDelete.Strings = (
      'DELETE FROM COMPANYPROFILE'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE COMPANYPROFILE'
      'SET'
      
        '  ID = :ID, NAME = :NAME, ADDRESS = :ADDRESS, ZIPCODE = :ZIPCODE' +
        ', CITY = :CITY, PHONE = :PHONE, FAX = :FAX, EMAIL = :EMAIL, WEBS' +
        'ITE = :WEBSITE, VATNUMBER = :VATNUMBER, BANKACCOUNT = :BANKACCOU' +
        'NT, IBAN = :IBAN, BIC = :BIC, BANKACCOUNT2 = :BANKACCOUNT2, IBAN' +
        '2 = :IBAN2, BIC2 = :BIC2, LOGO = :LOGO, INVOICESEQUENCE = :INVOI' +
        'CESEQUENCE, KILOMETERALLOWANCE = :KILOMETERALLOWANCE, INVOICESEQ' +
        'UENCEOBJECTS = :INVOICESEQUENCEOBJECTS, MAILTEMPLATEOFFEROBJECTS' +
        ' = :MAILTEMPLATEOFFEROBJECTS, MAILTEMPLATEPREINVOICEOBJECTS = :M' +
        'AILTEMPLATEPREINVOICEOBJECTS, MAILTEMPLATEFINALINVOICEOBJECTS = ' +
        ':MAILTEMPLATEFINALINVOICEOBJECTS, MAILTEMPLATEGENERALINVOICE = :' +
        'MAILTEMPLATEGENERALINVOICE, MAILTEMPLATEREMINDER1 = :MAILTEMPLAT' +
        'EREMINDER1, MAILTEMPLATEREMINDER2 = :MAILTEMPLATEREMINDER2, MAIL' +
        'TEMPLATEREMINDER3 = :MAILTEMPLATEREMINDER3, OFFERSEQUENCEOBJECTS' +
        ' = :OFFERSEQUENCEOBJECTS'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, NAME, ADDRESS, ZIPCODE, CITY, PHONE, FAX, EMAIL, WEBS' +
        'ITE, VATNUMBER, BANKACCOUNT, IBAN, BIC, BANKACCOUNT2, IBAN2, BIC' +
        '2, LOGO, INVOICESEQUENCE, KILOMETERALLOWANCE, INVOICESEQUENCEOBJ' +
        'ECTS, MAILTEMPLATEOFFEROBJECTS, MAILTEMPLATEPREINVOICEOBJECTS, M' +
        'AILTEMPLATEFINALINVOICEOBJECTS, MAILTEMPLATEGENERALINVOICE, MAIL' +
        'TEMPLATEREMINDER1, MAILTEMPLATEREMINDER2, MAILTEMPLATEREMINDER3,' +
        ' OFFERSEQUENCEOBJECTS FROM COMPANYPROFILE'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM COMPANYPROFILE'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COMPANYPROFILE'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from prices')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 664
    Top = 80
    object PricesID: TLargeintField
      FieldName = 'ID'
    end
    object PricesCATEGORY: TWideStringField
      FieldName = 'CATEGORY'
      Required = True
      Size = 50
    end
    object PricesDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 50
    end
    object PricesSEQUENCE: TIntegerField
      FieldName = 'SEQUENCE'
      Required = True
    end
    object PricesNUMBEROFMINUTES: TFloatField
      FieldName = 'NUMBEROFMINUTES'
    end
  end
  object DsPrices: TDataSource
    DataSet = Prices
    Left = 664
    Top = 128
  end
end
