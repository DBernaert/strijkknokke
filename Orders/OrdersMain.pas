unit OrdersMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid,
  uniDBNavigator, uniEdit, uniImage, uniLabel, uniGUIBaseClasses, uniPanel;

type
  TOrdersMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageOffers: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorOrders: TUniDBNavigator;
    GridOrders: TUniDBGrid;
    Orders: TIBCQuery;
    DsOrders: TDataSource;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    OrdersID: TLargeintField;
    OrdersCOMPANYID: TLargeintField;
    OrdersRELATION: TLargeintField;
    OrdersDATEARRIVAL: TDateField;
    OrdersDUEDATE: TDateField;
    OrdersNUMBEROFITEMS: TIntegerField;
    OrdersSTATUS: TLargeintField;
    OrdersTOTALAMOUNT: TFloatField;
    OrdersCOMPANYNAME: TWideStringField;
    OrdersNAME: TWideStringField;
    OrdersFIRSTNAME: TWideStringField;
    OrdersMOBILE: TWideStringField;
    OrdersTOTALMINUTES: TFloatField;
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure GridOrdersColumnSummary(Column: TUniDBGridColumn;
      GroupFieldValue: Variant);
    procedure GridOrdersColumnSummaryResult(Column: TUniDBGridColumn;
      GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
  private
    { Private declarations }
    procedure CallBackInsertUpdateOrder(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, OrderAdd, OrderModify;



{ TOrdersMainFrm }

procedure TOrdersMainFrm.BtnAddClick(Sender: TObject);
begin
 UniMainModule.Result_dbAction := 0;
  With OrderAddFrm do
  begin
    if Initialize_insert = True
    then ShowModal(CallBackInsertUpdateOrder);
  end;
end;

procedure TOrdersMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Orders.Active) and (not Orders.fieldbyname('Id').isNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With OrderModifyFrm do
    begin
      if Initialize_edit(Orders.fieldbyname('Id').AsInteger) = True
      then ShowModal(CallBackInsertUpdateOrder)
      else UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    end;
  end;
end;

procedure TOrdersMainFrm.CallBackInsertUpdateOrder(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Orders.Active then
    begin
      Orders.Refresh;
      Orders.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;


procedure TOrdersMainFrm.GridOrdersColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  if SameText(UpperCase(Column.FieldName), 'TOTALAMOUNT')
  then begin
         if Column.AuxValue = NULL
         then Column.AuxValue:=0.0;
         Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
       end;
end;

procedure TOrdersMainFrm.GridOrdersColumnSummaryResult(Column: TUniDBGridColumn;
  GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
var F : Currency;
begin
  if SameText(UpperCase(Column.FieldName), 'TOTALAMOUNT')
  then begin
         F                        := Column.AuxValue;
         if Column.AuxValues[1] = NULL
         then Column.AuxValues[1] := 0;
         Column.AuxValues[1]      := Column.AuxValues[1] + F;
         Result                   := FormatCurr('0,0.00 �', F);
         Attribs.Font.Style       := [fsBold];
         Attribs.Color            := ClRed;
         Attribs.Font.Color       := ClWhite;
       end;
  Column.AuxValue := NULL;
end;

procedure TOrdersMainFrm.Start_module;
begin
  Orders.Open;
end;

end.
