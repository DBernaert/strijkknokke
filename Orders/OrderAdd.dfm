object OrderAddFrm: TOrderAddFrm
  Left = 0
  Top = 0
  ClientHeight = 337
  ClientWidth = 553
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditRelation
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BtnSave: TUniThemeButton
    Left = 320
    Top = 296
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 0
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 432
    Top = 296
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 1
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object EditRelation: TUniDBLookupComboBox
    Left = 16
    Top = 16
    Width = 457
    Hint = ''
    ListField = 'DISPLAYNAME'
    ListSource = DsRelations
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'RELATION'
    DataSource = UniMainModule.DsUOrders
    TabOrder = 2
    Color = clWindow
    FieldLabel = 'Klant *'
    FieldLabelWidth = 130
  end
  object EditDateArrival: TUniDBDateTimePicker
    Left = 16
    Top = 48
    Width = 521
    Hint = ''
    DataField = 'DATEARRIVAL'
    DataSource = UniMainModule.DsUOrders
    DateTime = 43645.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 3
    FieldLabel = 'Datum aankomst *'
    FieldLabelWidth = 130
  end
  object EditDueDate: TUniDBDateTimePicker
    Left = 16
    Top = 80
    Width = 521
    Hint = ''
    DataField = 'DUEDATE'
    DataSource = UniMainModule.DsUOrders
    DateTime = 43645.000000000000000000
    DateFormat = 'dd/MM/yyyy'
    TimeFormat = 'HH:mm:ss'
    TabOrder = 4
    FieldLabel = 'Opleveringsdatum *'
    FieldLabelWidth = 130
  end
  object EditNumberOfItems: TUniDBNumberEdit
    Left = 16
    Top = 112
    Width = 521
    Height = 22
    Hint = ''
    DataField = 'NUMBEROFITEMS'
    DataSource = UniMainModule.DsUOrders
    TabOrder = 5
    FieldLabel = 'Aantal stuks *'
    FieldLabelWidth = 130
    DecimalSeparator = ','
  end
  object EditRemarks: TUniDBMemo
    Left = 16
    Top = 144
    Width = 521
    Height = 137
    Hint = ''
    DataField = 'REMARKS'
    DataSource = UniMainModule.DsUOrders
    TabOrder = 6
    FieldLabel = 'Opmerkingen'
    FieldLabelWidth = 130
  end
  object BtnAddCustomer: TUniThemeButton
    Left = 480
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Toevoegen klant'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 7
    Images = UniMainModule.ImageList
    ImageIndex = 10
    OnClick = BtnAddCustomerClick
    ButtonTheme = uctDefault
  end
  object BtnModifyCustomer: TUniThemeButton
    Left = 512
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Wijzigen klant'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    TabStop = False
    TabOrder = 8
    Images = UniMainModule.ImageList
    ImageIndex = 11
    OnClick = BtnModifyCustomerClick
    ButtonTheme = uctDefault
  end
  object Relations: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO RELATIONS'
      '  (ID)'
      'VALUES'
      '  (:ID)'
      'RETURNING '
      '  ID')
    SQLDelete.Strings = (
      'DELETE FROM RELATIONS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE RELATIONS'
      'SET'
      '  ID = :ID'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID FROM RELATIONS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM RELATIONS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM RELATIONS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, NAME || '#39' '#39' || FIRSTNAME AS FULLNAME, COMPANYNAME, RE' +
        'LATIONTYPE from relations')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    IndexFieldNames = 'DISPLAYNAME'
    OnCalcFields = RelationsCalcFields
    Left = 104
    Top = 154
    object RelationsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object RelationsFULLNAME: TWideStringField
      FieldName = 'FULLNAME'
      ReadOnly = True
      Size = 51
    end
    object RelationsCOMPANYNAME: TWideStringField
      FieldName = 'COMPANYNAME'
      Size = 50
    end
    object RelationsRELATIONTYPE: TIntegerField
      FieldName = 'RELATIONTYPE'
      Required = True
    end
    object RelationsDISPLAYNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'DISPLAYNAME'
      Size = 51
      Calculated = True
    end
  end
  object DsRelations: TDataSource
    DataSet = Relations
    Left = 104
    Top = 202
  end
end
