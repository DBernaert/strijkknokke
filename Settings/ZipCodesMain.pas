unit ZipCodesMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid, uniEdit, uniGUIBaseClasses, uniPanel,
  Data.DB, MemDS, DBAccess, IBC, uniDBNavigator;

type
  TZipcodesMainFrm = class(TUniForm)
    ZipCodes: TIBCQuery;
    DsZipcodes: TDataSource;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    GridZipCodes: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterLeft: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorZipCodes: TUniDBNavigator;
    procedure BtnCloseClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackInsertUpdateZipCodesTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

function ZipcodesMainFrm: TZipcodesMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UniFSConfirm, Main, ZipCodesAdd;

function ZipcodesMainFrm: TZipcodesMainFrm;
begin
  Result := TZipcodesMainFrm(UniMainModule.GetFormInstance(TZipcodesMainFrm));
end;

procedure TZipcodesMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.UZipCodes.Close;
  UniMainModule.UZipCodes.SQL.Clear;
  UniMainModule.UZipCodes.SQL.Add('Select first 0 * from zipcodes');
  UniMainModule.UZipCodes.Open;
  UniMainModule.UZipCodes.Append;
  UniMainModule.UZipCodes.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UniMainModule.UZipCodes.FieldByName('CountryCode').AsString := 'BEL';
  With ZipCodesAddFrm do
  begin
    Init_zipcodetable_creation;
    ShowModal(CallBackInsertUpdateZipCodesTable);
  end;
end;

procedure TZipcodesMainFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TZipcodesMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not ZipCodes.Active) or (ZipCodes.FieldByName('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen postcode?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UZipCodes.Close;
          UniMainModule.UZipCodes.SQL.Clear;
          UniMainModule.UZipCodes.SQL.Add('Select * from zipcodes where id=' + ZipCodes.FieldByName('Id').AsString);
          UniMainModule.UZipCodes.Open;
          UniMainModule.UZipCodes.Delete;
          UniMainModule.UpdTrZipCodes.Commit;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrZipCodes.Rollback;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        ZipCodes.Refresh;
      end;
    end);
end;

procedure TZipcodesMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (ZipCodes.Active) and (not ZipCodes.FieldByName('Id').isNull) then
  begin
    UniMainModule.UZipCodes.Close;
    UniMainModule.UZipCodes.SQL.Clear;
    UniMainModule.UZipCodes.SQL.Add('Select * from zipcodes where id=' + ZipCodes.FieldByName('Id').AsString);
    UniMainModule.UZipCodes.Open;
    Try
      UniMainModule.UZipCodes.Edit;
    Except
      UniMainModule.UZipCodes.Cancel;
      UniMainModule.UZipCodes.Close;
      UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
      exit;
    End;

    UniMainModule.Result_dbAction := 0;
    With ZipCodesAddFrm do
    begin
      Init_zipcocetable_modification;
      ShowModal(CallBackInsertUpdateZipCodesTable);
    end;
  end;
end;

procedure TZipcodesMainFrm.CallBackInsertUpdateZipCodesTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if ZipCodes.Active then
    begin
      ZipCodes.Refresh;
      ZipCodes.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TZipcodesMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' ZIPCODE LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' CITY LIKE ''%' + EditSearch.Text + '%''';
  ZipCodes.Filter := Filter;
end;

procedure TZipcodesMainFrm.Start_module;
var SQL: string;
begin
  ZipCodes.Close;
  SQL := 'Select * from zipcodes where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by zipcode, city';
  ZipCodes.SQL.Clear;
  ZipCodes.SQL.Add(SQL);
  ZipCodes.Open;
  EditSearch.SetFocus;
end;

procedure TZipcodesMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
