object ProfileMainFrm: TProfileMainFrm
  Left = 0
  Top = 0
  ClientHeight = 289
  ClientWidth = 681
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditName
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditName: TUniDBEdit
    Left = 16
    Top = 16
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'NAME'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 0
    FieldLabel = 'Naam <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditFirstName: TUniDBEdit
    Left = 16
    Top = 48
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'FIRSTNAME'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 1
    FieldLabel = 'Voornaam <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditEmailUser: TUniDBEdit
    Left = 16
    Top = 80
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'EMAIL'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 2
    FieldLabel = 'Email <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditPhone: TUniDBEdit
    Left = 16
    Top = 112
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'PHONE'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 3
    FieldLabel = 'Telefoon'
    FieldLabelWidth = 150
  end
  object EditMobile: TUniDBEdit
    Left = 16
    Top = 144
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'MOBILE'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 4
    FieldLabel = 'Gsm'
    FieldLabelWidth = 150
  end
  object ComboLanguage: TUniComboBox
    Left = 16
    Top = 176
    Width = 393
    Hint = ''
    Text = ''
    Items.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    TabOrder = 5
    ForceSelection = True
    FieldLabel = 'Taal <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
    IconItems = <>
  end
  object EditPassWordUser: TUniEdit
    Left = 16
    Top = 208
    Width = 393
    Hint = ''
    PasswordChar = '*'
    Text = ''
    TabOrder = 6
    FieldLabel = 'Wachtwoord <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object LblPhoto: TUniLabel
    Left = 416
    Top = 16
    Width = 26
    Height = 13
    Hint = ''
    Caption = 'Foto:'
    ParentFont = False
    TabOrder = 7
  end
  object BtnSelectPhoto: TUniSpeedButton
    AlignWithMargins = True
    Left = 608
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Selecteren foto'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    ParentColor = False
    Color = clWindow
    Images = UniMainModule.ImageList
    ImageIndex = 15
    TabOrder = 8
    TabStop = False
    OnClick = BtnSelectPhotoClick
  end
  object BtnClearPhoto: TUniSpeedButton
    Left = 640
    Top = 16
    Width = 25
    Height = 25
    Hint = 'Verwijderen foto'
    ShowHint = True
    ParentShowHint = False
    Caption = ''
    ParentColor = False
    Color = clWindow
    Images = UniMainModule.ImageList
    ImageIndex = 12
    TabOrder = 9
    TabStop = False
    OnClick = BtnClearPhotoClick
  end
  object PanelPhoto: TUniPanel
    Left = 416
    Top = 48
    Width = 249
    Height = 185
    Hint = ''
    TabOrder = 10
    Caption = ''
    Layout = 'border'
    object ImageLogo: TUniDBImage
      Left = 8
      Top = 8
      Width = 231
      Height = 175
      Hint = ''
      DataField = 'PHOTO'
      DataSource = UniMainModule.DsUUsers
      AutoSize = True
      Transparent = True
      LayoutConfig.Region = 'center'
    end
  end
  object BtnSave: TUniThemeButton
    Left = 448
    Top = 248
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 11
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 560
    Top = 248
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 12
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object FileUploadPhoto: TUniFileUpload
    OnCompleted = FileUploadPhotoCompleted
    MaxAllowedSize = 4096000
    Title = 'Selecteren foto'
    Messages.Uploading = 'Uploaden...'
    Messages.PleaseWait = 'Even geduld'
    Messages.Cancel = 'Annuleren'
    Messages.Processing = 'Verwerking...'
    Messages.UploadError = 'Upload fout'
    Messages.Upload = 'Upload'
    Messages.NoFileError = 'Gelieve een bestand te selecteren'
    Messages.BrowseText = 'Bladeren...'
    Left = 584
    Top = 152
  end
end
