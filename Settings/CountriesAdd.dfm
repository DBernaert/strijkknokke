object CountriesAddFrm: TCountriesAddFrm
  Left = 0
  Top = 0
  ClientHeight = 193
  ClientWidth = 545
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditCode
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditCode: TUniDBEdit
    Left = 16
    Top = 16
    Width = 289
    Height = 22
    Hint = ''
    DataField = 'CODE'
    DataSource = UniMainModule.DsUCountries
    CharCase = ecUpperCase
    TabOrder = 0
    FieldLabel = 'Code <font color="#cd0015">*</font>'
    FieldLabelWidth = 200
  end
  object EditDescriptionDutch: TUniDBEdit
    Left = 16
    Top = 48
    Width = 513
    Height = 22
    Hint = ''
    DataField = 'DUTCH'
    DataSource = UniMainModule.DsUCountries
    TabOrder = 1
    FieldLabel = 'Nederlandse omschrijving <font color="#cd0015">*</font>'
    FieldLabelWidth = 200
  end
  object EditDescriptionFrench: TUniDBEdit
    Left = 16
    Top = 80
    Width = 513
    Height = 22
    Hint = ''
    DataField = 'FRENCH'
    DataSource = UniMainModule.DsUCountries
    TabOrder = 2
    FieldLabel = 'Franse omschrijving <font color="#cd0015">*</font>'
    FieldLabelWidth = 200
  end
  object EditDescriptionEnglish: TUniDBEdit
    Left = 16
    Top = 112
    Width = 513
    Height = 22
    Hint = ''
    DataField = 'ENGLISH'
    DataSource = UniMainModule.DsUCountries
    TabOrder = 3
    FieldLabel = 'Engelse omschrijving <font color="#cd0015">*</font>'
    FieldLabelWidth = 200
  end
  object BtnSave: TUniThemeButton
    Left = 312
    Top = 152
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 4
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 424
    Top = 152
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 5
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
end
