unit GeneralSettingsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, Data.DB, MemDS, uniButton, UniThemeButton, uniPanel, uniGUIBaseClasses,
  uniEdit, uniDBEdit;

type
  TGeneralSettingsMainFrm = class(TUniForm)
    UCompany: TIBCQuery;
    DsUCompany: TDataSource;
    UpdTrCompany: TIBCTransaction;
    EditInvoiceSequence: TUniDBFormattedNumberEdit;
    EditInvoiceSequenceObjects: TUniDBFormattedNumberEdit;
    EditKilometerAllowance: TUniDBFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    EditOfferSequenceObjects: TUniDBFormattedNumberEdit;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module;
  end;

function GeneralSettingsMainFrm: TGeneralSettingsMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule;

function GeneralSettingsMainFrm: TGeneralSettingsMainFrm;
begin
  Result := TGeneralSettingsMainFrm(UniMainModule.GetFormInstance(TGeneralSettingsMainFrm));
end;

{ TGeneralSettingsMainFrm }

procedure TGeneralSettingsMainFrm.BtnCancelClick(Sender: TObject);
begin
  UCompany.Cancel;
  UCompany.Close;
  Close;
end;

procedure TGeneralSettingsMainFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if UCompany.FieldByName('INVOICESEQUENCE').IsNull then begin
    UniMainModule.show_warning('Het volgnummer facturatie is een verplichte ingave.');
    EditInvoiceSequence.SetFocus;
    Exit;
  end;

  if UCompany.FieldByName('OFFERSEQUENCEOBJECTS').IsNull then begin
    UniMainModule.show_warning('Het volgnummer offerte Objects is een verplichte ingave.');
    EditOfferSequenceObjects.SetFocus;
    Exit;
  end;

   if UCompany.FieldByName('INVOICESEQUENCEOBJECTS').IsNull then begin
    UniMainModule.show_warning('Het volgnummer facturatie Objects is een verplichte ingave.');
    EditInvoiceSequenceObjects.SetFocus;
    Exit;
  end;

  if UCompany.FieldByName('KILOMETERALLOWANCE').IsNull then begin
    UniMainModule.show_warning('Kilometer vergoeding is een verplichte ingave.');
    EditKilometerAllowance.SetFocus;
    Exit;
  end;

  Try
    UCompany.Post;
    UpdTrCompany.Commit;
    UCompany.Close;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       UpdTrCompany.Rollback;
       UCompany.Close;
       UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
     end;
  end;
  Close;
end;

procedure TGeneralSettingsMainFrm.Start_module;
begin
  Caption := 'Wijzigen algemene instellingen';
  UCompany.Close;
  UCompany.SQL.Clear;
  UCompany.SQL.Add('Select id, invoicesequence, invoicesequenceobjects, offersequenceobjects, ' +
                   'kilometerallowance from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
  UCompany.Open;
  Try
    UCompany.Edit;
  Except
    UCompany.Cancel;
    UCompany.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Exit;
  End;
end;

procedure TGeneralSettingsMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
