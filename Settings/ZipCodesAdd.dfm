object ZipCodesAddFrm: TZipCodesAddFrm
  Left = 0
  Top = 0
  ClientHeight = 161
  ClientWidth = 481
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = ComboCountryCode
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ComboCountryCode: TUniDBLookupComboBox
    Left = 16
    Top = 16
    Width = 449
    Hint = ''
    ListField = 'DUTCH'
    ListSource = DsCountries
    KeyField = 'CODE'
    ListFieldIndex = 0
    DataField = 'COUNTRYCODE'
    DataSource = UniMainModule.DsUZipCodes
    TabOrder = 0
    Color = clWindow
    FieldLabel = 'Landcode <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
    ForceSelection = True
    Style = csDropDown
  end
  object EditZipCode: TUniDBEdit
    Left = 16
    Top = 48
    Width = 313
    Height = 22
    Hint = ''
    DataField = 'ZIPCODE'
    DataSource = UniMainModule.DsUZipCodes
    TabOrder = 1
    FieldLabel = 'Postcode <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditCity: TUniDBEdit
    Left = 16
    Top = 80
    Width = 449
    Height = 22
    Hint = ''
    DataField = 'CITY'
    DataSource = UniMainModule.DsUZipCodes
    TabOrder = 2
    FieldLabel = 'Locatie <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object BtnSave: TUniThemeButton
    Left = 248
    Top = 120
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 3
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 360
    Top = 120
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 4
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object Countries: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from countries order by dutch')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 240
    Top = 40
  end
  object DsCountries: TDataSource
    DataSet = Countries
    Left = 240
    Top = 95
  end
end
