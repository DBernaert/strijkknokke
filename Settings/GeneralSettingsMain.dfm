object GeneralSettingsMainFrm: TGeneralSettingsMainFrm
  Left = 0
  Top = 0
  ClientHeight = 193
  ClientWidth = 513
  Caption = 'GeneralSettingsMainFrm'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditInvoiceSequence
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditInvoiceSequence: TUniDBFormattedNumberEdit
    Left = 16
    Top = 16
    Width = 481
    Height = 22
    Hint = ''
    DataField = 'INVOICESEQUENCE'
    DataSource = DsUCompany
    TabOrder = 0
    FieldLabel = 'Volgnummer facturatie <font color="#cd0015">*</font>'
    FieldLabelWidth = 220
    DecimalPrecision = 0
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object EditInvoiceSequenceObjects: TUniDBFormattedNumberEdit
    Left = 16
    Top = 80
    Width = 481
    Height = 22
    Hint = ''
    DataField = 'INVOICESEQUENCEOBJECTS'
    DataSource = DsUCompany
    TabOrder = 2
    FieldLabel = 'Volgnummer facturatie Objects<font color="#cd0015">*</font>'
    FieldLabelWidth = 220
    DecimalPrecision = 0
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object EditKilometerAllowance: TUniDBFormattedNumberEdit
    Left = 16
    Top = 112
    Width = 481
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.CurrencySignPos = cpsRight
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'KILOMETERALLOWANCE'
    DataSource = DsUCompany
    TabOrder = 3
    FieldLabel = 'Kilometer vergoeding <font color="#cd0015">*</font>'
    FieldLabelWidth = 220
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object BtnSave: TUniThemeButton
    Left = 280
    Top = 152
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 4
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 392
    Top = 152
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 5
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object EditOfferSequenceObjects: TUniDBFormattedNumberEdit
    Left = 16
    Top = 48
    Width = 481
    Height = 22
    Hint = ''
    DataField = 'OFFERSEQUENCEOBJECTS'
    DataSource = DsUCompany
    TabOrder = 1
    FieldLabel = 'Volgnummer offerte Objects<font color="#cd0015">*</font>'
    FieldLabelWidth = 220
    DecimalPrecision = 0
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object UCompany: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_COMPANYPROFILE_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COMPANYPROFILE'
      
        '  (ID, INVOICESEQUENCE, KILOMETERALLOWANCE, INVOICESEQUENCEOBJEC' +
        'TS, OFFERSEQUENCEOBJECTS)'
      'VALUES'
      
        '  (:ID, :INVOICESEQUENCE, :KILOMETERALLOWANCE, :INVOICESEQUENCEO' +
        'BJECTS, :OFFERSEQUENCEOBJECTS)'
      'RETURNING '
      
        '  ID, INVOICESEQUENCE, KILOMETERALLOWANCE, INVOICESEQUENCEOBJECT' +
        'S, OFFERSEQUENCEOBJECTS')
    SQLDelete.Strings = (
      'DELETE FROM COMPANYPROFILE'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE COMPANYPROFILE'
      'SET'
      
        '  ID = :ID, INVOICESEQUENCE = :INVOICESEQUENCE, KILOMETERALLOWAN' +
        'CE = :KILOMETERALLOWANCE, INVOICESEQUENCEOBJECTS = :INVOICESEQUE' +
        'NCEOBJECTS, OFFERSEQUENCEOBJECTS = :OFFERSEQUENCEOBJECTS'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, INVOICESEQUENCE, KILOMETERALLOWANCE, INVOICESEQUENCEO' +
        'BJECTS, OFFERSEQUENCEOBJECTS FROM COMPANYPROFILE'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM COMPANYPROFILE'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COMPANYPROFILE'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTrCompany
    SQL.Strings = (
      
        'Select id, invoicesequence, invoicesequenceobjects, kilometerall' +
        'owance, offersequenceobjects from companyprofile')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 240
    Top = 32
  end
  object DsUCompany: TDataSource
    DataSet = UCompany
    Left = 240
    Top = 80
  end
  object UpdTrCompany: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 240
    Top = 128
  end
end
