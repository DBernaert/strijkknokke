unit Company;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniImage, uniDBImage, uniPanel, uniButton, uniBitBtn, uniSpeedButton, uniLabel,
  uniGUIBaseClasses, uniEdit, uniDBEdit, UniThemeButton, uniFileUpload;

type
  TCompanyFrm = class(TUniForm)
    EditName: TUniDBEdit;
    EditAddress: TUniDBEdit;
    EditZipcode: TUniDBEdit;
    EditCity: TUniDBEdit;
    EditPhone: TUniDBEdit;
    EditFax: TUniDBEdit;
    EditEmail: TUniDBEdit;
    EditWebsite: TUniDBEdit;
    EditVatNumber: TUniDBEdit;
    EditBankAccount: TUniDBEdit;
    EditIban: TUniDBEdit;
    EditBic: TUniDBEdit;
    EditBankAccount2: TUniDBEdit;
    EditIban2: TUniDBEdit;
    EditBic2: TUniDBEdit;
    LblLogo: TUniLabel;
    BtnSelectLogo: TUniSpeedButton;
    BtnClearLogo: TUniSpeedButton;
    PanelLogo: TUniPanel;
    ImageLogo: TUniDBImage;
    FileUploadLogo: TUniFileUpload;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnSelectLogoClick(Sender: TObject);
    procedure BtnClearLogoClick(Sender: TObject);
    procedure FileUploadLogoCompleted(Sender: TObject; AStream: TFileStream);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InitializeModule;
  end;

function CompanyFrm: TCompanyFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule, Db;

function CompanyFrm: TCompanyFrm;
begin
  Result := TCompanyFrm(UniMainModule.GetFormInstance(TCompanyFrm));
end;

{ TCompanyFrm }

procedure TCompanyFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.UCompany.Cancel;
  UniMainModule.UCompany.Close;
  Close;
end;

procedure TCompanyFrm.BtnClearLogoClick(Sender: TObject);
begin
  UniMainModule.UCompany.FieldByName('LOGO').Clear;
end;

procedure TCompanyFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(UniMainModule.UCompany.FieldByName('NAME').AsString) = '' then
  begin
    UniMainModule.show_warning('De bedrijfsnaam is een verplichte ingave.');
    EditName.SetFocus;
    Exit;
  end;

  Try
    UniMainModule.UCompany.Post;
    UniMainModule.UpdTrCompany.Commit;
    UniMainModule.Company_name := UniMainModule.UCompany.FieldByName('NAME').AsString;
    UniMainModule.UCompany.Close;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UniMainModule.UpdTrCompany.Rollback;
      UniMainModule.UCompany.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;
  Close;
end;

procedure TCompanyFrm.BtnSelectLogoClick(Sender: TObject);
begin
  FileUploadLogo.Execute;
end;

procedure TCompanyFrm.FileUploadLogoCompleted(Sender: TObject; AStream: TFileStream);
begin
  with UniMainModule.UCompany do
    if Active then
    begin
      try
        TBlobField(FieldByName('LOGO')).LoadFromStream(AStream);
      except
        Cancel;
        raise;
      end;
    end;
end;

procedure TCompanyFrm.InitializeModule;
begin
  Caption := 'Wijzigen bedrijfsgegevens';
  UniMainModule.UCompany.Close;
  UniMainModule.UCompany.SQL.Clear;
  UniMainModule.UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
  UniMainModule.UCompany.Open;
  if UniMainModule.UCompany.RecordCount = 0 then
    UniMainModule.UCompany.Append
  else
  begin
    Try
      UniMainModule.UCompany.Edit;
    Except
      UniMainModule.UCompany.Cancel;
      UniMainModule.UCompany.Close;
      UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
      Exit;
    End;
  end;
end;

procedure TCompanyFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
