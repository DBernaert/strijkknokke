unit ProfileMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniImage, uniDBImage, uniPanel, uniBitBtn, uniSpeedButton,
  uniLabel, uniEdit, uniMultiItem, uniComboBox, uniGUIBaseClasses, uniDBEdit, uniFileUpload;

type
  TProfileMainFrm = class(TUniForm)
    EditName: TUniDBEdit;
    EditFirstName: TUniDBEdit;
    EditEmailUser: TUniDBEdit;
    EditPhone: TUniDBEdit;
    EditMobile: TUniDBEdit;
    ComboLanguage: TUniComboBox;
    EditPassWordUser: TUniEdit;
    LblPhoto: TUniLabel;
    BtnSelectPhoto: TUniSpeedButton;
    BtnClearPhoto: TUniSpeedButton;
    PanelPhoto: TUniPanel;
    ImageLogo: TUniDBImage;
    FileUploadPhoto: TUniFileUpload;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnClearPhotoClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure BtnSelectPhotoClick(Sender: TObject);
    procedure FileUploadPhotoCompleted(Sender: TObject; AStream: TFileStream);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module;
  end;

function ProfileMainFrm: TProfileMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, Utils, Db;

function ProfileMainFrm: TProfileMainFrm;
begin
  Result := TProfileMainFrm(UniMainModule.GetFormInstance(TProfileMainFrm));
end;

{ TProfileMainFrm }

procedure TProfileMainFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.UUsers.Cancel;
  UniMainModule.UUsers.Close;
  Close;
end;

procedure TProfileMainFrm.BtnClearPhotoClick(Sender: TObject);
begin
  UniMainModule.UUsers.FieldByName('PHOTO').Clear;
end;

procedure TProfileMainFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(UniMainModule.UUsers.FieldByName('Name').AsString) = '' then
  begin
    UniMainModule.Show_warning('Uw naam is een verplichte ingave.');
    EditName.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UUsers.FieldByName('Firstname').AsString) = '' then
  begin
    UniMainModule.Show_warning('Uw voornaam is een verplichte ingave.');
    EditFirstName.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UUsers.FieldByName('Email').AsString) = '' then
  begin
    UniMainModule.Show_warning('Uw email adres is een verplichte ingave.');
    EditEmailUser.SetFocus;
    Exit;
  end;

  if ComboLanguage.ItemIndex = -1 then
  begin
    UniMainModule.Show_warning('Uw taal is een verplichte ingave.');
    ComboLanguage.SetFocus;
    Exit;
  end;

  if Trim(EditPassWordUser.Text) = '' then
  begin
    UniMainModule.Show_warning('Het wachtwoord is een verplichte ingave.');
    EditPassWordUser.SetFocus;
    Exit;
  end;

  UniMainModule.UUsers.FieldByName('Password').AsString := Utils.Crypt(EditPassWordUser.Text, 35462);

  // Check for duplicates on edit
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select count (*) as counter from users where email=' + QuotedStr(UniMainModule.UUsers.FieldByName('Email').AsString) +
    ' and removed=''0'' and Id<>' + UniMainModule.UUsers.FieldByName('Id').AsString);
  UniMainModule.QWork.Open;
  if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0 then
  begin
    UniMainModule.QWork.Close;
    UniMainModule.Show_warning('Er bestaat reeds een gebruiker met dit email adres.');
    EditEmailUser.SetFocus;
    Exit;
  end;
  UniMainModule.QWork.Close;

  if ImageLogo.Picture.Graphic.Empty
  then UniMainModule.ShowAvatar := False
  else try
         UniMainModule.Avatar.Graphic := ImageLogo.Picture.Graphic;
         UniMainModule.ShowAvatar     := True;
       Except
         UniMainModule.ShowAvatar     := False;
       end;

  UniMainModule.User_name         := UniMainModule.UUsers.FieldByName('NAME').AsString;
  UniMainModule.User_firstname    := UniMainModule.UUsers.FieldByName('FIRSTNAME').AsString;

  try
    UniMainModule.UUsers.FieldByName('Language').AsInteger := ComboLanguage.ItemIndex;
    UniMainModule.UUsers.Post;
    UniMainModule.UpdTrUsers.Commit;
    UniMainModule.Result_dbAction := UniMainModule.UUsers.FieldByName('Id').AsInteger;
  except
    on E: Exception do
    begin
      UniMainModule.UpdTrUsers.Rollback;
      UniMainModule.Show_error('Fout bij het opslaan van de gegevens: ' + E.Message);
    end;
  end;
  UniMainModule.UUsers.Close;
  Close;
end;

procedure TProfileMainFrm.BtnSelectPhotoClick(Sender: TObject);
begin
  FileUploadPhoto.Execute;
end;

procedure TProfileMainFrm.FileUploadPhotoCompleted(Sender: TObject; AStream: TFileStream);
begin
  with UniMainModule.UUsers do
    if Active then
    begin
      try
        TBlobField(FieldByName('PHOTO')).LoadFromStream(AStream);
      except
        Cancel;
        raise;
      end;
    end;
end;

procedure TProfileMainFrm.Start_module;
begin
  Caption := 'Wijzigen profiel en voorkeuren';
  UniMainModule.UUsers.Close;
  UniMainModule.UUsers.SQL.Clear;
  UniMainModule.UUsers.SQL.Add('Select * from users where id=' + IntToStr(UniMainModule.User_id));
  UniMainModule.UUsers.Open;
  Try
    UniMainModule.UUsers.Edit;
    ComboLanguage.ItemIndex := UniMainModule.UUsers.FieldByName('LANGUAGE').AsInteger;
    EditPassWordUser.Text := Utils.Decrypt(UniMainModule.UUsers.FieldByName('PASSWORD').AsString, 35462);
  Except
    UniMainModule.UUsers.Cancel;
    UniMainModule.UUsers.Close;
    UniMainModule.Show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Close;
  End;
end;

procedure TProfileMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.


