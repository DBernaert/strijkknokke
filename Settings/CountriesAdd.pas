unit CountriesAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniGUIBaseClasses, uniEdit, uniDBEdit, uniButton, UniThemeButton, uniPanel;

type
  TCountriesAddFrm = class(TUniForm)
    EditCode: TUniDBEdit;
    EditDescriptionDutch: TUniDBEdit;
    EditDescriptionFrench: TUniDBEdit;
    EditDescriptionEnglish: TUniDBEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_country_creation;
    procedure Init_country_modification;
  end;

function CountriesAddFrm: TCountriesAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, Db;

function CountriesAddFrm: TCountriesAddFrm;
begin
  Result := TCountriesAddFrm(UniMainModule.GetFormInstance(TCountriesAddFrm));
end;

{ TCountriesAddFrm }

procedure TCountriesAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.UCountries.Cancel;
  UniMainModule.UCountries.Close;
  Close;
end;

procedure TCountriesAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(UniMainModule.UCountries.FieldByName('Code').AsString) = '' then
  begin
    UniMainModule.show_warning('De landcode is een verplichte ingave.');
    EditCode.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UCountries.FieldByName('Dutch').AsString) = '' then
  begin
    UniMainModule.show_warning('De nederlandse omschrijving is een verplichte ingave.');
    EditDescriptionDutch.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UCountries.FieldByName('French').AsString) = '' then
  begin
    UniMainModule.show_warning('De franse omschrijving is een verplichte ingave.');
    EditDescriptionFrench.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UCountries.FieldByName('English').AsString) = '' then
  begin
    UniMainModule.show_warning('De engelse omschrijving is een verplichte ingave.');
    EditDescriptionEnglish.SetFocus;
    Exit;
  end;

  // Check for duplicates on insert
  if UniMainModule.UCountries.State = dsInsert then
  begin
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select count (*) as counter from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' and code=' +
      QuotedStr(UniMainModule.UCountries.FieldByName('Code').AsString));
    UniMainModule.QWork.Open;
    if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0 then
    begin
      UniMainModule.QWork.Close;
      UniMainModule.show_warning('Er bestaat reeds een land met deze landcode.');
      EditCode.SetFocus;
      Exit;
    end;
    UniMainModule.QWork.Close;
  end;

  try
    UniMainModule.UCountries.Post;
    UniMainModule.UpdTrCountries.Commit;
    UniMainModule.Result_dbAction_string := UniMainModule.UCountries.FieldByName('Code').AsString;
  except
    on E: Exception do
    begin
      UniMainModule.UpdTrCountries.Rollback;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens: ' + E.Message);
    end;
  end;
  UniMainModule.UCountries.Close;
  Close;
end;

procedure TCountriesAddFrm.Init_country_creation;
begin
  Caption := 'Toevoegen land';
end;

procedure TCountriesAddFrm.Init_country_modification;
begin
  Caption := 'Wijzigen land';
end;

procedure TCountriesAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
