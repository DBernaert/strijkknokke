unit CountriesMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniBasicGrid, uniDBGrid, uniMultiItem, uniComboBox, uniEdit,
  uniButton, UniThemeButton, uniGUIBaseClasses, uniPanel;

type
  TCountriesMainFrm = class(TUniForm)
    Countries: TIBCQuery;
    DsCountries: TDataSource;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    GridCountries: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterLeft: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    procedure EditSearchChange(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackInsertUpdateCountriesTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

function CountriesMainFrm: TCountriesMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UniFSConfirm, Main, CountriesAdd;

function CountriesMainFrm: TCountriesMainFrm;
begin
  Result := TCountriesMainFrm(UniMainModule.GetFormInstance(TCountriesMainFrm));
end;

{ TCountriesMainFrm }

procedure TCountriesMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction_string := '';
  UniMainModule.UCountries.Close;
  UniMainModule.UCountries.SQL.Clear;
  UniMainModule.UCountries.SQL.Add('Select first 0 * from countries');
  UniMainModule.UCountries.Open;
  UniMainModule.UCountries.Append;
  UniMainModule.UCountries.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  With CountriesAddFrm do
  begin
    Init_country_creation;
    ShowModal(CallBackInsertUpdateCountriesTable);
  end;
end;

procedure TCountriesMainFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TCountriesMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Countries.Active) or (Countries.FieldByName('Code').isNull) then
    exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen land?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UCountries.Close;
          UniMainModule.UCountries.SQL.Clear;
          UniMainModule.UCountries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' and code=' +
            QuotedStr(Countries.FieldByName('Code').asString));
          UniMainModule.UCountries.Open;
          UniMainModule.UCountries.Delete;
          UniMainModule.UpdTrCountries.Commit;
          UniMainModule.UCountries.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrCountries.Rollback;
            UniMainModule.UCountries.Close;
            MainForm.Show_Message('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Countries.Refresh;
      end;
    end);
end;

procedure TCountriesMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Countries.Active) and (not Countries.FieldByName('Code').isNull) then
  begin
    UniMainModule.UCountries.Close;
    UniMainModule.UCountries.SQL.Clear;
    UniMainModule.UCountries.SQL.Add('Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' and code=' +
      QuotedStr(Countries.FieldByName('Code').asString));
    UniMainModule.UCountries.Open;
    Try
      UniMainModule.UCountries.Edit;
    Except
      UniMainModule.UCountries.Cancel;
      UniMainModule.UCountries.Close;
      MainForm.Show_Message('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
      exit;
    End;

    UniMainModule.Result_dbAction_string := '';
    With CountriesAddFrm do
    begin
      Init_country_modification;
      ShowModal(CallBackInsertUpdateCountriesTable);
    end;
  end;
end;

procedure TCountriesMainFrm.CallBackInsertUpdateCountriesTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction_string <> '' then
  begin
    if Countries.Active then
    begin
      Countries.Refresh;
      Countries.Locate('Code', UniMainModule.Result_dbAction_string, []);
    end;
  end;
end;

procedure TCountriesMainFrm.EditSearchChange(Sender: TObject);
var
  Filter: string;
begin
  Filter := ' CODE LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' DUTCH LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' FRENCH LIKE ''%' + EditSearch.Text +
    '%''' + ' OR ' + ' ENGLISH LIKE ''%' + EditSearch.Text + '%''';
  Countries.Filter := Filter;
end;

procedure TCountriesMainFrm.Start_module;
var
  SQL: string;
begin
  Countries.Close;
  SQL := 'Select * from countries where companyid=' + IntToStr(UniMainModule.Company_id) + ' order by dutch';
  Countries.SQL.Clear;
  Countries.SQL.Add(SQL);
  Countries.Open;
  EditSearch.SetFocus;
end;

procedure TCountriesMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.

