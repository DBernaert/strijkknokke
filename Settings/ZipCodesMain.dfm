object ZipcodesMainFrm: TZipcodesMainFrm
  Left = 0
  Top = 0
  ClientHeight = 521
  ClientWidth = 767
  Caption = 'Beheer postcodes'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditSearch
  Layout = 'border'
  OnCancel = BtnCloseClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    LayoutConfig.Margin = '8 8 8 8'
    object ContainerFilterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 273
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 265
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        OnChange = EditSearchChange
      end
    end
    object ContainerFilterRight: TUniContainerPanel
      Left = 647
      Top = 0
      Width = 120
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object NavigatorZipCodes: TUniDBNavigator
        Left = 0
        Top = 0
        Width = 121
        Height = 25
        Hint = ''
        DataSource = DsZipcodes
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        IconSet = icsFontAwesome
        TabStop = False
        TabOrder = 1
      end
    end
  end
  object GridZipCodes: TUniDBGrid
    Left = 0
    Top = 25
    Width = 767
    Height = 471
    Hint = ''
    DataSource = DsZipcodes
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '0 8 0 8'
    Align = alClient
    TabOrder = 1
    OnDblClick = BtnModifyClick
    Columns = <
      item
        FieldName = 'COUNTRYCODE'
        Title.Caption = 'Landcode'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'ZIPCODE'
        Title.Caption = 'Postcode'
        Width = 125
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'CITY'
        Title.Caption = 'Locatie'
        Width = 64
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 496
    Width = 767
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '8 8 8 8'
    object ContainerFooterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 337
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object BtnAdd: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Toevoegen'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddClick
        ButtonTheme = uctDefault
      end
      object BtnModify: TUniThemeButton
        Left = 112
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Wijzigen'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyClick
        ButtonTheme = uctDefault
      end
      object BtnDelete: TUniThemeButton
        Left = 224
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Verwijderen'
        ParentFont = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteClick
        ButtonTheme = uctDefault
      end
    end
    object ContainerFooterRight: TUniContainerPanel
      Left = 662
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object BtnClose: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 9
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object ZipCodes: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO ZIPCODES'
      
        '  (ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY, DISTANCE, TRAVELTI' +
        'ME)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :COUNTRYCODE, :ZIPCODE, :CITY, :DISTANCE, :T' +
        'RAVELTIME)'
      'RETURNING '
      
        '  ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY, DISTANCE, TRAVELTIM' +
        'E')
    SQLDelete.Strings = (
      'DELETE FROM ZIPCODES'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE ZIPCODES'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, COUNTRYCODE = :COUNTRYCODE, ' +
        'ZIPCODE = :ZIPCODE, CITY = :CITY, DISTANCE = :DISTANCE, TRAVELTI' +
        'ME = :TRAVELTIME'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, COUNTRYCODE, ZIPCODE, CITY, DISTANCE, TRAV' +
        'ELTIME FROM ZIPCODES'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM ZIPCODES'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM ZIPCODES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from zipcodes order by zipcode')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 336
    Top = 160
  end
  object DsZipcodes: TDataSource
    DataSet = ZipCodes
    Left = 336
    Top = 217
  end
end
