object ObjectsManagementAddFrm: TObjectsManagementAddFrm
  Left = 0
  Top = 0
  ClientHeight = 449
  ClientWidth = 681
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditTitleDutch
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditTitleDutch: TUniDBEdit
    Left = 16
    Top = 16
    Width = 649
    Height = 22
    Hint = ''
    DataField = 'TITLEDUTCH'
    DataSource = UniMainModule.DsUObjectDefinitions
    TabOrder = 0
    FieldLabel = 'Naam Nederlands <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditTitleFrench: TUniDBEdit
    Left = 16
    Top = 48
    Width = 649
    Height = 22
    Hint = ''
    DataField = 'TITLEFRENCH'
    DataSource = UniMainModule.DsUObjectDefinitions
    TabOrder = 1
    FieldLabel = 'Naam Frans'
    FieldLabelWidth = 150
  end
  object EditTitleEnglish: TUniDBEdit
    Left = 16
    Top = 80
    Width = 649
    Height = 22
    Hint = ''
    DataField = 'TITLEENGLISH'
    DataSource = UniMainModule.DsUObjectDefinitions
    TabOrder = 2
    FieldLabel = 'Naam Engels'
    FieldLabelWidth = 150
  end
  object EditTemplateDutch: TUniDBMemo
    Left = 16
    Top = 112
    Width = 649
    Height = 89
    Hint = ''
    DataField = 'TEMPLATEDUTCH'
    DataSource = UniMainModule.DsUObjectDefinitions
    TabOrder = 3
    FieldLabel = 'Omschrijving Nederlands'
    FieldLabelWidth = 150
  end
  object EditTemplateFrench: TUniDBMemo
    Left = 16
    Top = 208
    Width = 649
    Height = 89
    Hint = ''
    DataField = 'TEMPLATEFRENCH'
    DataSource = UniMainModule.DsUObjectDefinitions
    TabOrder = 4
    FieldLabel = 'Omschrijving Frans'
    FieldLabelWidth = 150
  end
  object EditTemplateEnglish: TUniDBMemo
    Left = 16
    Top = 304
    Width = 649
    Height = 89
    Hint = ''
    DataField = 'TEMPLATEENGLISH'
    DataSource = UniMainModule.DsUObjectDefinitions
    TabOrder = 5
    FieldLabel = 'Omschrijving Engels'
    FieldLabelWidth = 150
  end
  object BtnSave: TUniThemeButton
    Left = 448
    Top = 408
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 6
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 560
    Top = 408
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 7
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
end
