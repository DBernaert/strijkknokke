object BasicMainFrm: TBasicMainFrm
  Left = 0
  Top = 0
  ClientHeight = 492
  ClientWidth = 645
  Caption = 'Beheer basisbestanden'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = ComboTable
  Layout = 'border'
  OnCancel = BtnCloseClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 645
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    LayoutConfig.Margin = '8 8 8 8'
    object ContainerHeaderLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 489
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object ComboTable: TUniComboBox
        Left = 0
        Top = 0
        Width = 233
        Hint = ''
        Style = csDropDownList
        Text = ''
        Items.Strings = (
          'Aanspreektitels'
          'Adrestypes'
          'Bedrijfsvormen'
          'Btw-plichtigheid'
          'Labels'
          'Nationaliteiten'
          'Sectoren'
          'Talen')
        TabOrder = 1
        FieldLabel = 'Tabel'
        FieldLabelWidth = 50
        IconItems = <>
        OnChange = ComboTableChange
      end
      object EditSearch: TUniEdit
        Left = 240
        Top = 0
        Width = 241
        Hint = ''
        Text = ''
        TabOrder = 2
        CheckChangeDelay = 500
        ClearButton = True
        OnChange = EditSearchChange
      end
    end
    object ContainerFilterRight: TUniContainerPanel
      Left = 525
      Top = 0
      Width = 120
      Height = 25
      Hint = ''
      ParentColor = False
      Color = 16119285
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object NavigatorBasic: TUniDBNavigator
        Left = 0
        Top = 0
        Width = 121
        Height = 25
        Hint = ''
        DataSource = DsBasic
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        IconSet = icsFontAwesome
        TabStop = False
        TabOrder = 1
      end
    end
  end
  object GridBasic: TUniDBGrid
    Left = 0
    Top = 25
    Width = 645
    Height = 442
    Hint = ''
    DataSource = DsBasic
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '0 8 0 8'
    Align = alClient
    TabOrder = 1
    OnDblClick = BtnModifyClick
    Columns = <
      item
        FieldName = 'DUTCH'
        Title.Caption = 'Nederlands'
        Width = 340
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'FRENCH'
        Title.Caption = 'Frans'
        Width = 340
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 467
    Width = 645
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '8 8 8 8'
    object ContainerFooterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 337
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object BtnAdd: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Toevoegen'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddClick
        ButtonTheme = uctDefault
      end
      object BtnModify: TUniThemeButton
        Left = 112
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Wijzigen'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyClick
        ButtonTheme = uctDefault
      end
      object BtnDelete: TUniThemeButton
        Left = 224
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Verwijderen'
        ParentFont = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteClick
        ButtonTheme = uctDefault
      end
    end
    object ContainerFooterRight: TUniContainerPanel
      Left = 540
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object BtnClose: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 9
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object Basic: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from basictables order by dutch')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 312
    Top = 136
  end
  object DsBasic: TDataSource
    DataSet = Basic
    Left = 312
    Top = 200
  end
end
