unit UsersMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid, uniEdit, uniGUIBaseClasses, uniPanel,
  Data.DB, MemDS, DBAccess, IBC, uniDBNavigator;

type
  TUsersMainFrm = class(TUniForm)
    Users: TIBCQuery;
    DsUsers: TDataSource;
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    GridUsers: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterLeft: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorUsers: TUniDBNavigator;
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackInsertUpdateUsersTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

function UsersMainFrm: TUsersMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UniFSConfirm, Main, UsersAdd;

function UsersMainFrm: TUsersMainFrm;
begin
  Result := TUsersMainFrm(UniMainModule.GetFormInstance(TUsersMainFrm));
end;

procedure TUsersMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  UniMainModule.UUsers.Close;
  UniMainModule.UUsers.SQL.Clear;
  UniMainModule.UUsers.SQL.Add('Select first 0 * from users');
  UniMainModule.UUsers.Open;
  UniMainModule.UUsers.Append;
  UniMainModule.UUsers.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UniMainModule.UUsers.FieldByName('Removed').AsString := '0';
  With UsersAddFrm do
  begin
    Init_user_creation;
    ShowModal(CallBackInsertUpdateUsersTable);
  end;
end;

procedure TUsersMainFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TUsersMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Users.Active) or (Users.FieldByName('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen huidige gebruiker?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UUsers.Close;
          UniMainModule.UUsers.SQL.Clear;
          UniMainModule.UUsers.SQL.Add('Select * from users where id=' + Users.FieldByName('Id').AsString);
          UniMainModule.UUsers.Open;
          UniMainModule.UUsers.Edit;
          UniMainModule.UUsers.FieldByName('Removed').AsString := '1';
          UniMainModule.UUsers.Post;
          UniMainModule.UpdTrUsers.Commit;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrUsers.Rollback;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Users.Refresh;
      end;
    end);
end;

procedure TUsersMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Users.Active) and (not Users.FieldByName('Id').isNull) then
  begin
    UniMainModule.UUsers.Close;
    UniMainModule.UUsers.SQL.Clear;
    UniMainModule.UUsers.SQL.Add('Select * from users where id=' + Users.FieldByName('Id').AsString);
    UniMainModule.UUsers.Open;
    Try
      UniMainModule.UUsers.Edit;
    Except
      UniMainModule.UUsers.Cancel;
      UniMainModule.UUsers.Close;
      UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
      exit;
    End;

    UniMainModule.Result_dbAction := 0;
    With UsersAddFrm do
    begin
      Init_user_modification;
      ShowModal(CallBackInsertUpdateUsersTable);
    end;
  end;
end;

procedure TUsersMainFrm.CallBackInsertUpdateUsersTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Users.Active then
    begin
      Users.Refresh;
      Users.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TUsersMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' NAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' FIRSTNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' EMAIL LIKE ''%' +
    EditSearch.Text + '%''';
  Users.Filter := Filter;
end;

procedure TUsersMainFrm.Start_module;
var SQL: string;
begin
  Users.Close;
  SQL := 'Select * from users where companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by name, firstname';
  Users.SQL.Clear;
  Users.SQL.Add(SQL);
  Users.Open;
  EditSearch.SetFocus;
end;

procedure TUsersMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
