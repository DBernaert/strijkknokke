unit ZipCodesSelect;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniButton, UniThemeButton, uniPanel, uniGUIBaseClasses,
  uniBasicGrid, uniDBGrid;

type
  TZipCodesSelectFrm = class(TUniForm)
    ZipCodes: TIBCQuery;
    DsZipcodes: TDataSource;
    GridZipCodes: TUniDBGrid;
    PanelFooter: TUniContainerPanel;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    SelectionType: integer;
  public
    { Public declarations }
    procedure Init_zipcode_selection(city: string);
    procedure Init_city_selection(zipcode: string);
  end;

function ZipCodesSelectFrm: TZipCodesSelectFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication;

function ZipCodesSelectFrm: TZipCodesSelectFrm;
begin
  Result := TZipCodesSelectFrm(UniMainModule.GetFormInstance(TZipCodesSelectFrm));
end;

{ TZipCodesSelectFrm }

procedure TZipCodesSelectFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction_string := '';
  Close;
end;

procedure TZipCodesSelectFrm.BtnSaveClick(Sender: TObject);
begin
  if (ZipCodes.Active) and (not ZipCodes.FieldByName('Id').isNull) then
  begin
    if SelectionType = 1 then
      UniMainModule.Result_dbAction_string := ZipCodes.FieldByName('City').AsString
    else
      UniMainModule.Result_dbAction_string := ZipCodes.FieldByName('ZipCode').AsString;
    Close;
  end;
end;

procedure TZipCodesSelectFrm.Init_city_selection(zipcode: string);
begin
  Caption := 'Selecteren postcode - locatie';
  SelectionType := 1;
  ZipCodes.Close;
  ZipCodes.SQL.Clear;
  ZipCodes.SQL.Add('Select * from zipcodes where companyid=' + IntToStr(UniMainModule.Company_Id) + ' and UPPER(Zipcode) = ' + QuotedStr(zipcode) +
                   ' order by city');
  ZipCodes.Open;
end;

procedure TZipCodesSelectFrm.Init_zipcode_selection(city: string);
begin
  Caption := 'Selecteren postcode - locatie';
  SelectionType := 0;
  ZipCodes.Close;
  ZipCodes.SQL.Clear;
  ZipCodes.SQL.Add('Select * from zipcodes where companyid=' + IntToStr(UniMainModule.Company_Id) + ' and UPPER(City) = ' + QuotedStr(city) +
                   ' order by zipcode');
  ZipCodes.Open;
end;

procedure TZipCodesSelectFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
