unit BasicMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniEdit, uniGUIBaseClasses, uniPanel, uniButton, UniThemeButton, Data.DB, MemDS, DBAccess,
  IBC, uniBasicGrid, uniDBGrid, uniMultiItem, uniComboBox, uniDBNavigator;

type
  TBasicMainFrm = class(TUniForm)
    ContainerFilter: TUniContainerPanel;
    Basic: TIBCQuery;
    DsBasic: TDataSource;
    GridBasic: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterLeft: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    ContainerHeaderLeft: TUniContainerPanel;
    ComboTable: TUniComboBox;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorBasic: TUniDBNavigator;
    procedure UniFormCreate(Sender: TObject);
    procedure BtnCloseClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure ComboTableChange(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
  private
    { Private declarations }
    TableId: integer;
    procedure CallBackInsertUpdateBasicTable(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
  end;

function BasicMainFrm: TBasicMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, BasicAdd, Main, UniFSConfirm;

function BasicMainFrm: TBasicMainFrm;
begin
  Result := TBasicMainFrm(UniMainModule.GetFormInstance(TBasicMainFrm));
end;

{ TBasicMainFrm }

procedure TBasicMainFrm.BtnAddClick(Sender: TObject);
begin
  if Basic.Active = False
  then exit;

  UniMainModule.Result_dbAction := 0;
  UniMainModule.UBasic.Close;
  UniMainModule.UBasic.SQL.Clear;
  UniMainModule.UBasic.SQL.Add('Select first 0 * from basictables');
  UniMainModule.UBasic.Open;
  UniMainModule.UBasic.Append;
  UniMainModule.UBasic.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UniMainModule.UBasic.FieldByName('TableId').AsInteger   := TableId;
  UniMainModule.UBasic.FieldByName('Removed').AsString    := '0';
  With BasicAddFrm do
  begin
    Init_basic_creation;
    ShowModal(CallBackInsertUpdateBasicTable);
  end;
end;

procedure TBasicMainFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TBasicMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Basic.Active) or (Basic.FieldByName('Id').isNull) then
    exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen item?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UBasic.Close;
          UniMainModule.UBasic.SQL.Clear;
          UniMainModule.UBasic.SQL.Add('Select * from basictables where id=' + QuotedStr(Basic.FieldByName('Id').AsString));
          UniMainModule.UBasic.Open;
          UniMainModule.UBasic.Edit;
          UniMainModule.UBasic.FieldByName('Removed').AsString := '1';
          UniMainModule.UBasic.Post;
          UniMainModule.UpdTrBasic.Commit;
          UniMainModule.UBasic.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrBasic.Rollback;
            UniMainModule.UBasic.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        Basic.Refresh;
      end;
    end);
end;

procedure TBasicMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Basic.Active) and (not Basic.FieldByName('Id').isNull) then
  begin
    UniMainModule.UBasic.Close;
    UniMainModule.UBasic.SQL.Clear;
    UniMainModule.UBasic.SQL.Add('Select * from basictables where id=' + Basic.FieldByName('Id').AsString);
    UniMainModule.UBasic.Open;
    Try
      UniMainModule.UBasic.Edit;
    Except
      UniMainModule.UBasic.Cancel;
      UniMainModule.UBasic.Close;
      UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
      exit;
    End;

    UniMainModule.Result_dbAction := 0;
    With BasicAddFrm do
    begin
      Init_basic_modification;
      ShowModal(CallBackInsertUpdateBasicTable);
    end;
  end;
end;

procedure TBasicMainFrm.CallBackInsertUpdateBasicTable(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Basic.Active then
    begin
      Basic.Refresh;
      Basic.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TBasicMainFrm.ComboTableChange(Sender: TObject);
begin
  Basic.Close;

  if ComboTable.ItemIndex <> -1
  then begin
         case ComboTable.ItemIndex of
         0:  TableId := 1;    //Aanspreektitels
         1:  TableId := 6;    //Adrestypes
         2:  TableId := 4;    //Bedrijfsvormen
         3:  TableId := 7;    //Btw-plichtigheid
         4:  TableId := 2;    //Labels
         5:  TableId := 8;    //Nationaliteiten
         6:  TableId := 5;    //Sectoren
         7:  TableId := 3;    //Talen
         end;
       end;

  Basic.Close;
  Basic.SQL.Clear;
  Basic.SQL.Add('Select * from basictables where tableid=' + IntToStr(TableId) + ' and companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0''' +
                ' order by dutch');
  Basic.Open;
end;

procedure TBasicMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  if Basic.Active = False
  then exit;

  Filter := ' DUTCH LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' FRENCH LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' ENGLISH LIKE ''%' +
              EditSearch.Text + '%''';
  Basic.Filter := Filter;
end;

procedure TBasicMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
