unit ObjectsManagementMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, MemDS, DBAccess, IBC, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid,
  uniDBNavigator, uniEdit, uniGUIBaseClasses, uniPanel;

type
  TObjectsManagementMainFrm = class(TUniForm)
    ContainerFilter: TUniContainerPanel;
    ContainerFilterLeft: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerFilterRight: TUniContainerPanel;
    NavigatorObjectDefinitions: TUniDBNavigator;
    GridObjectDefinitions: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterLeft: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    ObjectDefinitions: TIBCQuery;
    DsObjectDefinitions: TDataSource;
    procedure BtnCloseClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
  private
    { Private declarations }
    procedure CallBackInsertUpdateObjectDefinition(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_Objects_Management_module;
  end;

function ObjectsManagementMainFrm: TObjectsManagementMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, UniFSConfirm, ObjectsManagementAdd, Main;

function ObjectsManagementMainFrm: TObjectsManagementMainFrm;
begin
  Result := TObjectsManagementMainFrm(UniMainModule.GetFormInstance(TObjectsManagementMainFrm));
end;

{ TObjectsManagementMainFrm }

procedure TObjectsManagementMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction_string := '';
  UniMainModule.UObjectDefinitions.Close;
  UniMainModule.UObjectDefinitions.SQL.Clear;
  UniMainModule.UObjectDefinitions.SQL.Add('Select first 0 * from ObjectDefinitions');
  UniMainModule.UObjectDefinitions.Open;
  UniMainModule.UObjectDefinitions.Append;
  UniMainModule.UObjectDefinitions.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UniMainModule.UObjectDefinitions.FieldByName('Removed').AsString    := '0';
  With ObjectsManagementAddFrm do
  begin
    Init_object_definition_creation;
    ShowModal(CallBackInsertUpdateObjectDefinition);
  end;
end;

procedure TObjectsManagementMainFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TObjectsManagementMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not ObjectDefinitions.Active) or (ObjectDefinitions.FieldByName('Id').isNull) then
    exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen Object?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UObjectDefinitions.Close;
          UniMainModule.UObjectDefinitions.SQL.Clear;
          UniMainModule.UObjectDefinitions.SQL.Add('Select * from ObjectDefinitions where Id=' + ObjectDefinitions.FieldByName('Id').AsString);
          UniMainModule.UObjectDefinitions.Open;
          UniMainModule.UObjectDefinitions.Edit;
          UniMainModule.UObjectDefinitions.Fieldbyname('Removed').asString := '1';
          UniMainModule.UObjectDefinitions.Post;
          UniMainModule.UpdTrObjectDefinitions.Commit;
          UniMainModule.UObjectDefinitions.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrObjectDefinitions.Rollback;
            UniMainModule.UObjectDefinitions.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        ObjectDefinitions.Refresh;
      end;
    end);
end;

procedure TObjectsManagementMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (ObjectDefinitions.Active) and (not ObjectDefinitions.FieldByName('Id').isNull) then
  begin
    UniMainModule.UObjectDefinitions.Close;
    UniMainModule.UObjectDefinitions.SQL.Clear;
    UniMainModule.UObjectDefinitions.SQL.Add('Select * from ObjectDefinitions where Id=' + ObjectDefinitions.FieldByName('Id').asString);
    UniMainModule.UObjectDefinitions.Open;
    Try
      UniMainModule.UObjectDefinitions.Edit;
    Except
      UniMainModule.UObjectDefinitions.Cancel;
      UniMainModule.UObjectDefinitions.Close;
      UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
      exit;
    End;

    UniMainModule.Result_dbAction_string := '';
    With ObjectsManagementAddFrm do
    begin
      Init_object_definition_modification;
      ShowModal(CallBackInsertUpdateObjectDefinition);
    end;
  end;
end;

procedure TObjectsManagementMainFrm.CallBackInsertUpdateObjectDefinition(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if ObjectDefinitions.Active then begin
      ObjectDefinitions.Refresh;
      ObjectDefinitions.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TObjectsManagementMainFrm.EditSearchChange(Sender: TObject);
var
  Filter: string;
begin
  Filter := ' TITLEDUTCH LIKE ''%' + EditSearch.Text + '%''';
  ObjectDefinitions.Filter := Filter;
end;

procedure TObjectsManagementMainFrm.Start_Objects_Management_module;
var
  SQL: string;
begin
  ObjectDefinitions.Close;
  SQL := 'Select * from objectdefinitions where companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' order by titledutch';
  ObjectDefinitions.SQL.Clear;
  ObjectDefinitions.SQL.Add(SQL);
  ObjectDefinitions.Open;
  EditSearch.SetFocus;
end;

procedure TObjectsManagementMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
