object UsersMainFrm: TUsersMainFrm
  Left = 0
  Top = 0
  ClientHeight = 419
  ClientWidth = 854
  Caption = 'Beheer gebruikers'
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditSearch
  Layout = 'border'
  OnCancel = BtnCloseClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ContainerFilter: TUniContainerPanel
    Left = 0
    Top = 0
    Width = 854
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alTop
    TabOrder = 0
    Layout = 'border'
    LayoutConfig.Region = 'north'
    LayoutConfig.Margin = '8 8 8 8'
    object ContainerFilterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 273
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object EditSearch: TUniEdit
        Left = 0
        Top = 0
        Width = 265
        Hint = ''
        Text = ''
        TabOrder = 1
        CheckChangeDelay = 500
        ClearButton = True
        OnChange = EditSearchChange
      end
    end
    object ContainerFilterRight: TUniContainerPanel
      Left = 734
      Top = 0
      Width = 120
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object NavigatorUsers: TUniDBNavigator
        Left = 0
        Top = 0
        Width = 121
        Height = 25
        Hint = ''
        DataSource = DsUsers
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
        IconSet = icsFontAwesome
        TabStop = False
        TabOrder = 1
      end
    end
  end
  object GridUsers: TUniDBGrid
    Left = 0
    Top = 25
    Width = 854
    Height = 369
    Hint = ''
    DataSource = DsUsers
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '0 8 0 8'
    Align = alClient
    TabOrder = 1
    OnDblClick = BtnModifyClick
    Columns = <
      item
        FieldName = 'NAME'
        Title.Caption = 'Naam'
        Width = 64
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'FIRSTNAME'
        Title.Caption = 'Voornaam'
        Width = 64
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'EMAIL'
        Title.Caption = 'Email'
        Width = 64
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 394
    Width = 854
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 2
    Layout = 'border'
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '8 8 8 8'
    object ContainerFooterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 337
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      object BtnAdd: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Toevoegen'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddClick
        ButtonTheme = uctDefault
      end
      object BtnModify: TUniThemeButton
        Left = 112
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Wijzigen'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyClick
        ButtonTheme = uctDefault
      end
      object BtnDelete: TUniThemeButton
        Left = 224
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Verwijderen'
        ParentFont = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteClick
        ButtonTheme = uctDefault
      end
    end
    object ContainerFooterRight: TUniContainerPanel
      Left = 749
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      object BtnClose: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 9
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object Users: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO USERS'
      '  (ID, NAME, FIRSTNAME, EMAIL, PASSWORD, REMOVED)'
      'VALUES'
      '  (:ID, :NAME, :FIRSTNAME, :EMAIL, :PASSWORD, :REMOVED)'
      'RETURNING '
      '  ID, NAME, FIRSTNAME, EMAIL, PASSWORD, REMOVED')
    SQLDelete.Strings = (
      'DELETE FROM USERS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE USERS'
      'SET'
      
        '  ID = :ID, NAME = :NAME, FIRSTNAME = :FIRSTNAME, EMAIL = :EMAIL' +
        ', PASSWORD = :PASSWORD, REMOVED = :REMOVED'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, NAME, FIRSTNAME, EMAIL, PASSWORD, REMOVED FROM USERS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM USERS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM USERS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from users')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 416
    Top = 192
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 416
    Top = 248
  end
end
