unit MailTemplatesMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniMemo, uniDBMemo, uniPanel, uniPageControl, uniGUIBaseClasses, DBAccess, IBC, Data.DB,
  MemDS, uniButton, UniThemeButton;

type
  TMailTemplatesMainFrm = class(TUniForm)
    UCompany: TIBCQuery;
    DsUCompany: TDataSource;
    UpdTrCompany: TIBCTransaction;
    PageControleMailTemplates: TUniPageControl;
    UniTabSheet1: TUniTabSheet;
    MemoOfferObjects: TUniDBMemo;
    UniTabSheet2: TUniTabSheet;
    MemoPreinvoiceObjects: TUniDBMemo;
    UniTabSheet3: TUniTabSheet;
    MemoFinalInvoiceObjects: TUniDBMemo;
    UniTabSheet4: TUniTabSheet;
    MemoGeneralInvoice: TUniDBMemo;
    UniTabSheet5: TUniTabSheet;
    MemoReminder1: TUniDBMemo;
    UniTabSheet6: TUniTabSheet;
    MemoReminder2: TUniDBMemo;
    UniTabSheet7: TUniTabSheet;
    MemoReminder3: TUniDBMemo;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module;
  end;

function MailTemplatesMainFrm: TMailTemplatesMainFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule;

function MailTemplatesMainFrm: TMailTemplatesMainFrm;
begin
  Result := TMailTemplatesMainFrm(UniMainModule.GetFormInstance(TMailTemplatesMainFrm));
end;

{ TMailTemplatesMainFrm }

procedure TMailTemplatesMainFrm.BtnCancelClick(Sender: TObject);
begin
  UCompany.Cancel;
  UCompany.Close;
  Close;
end;

procedure TMailTemplatesMainFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;
  Try
    UCompany.Post;
    UpdTrCompany.Commit;
    UCompany.Close;
  Except on E: EDataBaseError
  do begin
       UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
       UpdTrCompany.Rollback;
       UCompany.Close;
       UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
     end;
  end;
  Close;
end;

procedure TMailTemplatesMainFrm.Start_module;
begin
  Caption := 'Bewerken mail templates';
  UCompany.Close;
  UCompany.SQL.Clear;
  UCompany.SQL.Add('Select * from companyprofile where id=' + IntToStr(UniMainModule.Company_id));
  UCompany.Open;
  Try
    UCompany.Edit;
  Except
    UCompany.Cancel;
    UCompany.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Exit;
  End;
end;

procedure TMailTemplatesMainFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TMailTemplatesMainFrm.UniFormReady(Sender: TObject);
begin
  MemoOfferObjects.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  MemoPreinvoiceObjects.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  MemoFinalInvoiceObjects.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  MemoGeneralInvoice.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  MemoReminder1.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  MemoReminder2.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
  MemoReminder3.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

end.
