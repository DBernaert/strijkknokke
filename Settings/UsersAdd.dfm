object UsersAddFrm: TUsersAddFrm
  Left = 0
  Top = 0
  ClientHeight = 289
  ClientWidth = 425
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditName
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditName: TUniDBEdit
    Left = 16
    Top = 16
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'NAME'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 0
    FieldLabel = 'Naam <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditFirstName: TUniDBEdit
    Left = 16
    Top = 48
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'FIRSTNAME'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 1
    FieldLabel = 'Voornaam <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditEmailUser: TUniDBEdit
    Left = 16
    Top = 80
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'EMAIL'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 2
    FieldLabel = 'Email <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object EditPhone: TUniDBEdit
    Left = 16
    Top = 112
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'PHONE'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 3
    FieldLabel = 'Telefoon'
    FieldLabelWidth = 150
  end
  object EditMobile: TUniDBEdit
    Left = 16
    Top = 144
    Width = 393
    Height = 22
    Hint = ''
    DataField = 'MOBILE'
    DataSource = UniMainModule.DsUUsers
    TabOrder = 4
    FieldLabel = 'Gsm'
    FieldLabelWidth = 150
  end
  object ComboLanguage: TUniComboBox
    Left = 16
    Top = 176
    Width = 393
    Hint = ''
    Text = ''
    Items.Strings = (
      'Nederlands'
      'Frans'
      'Engels')
    TabOrder = 5
    ForceSelection = True
    FieldLabel = 'Taal <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
    IconItems = <>
  end
  object EditPassWordUser: TUniEdit
    Left = 16
    Top = 208
    Width = 393
    Hint = ''
    PasswordChar = '*'
    Text = ''
    TabOrder = 6
    FieldLabel = 'Wachtwoord <font color="#cd0015">*</font>'
    FieldLabelWidth = 150
  end
  object BtnSave: TUniThemeButton
    Left = 192
    Top = 248
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 7
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 304
    Top = 248
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 8
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
end
