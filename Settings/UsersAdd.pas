unit UsersAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniEdit, uniMultiItem, uniComboBox, uniGUIBaseClasses, uniDBEdit,
  uniPanel;

type
  TUsersAddFrm = class(TUniForm)
    EditName: TUniDBEdit;
    EditFirstName: TUniDBEdit;
    EditEmailUser: TUniDBEdit;
    EditPhone: TUniDBEdit;
    EditMobile: TUniDBEdit;
    ComboLanguage: TUniComboBox;
    EditPassWordUser: TUniEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_user_creation;
    procedure Init_user_modification;
  end;

function UsersAddFrm: TUsersAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, Utils, Db;

function UsersAddFrm: TUsersAddFrm;
begin
  Result := TUsersAddFrm(UniMainModule.GetFormInstance(TUsersAddFrm));
end;

procedure TUsersAddFrm.BtnCancelClick(Sender: TObject);
begin
  UniMainModule.UUsers.Cancel;
  UniMainModule.UUsers.Close;
  Close;
end;

procedure TUsersAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(UniMainModule.UUsers.FieldByName('Name').AsString) = '' then
  begin
    UniMainModule.show_warning('De naam is een verplichte ingave.');
    EditName.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UUsers.FieldByName('Firstname').AsString) = '' then
  begin
    UniMainModule.show_warning('De voornaam is een verplichte ingave.');
    EditFirstName.SetFocus;
    Exit;
  end;

  if Trim(UniMainModule.UUsers.FieldByName('Email').AsString) = '' then
  begin
    UniMainModule.show_warning('Het email adres is een verplichte ingave.');
    EditEmailUser.SetFocus;
    Exit;
  end;

  if ComboLanguage.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('De taal van de gebruiker is een verplichte ingave.');
    ComboLanguage.SetFocus;
    Exit;
  end;

  if Trim(EditPassWordUser.Text) = '' then
  begin
    UniMainModule.show_warning('Het wachtwoord is een verplichte ingave.');
    EditPassWordUser.SetFocus;
    Exit;
  end;

  UniMainModule.UUsers.FieldByName('Password').AsString := Utils.Crypt(EditPassWordUser.Text, 35462);

  // Check for duplicates on insert
  if UniMainModule.UUsers.State = dsInsert then
  begin
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select count (*) as counter from users where email=' + QuotedStr(UniMainModule.UUsers.FieldByName('Email').AsString) +
      ' and removed=''0''');
    UniMainModule.QWork.Open;
    if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0 then
    begin
      UniMainModule.QWork.Close;
      UniMainModule.show_warning('Er bestaat reeds een gebruiker met dit email adres.');
      EditEmailUser.SetFocus;
      Exit;
    end;
    UniMainModule.QWork.Close;
  end
  else
  begin
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select count (*) as counter from users where email=' + QuotedStr(UniMainModule.UUsers.FieldByName('Email').AsString) +
      ' and removed=''0'' and Id<>' + UniMainModule.UUsers.FieldByName('Id').AsString);
    UniMainModule.QWork.Open;
    if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0 then
    begin
      UniMainModule.QWork.Close;
      UniMainModule.show_warning('Er bestaat reeds een gebruiker met dit email adres.');
      EditEmailUser.SetFocus;
      Exit;
    end;
    UniMainModule.QWork.Close;
  end;

  try
    UniMainModule.UUsers.FieldByName('Language').AsInteger := ComboLanguage.ItemIndex;
    UniMainModule.UUsers.Post;
    UniMainModule.UpdTrUsers.Commit;
    UniMainModule.Result_dbAction := UniMainModule.UUsers.FieldByName('Id').AsInteger;
  except
    on E: Exception do
    begin
      UniMainModule.UpdTrUsers.Rollback;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens: ' + E.Message);
    end;
  end;
  UniMainModule.UUsers.Close;
  Close;
end;

procedure TUsersAddFrm.Init_user_creation;
begin
  Caption := 'Toevoegen gebruiker';
  ComboLanguage.ItemIndex := 0;
end;

procedure TUsersAddFrm.Init_user_modification;
begin
  Caption := 'Wijzigen gebruiker';
  ComboLanguage.ItemIndex := UniMainModule.UUsers.FieldByName('Language').AsInteger;
  EditPassWordUser.Text := Utils.Decrypt(UniMainModule.UUsers.FieldByName('PASSWORD').AsString, 35462);
end;

procedure TUsersAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
