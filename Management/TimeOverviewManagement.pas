unit TimeOverviewManagement;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniButton, UniThemeButton, uniBasicGrid, uniDBGrid,
  uniEdit, uniImage, uniLabel, uniGUIBaseClasses, uniPanel, uniMultiItem, uniComboBox, uniDBNavigator;

type
  TTimeOverviewManagementFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageTimeRegistration: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerButtonsFiles: TUniContainerPanel;
    GridTimeRegistrations: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    TimeRegistrations: TIBCQuery;
    TimeRegistrationsID: TLargeintField;
    TimeRegistrationsREGISTRATIONDATE: TDateField;
    TimeRegistrationsPROJECTNAME: TWideStringField;
    TimeRegistrationsACTIVITY: TWideStringField;
    TimeRegistrationsDESCRIPTION: TWideStringField;
    TimeRegistrationsINTERNALREMARKS: TBlobField;
    TimeRegistrationsSTARTTIMEVALUE: TFloatField;
    TimeRegistrationsENDTIMEVALUE: TFloatField;
    TimeRegistrationsSALDOVALUE: TFloatField;
    DsTimeRegistrations: TDataSource;
    ComboUsers: TUniComboBox;
    BtnSearch: TUniThemeButton;
    UsersWork: TIBCQuery;
    BtnPrintOverview: TUniThemeButton;
    UniDBNavigator1: TUniDBNavigator;
    procedure EditSearchChange(Sender: TObject);
    procedure BtnSearchClick(Sender: TObject);
    procedure UniFrameDestroy(Sender: TObject);
    procedure BtnPrintOverviewClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, ParamsReportOverViewInvoice;



{ TTimeOverviewManagementFrm }

procedure TTimeOverviewManagementFrm.BtnPrintOverviewClick(Sender: TObject);
begin
  if ComboUsers.ItemIndex = -1
  then exit;

  UsersWork.First;
  UsersWork.MoveBy(ComboUsers.ItemIndex);

  With ParamsReportOverviewInvoiceFrm do
  begin
    Start_module(UsersWork.FieldByName('Id').AsInteger);
    ShowModal;
  end;
end;

procedure TTimeOverviewManagementFrm.BtnSearchClick(Sender: TObject);
var SQL: string;
begin
  if ComboUsers.ItemIndex = -1
  then exit;

  UsersWork.First;
  UsersWork.MoveBy(ComboUsers.ItemIndex);

  TimeRegistrations.Close;
  TimeRegistrations.SQL.Clear;
  SQL := 'SELECT TIMEREGISTRATIONS.ID, TIMEREGISTRATIONS.REGISTRATIONDATE, TIMEREGISTRATIONS.STARTTIMEVALUE, ' +
    'TIMEREGISTRATIONS.ENDTIMEVALUE, TIMEREGISTRATIONS.SALDOVALUE, TIMEREGISTRATIONS.DESCRIPTION, ' + 'TIMEREGISTRATIONS.INTERNALREMARKS, ' +
    'PROJECTS.NAME AS PROJECTNAME, ' + 'BASICTABLES.DUTCH AS ACTIVITY ' + 'FROM ' + 'TIMEREGISTRATIONS ' +
    'LEFT OUTER JOIN PROJECTS ON (TIMEREGISTRATIONS.PROJECT = PROJECTS.ID) ' + 'LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = BASICTABLES.ID) ' +
    'WHERE TIMEREGISTRATIONS.USERID=' + UsersWork.FieldByName('Id').asString + ' ' + 'AND TIMEREGISTRATIONS.REMOVED=''0'' ';

  SQL := SQL + 'ORDER BY REGISTRATIONDATE DESC, STARTTIMEVALUE DESC';
  TimeRegistrations.SQL.Add(SQL);
  TimeRegistrations.Open;
end;

procedure TTimeOverviewManagementFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' PROJECTNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' ACTIVITY LIKE ''%' + EditSearch.Text + '%''' + ' OR ' + ' DESCRIPTION LIKE ''%' +
              EditSearch.Text + '%''';
  TimeRegistrations.Filter := Filter;
end;

procedure TTimeOverviewManagementFrm.Start_module;
begin
  UsersWork.Close;
  UsersWork.SQL.Clear;
  UsersWork.SQL.Add('Select id, name || '' '' || firstname as fullname from users where companyId=' + IntToStr(UniMainModule.Company_id) +
                    ' and removed=''0'' order by name, firstname');
  UsersWork.Open;
  UsersWork.First;
  ComboUsers.Items.Clear;
  while not UsersWork.Eof
  do begin
       ComboUsers.Items.Add(UsersWork.FieldByName('Fullname').asString);
       UsersWork.Next;
     end;
  EditSearch.SetFocus;
end;

procedure TTimeOverviewManagementFrm.UniFrameDestroy(Sender: TObject);
begin
  UsersWork.Close;
end;

end.
