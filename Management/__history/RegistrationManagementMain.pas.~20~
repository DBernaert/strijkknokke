unit RegistrationManagementMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, uniButton, UniThemeButton, uniEdit, uniImage, uniLabel, uniGUIBaseClasses, uniPanel,
  Data.DB, MemDS, DBAccess, IBC, uniBasicGrid, uniDBGrid, uniMultiItem, uniComboBox;

type
  TRegistrationManagementMainFrm = class(TUniFrame)
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageOffers: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerButtonsRegistrationManagement: TUniContainerPanel;
    BtnComments: TUniThemeButton;
    ContainerGrid: TUniContainerPanel;
    GridProjects: TUniDBGrid;
    Projects: TIBCQuery;
    ProjectsID: TLargeintField;
    ProjectsNAME: TWideStringField;
    ProjectsTOTALAMOUNT: TFloatField;
    ProjectsTOTALINVOICED: TFloatField;
    ProjectsAmountOpen: TCurrencyField;
    DsProjects: TDataSource;
    TimeRegistrations: TIBCQuery;
    TimeRegistrationsID: TLargeintField;
    TimeRegistrationsREGISTRATIONDATE: TDateField;
    TimeRegistrationsACTIVITY: TWideStringField;
    TimeRegistrationsDESCRIPTION: TWideStringField;
    TimeRegistrationsSTARTTIMEVALUE: TFloatField;
    TimeRegistrationsENDTIMEVALUE: TFloatField;
    TimeRegistrationsSALDOVALUE: TFloatField;
    TimeRegistrationsHOURLYRATE: TFloatField;
    TimeRegistrationsAmount: TCurrencyField;
    TimeRegistrationsPROJECT: TLargeintField;
    TimeRegistrationsINITIALS: TWideStringField;
    DsTimeRegistrations: TDataSource;
    ComboStatus: TUniComboBox;
    BtnSearch: TUniThemeButton;
    ContainerFooter: TUniContainerPanel;
    BtnInvoice: TUniThemeButton;
    BtnHourlyRate: TUniThemeButton;
    ContainerRight: TUniContainerPanel;
    GridTimeRegistrations: TUniDBGrid;
    GridLines: TUniDBGrid;
    InvoiceLines: TIBCQuery;
    DsInvoiceLines: TDataSource;
    procedure ProjectsCalcFields(DataSet: TDataSet);
    procedure TimeRegistrationsCalcFields(DataSet: TDataSet);
    procedure EditSearchChange(Sender: TObject);
    procedure GridTimeRegistrationsColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
    procedure GridTimeRegistrationsColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
      Attribs: TUniCellAttribs; var Result: string);
    procedure BtnSearchClick(Sender: TObject);
    procedure BtnCommentsClick(Sender: TObject);
    procedure BtnHourlyRateClick(Sender: TObject);
    procedure BtnInvoiceClick(Sender: TObject);
  private
    { Private declarations }
    procedure Open_registrations;
    procedure CallBackModifyHourlyRate(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, TimeRegistrationsComments, ModifyHourRateUser, InvoicesAdd;



{ TRegistrationManagementMainFrm }

procedure TRegistrationManagementMainFrm.BtnCommentsClick(Sender: TObject);
begin
  if (not TimeRegistrations.Active) or (TimeRegistrations.FieldByName('Id').IsNull)
  then exit;

  with TimeRegistrationCommentsFrm do
  begin
    Start_module(TimeRegistrations.FieldByName('Id').AsInteger);
    ShowModal();
  end;
end;

procedure TRegistrationManagementMainFrm.BtnHourlyRateClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  With ModifyHourRateUserFrm do
  begin
    Start_module;
    ShowModal(CallBackModifyHourlyRate);
  end;
end;

procedure TRegistrationManagementMainFrm.BtnInvoiceClick(Sender: TObject);
begin
  if (Projects.Active = false) or (Projects.FieldByName('Id').IsNull)
  then exit;

  With InvoicesAddFrm do
  begin
    Start_create_invoice(Projects.FieldByName('Id').AsInteger);
    ShowModal;
  end;
end;

procedure TRegistrationManagementMainFrm.BtnSearchClick(Sender: TObject);
begin
 Open_registrations;
end;

procedure TRegistrationManagementMainFrm.CallBackModifyHourlyRate(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if Projects.Active then
    begin
      Projects.Refresh;
      TimeRegistrations.Refresh;
    end;
  end;
end;

procedure TRegistrationManagementMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' NAME LIKE ''%' + EditSearch.Text + '%''';
  Projects.Filter := Filter;
end;

procedure TRegistrationManagementMainFrm.GridTimeRegistrationsColumnSummary(Column: TUniDBGridColumn;
  GroupFieldValue: Variant);
begin
  if SameText(UpperCase(Column.FieldName), 'SALDOVALUE') or
    SameText(UpperCase(Column.FieldName), 'AMOUNT') then
  begin
    if Column.AuxValue = NULL then
      Column.AuxValue := 0.0;
    Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
  end;
end;

procedure TRegistrationManagementMainFrm.GridTimeRegistrationsColumnSummaryResult(Column: TUniDBGridColumn;
  GroupFieldValue: Variant; Attribs: TUniCellAttribs; var Result: string);
var F: currency;
begin
  if SameText(UpperCase(Column.FieldName), 'SALDOVALUE') or
    SameText(UpperCase(Column.FieldName), 'AMOUNT') then
  begin
    F := Column.AuxValue;
    if UpperCase(Column.FieldName) = 'SALDOVALUE' then
      Result := FormatCurr('#,##0.00', F)
    else if UpperCase(Column.FieldName) = 'AMOUNT' then
      Result := FormatCurr('#,##0.00 �', F);
    Attribs.Color := $00855000;
    Attribs.Font.Color := ClWhite;
    Attribs.Font.Style := [fsBold];
  end;
  Column.AuxValue := NULL;
end;

procedure TRegistrationManagementMainFrm.Open_registrations;
var SQL: string;
begin
  Projects.Close;
  Projects.SQL.Clear;
  SQL := 'select p.ID, p.name, ' +
         '(select sum(saldovalue * hourlyrate) from timeregistrations t where t.PROJECT = p.id and t.REMOVED=''0'') as totalamount, ' +
         '(select sum(amount) from invoices i where i.PROJECT = p.id) as totalinvoiced ' +
         'from projects p ' +
         'where p.REMOVED=''0'' and p.VisibleInInvoicing=''1''';

  Case ComboStatus.Itemindex of
    0: SQL := SQL + ' AND p.STATUS=0';
    1: SQL := SQL + ' AND p.STATUS=1';
  end;

  SQL := SQL + ' ORDER BY p.NAME';
  Projects.SQL.Add(SQL);
  Projects.Open;

  TimeRegistrations.Close;
  TimeRegistrations.SQL.Clear;
  SQL := 'SELECT TIMEREGISTRATIONS.ID, TIMEREGISTRATIONS.PROJECT, TIMEREGISTRATIONS.REGISTRATIONDATE, '
    + 'TIMEREGISTRATIONS.STARTTIMEVALUE, TIMEREGISTRATIONS.ENDTIMEVALUE, TIMEREGISTRATIONS.SALDOVALUE, '
    + 'TIMEREGISTRATIONS.DESCRIPTION, ' +
    'BASICTABLES.DUTCH AS ACTIVITY, TIMEREGISTRATIONS.HOURLYRATE, USERS.INITIALS '
    + 'FROM ' + 'TIMEREGISTRATIONS ' +
    'LEFT OUTER JOIN BASICTABLES ON (TIMEREGISTRATIONS.ACTIVITY = BASICTABLES.ID) '
    + 'LEFT OUTER JOIN USERS ON (TIMEREGISTRATIONS.USERID = USERS.ID) ' +
    'WHERE TIMEREGISTRATIONS.REMOVED=''0'' ';

  SQL := SQL + 'ORDER BY REGISTRATIONDATE ASC, STARTTIMEVALUE ASC';
  TimeRegistrations.SQL.Add(SQL);
  TimeRegistrations.Open;

  InvoiceLines.Open;
end;

procedure TRegistrationManagementMainFrm.ProjectsCalcFields(DataSet: TDataSet);
begin
  Projects.FieldByName('AmountOpen').AsCurrency :=
  Projects.FieldByName('TotalAmount').AsCurrency - Projects.FieldByName('TotalInvoiced').AsCurrency;
end;

procedure TRegistrationManagementMainFrm.Start_module;
begin
  Open_registrations;
  EditSearch.SetFocus;
end;

procedure TRegistrationManagementMainFrm.TimeRegistrationsCalcFields(DataSet: TDataSet);
begin
  Try
    TimeRegistrations.FieldByName('Amount').AsCurrency :=
      TimeRegistrations.FieldByName('SaldoValue').AsCurrency *
      TimeRegistrations.FieldByName('HourlyRate').AsCurrency;
  except
  End;
end;

end.
