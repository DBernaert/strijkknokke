unit ModifyHourRateUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniEdit, uniDBEdit, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox,
  uniGUIBaseClasses, uniDateTimePicker, uniDBDateTimePicker, DBAccess, IBC, Data.DB, MemDS, VirtualTable, uniButton,
  UniThemeButton, uniPanel;

type
  TModifyHourRateUserFrm = class(TUniForm)
    Entry: TVirtualTable;
    EntryStartDate: TDateField;
    EntryEndDate: TDateField;
    EntryUserId: TLargeintField;
    EntryProjectId: TLargeintField;
    EntryActivityId: TLargeintField;
    EntryHourlyRate: TCurrencyField;
    DsEntry: TDataSource;
    Projects: TIBCQuery;
    DsProjects: TDataSource;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    Activities: TIBCQuery;
    DsActivities: TDataSource;
    EditBeginDate: TUniDBDateTimePicker;
    EditEndDate: TUniDBDateTimePicker;
    EditUser: TUniDBLookupComboBox;
    EditProject: TUniDBLookupComboBox;
    EditActivity: TUniDBLookupComboBox;
    EditHourlyRate: TUniDBFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Start_module;
  end;

function ModifyHourRateUserFrm: TModifyHourRateUserFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function ModifyHourRateUserFrm: TModifyHourRateUserFrm;
begin
  Result := TModifyHourRateUserFrm(UniMainModule.GetFormInstance(TModifyHourRateUserFrm));
end;

{ TModifyHourRateUserFrm }

procedure TModifyHourRateUserFrm.BtnCancelClick(Sender: TObject);
begin
  Entry.Cancel;
  Entry.Close;
  Close;
end;

procedure TModifyHourRateUserFrm.BtnSaveClick(Sender: TObject);
var SQL: string;
begin
  BtnSave.SetFocus;

  if Entry.FieldByName('StartDate').isNull then begin
    UniMainModule.show_warning('De begindatum is een verplichte ingave.');
    EditBeginDate.SetFocus;
    Exit;
  end;

  if Entry.FieldByName('EndDate').IsNull then begin
    UniMainModule.show_warning('De einddatum is een verplichte ingave.');
    EditEndDate.SetFocus;
    Exit;
  end;

  if Entry.FieldByName('UserId').IsNull then begin
    UniMainModule.show_warning('De medewerker is een verplichte ingave.');
    EditUser.SetFocus;
    Exit;
  end;

  if Entry.FieldByName('ProjectId').IsNull then begin
    UniMainModule.show_warning('Het project is een verplichte ingave.');
    EditProject.SetFocus;
    Exit;
  end;

  if Entry.FieldByName('HourlyRate').IsNull then begin
    UniMainModule.show_warning('Het uurtarief is een verplichte ingave.');
    EditHourlyRate.SetFocus;
    Exit;
  end;

  if Entry.FieldByName('EndDate').AsDateTime <= Entry.FieldByName('StartDate').AsDateTime then begin
    UniMainModule.show_warning('De einddatum moet groter zijn dan de startdatum.');
    EditEndDate.SetFocus;
    Exit;
  end;

  Try
    Entry.Post;
  Except
    Entry.Cancel;
    UniMainModule.show_error('Fout bij het opslaan van de gegevens.');
    Exit;
  End;

  Try
    UniMainModule.UTimeRegistrations.Close;
    UniMainModule.UTimeRegistrations.SQL.Clear;
    SQL := 'Select * from timeregistrations where companyId=' + IntToStr(UniMainModule.Company_id) +
           ' and removed=''0''';
    SQL := SQL + ' and project=' + Entry.FieldByName('ProjectId').AsString;
    SQL := SQL + ' and RegistrationDate >=' + QuotedStr(FormatDateTime('yyyy-mm-dd', Entry.FieldByName('StartDate').AsDateTime));
    SQL := SQL + ' and RegistrationDate <=' + QuotedStr(FormatDateTime('yyyy-mm-dd', Entry.FieldByName('EndDate').AsDateTime));
    SQL := SQL + ' and UserId=' + Entry.FieldByName('UserId').AsString;
    if not Entry.FieldByName('ActivityId').IsNull
    then SQL := SQL + ' and Activity=' + Entry.FieldByName('ActivityId').AsString;
    UniMainModule.UTimeRegistrations.SQL.Add(SQL);
    UniMainModule.UTimeRegistrations.Open;
    UniMainModule.UTimeRegistrations.First;
    while not UniMainModule.UTimeRegistrations.EOF do begin
      UniMainModule.UTimeRegistrations.Edit;
      UniMainModule.UTimeRegistrations.FieldByName('HourlyRate').AsCurrency := Entry.FieldByName('HourlyRate').AsCurrency;
      UniMainModule.UTimeRegistrations.Post;
      UniMainModule.UTimeRegistrations.Next;
    end;
    UniMainModule.UpdTrRegistrations.Commit;
    UniMainModule.Result_dbAction := 1;
    UniMainModule.UTimeRegistrations.Close;
  Except
    UniMainModule.UpdTrRegistrations.Rollback;
    UniMainModule.UTimeRegistrations.Close;
    UniMainModule.show_error('Fout bij het bijwerken van de gegevens.');
  End;
  Close;

end;

procedure TModifyHourRateUserFrm.Start_module;
begin
  Caption := 'Wijzigen uurtarief medewerker';
  Entry.Open;
  Entry.Append;

  Projects.Close;
  Projects.SQL.Clear;
  Projects.SQL.Add('Select id, name from projects where companyId=' + IntToStr(UniMainModule.Company_id) +
                   ' and removed=''0'' order by name');
  Projects.Open;

  Activities.Close;
  Activities.SQL.Clear;
  Activities.SQL.Add('Select id, dutch from basictables where tableid=2000 and CompanyId=' + IntToStr(UniMainModule.Company_id) +
                     ' and removed=''0'' order by dutch');
  Activities.Open;

  Users.Close;
  Users.SQL.Clear;
  Users.SQL.Add('Select id, name || '' '' || firstname as fullname from users where companyId=' + IntToStr(UniMainModule.Company_id) +
                ' and removed=''0'' order by fullname');
  Users.Open;
end;

procedure TModifyHourRateUserFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
