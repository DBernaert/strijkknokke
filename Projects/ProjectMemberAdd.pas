unit ProjectMemberAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, DBAccess, IBC, Data.DB, MemDS, uniButton, UniThemeButton, uniPanel, uniEdit, uniDBEdit,
  uniGUIBaseClasses, uniMultiItem, uniComboBox, uniDBComboBox, uniDBLookupComboBox;

type
  TProjectMemberAddFrm = class(TUniForm)
    UProjectMembers: TIBCQuery;
    DsUProjectMembers: TDataSource;
    UpdTrProjectMembers: TIBCTransaction;
    Users: TIBCQuery;
    DsUsers: TDataSource;
    EditUser: TUniDBLookupComboBox;
    EditHourlyRate: TUniDBFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure Open_reference_tables;
  public
    { Public declarations }
    procedure Init_projectmember_creation(project: longint);
    procedure Init_user_modification(projectmember: longint);
  end;

function ProjectMemberAddFrm: TProjectMemberAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main;

function ProjectMemberAddFrm: TProjectMemberAddFrm;
begin
  Result := TProjectMemberAddFrm(UniMainModule.GetFormInstance(TProjectMemberAddFrm));
end;

{ TProjectMemberAddFrm }

procedure TProjectMemberAddFrm.BtnCancelClick(Sender: TObject);
begin
  UProjectMembers.Cancel;
  UProjectMembers.Close;
  Close;
end;

procedure TProjectMemberAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if UProjectMembers.FieldByName('USERID').IsNull then begin
    UniMainModule.show_warning('De medewerker is een verplichte ingave.');
    EditUser.SetFocus;
    Exit;
  end;

  if UProjectMembers.FieldByName('HOURLYRATE').IsNull then begin
    UniMainModule.show_warning('Het uurtarief is een verplichte ingave.');
    EditHourlyRate.SetFocus;
    Exit;
  end;

  //Check for duplicates users in project
  if UProjectMembers.State = dsInsert then begin
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select count(*) as counter from projectmembers where UserId=' + UProjectMembers.FieldByName('UserId').AsString + ' ' +
                                'and Project=' + UProjectMembers.FieldByName('Project').AsString);
    UniMainModule.QWork.Open;
    if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0
    then begin
           UniMainModule.QWork.Close;
           UniMainModule.show_warning('Deze medewerker is reeds lid van dit project.');
           EditUser.SetFocus;
           Exit;
         end;
  end
  else begin
         UniMainModule.QWork.Close;
         UniMainModule.QWork.SQL.Clear;
         UniMainModule.QWork.SQL.Add('Select count(*) as counter from projectmembers where UserId=' + UProjectMembers.FieldByName('UserId').AsString +
                                     ' and Project=' + UProjectMembers.FieldByName('Project').AsString +
                                     ' and Id<>' + UProjectMembers.FieldByName('Id').asString);
         UniMainModule.QWork.Open;
         if UniMainModule.QWork.FieldByName('Counter').AsInteger > 0
         then begin
                UniMainModule.QWork.Close;
                UniMainModule.show_warning('Deze medewerker is reeds lid van dit project.');
                EditUser.SetFocus;
                Exit;
              end;
       end;

  try
    UProjectMembers.Post;
    UpdTrProjectMembers.Commit;
    UniMainModule.Result_dbAction := UProjectMembers.FieldByName('Id').AsInteger;
  except
    on E: Exception do begin
      UpdTrProjectMembers.Rollback;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens: ' + E.Message);
    end;
  end;
  UProjectMembers.Close;
  Close;
end;

procedure TProjectMemberAddFrm.Init_projectmember_creation(project: longint);
begin
  Caption := 'Toevoegen projectlid';
  Open_reference_tables;
  UProjectMembers.Close;
  UProjectMembers.SQL.Clear;
  UProjectMembers.SQL.Add('Select first 0 * from projectmembers');
  UProjectMembers.Open;
  UProjectMembers.Append;
  UProjectMembers.FieldByName('Project').AsInteger := Project;
end;

procedure TProjectMemberAddFrm.Init_user_modification(projectmember: longint);
begin
  Caption := 'Wijzigen projectlid';
  Open_reference_tables;
  UProjectMembers.Close;
  UProjectMembers.SQL.Clear;
  UProjectMembers.SQL.Add('Select * from projectmembers where id=' + IntToStr(ProjectMember));
  UProjectMembers.Open;
  Try
    UProjectMembers.Edit;
  Except
    UProjectMembers.Cancel;
    UProjectMembers.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Close;
    Exit;
  End;

end;

procedure TProjectMemberAddFrm.Open_reference_tables;
begin
  Users.Close;
  Users.SQL.Clear;
  Users.SQL.Add('Select id, name || '' '' || firstname as fullname from users ' +
                'where companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0'' ' +
                'order by fullname');
  Users.Open;
end;

procedure TProjectMemberAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
