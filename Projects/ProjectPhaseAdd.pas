unit ProjectPhaseAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniPanel, uniDBEdit, uniGUIBaseClasses, uniEdit, DBAccess, IBC,
  Data.DB, MemDS;

type
  TProjectPhaseAddFrm = class(TUniForm)
    UProjectPhases: TIBCQuery;
    DsUProjectPhases: TDataSource;
    UpdTrProjectPhases: TIBCTransaction;
    EditDescription: TUniDBEdit;
    EditPercentage: TUniDBFormattedNumberEdit;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Init_add_projectPhase(ProjectId: longint);
    procedure Init_modify_projectPhase(Id: longint);
  end;

function ProjectPhaseAddFrm: TProjectPhaseAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule;

function ProjectPhaseAddFrm: TProjectPhaseAddFrm;
begin
  Result := TProjectPhaseAddFrm(UniMainModule.GetFormInstance(TProjectPhaseAddFrm));
end;

{ TProjectPhaseAddFrm }

procedure TProjectPhaseAddFrm.BtnCancelClick(Sender: TObject);
begin
  UProjectPhases.Cancel;
  UProjectPhases.Close;
  Close;
end;

procedure TProjectPhaseAddFrm.BtnSaveClick(Sender: TObject);
begin
  BtnSave.SetFocus;

  if Trim(UProjectPhases.FieldByName('PHASEDESCRIPTION').AsString) = '' then
  begin
    UniMainModule.show_warning('De omschrijving is een verplichte ingave.');
    EditDescription.SetFocus;
    Exit;
  end;

  if UProjectPhases.FieldByName('PHASEPERCENTAGE').IsNull then
  begin
    UniMainModule.show_warning('Het percentage is een verplichte ingave.');
    EditPercentage.SetFocus;
    Exit;
  end;

  // Indien insert, ga de maximum sequence ophalen voor de volgorde
  if UProjectPhases.State = dsInsert then
  begin
    UniMainModule.QWork.Close;
    UniMainModule.QWork.SQL.Clear;
    UniMainModule.QWork.SQL.Add('Select max(PHASESEQUENCE) as maximum from projectphases where project=' + UProjectPhases.FieldByName('Project').asString);
    UniMainModule.QWork.Open;
    UProjectPhases.FieldByName('PHASESEQUENCE').AsInteger := UniMainModule.QWork.FieldByName('Maximum').AsInteger + 1;
    UniMainModule.QWork.Close;
  end;

  Try
    UProjectPhases.Post;
    UpdTrProjectPhases.Commit;
    UniMainModule.Result_dbAction := UProjectPhases.FieldByName('Id').AsInteger;
    UProjectPhases.Close;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UpdTrProjectPhases.Rollback;
      UProjectPhases.Close;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
    end;
  end;
  Close;
end;

procedure TProjectPhaseAddFrm.Init_add_projectPhase(ProjectId: longint);
begin
  Caption := 'Toevoegen fase';
  UProjectPhases.Close;
  UProjectPhases.SQL.Clear;
  UProjectPhases.SQL.Add('Select first 0 * from projectphases');
  UProjectPhases.Open;
  UProjectPhases.Append;
  UProjectPhases.FieldByName('CompanyId').AsInteger             := UniMainModule.Company_id;
  UProjectPhases.FieldByName('Project').AsInteger               := ProjectId;
  UProjectPhases.FieldByName('Phasepercentage').AsCurrency      := 0;
  UProjectPhases.FieldByName('PhaseSequence').AsInteger         := 0;
end;

procedure TProjectPhaseAddFrm.Init_modify_projectPhase(Id: longint);
begin
  Caption := 'Wijzigen fase';
  UProjectPhases.Close;
  UProjectPhases.SQL.Clear;
  UProjectPhases.SQL.Add('Select * from projectphases where id=' + IntToStr(Id));
  UProjectPhases.Open;
  Try
    UProjectPhases.Edit;
  Except
    UProjectPhases.Cancel;
    UProjectPhases.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Close;
    Exit;
  End;
end;

procedure TProjectPhaseAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
