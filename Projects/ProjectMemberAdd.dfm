object ProjectMemberAddFrm: TProjectMemberAddFrm
  Left = 0
  Top = 0
  ClientHeight = 129
  ClientWidth = 465
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = EditUser
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object EditUser: TUniDBLookupComboBox
    Left = 16
    Top = 16
    Width = 433
    Hint = ''
    ListField = 'FULLNAME'
    ListSource = DsUsers
    KeyField = 'ID'
    ListFieldIndex = 0
    DataField = 'USERID'
    DataSource = DsUProjectMembers
    TabOrder = 0
    Color = clWindow
    FieldLabel = 'Medewerker'
    FieldLabelWidth = 150
  end
  object EditHourlyRate: TUniDBFormattedNumberEdit
    Left = 16
    Top = 48
    Width = 433
    Height = 22
    Hint = ''
    FormattedInput.ShowCurrencySign = True
    FormattedInput.CurrencySignPos = cpsRight
    FormattedInput.DefaultCurrencySign = False
    FormattedInput.CurrencySign = #8364
    DataField = 'HOURLYRATE'
    DataSource = DsUProjectMembers
    TabOrder = 1
    SelectOnFocus = True
    FieldLabel = 'Uurtarief'
    FieldLabelWidth = 150
    DecimalSeparator = ','
    ThousandSeparator = '.'
  end
  object BtnSave: TUniThemeButton
    Left = 232
    Top = 88
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 2
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 344
    Top = 88
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 3
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object UProjectMembers: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_PROJECTMEMBERS_ID'
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO PROJECTMEMBERS'
      '  (ID, PROJECT, USERID, HOURLYRATE)'
      'VALUES'
      '  (:ID, :PROJECT, :USERID, :HOURLYRATE)'
      'RETURNING '
      '  ID, PROJECT, USERID, HOURLYRATE')
    SQLDelete.Strings = (
      'DELETE FROM PROJECTMEMBERS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE PROJECTMEMBERS'
      'SET'
      
        '  ID = :ID, PROJECT = :PROJECT, USERID = :USERID, HOURLYRATE = :' +
        'HOURLYRATE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID, PROJECT, USERID, HOURLYRATE FROM PROJECTMEMBERS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM PROJECTMEMBERS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM PROJECTMEMBERS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTrProjectMembers
    SQL.Strings = (
      'Select * from projectmembers')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 200
    Top = 64
  end
  object DsUProjectMembers: TDataSource
    DataSet = UProjectMembers
    Left = 200
    Top = 112
  end
  object UpdTrProjectMembers: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 200
    Top = 160
  end
  object Users: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO USERS'
      
        '  (ID, COMPANYID, NAME, FIRSTNAME, EMAIL, PASSWORD, REMOVED, LAN' +
        'GUAGE, PHONE, MOBILE, PHOTO, HOURLYRATE)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :NAME, :FIRSTNAME, :EMAIL, :PASSWORD, :REMOV' +
        'ED, :LANGUAGE, :PHONE, :MOBILE, :PHOTO, :HOURLYRATE)'
      'RETURNING '
      
        '  ID, COMPANYID, NAME, FIRSTNAME, EMAIL, PASSWORD, REMOVED, LANG' +
        'UAGE, PHONE, MOBILE, HOURLYRATE')
    SQLDelete.Strings = (
      'DELETE FROM USERS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE USERS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, NAME = :NAME, FIRSTNAME = :F' +
        'IRSTNAME, EMAIL = :EMAIL, PASSWORD = :PASSWORD, REMOVED = :REMOV' +
        'ED, LANGUAGE = :LANGUAGE, PHONE = :PHONE, MOBILE = :MOBILE, PHOT' +
        'O = :PHOTO, HOURLYRATE = :HOURLYRATE'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, NAME, FIRSTNAME, EMAIL, PASSWORD, REMOVED,' +
        ' LANGUAGE, PHONE, MOBILE, PHOTO, HOURLYRATE FROM USERS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM USERS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM USERS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select id, name || '#39' '#39' || firstname as fullname from users')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 360
    Top = 64
  end
  object DsUsers: TDataSource
    DataSet = Users
    Left = 360
    Top = 120
  end
end
