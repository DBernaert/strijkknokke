object ProjectDetailsFrm: TProjectDetailsFrm
  Left = 0
  Top = 0
  ClientHeight = 464
  ClientWidth = 853
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = GridProjectPurchases
  Layout = 'border'
  OnCancel = BtnCloseClick
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridProjectPurchases: TUniDBGrid
    Left = 0
    Top = 0
    Width = 853
    Height = 439
    Hint = ''
    DataSource = DsProjectPurchases
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
    WebOptions.Paged = False
    LoadMask.Enabled = False
    LoadMask.ShowMessage = False
    LoadMask.Message = 'Loading data...'
    ForceFit = True
    LayoutConfig.Region = 'center'
    LayoutConfig.Margin = '8 8 8 8'
    Align = alClient
    TabOrder = 0
    OnDblClick = BtnModifyClick
    OnFieldImage = GridProjectPurchasesFieldImage
    Columns = <
      item
        FieldName = 'INVOICED'
        Title.Alignment = taCenter
        Title.Caption = 'GEFACT.'
        Width = 75
        Alignment = taCenter
        ImageOptions.Visible = True
        ImageOptions.Width = 12
        ImageOptions.Height = 12
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'DATEPURCHASE'
        Title.Caption = 'DATUM'
        Width = 95
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        Flex = 1
        FieldName = 'DESCRIPTION'
        Title.Caption = 'OMSCHRIJVING'
        Width = 100
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'QUANTITY'
        Title.Alignment = taRightJustify
        Title.Caption = 'AANTAL'
        Width = 85
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'SALESPRICE'
        Title.Alignment = taRightJustify
        Title.Caption = 'PRIJS'
        Width = 110
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end
      item
        FieldName = 'TOTALSALESPRICE'
        Title.Alignment = taRightJustify
        Title.Caption = 'TOTAAL'
        Width = 110
        Menu.MenuEnabled = False
        Menu.ColumnHideable = False
      end>
  end
  object ContainerFooter: TUniContainerPanel
    Left = 0
    Top = 439
    Width = 853
    Height = 25
    Hint = ''
    ParentColor = False
    Align = alBottom
    TabOrder = 1
    Layout = 'border'
    LayoutConfig.Region = 'south'
    LayoutConfig.Margin = '0 0 8 0'
    object ContainerFooterLeft: TUniContainerPanel
      Left = 0
      Top = 0
      Width = 337
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alLeft
      TabOrder = 1
      LayoutConfig.Region = 'west'
      LayoutConfig.Margin = '0 0 0 8'
      object BtnAdd: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Toevoegen'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 10
        OnClick = BtnAddClick
        ButtonTheme = uctDefault
      end
      object BtnModify: TUniThemeButton
        Left = 112
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Wijzigen'
        ParentFont = False
        TabOrder = 2
        Images = UniMainModule.ImageList
        ImageIndex = 11
        OnClick = BtnModifyClick
        ButtonTheme = uctDefault
      end
      object BtnDelete: TUniThemeButton
        Left = 224
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Verwijderen'
        ParentFont = False
        TabOrder = 3
        Images = UniMainModule.ImageList
        ImageIndex = 12
        OnClick = BtnDeleteClick
        ButtonTheme = uctDefault
      end
    end
    object ContainerFooterRight: TUniContainerPanel
      Left = 748
      Top = 0
      Width = 105
      Height = 25
      Hint = ''
      ParentColor = False
      Align = alRight
      TabOrder = 2
      LayoutConfig.Region = 'east'
      LayoutConfig.Margin = '0 8 0 0'
      object BtnClose: TUniThemeButton
        Left = 0
        Top = 0
        Width = 105
        Height = 25
        Hint = ''
        Caption = 'Sluiten'
        ParentFont = False
        TabOrder = 1
        Images = UniMainModule.ImageList
        ImageIndex = 9
        OnClick = BtnCloseClick
        ButtonTheme = uctSecondary
      end
    end
  end
  object ProjectPurchases: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO PROJECTS'
      
        '  (ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDAT' +
        'E, ENDDATE, REMARKS, RELATION, REMOVED)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :STATUS, :NAME, :ADDRESS, :ZIPCODE, :CITY, :' +
        'STARTDATE, :ENDDATE, :REMARKS, :RELATION, :REMOVED)'
      'RETURNING '
      
        '  ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDATE' +
        ', ENDDATE, RELATION, REMOVED')
    SQLDelete.Strings = (
      'DELETE FROM PROJECTS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE PROJECTS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, STATUS = :STATUS, NAME = :NA' +
        'ME, ADDRESS = :ADDRESS, ZIPCODE = :ZIPCODE, CITY = :CITY, STARTD' +
        'ATE = :STARTDATE, ENDDATE = :ENDDATE, REMARKS = :REMARKS, RELATI' +
        'ON = :RELATION, REMOVED = :REMOVED'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STAR' +
        'TDATE, ENDDATE, REMARKS, RELATION, REMOVED FROM PROJECTS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM PROJECTS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM PROJECTS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from projectpurchases')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 360
    Top = 88
    object ProjectPurchasesID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object ProjectPurchasesPROJECT: TLargeintField
      FieldName = 'PROJECT'
      Required = True
    end
    object ProjectPurchasesDATEPURCHASE: TDateField
      FieldName = 'DATEPURCHASE'
      Required = True
    end
    object ProjectPurchasesDESCRIPTION: TWideStringField
      FieldName = 'DESCRIPTION'
      Required = True
      Size = 100
    end
    object ProjectPurchasesQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Required = True
    end
    object ProjectPurchasesPRICE: TFloatField
      FieldName = 'PRICE'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ProjectPurchasesTOTALPRICE: TFloatField
      FieldName = 'TOTALPRICE'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ProjectPurchasesINVOICED: TIntegerField
      FieldName = 'INVOICED'
      Required = True
    end
    object ProjectPurchasesSALESPRICE: TFloatField
      FieldName = 'SALESPRICE'
      DisplayFormat = '#,##0.00 '#8364
    end
    object ProjectPurchasesTOTALSALESPRICE: TFloatField
      FieldName = 'TOTALSALESPRICE'
      DisplayFormat = '#,##0.00 '#8364
    end
  end
  object DsProjectPurchases: TDataSource
    DataSet = ProjectPurchases
    Left = 360
    Top = 144
  end
  object PopupMenuProjectPurchases: TUniPopupMenu
    Images = UniMainModule.ImageList
    Left = 355
    Top = 200
    object BtnDeletePurchase: TUniMenuItem
      Caption = 'Verwijderen aankoop'
      ImageIndex = 12
    end
  end
end
