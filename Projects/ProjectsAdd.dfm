object ProjectsAddFrm: TProjectsAddFrm
  Left = 0
  Top = 0
  ClientHeight = 473
  ClientWidth = 985
  Caption = ''
  BorderStyle = bsDialog
  OldCreateOrder = False
  BorderIcons = []
  NavigateKeys.Enabled = True
  NavigateKeys.KeySubmit.Key = 13
  NavigateKeys.KeyCancel.Key = 27
  MonitoredKeys.Keys = <>
  AlignmentControl = uniAlignmentClient
  ActiveControl = ComboStatus
  OnSubmit = BtnSaveClick
  OnCancel = BtnCancelClick
  OnReady = UniFormReady
  OnCreate = UniFormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControlProjects: TUniPageControl
    Left = 16
    Top = 16
    Width = 953
    Height = 401
    Hint = ''
    ActivePage = TabSheetGeneral
    Images = UniMainModule.ImageList
    TabOrder = 0
    object TabSheetGeneral: TUniTabSheet
      Hint = ''
      ImageIndex = 17
      Caption = 'Algemene informatie'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 289
      ExplicitHeight = 193
      object ComboStatus: TUniComboBox
        Left = 8
        Top = 8
        Width = 449
        Hint = ''
        Text = 'ComboStatus'
        Items.Strings = (
          'Actief'
          'Afgesloten')
        TabOrder = 0
        ForceSelection = True
        FieldLabel = 'Status project <font color="#cd0015">*</font>'
        FieldLabelWidth = 150
        IconItems = <>
      end
      object EditName: TUniDBEdit
        Left = 8
        Top = 72
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'NAME'
        DataSource = DsUProjects
        TabOrder = 2
        FieldLabel = 'Naam project <font color="#cd0015">*</font>'
        FieldLabelWidth = 150
      end
      object EditAddress: TUniDBEdit
        Left = 8
        Top = 104
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'ADDRESS'
        DataSource = DsUProjects
        TabOrder = 3
        FieldLabel = 'Straat + nr. werf'
        FieldLabelWidth = 150
      end
      object EditZipCode: TUniDBEdit
        Left = 8
        Top = 136
        Width = 273
        Height = 22
        Hint = ''
        DataField = 'ZIPCODE'
        DataSource = DsUProjects
        TabOrder = 4
        FieldLabel = 'Postcode werf'
        FieldLabelWidth = 150
        OnExit = EditZipCodeExit
      end
      object EditCity: TUniDBEdit
        Left = 8
        Top = 168
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'CITY'
        DataSource = DsUProjects
        TabOrder = 5
        FieldLabel = 'Gemeente werf'
        FieldLabelWidth = 150
        OnExit = EditCityExit
      end
      object EditRelation: TUniDBLookupComboBox
        Left = 8
        Top = 40
        Width = 449
        Hint = ''
        ListField = 'DISPLAYNAME'
        ListSource = DsRelations
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'RELATION'
        DataSource = DsUProjects
        TabOrder = 1
        Color = clWindow
        FieldLabel = 'Klant <font color="#cd0015">*</font>'
        FieldLabelWidth = 150
        ForceSelection = True
        Style = csDropDown
      end
      object EditStartDate: TUniDBDateTimePicker
        Left = 8
        Top = 200
        Width = 449
        Hint = ''
        DataField = 'STARTDATE'
        DataSource = DsUProjects
        DateTime = 43489.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 6
        FieldLabel = 'Startdatum project'
        FieldLabelWidth = 150
      end
      object EditEndDate: TUniDBDateTimePicker
        Left = 8
        Top = 232
        Width = 449
        Hint = ''
        DataField = 'ENDDATE'
        DataSource = DsUProjects
        DateTime = 43489.000000000000000000
        DateFormat = 'dd/MM/yyyy'
        TimeFormat = 'HH:mm:ss'
        TabOrder = 7
        FieldLabel = 'Einddatum project'
        FieldLabelWidth = 150
      end
      object EditRemarks: TUniDBMemo
        Left = 472
        Top = 8
        Width = 473
        Height = 353
        Hint = ''
        DataField = 'REMARKS'
        DataSource = DsUProjects
        TabOrder = 9
        FieldLabel = 'Opmerkingen'
        FieldLabelAlign = laTop
      end
      object CheckBoxVisibleInvoicing: TUniDBCheckBox
        Left = 8
        Top = 264
        Width = 449
        Height = 17
        Hint = ''
        DataField = 'VISIBLEININVOICING'
        DataSource = DsUProjects
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = ''
        TabOrder = 8
        ParentColor = False
        Color = clBtnFace
        FieldLabel = 'Zichtbaar in de facturatie'
        FieldLabelWidth = 150
      end
    end
    object TabSheetRegie: TUniTabSheet
      Hint = ''
      ImageIndex = 19
      Caption = 'Parameters ereloon'
      Layout = 'border'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object ContainerProcentual: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 945
        Height = 89
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 0
        LayoutConfig.Region = 'north'
        object EditBudget: TUniDBFormattedNumberEdit
          Left = 480
          Top = 8
          Width = 225
          Height = 22
          Hint = ''
          FormattedInput.ShowCurrencySign = True
          FormattedInput.CurrencySignPos = cpsRight
          FormattedInput.DefaultCurrencySign = False
          FormattedInput.CurrencySign = #8364
          DataField = 'BUDGET'
          DataSource = DsUProjects
          TabOrder = 4
          SelectOnFocus = True
          FieldLabel = 'Budget'
          FieldLabelWidth = 120
          DecimalSeparator = ','
          ThousandSeparator = '.'
        end
        object EditEstimation: TUniDBFormattedNumberEdit
          Left = 480
          Top = 40
          Width = 225
          Height = 22
          Hint = ''
          FormattedInput.ShowCurrencySign = True
          FormattedInput.CurrencySignPos = cpsRight
          FormattedInput.DefaultCurrencySign = False
          FormattedInput.CurrencySign = #8364
          DataField = 'ESTIMATION'
          DataSource = DsUProjects
          TabOrder = 5
          SelectOnFocus = True
          FieldLabel = 'Raming'
          FieldLabelWidth = 120
          DecimalSeparator = ','
          ThousandSeparator = '.'
        end
        object EditTotalBudget: TUniDBFormattedNumberEdit
          Left = 720
          Top = 8
          Width = 225
          Height = 22
          Hint = ''
          FormattedInput.ShowCurrencySign = True
          FormattedInput.CurrencySignPos = cpsRight
          FormattedInput.DefaultCurrencySign = False
          FormattedInput.CurrencySign = #8364
          DataField = 'TOTALBUDGET'
          DataSource = DsUProjects
          TabOrder = 6
          SelectOnFocus = True
          FieldLabel = 'Totaal budget'
          FieldLabelWidth = 120
          DecimalSeparator = ','
          ThousandSeparator = '.'
        end
        object ComboProjectType: TUniComboBox
          Left = 8
          Top = 8
          Width = 449
          Hint = ''
          Text = 'ComboProjectType'
          Items.Strings = (
            'Regie'
            'Procentueel')
          TabOrder = 1
          ForceSelection = True
          FieldLabel = 'Type project <font color="#cd0015">*</font>'
          FieldLabelWidth = 150
          IconItems = <>
        end
        object EditPaymentTerm: TUniDBFormattedNumberEdit
          Left = 8
          Top = 35
          Width = 449
          Height = 22
          Hint = ''
          DataField = 'PAYMENTTERM'
          DataSource = DsUProjects
          TabOrder = 2
          FieldLabel = 'Betalingstermijn <font color="#cd0015">*</font>'
          FieldLabelWidth = 150
          DecimalPrecision = 0
          DecimalSeparator = ','
          ThousandSeparator = '.'
        end
        object CheckBoxSendDetailledReport: TUniDBCheckBox
          Left = 8
          Top = 64
          Width = 449
          Height = 17
          Hint = ''
          DataField = 'SENDDETAILLEDREPORT'
          DataSource = DsUProjects
          ValueChecked = '1'
          ValueUnchecked = '0'
          Caption = ''
          TabOrder = 3
          ParentColor = False
          Color = clBtnFace
          FieldLabel = 'Detail meersturen'
          FieldLabelWidth = 150
        end
        object EditPercentageFee: TUniDBFormattedNumberEdit
          Left = 720
          Top = 40
          Width = 225
          Height = 22
          Hint = ''
          FormattedInput.ShowCurrencySign = True
          FormattedInput.CurrencySignPos = cpsRight
          FormattedInput.DefaultCurrencySign = False
          FormattedInput.CurrencySign = '%'
          DataField = 'PERCENTAGEFEE'
          DataSource = DsUProjects
          TabOrder = 7
          SelectOnFocus = True
          FieldLabel = 'Hoofdpercentage'
          FieldLabelWidth = 120
          DecimalSeparator = ','
          ThousandSeparator = '.'
        end
      end
      object ContainerGridProcentual: TUniContainerPanel
        Left = 0
        Top = 89
        Width = 945
        Height = 41
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 1
        Layout = 'border'
        LayoutConfig.Region = 'north'
        object UniContainerPanel1: TUniContainerPanel
          Left = 552
          Top = 0
          Width = 393
          Height = 41
          Hint = ''
          ParentColor = False
          Align = alRight
          TabOrder = 1
          LayoutConfig.Region = 'east'
          LayoutConfig.Margin = '0 8 0 0'
          object BtnModifyPhase: TUniThemeButton
            Left = 112
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Wijzigen'
            ParentFont = False
            TabStop = False
            TabOrder = 1
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 11
            OnClick = BtnModifyPhaseClick
            ButtonTheme = uctDefault
          end
          object BtnAddPhase: TUniThemeButton
            Left = 0
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Toevoegen'
            ParentFont = False
            TabStop = False
            TabOrder = 2
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 10
            OnClick = BtnAddPhaseClick
            ButtonTheme = uctDefault
          end
          object BtnMovePhaseUp: TUniThemeButton
            Left = 336
            Top = 8
            Width = 25
            Height = 25
            Hint = 'Verplaatsen naar boven'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            ParentFont = False
            TabStop = False
            TabOrder = 3
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 32
            OnClick = BtnMovePhaseUpClick
            ButtonTheme = uctDefault
          end
          object BtnMovePhaseDown: TUniThemeButton
            Left = 368
            Top = 8
            Width = 25
            Height = 25
            Hint = 'Verplaatsen naar beneden'
            ShowHint = True
            ParentShowHint = False
            Caption = ''
            ParentFont = False
            TabStop = False
            TabOrder = 4
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 33
            OnClick = BtnMovePhaseDownClick
            ButtonTheme = uctDefault
          end
          object BtnDeletePhase: TUniThemeButton
            Left = 224
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Verwijderen'
            ParentFont = False
            TabStop = False
            TabOrder = 5
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 12
            OnClick = BtnDeletePhaseClick
            ButtonTheme = uctDefault
          end
        end
      end
      object GridProjectPhases: TUniDBGrid
        Left = 0
        Top = 130
        Width = 945
        Height = 243
        Hint = ''
        DataSource = DsProjectPhases
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
        WebOptions.Paged = False
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        LayoutConfig.Region = 'center'
        LayoutConfig.Margin = '4 8 8 8'
        Align = alClient
        TabOrder = 2
        ParentColor = False
        Color = clBtnFace
        Summary.Align = taBottom
        Summary.Enabled = True
        OnDblClick = BtnModifyPhaseClick
        OnColumnSummary = GridProjectPhasesColumnSummary
        OnColumnSummaryResult = GridProjectPhasesColumnSummaryResult
        Columns = <
          item
            Flex = 1
            FieldName = 'PHASEDESCRIPTION'
            Title.Caption = 'FASE'
            Width = 604
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'PHASEPERCENTAGE'
            Title.Alignment = taRightJustify
            Title.Caption = 'PERCENTAGE'
            Width = 120
            ShowSummary = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
    end
    object TabSheetProjectMembers: TUniTabSheet
      Hint = ''
      ImageIndex = 6
      Caption = 'Projectleden'
      Layout = 'border'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 256
      ExplicitHeight = 128
      object ContainerHeaderMembers: TUniContainerPanel
        Left = 0
        Top = 0
        Width = 945
        Height = 41
        Hint = ''
        ParentColor = False
        Align = alTop
        TabOrder = 0
        Layout = 'border'
        LayoutConfig.Region = 'north'
        object UniContainerPanel5: TUniContainerPanel
          Left = 504
          Top = 0
          Width = 441
          Height = 41
          Hint = ''
          ParentColor = False
          Align = alRight
          TabOrder = 1
          LayoutConfig.Region = 'east'
          LayoutConfig.Margin = '0 8 0 0'
          object BtnAddProjectMember: TUniThemeButton
            Left = 112
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Toevoegen'
            ParentFont = False
            TabStop = False
            TabOrder = 1
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 10
            OnClick = BtnAddProjectMemberClick
            ButtonTheme = uctDefault
          end
          object BtnModifyProjectMember: TUniThemeButton
            Left = 224
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Wijzigen'
            ParentFont = False
            TabStop = False
            TabOrder = 2
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 11
            OnClick = BtnModifyProjectMemberClick
            ButtonTheme = uctDefault
          end
          object BtnAddAllUsers: TUniThemeButton
            Left = 0
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Medewerkers'
            ParentFont = False
            TabStop = False
            TabOrder = 3
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 10
            OnClick = BtnAddAllUsersClick
            ButtonTheme = uctDefault
          end
          object BtnDeleteProjectMember: TUniThemeButton
            Left = 336
            Top = 8
            Width = 105
            Height = 25
            Hint = ''
            Caption = 'Verwijderen'
            ParentFont = False
            TabStop = False
            TabOrder = 4
            ScreenMask.Target = Owner
            Images = UniMainModule.ImageList
            ImageIndex = 12
            OnClick = BtnDeleteProjectMemberClick
            ButtonTheme = uctDefault
          end
        end
      end
      object GridProjectMembers: TUniDBGrid
        Left = 0
        Top = 41
        Width = 945
        Height = 332
        Hint = ''
        DataSource = DsProjectMembers
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgAutoRefreshRow]
        WebOptions.Paged = False
        LoadMask.Enabled = False
        LoadMask.Message = 'Loading data...'
        ForceFit = True
        LayoutConfig.Region = 'center'
        LayoutConfig.Margin = '4 8 8 8'
        Align = alClient
        TabOrder = 1
        ParentColor = False
        Color = clBtnFace
        OnDblClick = BtnModifyProjectMemberClick
        Columns = <
          item
            Flex = 1
            FieldName = 'USERFULLNAME'
            Title.Caption = 'MEDEWERKER'
            Width = 610
            ReadOnly = True
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end
          item
            FieldName = 'HOURLYRATE'
            Title.Alignment = taRightJustify
            Title.Caption = 'UURTARIEF'
            Width = 140
            Menu.MenuEnabled = False
            Menu.ColumnHideable = False
          end>
      end
    end
    object TabSheetInvoicing: TUniTabSheet
      Hint = ''
      ImageIndex = 3
      Caption = 'Facturatie'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 968
      ExplicitHeight = 385
      object ComboRelationTypeCustom1: TUniComboBox
        Left = 8
        Top = 40
        Width = 449
        Hint = ''
        Text = 'ComboRelationTypeCustom1'
        Items.Strings = (
          'Bedrijf'
          'Particulier')
        TabOrder = 1
        ForceSelection = True
        FieldLabel = 'Type <font color="#cd0015">*</font>'
        FieldLabelWidth = 150
        IconItems = <>
      end
      object EditCompanyNameCustom1: TUniDBEdit
        Left = 8
        Top = 72
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'COMPANYNAMECUSTOM1'
        DataSource = DsUProjects
        TabOrder = 2
        FieldLabel = 'Bedrijfsnaam'
        FieldLabelWidth = 150
      end
      object EditAppelationCustom1: TUniDBLookupComboBox
        Left = 8
        Top = 104
        Width = 449
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsAppelations
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'APPELATIONCUSTOM1'
        DataSource = DsUProjects
        TabOrder = 3
        Color = clWindow
        FieldLabel = 'Aanspreektitel'
        FieldLabelWidth = 150
        ForceSelection = True
        Style = csDropDown
      end
      object EditNameCustom1: TUniDBEdit
        Left = 8
        Top = 136
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'NAMECUSTOM1'
        DataSource = DsUProjects
        TabOrder = 4
        FieldLabel = 'Naam'
        FieldLabelWidth = 150
      end
      object EditFirstNameCustom1: TUniDBEdit
        Left = 8
        Top = 168
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'FIRSTNAMECUSTOM1'
        DataSource = DsUProjects
        TabOrder = 5
        FieldLabel = 'Voornaam'
        FieldLabelWidth = 150
      end
      object EditAddresCustom1: TUniDBEdit
        Left = 8
        Top = 200
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'ADDRESSCUSTOM1'
        DataSource = DsUProjects
        TabOrder = 6
        FieldLabel = 'Adres'
        FieldLabelWidth = 150
      end
      object EditHousenumberCustom1: TUniDBEdit
        Left = 8
        Top = 232
        Width = 241
        Height = 22
        Hint = ''
        DataField = 'HOUSENUMBERCUSTOM1'
        DataSource = DsUProjects
        TabOrder = 7
        FieldLabel = 'Huisnummer'
        FieldLabelWidth = 150
      end
      object EditZipCodeCustom1: TUniDBEdit
        Left = 8
        Top = 264
        Width = 281
        Height = 22
        Hint = ''
        DataField = 'ZIPCODECUSTOM1'
        DataSource = DsUProjects
        TabOrder = 8
        FieldLabel = 'Postcode'
        FieldLabelWidth = 150
        OnExit = EditZipCodeExit
      end
      object EditCityCustom1: TUniDBEdit
        Left = 8
        Top = 296
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'CITYCUSTOM1'
        DataSource = DsUProjects
        TabOrder = 9
        FieldLabel = 'Locatie'
        FieldLabelWidth = 150
        OnExit = EditCityExit
      end
      object EditVatNumberCustom1: TUniDBEdit
        Left = 8
        Top = 328
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'VATNUMBERCUSTOM1'
        DataSource = DsUProjects
        TabOrder = 10
        FieldLabel = 'Btw-nummer'
        FieldLabelWidth = 150
      end
      object CheckBoxSplitInvoices: TUniDBCheckBox
        Left = 8
        Top = 8
        Width = 193
        Height = 25
        Hint = ''
        DataField = 'SPLITINVOICES'
        DataSource = DsUProjects
        ValueChecked = '1'
        ValueUnchecked = '0'
        Caption = 'Gesplitste facturatie gebruiken'
        ParentFont = False
        TabOrder = 0
        ParentColor = False
        Color = clBtnFace
      end
      object ComboRelationTypeCustom2: TUniComboBox
        Left = 496
        Top = 40
        Width = 449
        Hint = ''
        Text = 'ComboRelationType'
        Items.Strings = (
          'Bedrijf'
          'Particulier')
        TabOrder = 11
        ForceSelection = True
        FieldLabel = 'Type <font color="#cd0015">*</font>'
        FieldLabelWidth = 150
        IconItems = <>
      end
      object EditCompanyNameCustom2: TUniDBEdit
        Left = 496
        Top = 72
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'COMPANYNAMECUSTOM2'
        DataSource = DsUProjects
        TabOrder = 12
        FieldLabel = 'Bedrijfsnaam'
        FieldLabelWidth = 150
      end
      object EditAppelationCustom2: TUniDBLookupComboBox
        Left = 496
        Top = 104
        Width = 449
        Hint = ''
        ListField = 'DUTCH'
        ListSource = DsAppelations
        KeyField = 'ID'
        ListFieldIndex = 0
        DataField = 'APPELATIONCUSTOM2'
        DataSource = DsUProjects
        TabOrder = 13
        Color = clWindow
        FieldLabel = 'Aanspreektitel'
        FieldLabelWidth = 150
        ForceSelection = True
        Style = csDropDown
      end
      object EditNameCustom2: TUniDBEdit
        Left = 496
        Top = 136
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'NAMECUSTOM2'
        DataSource = DsUProjects
        TabOrder = 14
        FieldLabel = 'Naam'
        FieldLabelWidth = 150
      end
      object EditFirstNameCustom2: TUniDBEdit
        Left = 496
        Top = 168
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'FIRSTNAMECUSTOM2'
        DataSource = DsUProjects
        TabOrder = 15
        FieldLabel = 'Voornaam'
        FieldLabelWidth = 150
      end
      object EditAddressCustom2: TUniDBEdit
        Left = 496
        Top = 200
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'ADDRESSCUSTOM2'
        DataSource = DsUProjects
        TabOrder = 16
        FieldLabel = 'Adres'
        FieldLabelWidth = 150
      end
      object EditHouseNumberCustom2: TUniDBEdit
        Left = 496
        Top = 232
        Width = 241
        Height = 22
        Hint = ''
        DataField = 'HOUSENUMBERCUSTOM2'
        DataSource = DsUProjects
        TabOrder = 17
        FieldLabel = 'Huisnummer'
        FieldLabelWidth = 150
      end
      object EditZipCodeCustom2: TUniDBEdit
        Left = 496
        Top = 264
        Width = 281
        Height = 22
        Hint = ''
        DataField = 'ZIPCODECUSTOM2'
        DataSource = DsUProjects
        TabOrder = 18
        FieldLabel = 'Postcode'
        FieldLabelWidth = 150
        OnExit = EditZipCodeExit
      end
      object EditCityCustom2: TUniDBEdit
        Left = 496
        Top = 296
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'CITYCUSTOM2'
        DataSource = DsUProjects
        TabOrder = 19
        FieldLabel = 'Locatie'
        FieldLabelWidth = 150
        OnExit = EditCityExit
      end
      object EditVatNumberCustom2: TUniDBEdit
        Left = 496
        Top = 328
        Width = 449
        Height = 22
        Hint = ''
        DataField = 'VATNUMBERCUSTOM2'
        DataSource = DsUProjects
        TabOrder = 20
        FieldLabel = 'Btw-nummer'
        FieldLabelWidth = 150
      end
    end
  end
  object BtnSave: TUniThemeButton
    Left = 752
    Top = 432
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Opslaan'
    ParentFont = False
    TabOrder = 1
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 8
    OnClick = BtnSaveClick
    ButtonTheme = uctPrimary
  end
  object BtnCancel: TUniThemeButton
    Left = 864
    Top = 432
    Width = 105
    Height = 25
    Hint = ''
    Caption = 'Annuleren'
    ParentFont = False
    TabOrder = 2
    ScreenMask.Target = Owner
    Images = UniMainModule.ImageList
    ImageIndex = 9
    OnClick = BtnCancelClick
    ButtonTheme = uctSecondary
  end
  object UProjects: TIBCQuery
    KeyFields = 'ID'
    KeyGenerator = 'GEN_PROJECTS_ID'
    GeneratorMode = gmInsert
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO PROJECTS'
      
        '  (ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDAT' +
        'E, ENDDATE, REMARKS, RELATION, REMOVED, PROJECTTYPE, PAYMENTTERM' +
        ', BUDGET, ESTIMATION, TOTALBUDGET, SENDDETAILLEDREPORT, PERCENTA' +
        'GEFEE, SPLITINVOICES, RELATIONTYPECUSTOM1, RELATIONTYPECUSTOM2, ' +
        'COMPANYNAMECUSTOM1, COMPANYNAMECUSTOM2, NAMECUSTOM1, NAMECUSTOM2' +
        ', FIRSTNAMECUSTOM1, FIRSTNAMECUSTOM2, APPELATIONCUSTOM1, APPELAT' +
        'IONCUSTOM2, ADDRESSCUSTOM1, ADDRESSCUSTOM2, HOUSENUMBERCUSTOM1, ' +
        'HOUSENUMBERCUSTOM2, ZIPCODECUSTOM1, ZIPCODECUSTOM2, CITYCUSTOM1,' +
        ' CITYCUSTOM2, VATNUMBERCUSTOM1, VATNUMBERCUSTOM2, VISIBLEININVOI' +
        'CING)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :STATUS, :NAME, :ADDRESS, :ZIPCODE, :CITY, :' +
        'STARTDATE, :ENDDATE, :REMARKS, :RELATION, :REMOVED, :PROJECTTYPE' +
        ', :PAYMENTTERM, :BUDGET, :ESTIMATION, :TOTALBUDGET, :SENDDETAILL' +
        'EDREPORT, :PERCENTAGEFEE, :SPLITINVOICES, :RELATIONTYPECUSTOM1, ' +
        ':RELATIONTYPECUSTOM2, :COMPANYNAMECUSTOM1, :COMPANYNAMECUSTOM2, ' +
        ':NAMECUSTOM1, :NAMECUSTOM2, :FIRSTNAMECUSTOM1, :FIRSTNAMECUSTOM2' +
        ', :APPELATIONCUSTOM1, :APPELATIONCUSTOM2, :ADDRESSCUSTOM1, :ADDR' +
        'ESSCUSTOM2, :HOUSENUMBERCUSTOM1, :HOUSENUMBERCUSTOM2, :ZIPCODECU' +
        'STOM1, :ZIPCODECUSTOM2, :CITYCUSTOM1, :CITYCUSTOM2, :VATNUMBERCU' +
        'STOM1, :VATNUMBERCUSTOM2, :VISIBLEININVOICING)'
      'RETURNING '
      
        '  ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDATE' +
        ', ENDDATE, RELATION, REMOVED, PROJECTTYPE, PAYMENTTERM, BUDGET, ' +
        'ESTIMATION, TOTALBUDGET, SENDDETAILLEDREPORT, PERCENTAGEFEE, SPL' +
        'ITINVOICES, RELATIONTYPECUSTOM1, RELATIONTYPECUSTOM2, COMPANYNAM' +
        'ECUSTOM1, COMPANYNAMECUSTOM2, NAMECUSTOM1, NAMECUSTOM2, FIRSTNAM' +
        'ECUSTOM1, FIRSTNAMECUSTOM2, APPELATIONCUSTOM1, APPELATIONCUSTOM2' +
        ', ADDRESSCUSTOM1, ADDRESSCUSTOM2, HOUSENUMBERCUSTOM1, HOUSENUMBE' +
        'RCUSTOM2, ZIPCODECUSTOM1, ZIPCODECUSTOM2, CITYCUSTOM1, CITYCUSTO' +
        'M2, VATNUMBERCUSTOM1, VATNUMBERCUSTOM2, VISIBLEININVOICING')
    SQLDelete.Strings = (
      'DELETE FROM PROJECTS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE PROJECTS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, STATUS = :STATUS, NAME = :NA' +
        'ME, ADDRESS = :ADDRESS, ZIPCODE = :ZIPCODE, CITY = :CITY, STARTD' +
        'ATE = :STARTDATE, ENDDATE = :ENDDATE, REMARKS = :REMARKS, RELATI' +
        'ON = :RELATION, REMOVED = :REMOVED, PROJECTTYPE = :PROJECTTYPE, ' +
        'PAYMENTTERM = :PAYMENTTERM, BUDGET = :BUDGET, ESTIMATION = :ESTI' +
        'MATION, TOTALBUDGET = :TOTALBUDGET, SENDDETAILLEDREPORT = :SENDD' +
        'ETAILLEDREPORT, PERCENTAGEFEE = :PERCENTAGEFEE, SPLITINVOICES = ' +
        ':SPLITINVOICES, RELATIONTYPECUSTOM1 = :RELATIONTYPECUSTOM1, RELA' +
        'TIONTYPECUSTOM2 = :RELATIONTYPECUSTOM2, COMPANYNAMECUSTOM1 = :CO' +
        'MPANYNAMECUSTOM1, COMPANYNAMECUSTOM2 = :COMPANYNAMECUSTOM2, NAME' +
        'CUSTOM1 = :NAMECUSTOM1, NAMECUSTOM2 = :NAMECUSTOM2, FIRSTNAMECUS' +
        'TOM1 = :FIRSTNAMECUSTOM1, FIRSTNAMECUSTOM2 = :FIRSTNAMECUSTOM2, ' +
        'APPELATIONCUSTOM1 = :APPELATIONCUSTOM1, APPELATIONCUSTOM2 = :APP' +
        'ELATIONCUSTOM2, ADDRESSCUSTOM1 = :ADDRESSCUSTOM1, ADDRESSCUSTOM2' +
        ' = :ADDRESSCUSTOM2, HOUSENUMBERCUSTOM1 = :HOUSENUMBERCUSTOM1, HO' +
        'USENUMBERCUSTOM2 = :HOUSENUMBERCUSTOM2, ZIPCODECUSTOM1 = :ZIPCOD' +
        'ECUSTOM1, ZIPCODECUSTOM2 = :ZIPCODECUSTOM2, CITYCUSTOM1 = :CITYC' +
        'USTOM1, CITYCUSTOM2 = :CITYCUSTOM2, VATNUMBERCUSTOM1 = :VATNUMBE' +
        'RCUSTOM1, VATNUMBERCUSTOM2 = :VATNUMBERCUSTOM2, VISIBLEININVOICI' +
        'NG = :VISIBLEININVOICING'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STAR' +
        'TDATE, ENDDATE, REMARKS, RELATION, REMOVED, PROJECTTYPE, PAYMENT' +
        'TERM, BUDGET, ESTIMATION, TOTALBUDGET, SENDDETAILLEDREPORT, PERC' +
        'ENTAGEFEE, SPLITINVOICES, RELATIONTYPECUSTOM1, RELATIONTYPECUSTO' +
        'M2, COMPANYNAMECUSTOM1, COMPANYNAMECUSTOM2, NAMECUSTOM1, NAMECUS' +
        'TOM2, FIRSTNAMECUSTOM1, FIRSTNAMECUSTOM2, APPELATIONCUSTOM1, APP' +
        'ELATIONCUSTOM2, ADDRESSCUSTOM1, ADDRESSCUSTOM2, HOUSENUMBERCUSTO' +
        'M1, HOUSENUMBERCUSTOM2, ZIPCODECUSTOM1, ZIPCODECUSTOM2, CITYCUST' +
        'OM1, CITYCUSTOM2, VATNUMBERCUSTOM1, VATNUMBERCUSTOM2, VISIBLEINI' +
        'NVOICING FROM PROJECTS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM PROJECTS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM PROJECTS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    UpdateTransaction = UpdTrProjects
    SQL.Strings = (
      'Select * from projects')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 280
    Top = 184
  end
  object DsUProjects: TDataSource
    DataSet = UProjects
    Left = 280
    Top = 232
  end
  object UpdTrProjects: TIBCTransaction
    DefaultConnection = UniMainModule.DbModule
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 280
    Top = 288
  end
  object Relations: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO RELATIONS'
      '  (ID)'
      'VALUES'
      '  (:ID)'
      'RETURNING '
      '  ID')
    SQLDelete.Strings = (
      'DELETE FROM RELATIONS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE RELATIONS'
      'SET'
      '  ID = :ID'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      'SELECT ID FROM RELATIONS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM RELATIONS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM RELATIONS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      
        'Select ID, NAME || '#39' '#39' || FIRSTNAME AS FULLNAME, COMPANYNAME, RE' +
        'LATIONTYPE from relations')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    IndexFieldNames = 'DISPLAYNAME'
    OnCalcFields = RelationsCalcFields
    Left = 376
    Top = 186
    object RelationsID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object RelationsFULLNAME: TWideStringField
      FieldName = 'FULLNAME'
      ReadOnly = True
      Size = 51
    end
    object RelationsCOMPANYNAME: TWideStringField
      FieldName = 'COMPANYNAME'
      Size = 50
    end
    object RelationsRELATIONTYPE: TIntegerField
      FieldName = 'RELATIONTYPE'
      Required = True
    end
    object RelationsDISPLAYNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'DISPLAYNAME'
      Size = 51
      Calculated = True
    end
  end
  object DsRelations: TDataSource
    DataSet = Relations
    Left = 376
    Top = 234
  end
  object ProjectMembers: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO PROJECTS'
      
        '  (ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDAT' +
        'E, ENDDATE, REMARKS, RELATION, REMOVED)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :STATUS, :NAME, :ADDRESS, :ZIPCODE, :CITY, :' +
        'STARTDATE, :ENDDATE, :REMARKS, :RELATION, :REMOVED)'
      'RETURNING '
      
        '  ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDATE' +
        ', ENDDATE, RELATION, REMOVED')
    SQLDelete.Strings = (
      'DELETE FROM PROJECTS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE PROJECTS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, STATUS = :STATUS, NAME = :NA' +
        'ME, ADDRESS = :ADDRESS, ZIPCODE = :ZIPCODE, CITY = :CITY, STARTD' +
        'ATE = :STARTDATE, ENDDATE = :ENDDATE, REMARKS = :REMARKS, RELATI' +
        'ON = :RELATION, REMOVED = :REMOVED'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STAR' +
        'TDATE, ENDDATE, REMARKS, RELATION, REMOVED FROM PROJECTS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM PROJECTS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM PROJECTS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'SELECT '
      '  PROJECTMEMBERS.ID,'
      '  USERS.NAME || '#39' '#39' || USERS.FIRSTNAME AS USERFULLNAME,'
      '  PROJECTMEMBERS.HOURLYRATE'
      'FROM'
      '  PROJECTMEMBERS'
      '  LEFT OUTER JOIN USERS ON (PROJECTMEMBERS.USERID = USERS.ID)'
      'ORDER BY USERFULLNAME')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 496
    Top = 184
    object ProjectMembersID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object ProjectMembersUSERFULLNAME: TWideStringField
      FieldName = 'USERFULLNAME'
      ReadOnly = True
      Size = 101
    end
    object ProjectMembersHOURLYRATE: TFloatField
      FieldName = 'HOURLYRATE'
      DisplayFormat = '#,##0.00 '#8364
    end
  end
  object DsProjectMembers: TDataSource
    DataSet = ProjectMembers
    Left = 496
    Top = 232
  end
  object ProjectPhases: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO PROJECTS'
      
        '  (ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDAT' +
        'E, ENDDATE, REMARKS, RELATION, REMOVED)'
      'VALUES'
      
        '  (:ID, :COMPANYID, :STATUS, :NAME, :ADDRESS, :ZIPCODE, :CITY, :' +
        'STARTDATE, :ENDDATE, :REMARKS, :RELATION, :REMOVED)'
      'RETURNING '
      
        '  ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STARTDATE' +
        ', ENDDATE, RELATION, REMOVED')
    SQLDelete.Strings = (
      'DELETE FROM PROJECTS'
      'WHERE'
      '  ID = :Old_ID')
    SQLUpdate.Strings = (
      'UPDATE PROJECTS'
      'SET'
      
        '  ID = :ID, COMPANYID = :COMPANYID, STATUS = :STATUS, NAME = :NA' +
        'ME, ADDRESS = :ADDRESS, ZIPCODE = :ZIPCODE, CITY = :CITY, STARTD' +
        'ATE = :STARTDATE, ENDDATE = :ENDDATE, REMARKS = :REMARKS, RELATI' +
        'ON = :RELATION, REMOVED = :REMOVED'
      'WHERE'
      '  ID = :Old_ID')
    SQLRefresh.Strings = (
      
        'SELECT ID, COMPANYID, STATUS, NAME, ADDRESS, ZIPCODE, CITY, STAR' +
        'TDATE, ENDDATE, REMARKS, RELATION, REMOVED FROM PROJECTS'
      'WHERE'
      '  ID = :ID')
    SQLLock.Strings = (
      'SELECT NULL FROM PROJECTS'
      'WHERE'
      'ID = :Old_ID'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM PROJECTS'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select * from projectphases')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    Filtered = True
    FilterOptions = [foCaseInsensitive]
    Left = 624
    Top = 184
    object ProjectPhasesID: TLargeintField
      FieldName = 'ID'
      Required = True
    end
    object ProjectPhasesCOMPANYID: TLargeintField
      FieldName = 'COMPANYID'
      Required = True
    end
    object ProjectPhasesPROJECT: TLargeintField
      FieldName = 'PROJECT'
      Required = True
    end
    object ProjectPhasesPHASEDESCRIPTION: TWideStringField
      FieldName = 'PHASEDESCRIPTION'
      Required = True
      Size = 100
    end
    object ProjectPhasesPHASEPERCENTAGE: TFloatField
      FieldName = 'PHASEPERCENTAGE'
      DisplayFormat = '#,##0.00 %'
    end
    object ProjectPhasesPHASESEQUENCE: TIntegerField
      FieldName = 'PHASESEQUENCE'
      Required = True
    end
  end
  object DsProjectPhases: TDataSource
    DataSet = ProjectPhases
    Left = 624
    Top = 240
  end
  object Appelations: TIBCQuery
    DMLRefresh = True
    SQLInsert.Strings = (
      'INSERT INTO COUNTRIES'
      '  (CODE, DUTCH, FRENCH, ENGLISH)'
      'VALUES'
      '  (:CODE, :DUTCH, :FRENCH, :ENGLISH)'
      'RETURNING '
      '  CODE, DUTCH, FRENCH, ENGLISH')
    SQLDelete.Strings = (
      'DELETE FROM COUNTRIES'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLUpdate.Strings = (
      'UPDATE COUNTRIES'
      'SET'
      
        '  CODE = :CODE, DUTCH = :DUTCH, FRENCH = :FRENCH, ENGLISH = :ENG' +
        'LISH'
      'WHERE'
      '  CODE = :Old_CODE')
    SQLRefresh.Strings = (
      'SELECT CODE, DUTCH, FRENCH, ENGLISH FROM COUNTRIES'
      'WHERE'
      '  CODE = :CODE')
    SQLLock.Strings = (
      'SELECT NULL FROM COUNTRIES'
      'WHERE'
      'CODE = :Old_CODE'
      'FOR UPDATE WITH LOCK')
    SQLRecCount.Strings = (
      'SELECT COUNT(*) FROM ('
      'SELECT 1 AS C  FROM COUNTRIES'
      ''
      ') q')
    Connection = UniMainModule.DbModule
    SQL.Strings = (
      'Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES')
    FetchAll = True
    AutoCommit = False
    LockMode = lmLockImmediate
    RefreshOptions = [roBeforeEdit]
    Options.DeferredBlobRead = True
    FilterOptions = [foCaseInsensitive]
    Left = 768
    Top = 184
  end
  object DsAppelations: TDataSource
    DataSet = Appelations
    Left = 768
    Top = 239
  end
end
