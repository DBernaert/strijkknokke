unit ProjectsAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, Data.DB, DBAccess, IBC, MemDS, uniBasicGrid, uniDBGrid, uniBitBtn, uniMenuButton,
  uniButton, UniThemeButton, uniCheckBox, uniDBCheckBox, uniDBEdit, uniPanel, uniMemo, uniDBMemo, uniDateTimePicker,
  uniDBDateTimePicker, uniDBComboBox, uniDBLookupComboBox, uniEdit, uniMultiItem, uniComboBox, uniPageControl,
  uniGUIBaseClasses;

type
  TProjectsAddFrm = class(TUniForm)
    UProjects: TIBCQuery;
    DsUProjects: TDataSource;
    UpdTrProjects: TIBCTransaction;
    Relations: TIBCQuery;
    RelationsID: TLargeintField;
    RelationsFULLNAME: TWideStringField;
    RelationsCOMPANYNAME: TWideStringField;
    RelationsRELATIONTYPE: TIntegerField;
    RelationsDISPLAYNAME: TStringField;
    DsRelations: TDataSource;
    ProjectMembers: TIBCQuery;
    ProjectMembersID: TLargeintField;
    ProjectMembersUSERFULLNAME: TWideStringField;
    ProjectMembersHOURLYRATE: TFloatField;
    DsProjectMembers: TDataSource;
    ProjectPhases: TIBCQuery;
    ProjectPhasesID: TLargeintField;
    ProjectPhasesCOMPANYID: TLargeintField;
    ProjectPhasesPROJECT: TLargeintField;
    ProjectPhasesPHASEDESCRIPTION: TWideStringField;
    ProjectPhasesPHASEPERCENTAGE: TFloatField;
    ProjectPhasesPHASESEQUENCE: TIntegerField;
    DsProjectPhases: TDataSource;
    PageControlProjects: TUniPageControl;
    TabSheetGeneral: TUniTabSheet;
    ComboStatus: TUniComboBox;
    EditName: TUniDBEdit;
    EditAddress: TUniDBEdit;
    EditZipCode: TUniDBEdit;
    EditCity: TUniDBEdit;
    EditRelation: TUniDBLookupComboBox;
    EditStartDate: TUniDBDateTimePicker;
    EditEndDate: TUniDBDateTimePicker;
    EditRemarks: TUniDBMemo;
    TabSheetRegie: TUniTabSheet;
    ContainerProcentual: TUniContainerPanel;
    EditBudget: TUniDBFormattedNumberEdit;
    EditEstimation: TUniDBFormattedNumberEdit;
    EditTotalBudget: TUniDBFormattedNumberEdit;
    ComboProjectType: TUniComboBox;
    EditPaymentTerm: TUniDBFormattedNumberEdit;
    CheckBoxSendDetailledReport: TUniDBCheckBox;
    ContainerGridProcentual: TUniContainerPanel;
    UniContainerPanel1: TUniContainerPanel;
    BtnModifyPhase: TUniThemeButton;
    BtnAddPhase: TUniThemeButton;
    BtnMovePhaseUp: TUniThemeButton;
    BtnMovePhaseDown: TUniThemeButton;
    GridProjectPhases: TUniDBGrid;
    TabSheetProjectMembers: TUniTabSheet;
    ContainerHeaderMembers: TUniContainerPanel;
    UniContainerPanel5: TUniContainerPanel;
    BtnAddProjectMember: TUniThemeButton;
    BtnModifyProjectMember: TUniThemeButton;
    BtnAddAllUsers: TUniThemeButton;
    GridProjectMembers: TUniDBGrid;
    PanelFooter: TUniContainerPanel;
    BtnSave: TUniThemeButton;
    BtnCancel: TUniThemeButton;
    BtnDeleteProjectMember: TUniThemeButton;
    BtnDeletePhase: TUniThemeButton;
    EditPercentageFee: TUniDBFormattedNumberEdit;
    TabSheetInvoicing: TUniTabSheet;
    ComboRelationTypeCustom1: TUniComboBox;
    EditCompanyNameCustom1: TUniDBEdit;
    EditAppelationCustom1: TUniDBLookupComboBox;
    EditNameCustom1: TUniDBEdit;
    EditFirstNameCustom1: TUniDBEdit;
    EditAddresCustom1: TUniDBEdit;
    EditHousenumberCustom1: TUniDBEdit;
    EditZipCodeCustom1: TUniDBEdit;
    EditCityCustom1: TUniDBEdit;
    EditVatNumberCustom1: TUniDBEdit;
    CheckBoxSplitInvoices: TUniDBCheckBox;
    ComboRelationTypeCustom2: TUniComboBox;
    EditCompanyNameCustom2: TUniDBEdit;
    EditAppelationCustom2: TUniDBLookupComboBox;
    EditNameCustom2: TUniDBEdit;
    EditFirstNameCustom2: TUniDBEdit;
    EditAddressCustom2: TUniDBEdit;
    EditHouseNumberCustom2: TUniDBEdit;
    EditZipCodeCustom2: TUniDBEdit;
    EditCityCustom2: TUniDBEdit;
    EditVatNumberCustom2: TUniDBEdit;
    Appelations: TIBCQuery;
    DsAppelations: TDataSource;
    CheckBoxVisibleInvoicing: TUniDBCheckBox;
    procedure BtnCancelClick(Sender: TObject);
    procedure BtnSaveClick(Sender: TObject);
    procedure RelationsCalcFields(DataSet: TDataSet);
    procedure BtnAddAllUsersClick(Sender: TObject);
    procedure BtnAddProjectMemberClick(Sender: TObject);
    procedure BtnModifyProjectMemberClick(Sender: TObject);
    procedure BtnDeleteProjectMemberClick(Sender: TObject);
    procedure BtnAddPhaseClick(Sender: TObject);
    procedure BtnModifyPhaseClick(Sender: TObject);
    procedure BtnDeletePhaseClick(Sender: TObject);
    procedure BtnMovePhaseUpClick(Sender: TObject);
    procedure BtnMovePhaseDownClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure UniFormReady(Sender: TObject);
    procedure EditCityExit(Sender: TObject);
    procedure EditZipCodeExit(Sender: TObject);
    procedure GridProjectPhasesColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
    procedure GridProjectPhasesColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
      Attribs: TUniCellAttribs; var Result: string);
  private
    { Private declarations }
    procedure Open_reference_tables;
    procedure Open_project_members;
    procedure Open_project_phases;
    procedure Add_all_users_to_project;
    function  Save_project: boolean;
    procedure CallBackSelectZipCode(Sender: TComponent; AResult: Integer);
    procedure CallBackSelectCity(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateProjectMember(Sender: TComponent; AResult: Integer);
    procedure CallBackInsertUpdateProjectPhase(Sender: TComponent; AResult: Integer);
  public
    { Public declarations }
    procedure Initialize_insert;
    procedure Initialize_edit(Id: longint);
  end;

function ProjectsAddFrm: TProjectsAddFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, ServerModule, UniFSConfirm, ProjectMemberAdd, ProjectPhaseAdd, ZipCodesSelect;

function ProjectsAddFrm: TProjectsAddFrm;
begin
  Result := TProjectsAddFrm(UniMainModule.GetFormInstance(TProjectsAddFrm));
end;

{ TProjectsAddFrm }

procedure TProjectsAddFrm.Add_all_users_to_project;
begin
  UniMainModule.QWork.Close;
  UniMainModule.QWork.SQL.Clear;
  UniMainModule.QWork.SQL.Add('Select id from users where companyid=' + IntToStr(UniMainModule.Company_id) + ' and removed=''0''');
  UniMainModule.QWork.Open;

  UniMainModule.QWork2.Close;
  UniMainModule.QWork2.SQL.Clear;
  UniMainModule.QWork2.SQL.Add('Select userid from projectmembers where project=' + UProjects.FieldByName('Id').asString);
  UniMainModule.QWork2.Open;

  UniMainModule.QWork.First;
  while not UniMainModule.QWork.EOF do
  begin
    if not UniMainModule.QWork2.Locate('UserId', UniMainModule.QWork.FieldByName('Id').AsInteger, []) then
    begin
      UniMainModule.UProjectMembers.Close;
      UniMainModule.UProjectMembers.SQL.Clear;
      UniMainModule.UProjectMembers.SQL.Add('Select first 0 * from projectmembers');
      UniMainModule.UProjectMembers.Open;
      UniMainModule.UProjectMembers.Append;
      UniMainModule.UProjectMembers.FieldByName('Project').AsInteger := UProjects.FieldByName('Id').AsInteger;
      UniMainModule.UProjectMembers.FieldByName('UserId').AsInteger := UniMainModule.QWork.FieldByName('Id').AsInteger;
      UniMainModule.UProjectMembers.FieldByName('HourlyRate').AsCurrency := 0;
      UniMainModule.UProjectMembers.Post;
      Try
        UniMainModule.UpdTrProjectMembers.Commit;
        UniMainModule.UProjectMembers.Close;
      Except
        on E: EDataBaseError do
        begin
          UniMainModule.UpdTrProjectMembers.Rollback;
          UniMainModule.UProjectMembers.Close;
          UniMainModule.show_error('Fout bij het toevoegen: ' + E.Message);
        end;
      end;
    end;
    UniMainModule.QWork.Next;
  end;
  UniMainModule.QWork.Close;
  UniMainModule.QWork2.Close;
  if not ProjectMembers.Active then
    Open_project_members
  else
    ProjectMembers.Refresh;
end;

procedure TProjectsAddFrm.BtnAddAllUsersClick(Sender: TObject);
begin
  if UProjects.State = dsInsert then
  begin
    MainForm.Confirm.Question(UniMainModule.Company_name, 'Het project moet eerst worden bewaard. Bewaren?', 'fa fa-question-circle',
      procedure(Button: TConfirmButton)
      begin
        if Button = Yes then
        begin
          if Save_project then
          begin
            UProjects.Edit;
            Add_all_users_to_project;
          end;
        end;
      end);
  end
  else
    Add_all_users_to_project;
end;

procedure TProjectsAddFrm.BtnAddPhaseClick(Sender: TObject);
begin
  if UProjects.State = dsInsert then
  begin
    MainForm.Confirm.Question(UniMainModule.Company_name, 'Het project moet eerst worden bewaard. Bewaren?', 'fa fa-question-circle',
      procedure(Button: TConfirmButton)
      begin
        if Button = Yes then
        begin
          if Save_project then
          begin
            UProjects.Edit;
            Open_project_phases;
            Open_project_members;
            UniMainModule.Result_dbAction := 0;
            With ProjectPhaseAddFrm do
            begin
              Init_add_projectPhase(UProjects.FieldByName('Id').AsInteger);
              ShowModal(CallBackInsertUpdateProjectPhase);
            end;
          end;
        end;
      end);
  end
  else
  begin
    UniMainModule.Result_dbAction := 0;
    With ProjectPhaseAddFrm do
    begin
      Init_add_projectPhase(UProjects.FieldByName('Id').AsInteger);
      ShowModal(CallBackInsertUpdateProjectPhase);
    end;
  end;
end;

procedure TProjectsAddFrm.BtnAddProjectMemberClick(Sender: TObject);
begin
  if UProjects.State = dsInsert then
  begin
    MainForm.Confirm.Question(UniMainModule.Company_name, 'Het project moet eerst worden bewaard. Bewaren?', 'fa fa-question-circle',
      procedure(Button: TConfirmButton)
      begin
        if Button = Yes then
        begin
          if Save_project then
          begin
            UProjects.Edit;
            Open_project_phases;
            Open_project_members;
            UniMainModule.Result_dbAction := 0;
            With ProjectMemberAddFrm do
            begin
              Init_projectmember_creation(UProjects.FieldByName('Id').AsInteger);
              ShowModal(CallBackInsertUpdateProjectMember);
            end;
          end;
        end;
      end);
  end
  else
  begin
    UniMainModule.Result_dbAction := 0;
    With ProjectMemberAddFrm do
    begin
      Init_projectmember_creation(UProjects.FieldByName('Id').AsInteger);
      ShowModal(CallBackInsertUpdateProjectMember);
    end;
  end;
end;

procedure TProjectsAddFrm.BtnCancelClick(Sender: TObject);
begin
  UProjects.Cancel;
  UProjects.Close;
  Close;
end;

procedure TProjectsAddFrm.BtnDeletePhaseClick(Sender: TObject);
begin
  if (not ProjectPhases.Active) or (ProjectPhases.FieldByName('Id').isNull) then
    exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen fase?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UProjectPhases.Close;
          UniMainModule.UProjectPhases.SQL.Clear;
          UniMainModule.UProjectPhases.SQL.Add('Select * from projectphases where id=' + QuotedStr(ProjectPhases.FieldByName('Id').asString));
          UniMainModule.UProjectPhases.Open;
          UniMainModule.UProjectPhases.Delete;
          UniMainModule.UpdTrProjectPhases.Commit;
          UniMainModule.UProjectPhases.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrProjectPhases.Rollback;
            UniMainModule.UProjectPhases.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        ProjectPhases.Refresh;
      end;
    end);
end;

procedure TProjectsAddFrm.BtnDeleteProjectMemberClick(Sender: TObject);
begin
  if (not ProjectMembers.Active) or (ProjectMembers.FieldByName('Id').isNull) then
    exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen projectlid?', 'fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes then
      begin
        Try
          UniMainModule.UProjectMembers.Close;
          UniMainModule.UProjectMembers.SQL.Clear;
          UniMainModule.UProjectMembers.SQL.Add('Select * from projectmembers where id=' + QuotedStr(ProjectMembers.FieldByName('Id').asString));
          UniMainModule.UProjectMembers.Open;
          UniMainModule.UProjectMembers.Delete;
          UniMainModule.UpdTrProjectMembers.Commit;
          UniMainModule.UProjectMembers.Close;
        Except
          on E: EDataBaseError do
          begin
            UniMainModule.UpdTrProjectMembers.Rollback;
            UniMainModule.UProjectMembers.Close;
            UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
          end;
        end;
        ProjectMembers.Refresh;
      end;
    end);
end;

procedure TProjectsAddFrm.BtnModifyPhaseClick(Sender: TObject);
begin
  if (ProjectPhases.Active) and (not ProjectPhases.FieldByName('Id').isNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With ProjectPhaseAddFrm do
    begin
      Init_modify_projectPhase(ProjectPhases.FieldByName('Id').AsInteger);
      ShowModal(CallBackInsertUpdateProjectPhase);
    end;
  end;
end;

procedure TProjectsAddFrm.BtnModifyProjectMemberClick(Sender: TObject);
begin
  if (ProjectMembers.Active) and (not ProjectMembers.FieldByName('Id').isNull) then
  begin
    UniMainModule.Result_dbAction := 0;
    With ProjectMemberAddFrm do
    begin
      Init_user_modification(ProjectMembers.FieldByName('Id').AsInteger);
      ShowModal(CallBackInsertUpdateProjectMember);
    end;
  end;
end;

procedure TProjectsAddFrm.BtnMovePhaseDownClick(Sender: TObject);
var
  firstsequence, secondsequence: Integer;
  firstid, secondid: longint;
begin
  if (ProjectPhases.Active = False) or (ProjectPhases.FieldByName('Id').isNull) then
    exit;

  if ProjectPhases.Eof then
    exit;

  // Swap sequence
  firstsequence := ProjectPhases.FieldByName('PhaseSequence').AsInteger;
  firstid       := ProjectPhases.FieldByName('Id').AsInteger;

  ProjectPhases.Next;
  secondsequence := ProjectPhases.FieldByName('PhaseSequence').AsInteger;
  secondid       := ProjectPhases.FieldByName('Id').AsInteger;

  UniMainModule.UpdTrProjectPhases.StartTransaction;
  UniMainModule.UProjectPhases.Close;
  UniMainModule.UProjectPhases.SQL.Clear;
  UniMainModule.UProjectPhases.SQL.Add('Select * from projectphases where Project=' + UProjects.FieldByName('Id').asString);
  UniMainModule.UProjectPhases.Open;

  if UniMainModule.UProjectPhases.Locate('Id', firstid, []) then
  begin
    UniMainModule.UProjectPhases.Edit;
    UniMainModule.UProjectPhases.FieldByName('PhaseSequence').AsInteger := secondsequence;
    UniMainModule.UProjectPhases.Post;
  end;

  if UniMainModule.UProjectPhases.Locate('Id', secondid, []) then
  begin
    UniMainModule.UProjectPhases.Edit;
    UniMainModule.UProjectPhases.FieldByName('PhaseSequence').AsInteger := firstsequence;
    UniMainModule.UProjectPhases.Post;
  end;

  Try
    UniMainModule.UpdTrProjectPhases.Commit;
    UniMainModule.UProjectPhases.Close;
    ProjectPhases.Refresh;
    ProjectPhases.Locate('Id', firstid, []);
  Except
    UniMainModule.UProjectPhases.Close;
    UniMainModule.UpdTrProjectPhases.Rollback;
  End;
end;

procedure TProjectsAddFrm.BtnMovePhaseUpClick(Sender: TObject);
var
  firstsequence, secondsequence: Integer;
  firstid, secondid: longint;
begin
  if (ProjectPhases.Active = False) or (ProjectPhases.FieldByName('Id').isNull) then
    exit;

  if ProjectPhases.Bof then
    exit;

  // Swap sequence
  firstsequence := ProjectPhases.FieldByName('PhaseSequence').AsInteger;
  firstid       := ProjectPhases.FieldByName('Id').AsInteger;

  ProjectPhases.Prior;
  secondsequence := ProjectPhases.FieldByName('PhaseSequence').AsInteger;
  secondid       := ProjectPhases.FieldByName('Id').AsInteger;

  UniMainModule.UpdTrProjectPhases.StartTransaction;
  UniMainModule.UProjectPhases.Close;
  UniMainModule.UProjectPhases.SQL.Clear;
  UniMainModule.UProjectPhases.SQL.Add('Select * from projectPhases where project=' + UProjects.FieldByName('Id').asString);
  UniMainModule.UProjectPhases.Open;

  if UniMainModule.UProjectPhases.Locate('Id', firstid, []) then
  begin
    UniMainModule.UProjectPhases.Edit;
    UniMainModule.UProjectPhases.FieldByName('PhaseSequence').AsInteger := secondsequence;
    UniMainModule.UProjectPhases.Post;
  end;

  if UniMainModule.UProjectPhases.Locate('Id', secondid, []) then
  begin
    UniMainModule.UProjectPhases.Edit;
    UniMainModule.UProjectPhases.FieldByName('PhaseSequence').AsInteger := firstsequence;
    UniMainModule.UProjectPhases.Post;
  end;

  Try
    UniMainModule.UpdTrProjectPhases.Commit;
    UniMainModule.UProjectPhases.Close;
    ProjectPhases.Refresh;
    ProjectPhases.Locate('Id', firstid, []);
  Except
    UniMainModule.UProjectPhases.Close;
    UniMainModule.UpdTrProjectPhases.Rollback;
  End;
end;

procedure TProjectsAddFrm.BtnSaveClick(Sender: TObject);
begin
 if Save_project
 then Close;
end;

procedure TProjectsAddFrm.CallBackInsertUpdateProjectMember(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if ProjectMembers.Active then
    begin
      ProjectMembers.Refresh;
      ProjectMembers.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TProjectsAddFrm.CallBackInsertUpdateProjectPhase(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then
  begin
    if ProjectPhases.Active then
    begin
      ProjectPhases.Refresh;
      ProjectPhases.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TProjectsAddFrm.CallBackSelectCity(Sender: TComponent; AResult: Integer);
begin
  if Trim(UniMainModule.Result_dbAction_string) <> ''
  then UProjects.FieldByName('City').asString := UniMainModule.Result_dbAction_string;
end;

procedure TProjectsAddFrm.CallBackSelectZipCode(Sender: TComponent; AResult: Integer);
begin
  if Trim(UniMainModule.Result_dbAction_string) <> ''
  then UProjects.FieldByName('ZipCode').asString := UniMainModule.Result_dbAction_string;
end;

procedure TProjectsAddFrm.EditCityExit(Sender: TObject);
var Zipcode: string;
begin
  if (Trim(EditCity.Text) <> '') and (Trim(EditZipCode.Text) = '') then
  begin
    Zipcode := UniMainModule.Search_zipcode(EditCity.Text);
    if Zipcode = '0' then
      exit;

    if Zipcode = '+1' then
    begin
      UniMainModule.Result_dbAction_string := '';
      With ZipCodesSelectFrm do
      begin
        Init_zipcode_selection(EditCity.Text);
        ShowModal(CallBackSelectZipCode);
      end;
    end
    else
      UProjects.FieldByName('ZipCode').asString := Zipcode;
  end;
end;

procedure TProjectsAddFrm.EditZipCodeExit(Sender: TObject);
var city: string;
begin
  if (Trim(EditZipCode.Text) <> '') and (Trim(EditCity.Text) = '') then
  begin
    city := UniMainModule.Search_city(UpperCase(EditZipCode.Text));
    if city = '0' then
      exit;

    if city = '+1' then
    begin
      UniMainModule.Result_dbAction_string := '';
      With ZipCodesSelectFrm do
      begin
        Init_city_selection(EditZipCode.Text);
        ShowModal(CallBackSelectCity);
      end;
    end
    else
      UProjects.FieldByName('City').asString := city;
  end;
end;

procedure TProjectsAddFrm.GridProjectPhasesColumnSummary(Column: TUniDBGridColumn; GroupFieldValue: Variant);
begin
 if SameText(UpperCase(Column.FieldName), 'PHASEPERCENTAGE') then
  begin
    if Column.AuxValue = NULL then
      Column.AuxValue := 0.0;
    Column.AuxValue := Column.AuxValue + Column.Field.AsCurrency;
  end;
end;

procedure TProjectsAddFrm.GridProjectPhasesColumnSummaryResult(Column: TUniDBGridColumn; GroupFieldValue: Variant;
  Attribs: TUniCellAttribs; var Result: string);
var F: currency;
begin
  if SameText(UpperCase(Column.FieldName), 'PHASEPERCENTAGE') then
  begin
    F := Column.AuxValue;
    Result := FormatCurr('#,##0.00 %', F);
    Attribs.Color := $00855000;
    Attribs.Font.Color := ClWhite;
    Attribs.Font.Style := [fsBold];
  end;
  Column.AuxValue := NULL;
end;

procedure TProjectsAddFrm.Initialize_edit(Id: longint);
begin
  Caption := 'Wijzigen project';

  Open_reference_tables;

  if UniMainModule.User_admin = False
  then begin
         TabSheetRegie.Visible           := False;
         TabSheetProjectMembers.Visible := False
       end;

  UProjects.Close;
  UProjects.SQL.Clear;
  UProjects.SQL.Add('Select * from projects where id=' + IntToStr(Id));
  UProjects.Open;
  Try
    UProjects.Edit;
    ComboProjectType.ItemIndex := UProjects.FieldByName('Projecttype').AsInteger;
    ComboStatus.ItemIndex := UProjects.FieldByName('Status').AsInteger;
    ComboRelationTypeCustom1.ItemIndex := UProjects.FieldByName('RelationTypeCustom1').AsInteger;
    ComboRelationTypeCustom2.ItemIndex := UProjects.FieldByName('RelationTypeCustom2').AsInteger;
    Open_project_phases;
    Open_project_members;
  Except
    UProjects.Cancel;
    UProjects.Close;
    UniMainModule.show_error('Gegevens kunnen niet aangepast worden, in gebruik door een andere gebruiker.');
    Close;
    exit;
  End;
end;

procedure TProjectsAddFrm.Initialize_insert;
begin
  Caption := 'Toevoegen project';

  Open_reference_tables;

  UProjects.Close;
  UProjects.SQL.Clear;
  UProjects.SQL.Add('Select first 0 * from projects');
  UProjects.Open;
  UProjects.Append;
  UProjects.FieldByName('CompanyId').AsInteger := UniMainModule.Company_id;
  UProjects.FieldByName('Removed').asString := '0';
  UProjects.FieldByName('Status').AsInteger := 0;
  UProjects.FieldByName('Senddetailledreport').AsInteger := 1;
  UProjects.FieldByName('VisibleInInvoicing').AsInteger  := 1;

  UProjects.FieldByName('SPLITINVOICES').AsInteger       := 0;

  ComboProjectType.ItemIndex := 0;
  ComboStatus.ItemIndex := 0;

  ComboRelationTypeCustom1.ItemIndex := 0;
  ComboRelationTypeCustom2.ItemIndex := 0;
end;

procedure TProjectsAddFrm.Open_project_members;
begin
  ProjectMembers.Close;
  ProjectMembers.SQL.Clear;
  ProjectMembers.SQL.Add('SELECT PROJECTMEMBERS.ID, USERS.NAME || '' '' || USERS.FIRSTNAME AS USERFULLNAME, ' + 'PROJECTMEMBERS.HOURLYRATE ' + 'FROM ' +
    'PROJECTMEMBERS ' + 'LEFT OUTER JOIN USERS ON (PROJECTMEMBERS.USERID = USERS.ID) ' + 'WHERE PROJECT=' + UProjects.FieldByName('Id').asString + ' ' +
    'ORDER BY USERFULLNAME');
  ProjectMembers.Open;
end;

procedure TProjectsAddFrm.Open_project_phases;
begin
  ProjectPhases.Close;
  ProjectPhases.SQL.Clear;
  ProjectPhases.SQL.Add('Select * from projectPhases where project=' + UProjects.FieldByName('Id').AsString + ' order by phasesequence');
  ProjectPhases.Open;
end;

procedure TProjectsAddFrm.Open_reference_tables;
begin
  Relations.Close;
  Relations.SQL.Clear;
  Relations.SQL.Add('Select ID, NAME || '' '' || FIRSTNAME AS FULLNAME, COMPANYNAME, RELATIONTYPE from relations' + ' where companyid=' +
    IntToStr(UniMainModule.Company_id) + ' and removed=''0''');
  Relations.Open;

  Appelations.Close;
  Appelations.SQL.Clear;
  Appelations.SQL.Add('Select ID, DUTCH, FRENCH, ENGLISH from BASICTABLES' +
                  ' where COMPANYID=' + IntToStr(UniMainModule.Company_id) +
                  ' and TABLEID=1 ' +
                  ' and REMOVED=''0''' +
                  ' order by DUTCH');
  Appelations.Open;
end;

procedure TProjectsAddFrm.RelationsCalcFields(DataSet: TDataSet);
begin
 case Relations.FieldByName('RelationType').AsInteger of
  0: Relations.FieldByName('DisplayName').AsString := Relations.FieldByName('CompanyName').AsString;
  1: Relations.FieldByName('DisplayName').AsString := Relations.FieldByName('FullName').AsString;
  end;
end;

function TProjectsAddFrm.Save_project: boolean;
begin
  Result := False;
  BtnSave.SetFocus;

  if ComboStatus.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('De status van het project is een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetGeneral;
    ComboStatus.SetFocus;
    exit;
  end;

  if UProjects.FieldByName('Relation').isNull then
  begin
    UniMainModule.show_warning('De klant is een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetGeneral;
    EditRelation.SetFocus;
    exit;
  end;

  if Trim(UProjects.FieldByName('NAME').asString) = '' then
  begin
    UniMainModule.show_warning('De naam van het project een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetGeneral;
    EditName.SetFocus;
    exit;
  end;

  if ComboProjectType.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('Het projecttype is een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetRegie;
    ComboProjectType.SetFocus;
    exit;
  end;

  if UProjects.FieldByName('PaymentTerm').isNull then
  begin
    UniMainModule.show_warning('De betalingstermijn is een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetRegie;
    EditPaymentTerm.SetFocus;
    exit;
  end;

  if ComboRelationTypeCustom1.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('Type relatie is een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetInvoicing;
    ComboRelationTypeCustom1.SetFocus;
    Exit;
  end;

  if ComboRelationTypeCustom2.ItemIndex = -1 then
  begin
    UniMainModule.show_warning('Type relatie is een verplichte ingave.');
    PageControlProjects.ActivePage := TabSheetInvoicing;
    ComboRelationTypeCustom2.SetFocus;
    Exit;
  end;

  UProjects.FieldByName('RelationTypeCustom1').AsInteger := ComboRelationTypeCustom1.ItemIndex;
  UProjects.FieldByName('RelationTypeCustom2').AsInteger := ComboRelationTypeCustom2.ItemIndex;
  UProjects.FieldByName('Status').AsInteger              := ComboStatus.ItemIndex;
  UProjects.FieldByName('ProjectType').AsInteger         := ComboProjectType.ItemIndex;

  Try
    UProjects.Post;
    UpdTrProjects.Commit;
    UniMainModule.Result_dbAction := UProjects.FieldByName('Id').AsInteger;
    Result := True;
  Except
    on E: EDataBaseError do
    begin
      UniServerModule.Logger.AddLog(E.ClassName + ': ' + E.Message + ' / Post');
      UpdTrProjects.Rollback;
      UniMainModule.show_error('Fout bij het opslaan van de gegevens!');
      Result := False;
    end;
  end;
end;

procedure TProjectsAddFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

procedure TProjectsAddFrm.UniFormReady(Sender: TObject);
begin
  EditRemarks.JSInterface.JSCode(#1'.inputEl.dom.addEventListener("keydown", function(e){e.stopPropagation()});');
end;

end.
