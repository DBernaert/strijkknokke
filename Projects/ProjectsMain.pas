unit ProjectsMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIFrame, Data.DB, MemDS, DBAccess, IBC, uniImage, uniLabel, uniGUIBaseClasses, uniPanel,
  uniBasicGrid, uniDBGrid, uniButton, UniThemeButton, uniEdit, uniMultiItem, uniComboBox, Vcl.Imaging.pngimage,
  uniDBNavigator;

type
  TProjectsMainFrm = class(TUniFrame)
    Projects: TIBCQuery;
    DsProjects: TDataSource;
    ContainerHeader: TUniContainerPanel;
    ContainerTitle: TUniContainerPanel;
    LblTitle: TUniLabel;
    ImageOffers: TUniImage;
    ContainerFilter: TUniContainerPanel;
    ContainerSearch: TUniContainerPanel;
    EditSearch: TUniEdit;
    ContainerButtonsFiles: TUniContainerPanel;
    BtnProjectDetails: TUniThemeButton;
    GridProjects: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ComboStatus: TUniComboBox;
    BtnSearch: TUniThemeButton;
    UniDBNavigator1: TUniDBNavigator;
    procedure BtnDeleteClick(Sender: TObject);
    procedure EditSearchChange(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure BtnSearchClick(Sender: TObject);
    procedure BtnProjectDetailsClick(Sender: TObject);
    procedure GridProjectsFieldImage(const Column: TUniDBGridColumn; const AField: TField; var OutImage: TGraphic;
      var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
  private
    { Private declarations }
    procedure Open_projects;
    procedure CallBackInsertUpdateProject(Sender: TComponent; AResult:Integer);
  public
    { Public declarations }
    procedure Start_module;
  end;

implementation

{$R *.dfm}

uses MainModule, Main, UniFSConfirm, ProjectsAdd, ProjectDetails;



{ TProjectsMainFrm }

procedure TProjectsMainFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  With ProjectsAddFrm do begin
    Initialize_insert;
    ShowModal(CallBackInsertUpdateProject);
  end;
end;

procedure TProjectsMainFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not Projects.Active) or (Projects.fieldbyname('Id').isNull)
  then exit;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen project?','fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes
      then begin
             Try
               UniMainModule.UProjects.Close;
               UniMainModule.UProjects.SQL.Clear;
               UniMainModule.UProjects.SQL.Add('Select * from projects where id=' +
                                                QuotedStr(Projects.FieldByName('Id').asString));
               UniMainModule.UProjects.Open;
               UniMainModule.UProjects.Edit;
               UniMainModule.UProjects.FieldByName('Removed').AsString := '1';
               UniMainModule.UProjects.Post;
               UniMainModule.UpdTrProjects.Commit;
               UniMainModule.UProjects.Close;
             Except on E: EDataBaseError
             do begin
                  UniMainModule.UpdTrProjects.Rollback;
                  UniMainModule.UProjects.Close;
                  UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
                end;
             end;
             Projects.Refresh;
           end;
    end);
end;

procedure TProjectsMainFrm.BtnModifyClick(Sender: TObject);
begin
  if (Projects.Active) and (not Projects.fieldbyname('Id').isNull) then begin
    UniMainModule.Result_dbAction := 0;
    With ProjectsAddFrm do begin
        Initialize_edit(Projects.FieldByName('Id').AsInteger);
        ShowModal(CallBackInsertUpdateProject);
    end;
  end;
end;

procedure TProjectsMainFrm.BtnProjectDetailsClick(Sender: TObject);
begin
  if (not Projects.Active) or (Projects.FieldByName('Id').IsNull)
  then exit;

  with ProjectDetailsFrm do begin
    Start_module(Projects.FieldByName('Id').AsInteger);
    ShowModal();
  end;
end;

procedure TProjectsMainFrm.BtnSearchClick(Sender: TObject);
begin
  Open_projects;
end;

procedure TProjectsMainFrm.CallBackInsertUpdateProject(Sender: TComponent; AResult: Integer);
begin
  if UniMainModule.Result_dbAction <> 0 then begin
    if Projects.Active then begin
      Projects.Refresh;
      Projects.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TProjectsMainFrm.EditSearchChange(Sender: TObject);
var Filter: string;
begin
  Filter := ' PROJECTNAME LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' EMAIL LIKE ''%' + EditSearch.Text + '%''' + ' OR ' +
            ' MOBILE LIKE ''%' + EditSearch.Text + '%''';
  Projects.Filter := Filter;
end;

procedure TProjectsMainFrm.GridProjectsFieldImage(const Column: TUniDBGridColumn; const AField: TField;
  var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(UpperCase(AField.FieldName), 'STATUS') then
  begin DoNotDispose := True; // we provide an static image so do not free it.
    Case AField.AsInteger of
     0: OutImage := MainForm.ImageGreen.Picture.Graphic;
     1: OutImage := MainForm.ImageRed.Picture.Graphic;
    End;
  end;
end;

procedure TProjectsMainFrm.Open_projects;
 var SQL: string;
begin
  Projects.Close;
  Projects.SQL.Clear;
  SQL := 'SELECT PROJECTS.ID, PROJECTS.STATUS, PROJECTS.NAME AS PROJECTNAME, ' +
         'RELATIONS.MOBILE, RELATIONS.EMAIL ' +
         'FROM PROJECTS ' +
         'LEFT OUTER JOIN RELATIONS ON (PROJECTS.RELATION = RELATIONS.ID)';

  SQL := SQL + ' WHERE PROJECTS.COMPANYID=' + IntToStr(UniMainModule.Company_id) +
               ' AND PROJECTS.REMOVED=''0''';

  Case ComboStatus.Itemindex of
  0: SQL := SQL + ' AND PROJECTS.STATUS=0';
  1: SQL := SQL + ' AND PROJECTS.STATUS=1';
  end;

  SQL := SQL + ' ORDER BY PROJECTNAME';
  Projects.SQL.Add(SQL);
  Projects.Open;
end;

procedure TProjectsMainFrm.Start_module;
begin
  Open_projects;
  EditSearch.SetFocus;
end;

end.
