unit ProjectDetails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, uniGUITypes, uniGUIAbstractClasses,
  uniGUIClasses, uniGUIForm, uniButton, UniThemeButton, uniPanel, uniGUIBaseClasses, uniBasicGrid, uniDBGrid, Vcl.Menus,
  uniMainMenu, Data.DB, MemDS, DBAccess, IBC, Vcl.Imaging.pngimage, uniImage;

type
  TProjectDetailsFrm = class(TUniForm)
    ProjectPurchases: TIBCQuery;
    ProjectPurchasesID: TLargeintField;
    ProjectPurchasesPROJECT: TLargeintField;
    ProjectPurchasesDATEPURCHASE: TDateField;
    ProjectPurchasesDESCRIPTION: TWideStringField;
    ProjectPurchasesQUANTITY: TIntegerField;
    ProjectPurchasesPRICE: TFloatField;
    ProjectPurchasesTOTALPRICE: TFloatField;
    ProjectPurchasesINVOICED: TIntegerField;
    ProjectPurchasesSALESPRICE: TFloatField;
    ProjectPurchasesTOTALSALESPRICE: TFloatField;
    DsProjectPurchases: TDataSource;
    PopupMenuProjectPurchases: TUniPopupMenu;
    BtnDeletePurchase: TUniMenuItem;
    GridProjectPurchases: TUniDBGrid;
    ContainerFooter: TUniContainerPanel;
    ContainerFooterLeft: TUniContainerPanel;
    BtnAdd: TUniThemeButton;
    BtnModify: TUniThemeButton;
    BtnDelete: TUniThemeButton;
    ContainerFooterRight: TUniContainerPanel;
    BtnClose: TUniThemeButton;
    procedure BtnCloseClick(Sender: TObject);
    procedure BtnDeleteClick(Sender: TObject);
    procedure UniFormCreate(Sender: TObject);
    procedure BtnAddClick(Sender: TObject);
    procedure BtnModifyClick(Sender: TObject);
    procedure GridProjectPurchasesFieldImage(const Column: TUniDBGridColumn; const AField: TField;
      var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
  private
    { Private declarations }
    Id_project: longint;
    procedure CallBackInsertUpdateProjectPurchase(Sender: TComponent; AResult:Integer);
  public
    { Public declarations }
    procedure Start_module(ProjectId: longint);
  end;

function ProjectDetailsFrm: TProjectDetailsFrm;

implementation

{$R *.dfm}

uses
  MainModule, uniGUIApplication, Main, UniFSConfirm, ProductPurchasesAdd;

function ProjectDetailsFrm: TProjectDetailsFrm;
begin
  Result := TProjectDetailsFrm(UniMainModule.GetFormInstance(TProjectDetailsFrm));
end;

{ TProjectDetailsFrm }

procedure TProjectDetailsFrm.BtnAddClick(Sender: TObject);
begin
  UniMainModule.Result_dbAction := 0;
  With ProductPurchasesAddFrm do begin
    Init_productpurchase_creation(Id_project);
    ShowModal(CallBackInsertUpdateProjectPurchase);
  end;
end;

procedure TProjectDetailsFrm.BtnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TProjectDetailsFrm.BtnDeleteClick(Sender: TObject);
begin
  if (not ProjectPurchases.Active) or (ProjectPurchases.fieldbyname('Id').isNull)
  then exit;

  if ProjectPurchases.FieldByName('Invoiced').AsInteger = 1
  then begin
         UniMainModule.show_warning('Aankoop kan niet verwijderd worden, is reeds gefactureerd.');
         exit;
       end;

  MainForm.Confirm.Question(UniMainModule.Company_name, 'Verwijderen aankoop?','fa fa-question-circle',
    procedure(Button: TConfirmButton)
    begin
      if Button = Yes
      then begin
             Try
               UniMainModule.UProjectPurchases.Close;
               UniMainModule.UProjectPurchases.SQL.Clear;
               UniMainModule.UProjectPurchases.SQL.Add('Select * from projectpurchases where id=' +
                                                        QuotedStr(ProjectPurchases.FieldByName('Id').asString));
               UniMainModule.UProjectPurchases.Open;
               UniMainModule.UProjectPurchases.Delete;
               UniMainModule.UpdTrProjectPurchases.Commit;
               UniMainModule.UProjectPurchases.Close;
             Except on E: EDataBaseError
             do begin
                  UniMainModule.UpdTrProjectPurchases.Rollback;
                  UniMainModule.UProjectPurchases.Close;
                  UniMainModule.show_error('Fout bij het verwijderen: ' + E.Message);
                end;
             end;
             ProjectPurchases.Refresh;
           end;
    end);
end;

procedure TProjectDetailsFrm.BtnModifyClick(Sender: TObject);
begin
  if (ProjectPurchases.Active) and (not ProjectPurchases.FieldByName('Id').IsNull) then begin
    UniMainModule.Result_dbAction := 0;
    With ProductPurchasesAddFrm do begin
      Init_productpurchase_modification(ProjectPurchases.FieldByName('Id').AsInteger);
      ShowModal(CallBackInsertUpdateProjectPurchase);
    end;
  end;
end;

procedure TProjectDetailsFrm.CallBackInsertUpdateProjectPurchase(Sender: TComponent; AResult: Integer);
begin
   if UniMainModule.Result_dbAction <> 0 then begin
    if ProjectPurchases.Active then begin
      ProjectPurchases.Refresh;
      ProjectPurchases.Locate('Id', UniMainModule.Result_dbAction, []);
    end;
  end;
end;

procedure TProjectDetailsFrm.GridProjectPurchasesFieldImage(const Column: TUniDBGridColumn; const AField: TField;
  var OutImage: TGraphic; var DoNotDispose: Boolean; var ATransparent: TUniTransparentOption);
begin
  if SameText(UpperCase(AField.FieldName), 'INVOICED') then
  begin
    DoNotDispose := True;
    Case AField.AsInteger of
     0: OutImage := MainForm.ImageRed.Picture.Graphic;
     1: OutImage := MainForm.ImageGreen.Picture.Graphic;
    End;
  end;
end;

procedure TProjectDetailsFrm.Start_module(ProjectId: longint);
begin
  Caption := 'Details project';
  Id_project       := ProjectId;

  ProjectPurchases.Close;
  ProjectPurchases.SQL.Clear;
  ProjectPurchases.SQL.Add('Select * from projectpurchases where project=' + IntToStr(ProjectId) +
                           ' order by description');
  ProjectPurchases.Open;
end;

procedure TProjectDetailsFrm.UniFormCreate(Sender: TObject);
begin
  with Self do
    if BorderStyle = bsDialog
    then UniSession.AddJS(WebForm.JSName + '.setIconCls("");');
end;

end.
